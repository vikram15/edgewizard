(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('HomeController', ['$scope','DashboardService','AuthService', '$state', '$rootScope','$stateParams','$location','USERSMIMG','$interval','ReaderServices','$cookies','UserService','AssetService','_','$timeout','$localStorage','$uibModal','PosService',
	                               function($scope, DashboardService, AuthService, $state, $rootScope, $stateParams, $location ,USERSMIMG, $interval, ReaderServices , $cookies, UserService, AssetService,_, $timeout, $localStorage, $uibModal, PosService) {
		var golObj = $cookies.get("globals");
		//For Android
		$scope.isReqFrom = $cookies.get("isReqFrom");
		if($scope.isReqFrom === 'AND'){
			$scope.hidingObjStyle = {
					'padding-top': '0px'
			};
		}
		
		
		var promise;
		if(golObj){
			$scope.globalCompId = AuthService.getCompanyId();
			$scope.globalUserName = AuthService.getUserName();
			$scope.DefaultDocDoor = JSON.parse($cookies.get("globals")).dockDoorName;
			$scope.dockDoorId = JSON.parse($cookies.get("globals")).dockDoorId;
			$scope.userID = AuthService.getUserId();
		}
						
		$scope.autoScroll = true;
		$scope.readerDlg = true;
		$rootScope.pageTitle = "Home Dashboard | EdgeWizard";
		
		$scope.userDetails = function(){
			$scope.requestParams = {
					"userId" :$scope.userID,
					"companyId":$scope.globalCompId
			}
			UserService.getUserDetails($scope.requestParams, $scope.getUserDetSuccess, $scope.getUserDetError);
		};
						
		$scope.getUserDetSuccess = function(response){
			if(response.data.resData !== null){
				if(response.data.resData[0].userPhoto){
					/*console.log("1");*/
					$scope.requestParams = {
							"companyId":response.data.resData[0].companyId,
							"downloadFrom":USERSMIMG,
							"imageId":response.data.resData[0].userId,
							"imageName":response.data.resData[0].userPhoto
					};
					AuthService.downloadLogo($scope.requestParams, $scope.onGetLogoSuccess, $scope.onGetLogoError);
				}else{
					/*	console.log("2");*/
				}
			}
		};
						
		$scope.getUserDetError = function(){
							
		};
						
		$scope.onGetLogoSuccess = function(response) {
			$scope.userLogo = response.data.resData.image;
		};
						
		$scope.onGetLogoError = function(){
			
		};
						
		if($scope.userID > 0){
			$scope.userImg = true;
			$scope.userDetails();
		}else{
			$scope.userImg = false;
			/*console.log("2");*/
		}
						
		/*Display current time */
		function digclock()
		{
			var d = new Date()
			var t = d.toLocaleString()
			var ss = moment(d).format('DD-MM-YYYY : HH:mm:ss');
			$scope.currentTime = ss;
		}
		digclock();
		$interval(function(){digclock()},1000)
		
		$scope.readerDetails = function(){
			$scope.requestParams = {
					"companyId": $scope.globalCompId
			};
			if($scope.dockDoorId !== 0 && $scope.dockDoorId !== undefined && $scope.dockDoorId !== null){
				ReaderServices.getAllreadersList($scope.requestParams, $scope.getAllReaderSuccess, $scope.getAllReadersError);
			}
		};
						
		$scope.getAllReaderSuccess = function(response){
			$scope.allReaders_details = response.data.resData;
			var count = 0;
			var readerCount = 0;
			angular.forEach($scope.allReaders_details,function(value){
				if(value.status === "0"){
					count++;
				}
				if(value.status){
					readerCount++;
				}
			});
			$scope.totOffReaders = count;
			$scope.totReaders = readerCount;
		}; 
		
		$scope.getAllReadersError = function(){
			
		};
						
		$scope.lastLoginDetails = function(){
			$scope.requestParams = { 
					"empId":$scope.userID,
					"userId":$scope.globalUserName,
					"companyId":$scope.globalCompId
			};
			/*console.log($scope.requestParams);*/
			AuthService.getLastLoginDetails($scope.requestParams, $scope.getLastLogSucc, $scope.getLastLogErro)
		};
		
		$scope.getLastLogSucc = function(response){
			if(response.data.resCode === "res0000"){
				$scope.lastLogin = response.data;
			}else{
				//swal("Not deleted!", response.data.resMsg, "error");
			}
		};
						
		$scope.getLastLogErro = function(){
			
		};
						
		/*For a bar chart get asset details by category type*/
		$scope.getCategoryAssetDetails = function(){
			$scope.requestParams = {
					"categoryPrimery":
					{
						"companyId":1
					}
			};
			AssetService.getCategoryAssetDetails($scope.requestParams, $scope.getCatAssSuccess, $scope.getCatAssError)
		};
						
		$scope.getCatAssSuccess = function(response){
			$scope.data = [{
				values : response.data.resData
			}];
		};
						
		$scope.getCatAssError = function(response){
			console.log("get catAss error");
		};
						
		$scope.options = {
				chart: {
					type: 'discreteBarChart',
					height: 90,
					useInteractiveGuideline: false,
					margin : {
						top: 10,
						right: 10,
						bottom: 15,
						left: 10
					},
					x: function(d){
						return d.label;
					},
					y: function(d){
						return d.value;
					},
					showValues: true,
					color:['#801EBC','#FF3D7A','#C64655'],
					valueFormat: function(d){
						return d3.format(',f')(d);
					},
					duration: 500,
					xAxis: {
						axisLabel: 'X Axis',
					},	
					yAxis: {
						axisLabel: 'Y Axis',
						axisLabelDistance: -10,
					}
				}
		};
						 
		/*For a Pie chart total issued and todays issued*/
		/*show the below labels using .tick css class*/
		$scope.getTotalIssue_return = function(){
			AssetService.getTotalIssuesAndReturn($scope.getTtlIssuesReturnSuccess, $scope.getTtlIssuesReturnError);
		};
		
		$scope.getTtlIssuesReturnSuccess = function(response){
			/*console.log(response.data.resData);*/
			if(response.data.resData){
				$scope.irasset = [{
					key: "Total Issued",
					y: response.data.resData.totalIssuedAsset
				},
				{
					key: "Todays Issued",
					y: response.data.resData.todaysIssuedAsset
				},
				{
					key: "Todays Return",
					y: response.data.resData.todaysReceivedAsset
				}];
				$scope.totalIss = response.data.resData.totalIssuedAsset - response.data.resData.todaysReceivedAsset;
			}
		};
						
		$scope.getTtlIssuesReturnError = function(){
							
		};
						
		$scope.iroptions = {
				chart: {
					type: 'pieChart',
					height: 165,
					useInteractiveGuideline: false,
					x: function(d){return d.key;},
					y: function(d){return d.y;},
					showLabels: true,
									
					color:['#FF9933','#5E76EE','#FF99E6'],
					/*showValues: true,*/
					labelType : "value",
					
					legendPosition: 'right',
					/*legend.margin({
										top: 0,
										right:30,
										left:80,
										bottom: 150
									}),*/
					valueFormat: function(d){
						return d3.format(',f')(d);
					},
					duration: 500,
					labelThreshold: 0.00,
					labelSunbeamLayout: false,
						legend: {
							margin: {
								top: 12,
								right: 0,
								bottom: 0,
								left: 0
							}
						}
				},
				/*chart.rotateLabels(-45);	*/
				/*var chart = nv.models.pieChart()
								 chart.rotateLabels(-45);*/
		};	
		
		//WIDGETS 4 (Default DOCK DOOR)
		$scope.dockDoorReaders = function(){
			$scope.requestParams = {  
					"dockDoorId":$scope.dockDoorId,
					"companyId": $scope.globalCompId
			};
			if($scope.dockDoorId !== 0 && $scope.dockDoorId !== undefined && $scope.dockDoorId !== null){
				ReaderServices.getDockDoorReaders($scope.requestParams, $scope.getDockDoorSuccess, $scope.getDockDoorError);
			}
		};
		
		$scope.getDockDoorSuccess = function(response){
			/*console.log(response.data);*/
			if(response.data.resData){
				$scope.dockDoorReaders_details = response.data.resData[0].dockDoorDetailsList;
				if(_.any(response.data.resData[0].dockDoorDetailsList, _.matches({"status":"1"}))){
					$scope.dockDoorBgColor = true;
				}else{
					$scope.dockDoorBgColor = false;
				}
			};
		};
		
		$scope.getDockDoorError = function(response){
			console.log(response.data);
		};
		
		//WIDGETS 1 (SPEEDOMETER)
		$scope.getTtlAssets = function(){
			$scope.requestParams = {
					"companyId": $scope.globalCompId
			};
			DashboardService.getTtlAssetsFrmLicense($scope.requestParams, $scope.getTtlAssetSuccess, $scope.getTtlAssetError);
		};
		$scope.getTtlAssetSuccess = function(response){
			/*console.log("getTtlAssetSuccess.."+ JSON.stringify(response.data));*/
		};
						
		$scope.getTtlAssetError = function(){
			console.log("error");
		};
		
		if($cookies.get("totalAssetPermission")){
			var cookieStoreTtlAssets = JSON.parse($cookies.get("totalAssetPermission"));
			$rootScope.totalAssetFrmLicense = cookieStoreTtlAssets.assetPermissionCount;
			$rootScope.totalRegisteredAssets =[];
			$rootScope.totalRegisterAssets = cookieStoreTtlAssets.totalRegisteredAssetCount;
			$rootScope.totalRegisteredAssets.push($scope.totalRegisterAssets);
		}
						
		var first = $rootScope.totalAssetFrmLicense /5;
		var second = first + first;
		var third = second + first;
		var fourth = third + first;
		var fifth = fourth + first;
		
		$scope.Options = {
				chart: { 
					style: {
						fontFamily: 'monospace',
						color: "#fff"
					},
					type: 'gauge',
					/* plotBackgroundColor: null,
							        plotBackgroundImage: null,
							        	plotBorderWidth: 0,
							        plotShadow: false*/
				},	
							    
				title: {
					text: ''
				},
				credits: {
					enabled: false
				},
						    
				pane: {
					center: ['50%', '100%'],
					size: '210%',
					startAngle: -90,
					endAngle: 90,
					background: [{
						backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
						innerRadius: '50%',
						outerRadius: '50%',
						shape: 'arc'
					}, {
						backgroundColor: '#1eaac2',
						color : 'white',
						borderWidth: 0.5,
						borderColor: 'black',
						outerRadius: '65%',
						innerRadius: '103%'
					}
					],
					style: {
						fontFamily: 'monospace',
						color: "#fff"
					}
				},
				
				// the value axis
				yAxis: {
					min: 0,
					max: $rootScope.totalAssetFrmLicense,
					minorTickInterval: 'auto',
					minorTickWidth: 1,
					minorTickLength: 7,
					minorTickPosition: 'inside',
					minorTickColor: '#666',
					
					tickPixelInterval: 30,
						tickWidth: 2,
						tickPosition: 'inside',
						tickLength: 10,
							tickColor: '#666',
							labels: {
								style: {
									color: '#fff',
									font: '11px Trebuchet MS, Verdana, sans-serif'
								},	
								step: 2,
								rotation: 'auto'
							},
							/*title: {
							            text: 'km/h'
							        },*/
							plotBands: [{
								from: 0,
								to: first,
								color: '#86e87f' // green
							}, {
								from: first,
								to: second,
								color: '#86e87f' // yellow
							}, {
								from: second,
								to: third,
								color: '#86e87f' // red
							}, {
								from: third,
								to: fourth,
								color: '#f7b679' // red
							}, {
								from: fourth,
								to: fifth,
								color: '#f97572' // red
							}]        
				},
				
				series: [{
					name: 'Used Assets',
					data: $rootScope.totalRegisteredAssets,
									/*
									 * tooltip: { valueSuffix: ' km/h' }
									 */
				}]
		};
		
		promise = $interval(function (){
			$scope.readerDetails();
			$scope.dockDoorReaders();
			/*$scope.getTotalIssue_return();*/
			/*$scope.getCategoryAssetDetails();*/
		}, 1000);
						
		function calcTime(city, offset) {
			// create Date object for current location
			var d = new Date();
			// convert to msec
			// add local time zone offset 
			// get UTC time in msec
			var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
			// create new Date object for different city
			// using supplied offset
			var nd = new Date(utc + (3600000*offset));
			// return time as a string
			return "The local time in " + city + " is " + nd.toLocaleString();
		};
		/*console.log(calcTime('New York', '+5.50'));*/
		$scope.newYorkTime = (calcTime('New York', '-5'))
		/*console.log("server time..." + $rootScope.serverClock.time);*/
						
		$scope.stop = function(){
			$interval.cancel(promise)
		};
						
		$scope.$on('$destroy', function() {
			$scope.stop();
		});	
						
		/*
		 * var promise = $interval(function(){
		 * //vm.issueUserReadTag();
		 * $scope.getTtlAssets(); },1000);
		 */	
		
		$scope.openDockdoor = function(){
			var modalInstance = $uibModal.open({
				backdrop : 'static',
				animation : $scope.animationsEnabled,
				templateUrl : 'resources/views/asset/dockDoorPop_up.html',
				controller : 'DockDoorPopUpController',
				controllerAs : 'dockdoorpopupController',
			});
			
			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				/*$log.info('Modal dismissed at: '+ new Date());*/
			});
		};
		
		$scope.getPosSettingDetails = function(){
			$scope.requestParams = {
					"companyId" : $scope.globalCompId
			};	
			PosService.getposSettingDetails($scope.requestParams, $scope.onSuccessSettingDetails, $scope.onErrorSettingDetails);
		};
			
		$scope.onSuccessSettingDetails = function(response){
			$scope.possetData = response.data.resData;
		};
			
		$scope.onErrorSettingDetails = function(response){
			console.log(response.data.resMsg);
		};
						
		$scope.getTtlAssets();
		$scope.readerDetails(); 
		$scope.dockDoorReaders();
		$scope.getTotalIssue_return();
		$scope.lastLoginDetails();
		$scope.getCategoryAssetDetails();
		$scope.getPosSettingDetails();
	}]);
})();
(function() {
    'use strict';
    angular.module('rfidDemoApp.controller')
        .controller('TaxController', ['MasterService', function(MasterService) {
            console.log("tax controller");
            var vm = this;
            vm.tableHeader = "Tax Details";

            vm.getAllTaxMasters = function() {
                MasterService.taxMastersDetails(
                    vm.taxmasterSuccess,
                    vm.taxmasterError);
            };

            vm.taxmasterSuccess = function(response) {
                console.log("getAllTaxMasters");
                vm.taxDetails = response.data;
            };

            vm.taxmasterError = function(response) {
                console.log("error");
            };

            vm.getAllTaxMasters();


        }]);
})();

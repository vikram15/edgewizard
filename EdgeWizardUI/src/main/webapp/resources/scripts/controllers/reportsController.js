(function(){
	'use strict';
	 angular.module('rfidDemoApp.controller')
	 .controller('ReportsController', ['AuthService','ReportsService','$scope','$timeout','$state','$localStorage', 
	                                   function(AuthService, ReportsService, $scope, $timeout, $state, $localStorage){
		 var vm = this; 
		 var GAllIssReList;
		 
		 vm.ascordesc = "asc";
		 vm.comp_Id = AuthService.getCompanyId();
		 vm.userName = AuthService.getUserName();
		 vm.comp_name = AuthService.getCompanyName();
		 
		 
		 vm.hours = ['00', '01', '02','03', '04', '05','06', '07', '08','09', '10', '11','12', '13', '14','15', '16', '17','18', '19', '20','21', '22', '23']; 
		 vm.minutes = ['00', '01', '02','03', '04', '05','06', '07', '08','09', '10', '11','12', '13', '14','15', '16', '17', '18', '19', '20','21', '22', '23','24', '25', '26','27', '28', '29','30', '31', '32', '33', '34', '35','36', '37', '38','39', '40', '41', '42', '43', '44','45', '46', '47','48', '49', '50','51', '52', '53','54', '55', '56','57', '58', '59']; 
		 vm.seconds = ['00', '01', '02','03', '04', '05','06', '07', '08','09', '10', '11','12', '13', '14','15', '16', '17', '18', '19', '20','21', '22', '23','24', '25', '26','27', '28', '29','30', '31', '32', '33', '34', '35','36', '37', '38','39', '40', '41', '42', '43', '44','45', '46', '47','48', '49', '50','51', '52', '53','54', '55', '56','57', '58', '59']; 
		 vm.shh =  vm.hours[0];
		 vm.smm =  vm.hours[0];
		 vm.sss =  vm.hours[0];
	
		 vm.ehh =  vm.hours[0];
		 vm.emm =  vm.hours[0];
		 vm.ess =  vm.hours[0];
	
		 vm.generateFun = function($filter){
			 vm.a = moment(vm.fromDate).format('DD-MMM-YYYY');
			 vm.b = moment(vm.toDate).format('DD-MMM-YYYY'); 
			 
			 vm.startDate = moment(vm.fromDate).valueOf();
			 vm.endDate = moment(vm.toDate).valueOf();
			 
			 
			/* vm.startDate = vm.a +" "+ vm.shh+":"+vm.smm+":"+vm.sss;
			 vm.endDate = vm.b +" "+ vm.ehh+":"+vm.emm+":"+vm.ess;*/
			
			 vm.requestParams = {
					 "companyId":vm.comp_Id,
					 "companyName":vm.comp_name,
					 "generatedBy":vm.userName,
					 "startDate":vm.startDate,
					 "endDate":vm.endDate,
					 "ascOrDesc": vm.ascordesc
			 };
			
			 switch(vm.reportType) {
			    case "1":// all issued asset report
		        	$localStorage.GAllIssReList = vm.requestParams;
		        	
			    	$state.go('homeDashboard.assetIssueRecieve.reports.issuedAssetRpt',
			    		{myParam: "allIssuedAssetReport"}
			    	);
			    	
			    	/*ReportsService.getIssuedAssetsReport(vm.requestParams, vm.getIssueAssetDeSuccess, vm.getIssueAssetDeError);*/
			        break;
			    case "2":// all received asset report
			    	$localStorage.GAllIssReList = vm.requestParams;
			    	$state.go('homeDashboard.assetIssueRecieve.reports.issuedAssetRpt',
				    		{myParam: "allReceivedAssetReport"}
			    	);
			    	
			    	/*ReportsService.getReceivedAssetsReport(vm.requestParams, vm.getIssueAssetDeSuccess, vm.getIssueAssetDeError);*/
			        break;
			    case "3":// all received exception report
			    	$localStorage.GAllIssReList = vm.requestParams;
			    	$state.go('homeDashboard.assetIssueRecieve.reports.issuedAssetRpt',
				    		{myParam: "allReceivedExceReport"}
			    	);
			    	/*ReportsService.getAllReceivedExceptionReport(vm.requestParams, vm.getIssueAssetDeSuccess, vm.getIssueAssetDeError);*/
				    break;
			    case "4"://asset details report
			    	$localStorage.GAllIssReList = vm.requestParams;
			    	$state.go('homeDashboard.assetIssueRecieve.reports.issuedAssetRpt',
				    		{myParam: "assetDetailsReport"}
			    	);
			    	/*ReportsService.getAssetDetailsReport(vm.requestParams, vm.getIssueAssetDeSuccess, vm.getIssueAssetDeError);*/
				    break;
			    case "5"://All Issued/Received Asset List report
			    	$localStorage.GAllIssReList = vm.requestParams;
			    	$state.go('homeDashboard.assetIssueRecieve.reports.issuedAssetRpt',
			    			{myParam: "allAssetsIss_ReceiveReport"}
			    	);
			    	break;
			}
		 };
		 
		 vm.getIssueAssetDeSuccess = function(response){
			console.log(response.data); 
			var file = new Blob([response.data], {type: 'application/pdf'});
			var fileURL = URL.createObjectURL(file);
			window.open(fileURL);
		 };
		 
		 vm.getIssueAssetDeError = function(){
			 
		 };
		 
		 /*========ANGULAR DATE PICKER============*/
			vm.today = function() {
				vm.fromDate = new Date();
				vm.toDate = new Date();
			};
			vm.today();

			vm.clear = function() {
				vm.fromDate = null;
				vm.toDate = null;
			};

			vm.dateOptions = {
					/*dateDisabled: disabled,*/
					formatYear: 'yy',
					maxDate: new Date(2020, 5, 22),
					minDate: new Date(2000, 5, 22),
					startingDay: 1
			};

			// Disable weekend selection
			function disabled(data) {
				var date = data.date,
				mode = data.mode;
				return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			}

			vm.fromOpen = function(){
				vm.from.opened = true;
			};
		  
			vm.toOpen = function(){
				vm.to.opened = true;  
			};
		  
			vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			vm.format = vm.formats[0];
			vm.altInputFormats = ['M!/d!/yyyy'];
			
			vm.from = {
					opened: false
			};
			vm.to = {
					opened: false
			};
			
		  function getDayClass(data) {
			  var date = data.date,
			  mode = data.mode;
			  if (mode === 'day') {
				  var dayToCheck = new Date(date).setHours(0,0,0,0);
				  
				  for (var i = 0; i < $scope.events.length; i++) {
					  var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
			
					  if (dayToCheck === currentDay) {
						  return $scope.events[i].status;
					  }
				  }
			  }
			
			  return '';
		  }
		  
		 /* $scope.exportToExcel=function(tableId){ // ex: '#my-table'
	            var exportHref=Excel.tableToExcel(tableId,'WireWorkbenchDataExport');
	            $timeout(function(){location.href=exportHref;},100); // trigger download
	        }*/

	 }]);
})();
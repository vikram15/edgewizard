
(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('DivisionEditController',['$scope','$log','$uibModalInstance','AuthService','MasterService','jw','$state','$stateParams','CustomService',
	                                      function($scope, $log, $uibModalInstance,AuthService,MasterService, jw, $state,$stateParams, CustomService){
		
		var vm = this;
		vm.tableHeader = "Division Edit";
		$scope.division = jw;
		
		var contNo = CustomService.validationService($scope.division.contactNo);
		var email = CustomService.validationService($scope.division.email);
		
		$scope.division.contactNo = contNo;
		$scope.division.email = email;
		
		$scope.addButton = false;
		$scope.updateButton = true;

		$scope.globalCompId = AuthService.getCompanyId();
		$scope.globalCompName = AuthService.getCompanyName();
		
		$scope.validateEmail=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		
		$scope.divisionUpdate = function (){
			$scope.division = {
			      "divisionId": $scope.division.divisionId,
			      "divisionNm": $scope.division.divisionNm,
			      "contactPer": $scope.division.contactPer,
			      "contactNo": $scope.division.contactNo,
			      "email": $scope.division.email,
			      "address": $scope.division.address,
			      "remark": $scope.division.remark,
			      "companyId": $scope.globalCompId
			    }
			var requestParams  = $scope.division;
			MasterService.DivisionUpdateService(requestParams, $scope.onDivisionInfoSuccess, $scope.onDivisionInfoError);
		}

		$scope.onDivisionInfoSuccess = function(response){
			if(response.data.resCode === "res0000"){
				$scope.division = response.data;
				$uibModalInstance.dismiss('cancel');
			    /*$state.transitionTo($state.current, $stateParams, {
			     reload: true,
			     inherit: false,
			     notify: true
			    });*/
			    swal("Updated", "Division updated successfully!", "success");
			}
			else if(response.data.resCode === "res0100"){
				swal("Not updated!", "Division Name Already Exists.", "error");
			}
		};
		
		$scope.onDivisionInfoError = function(response){
			console.log("error")
		},
		
		$scope.getCompany = function(){
			$scope.companyName = AuthService.getCompanyName();
			$scope.companyId = AuthService.getCompanyId();
			
			$scope.CompanyDetail = {
					"companyName":$scope.companyName,
					"companyId":$scope.companyId
			}
		}
		
		$scope.onCompanySuccess = function(response){
			
			if(response.data.resCode === "res0000"){
				$scope.division.company = response.data;
			}
		}
		
		$scope.onCompanyError = function(response){
			console.log("error")
		},
				
		$scope.cancel = function () {
			$uibModalInstance.dismiss('cancel');
		};
		$scope.getCompany();
		//vm.getDivisionById();
	}]);
})();
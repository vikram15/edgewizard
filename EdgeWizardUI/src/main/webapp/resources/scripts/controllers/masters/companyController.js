(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('CompanyController', ['$scope', 'AuthService','$uibModal', '$log', '$rootScope', function($scope, AuthService, $uibModal, $log, $rootScope) {
		var vm = this;
		$rootScope.pageTitle = "Company | EdgeWizard";
		
		$scope.globalCompId = AuthService.getCompanyId();
		$scope.globalCompName = AuthService.getCompanyName();
		
		vm.getCompanyDetails = function() {
			var requestParams = {
					"companyId" : $scope.globalCompId
			};
			AuthService.CompanyDetailService(requestParams, $scope.onCompanyInfoSuccess, $scope.onCompanyInfoError);
		};
		
		$scope.onCompanyInfoSuccess = function(response) {
			$scope.companyInfo = response.data;
			if (response.data.resCode === "res0000") {
				$scope.company = response.data.resData;
			}
		};
		
		$scope.onCompanyInfoError = function(response) {
			console.log("error")
		};

		$scope.editCompanyFun = function(d) {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : $scope.animationsEnabled,
				templateUrl : 'resources/views/master/EditCompany.html',
				controller : 'CompanyEditController',
				controllerAs : 'companyeditController',
				resolve : {
					jw : function() {
						return angular.copy(d, $scope.data);
					}
				}
			});	

			var requestParams = {
					"companyId" : $scope.globalCompId
			};

			modalInstance.result.then(function(selectedItem) {
				console.log("1111");
			}, function() {
				vm.getCompanyDetails();
			});
		};
		
		vm.getCompanyDetails();
	}]);
})()
(function() {
	'use strict';
	angular
			.module('rfidDemoApp.controller')
			.controller(
					'VendorController',
					[
							'$log',
							'$uibModal',
							'AuthService',
							'MasterService',
							'$timeout',
							'$rootScope',
							function($log, $uibModal, AuthService,
									MasterService, $timeout, $rootScope) {
								var vm = this;
								$rootScope.pageTitle = "Vendor | EdgeWizard";
								vm.comp_Id = AuthService.getCompanyId();
								vm.sortType     = 'name'; // set the default sort type
								vm.sortReverse  = false;  // set the default sort order

								vm.alerts = [];
								vm.closeAlert = function(index) {
									vm.alerts.splice(index, 1);
								};

								vm.getVendorDetails = function() {
									vm.requestParams = {
										"companyId" : vm.comp_Id
									};
									MasterService.getVendorData(
											vm.requestParams,
											vm.onSuccessVendorData,
											vm.onErrorVendorData);
								}

								vm.onSuccessVendorData = function(response) {
									vm.vendordetails = response.data;
								}

								vm.onErrorVendorData = function() {
									console.log("error block");
								}

								vm.addVendorFun = function(d) {
									var modalInstance = $uibModal
											.open({
												backdrop : 'static',
												animation : vm.animationsEnabled,
												templateUrl : 'resources/views/master/vendorAdd.html',
												controller : 'VendorAddController',
												controllerAs : 'vendorAddController',
												resolve : {
													jw : function() {
														return angular.copy(d);
													}
												}
											});

									modalInstance.result.then(function(
											selectedItem) {
										/* $scope.selected = selectedItem; */
									}, function() {
										$log.info('Modal dismissed at: ' + new Date());
										vm.getVendorDetails();
									});
								};

								vm.editVendorFun = function(vendors) {
									var modalInstance = $uibModal
											.open({
												backdrop : 'static',
												animation : vm.animationsEnabled,
												templateUrl : 'resources/views/master/vendorEdit.html',
												controller : 'VendorEditController',
												controllerAs : 'vendorEditController',
												resolve : {
													vendors : function() {
														return angular
																.copy(vendors);
													}
												}
											});

									modalInstance.result.then(function(
											selectedItem) {
										/* $scope.selected = selectedItem; */
									}, function() {
										$log.info('Modal dismissed at: ' + new Date());
										vm.getVendorDetails();
									});
								}

								vm.deleteVendorFun = function(vendors) {

									swal(
											{
												title : "Are you sure?",
												text : "Are you sure want to delete!",
												type : "warning",
												showCancelButton : true,
												confirmButtonColor : "#DD6B55",
												confirmButtonText : "Yes, delete it!",
												cancelButtonText : "No, cancel please!",
												closeOnConfirm : false,
												closeOnCancel : false
											},
											function(isConfirm) {
												if (isConfirm) {
													swal(
															"Deleted!",
															"Deleted Successfully.",
															"success");
													vm.requestParams = {
														"vendorId" : vendors.vendorId,
														"companyId" : vm.comp_Id
													};
													MasterService
															.deleteVendorDetails(
																	vm.requestParams,
																	vm.onSuccessDltVendor,
																	vm.onErrorDltVendor);

												} else {
													swal(
															"Cancelled",
															"Your imaginary file is safe :)",
															"error");
												}
											});

								};

								vm.onSuccessDltVendor = function(response) {
									var str = response.data.resMsg
											.match("Please remove the vendor");
									if (str) {
										swal("Not deleted!",
												"You can not delete vendor.",
												"error");
									}
									vm.getVendorDetails();
								};

								vm.onErrorDltVendor = function() {
									console.log("error block");
								};

								vm.getVendorDetails();
							} ]);
})();

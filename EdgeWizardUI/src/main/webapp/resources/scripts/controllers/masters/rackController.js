(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('RackController', ['$scope','AuthService', 'MasterService', '$uibModal', '$log',
	                               function($scope, AuthService, MasterService, $uibModal, $log) {
		var vm = this;
		vm.tableHeader = "Rack Registration";

		vm.getRackDetails = function() {
			/*
			 * var requestParams = { "companyId":
			 * $scope.globalCompId };
			 */
			MasterService.RackListService(vm.onRackInfoSuccess, vm.onRackInfoError);
		};

		vm.onRackInfoSuccess = function(response) {
			if (response.data.resCode === "res0000") {
				$scope.rack = response.data;
			}
		};
		
		vm.onRackInfoError = function(response) {
			console.log("error")
		}

		vm.addRackFun = function(d) {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : $scope.animationsEnabled,
				templateUrl : 'resources/views/master/AddRack.html',
				controller : 'RackAddController',
				controllerAs : 'rackaddController',
				resolve : {
					jw : function() {
							return angular.copy(d);
					}
				}
			});
			
			modalInstance.result.then(function( selectedItem) {
				console.log("1111");
			}, function() {
				vm.getRackDetails();
			});
		};
		
		vm.editRackFun = function(d) {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : $scope.animationsEnabled,
				templateUrl : 'resources/views/master/EditRack.html',
				controller : 'RackEditController',
				controllerAs : 'rackEditController',
				resolve : {
					jw : function() {
						return angular.copy(d);
					}
				}
			});
			
			modalInstance.result.then(function(selectedItem) {
			}, function() {
				vm.getRackDetails();
			});
		};
		
		vm.deleteRack = function(data) {
			swal({
				title : "Are you sure?",
				text : "Are you sure want to delete!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "Yes, delete it!",
				cancelButtonText : "No, cancel please!",
				closeOnConfirm : false,
				closeOnCancel : false
			},
			function(isConfirm) {
				if (isConfirm) {
					vm.requestParams = data;
					MasterService.RackDeleteService(vm.requestParams, vm.onDeleteSuccess, vm.onDeleteError);
				} else {
					swal("Cancelled", "Your rack record is safe :)", "error");
				}
			});
		};	
			
		vm.onDeleteSuccess = function(response) {
			if (response.data.resCode === "res0000") {
				swal("Deleted!", response.data.resMsg, "success");
			} else if (response.data.resCode === "res0100") {
				swal("Not deleted!", response.data.resMsg, "error");
			}
			vm.getRackDetails();
		};
		
		vm.onDeleteError = function() {
			console.log("error")
		};
		
		vm.getRackDetails();
	}]);
})()	
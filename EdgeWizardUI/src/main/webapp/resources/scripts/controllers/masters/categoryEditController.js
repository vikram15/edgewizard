(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
		.controller('CategoryEditController', ['category', '$uibModalInstance', 'MasterService', '$state', '$stateParams', 'AuthService',
		                                       function(category, $uibModalInstance, MasterService, $state, $stateParams, AuthService) {
			var vm = this;
			
			vm.tableHeader = "Category Edit";
			vm.comp_Id = AuthService.getCompanyId();
			vm.category_list = category;
			vm.categorsys = JSON.stringify(vm.category_list.categoryPrimery.category);
			vm.categoryId = vm.category_list;
								
			vm.getImage = function() {
				if (vm.category_list.categoryPrimery.category == 0) {
					vm.category = "ASSET_CATEGORY";
				} else if (vm.category_list.categoryPrimery.category == 2) {
					vm.category = "USER_CATEGORY";
				} else if (vm.category_list.categoryPrimery.category == 3) {
					vm.category = "NONCUSERS_CATEGORY";
				} else {
					vm.category = "CATEGORY";
				}
				vm.requestParams = {
						"companyId" : vm.comp_Id,
						"downloadFrom" : vm.category,
						"imageId" : vm.category_list.categoryPrimery.categoryId.toString(),
						"imageName" : vm.category_list.catLogo
				};
				AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
			};

			vm.onGetLogoSuccess = function(response) {
				vm.categoryLogo = response.data.resData.image;
			};
			
			vm.onGetLogoError = function(response) {
				console.log("error")
			};
								
			vm.catListBy_Id = function() {
				vm.requestParams = vm.categoryId;
				MasterService.categoryGetDetails(vm.requestParams,	vm.categoryListByIDSucc, vm.categoryListByIDErr);
			};

			vm.categoryListByIDSucc = function(response) {
				vm.cat_listByID = response.data;
				/*
				 * vm.categorsys =
				 * JSON.stringify(vm.cat_listByID.resData.categoryPrimery.category);
				 */
			};
			
			vm.categoryListByIDErr = function(response) {
				console.log("error");
			};
			
			vm.categoryUpdate = function(picFile) {
				vm.requestParams = {
						"categorynm" : vm.category_list.categorynm,
						"categorydesc" : vm.category_list.categorydesc,
						"catLogo" : "catlogo",
						"categoryPrimery" : vm.category_list.categoryPrimery
				};
				MasterService.categoryUpdatedetails(vm.requestParams,vm.catUpdateDetSucc,vm.catUpdateDetErr);
			};	

			vm.catUpdateDetSucc = function(response) {
				if (response.data.resCode === "res0000") {
					swal("Updated", "Category Update Successfully!", "success")
					$uibModalInstance.dismiss('cancel');
					$state.transitionTo($state.current,
							$stateParams, {
						reload : true,
						inherit : false,
						notify : true
					});
				}
			};

								vm.catUpdateDetErr = function(response) {
									console.log("error block");
								};

								vm.cancel = function() {
									$uibModalInstance.dismiss('cancel');
								};

								vm.catListBy_Id();
								vm.getImage();
							} ]);
})();

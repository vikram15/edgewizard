(function() {
	'use strict';
	angular.module('rfidDemoApp.controller').controller(
			'VendorAddController',
			[
					'$log',
					'$uibModalInstance',
					'$scope',
					'$state',
					'$stateParams',
					'AuthService',
					'MasterService',
					function($log, $uibModalInstance, $scope, $state,
							$stateParams, AuthService, MasterService) {
						$log.debug("VendorAddController");
						var vm = this;
						$scope.tableHeader = "Add Vendor";
						$scope.comp_Id = AuthService.getCompanyId();

						$scope.vendorAddFun = function() {
							console.log("function");
							$scope.requestParams = {
								"companyId" : $scope.comp_Id,
								"vendorName" : $scope.vendorName,
								"contPer" : $scope.contactPerson,
								"contactNo" : $scope.contactNumber,
								"email" : $scope.email,
								"remark" : $scope.remark,
								"address" : $scope.address
							}

							MasterService.addVendorData($scope.requestParams,
									$scope.onSuccessAddVendorData,
									$scope.onErrorAddVendorData);
						};

						$scope.onSuccessAddVendorData = function(response) {
							if (response.data.resCode === "res0000") {
								swal("Added", "Vendor Add Successfully!",
										"success")
								$uibModalInstance.dismiss('cancel');
								/*$state.transitionTo($state.current,
										$stateParams, {
											reload : true,
											inherit : false,
											notify : true
										});*/
							} else if (response.data.resCode === "res0100") {
								swal("Not Added!", response.data.resMsg, "error");
							} else {

							}

						};

						$scope.onErrorVendorData = function() {
							console.log("error block");
						};

						$scope.cancel = function() {
							$uibModalInstance.dismiss('cancel');
						};

					} ]);
})();

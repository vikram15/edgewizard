(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('ManufactureEditController',['$scope', '$log', '$uibModalInstance', 'manFactRowData', 'AuthService', 'MasterService', '$state', '$stateParams','CustomService',
	                                         function($scope, $log, $uibModalInstance, manFactRowData, AuthService, MasterService, $state, $stateParams, CustomService) {
		$scope.tableHeader = "Manufacture edit";
		$scope.addButton = false;
		$scope.updateButton = true;

		$scope.manfactuerEditData = manFactRowData;
		$scope.comp_Id = AuthService.getCompanyId();
		
		var contNo = CustomService.validationService($scope.manfactuerEditData.contactNo);
		var email = CustomService.validationService($scope.manfactuerEditData.email);
		
		$scope.manfactuerEditData.contactNo = contNo;
		$scope.manfactuerEditData.email = email;
		
		$scope.manufactEditFun = function() {
		
			$scope.requestParams = {
					"companyId" : $scope.comp_Id,
					"manufactureId" : $scope.manfactuerEditData.manufactureId,
					"manufactureNm" : $scope.manfactuerEditData.manufactureNm,
					"contPer" : $scope.manfactuerEditData.contPer,
					"contactNo" : $scope.manfactuerEditData.contactNo,
					"email" : $scope.manfactuerEditData.email,
					"address" : $scope.manfactuerEditData.address,
					"remark" : $scope.manfactuerEditData.remark,
					"crDate" : null,
					"modDate" : null
			};

			MasterService.manufactureUpdate($scope.requestParams,$scope.onManufactEditSuccess,$scope.onManufactEditError);
		};

		$scope.onManufactEditSuccess = function(response) {
			if (response.data.resCode === "res0000") {
				swal("Updated","Manufacturer Updated Successfully!","success")
				$uibModalInstance.dismiss('cancel');
				/*$state.transitionTo($state.current,
						$stateParams, {
					reload : true,
					inherit : false,
					notify : true
				});*/
			}else{
				swal("Not Updated!", response.data.resMsg, "error");
			}
		}

		$scope.onManufactEditError = function() {
			console.log("error block");
		};

		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
		
	} ]);
})();

(function() {
	'use strict';

	angular
			.module('rfidDemoApp.controller')
			.controller(
					'SectorEditController',
					[
							'$scope',
							'$uibModalInstance',
							'jw',
							'AuthService',
							'MasterService',
							'$state',
							'$stateParams',
							function($scope, $uibModalInstance, jw,
									AuthService, MasterService, $state,
									$stateParams) {
								var vm = this;
								$scope.sector = jw;
								vm.tableHeader = "Sector Edit";
								$scope.globalCompId = AuthService
										.getCompanyId();
								$scope.globalCompName = AuthService
										.getCompanyName();

								$scope.divId = $scope.sector.divisionId;
								$scope.sectorUpdate = function() {

									var requestParams = {
										"sectorId" : $scope.sector.sectorId,
										"sectorNm" : $scope.sector.sectorNm,
										"contactPerson" : $scope.sector.contactPerson,
										"contactNo" : $scope.sector.contactNo,
										"email" : $scope.sector.email,
										"address" : $scope.sector.address,
										"remark" : $scope.sector.remark,
										"companyId" : $scope.globalCompId,
										"divisionId" : $scope.divId
									};
									MasterService.SectorUpdateService(
											requestParams,
											$scope.onSectorSuccess,
											$scope.onSectorError);
								};

								$scope.onSectorSuccess = function(response) {
									if (response.data.resCode === "res0000") {
										$scope.sector = response.data.resData;
										$uibModalInstance.dismiss('cancel');
										$state.transitionTo($state.current,
												$stateParams, {
													reload : true,
													inherit : false,
													notify : true
												});
										// console.log($scope.sector,"555555");
										swal("Updated",
												"Sector updated successfully!",
												"success");
									} else if (response.data.resCode === "res0100") {
										swal("Not updated!",
												"Sector Name Already Exists.",
												"error");
									}
								}

								$scope.onSectorError = function() {
									console.log("error");
								}

								$scope.cancel = function() {
									$uibModalInstance.dismiss('cancel');
								};

								$scope.divisionList = function() {
									$scope.companyId = AuthService
											.getCompanyId();
									var requestParams = {
										"companyId" : $scope.companyId
									}
									MasterService.DivisionDetailService(
											requestParams,
											$scope.onDivisionSuccess,
											$scope.onDivisionError);
								}
								$scope.onDivisionSuccess = function(response) {
									if (response.data.resCode === "res0000") {
										$scope.division = response.data;
									}
								},

								$scope.onDivisionError = function(response) {
									console.log("error")
								},

								$scope.divisionList();
								// $scope.getCompany();

							} ]);
})();
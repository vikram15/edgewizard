(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('RackAddController',['$rootScope', '$state', '$uibModal', '$log', 'MasterService', 'AuthService', '$uibModalInstance', '$stateParams', 'AssetService','$cookies',
	                                 function($rootScope, $state, $uibModal, $log, MasterService, AuthService, $uibModalInstance, $stateParams, AssetService, $cookies) {
		var vm = this;
		var golObj = JSON.parse($cookies.get("globals"));

		vm.tableHeader = "Add Rack";
		vm.rack = {};
		vm.rack.dockdoor = golObj.dockDoorName
		vm.globalCompId = golObj.companyId;
		vm.globalCompName = golObj.CompanyName;
		vm.rack.noofclm = 1;
		vm.rack.noofshlf = 1;
		vm.rack.shlfName = "shelf";
		vm.rack.columnName = "column";
		vm.rackSlave = [];

		vm.getLocation = function() {
			var requestParams = {
					"companyId" : vm.globalCompId
			};
			MasterService.LocationListService( requestParams, vm.onLocInfoSuccess, vm.onLocInfoError);
		};

		vm.onLocInfoSuccess = function(response) {
			vm.location_data = response.data;
			vm.rack.location = response.data.resData[0];
			vm.locChange();
		}
		vm.onLocInfoError = function() {
			console.log("error");
		}

		vm.locChange = function() {
			vm.locationId = {
					"locationId" : vm.rack.location.locationId
			};
			AssetService.getDepDetailsByLocID( vm.locationId, vm.locChangeSuccess, vm.locChangeError);
		};

		vm.locChangeSuccess = function(response) {
			vm.depart_data = response.data;
			if(response.data.resData){
					vm.rack.department = vm.depart_data.resData[0];
			}
		};

		vm.locChangeError = function() {
			console.log("error");
		};

		vm.rackAdd = function() {
			vm.rackMasterParams = {
							"rackName": vm.rack.name,
							"tagId": "",
							"description": vm.rack.description,
							"columnCount": vm.rack.noofclm,
							"shelfCount": vm.rack.noofshlf,
							"columnName": vm.rack.columnName,
							"shelfName": vm.rack.shlfName,
							"locationName": vm.rack.location.locationNm,
							"departmentName": vm.rack.department.deptNm,
						  "rackSlave": vm.rackSlave
						};
			MasterService.RackAddService(vm.rackMasterParams, vm.onRMAddSuccess, vm.onRMAddError);
		};
		vm.totalCell = vm.rack.noofclm * vm.rack.noofshlf;

		vm.onRMAddSuccess = function(response){
			if (response.data.resCode === "res0000") {
				$uibModalInstance.dismiss('cancel');
				swal("Added!", response.data.resMsg, "success");
			} else if (response.data.resCode === "res0100") {
				swal("Not added!", response.data.resMsg, "error");
			}
		};

		vm.onRMAddError = function(response){
			console.log(response);
		};

		vm.createRack = function() {
			vm.showSlave = true;
			vm.total = vm.rack.noofclm * vm.rack.noofshlf;
			vm.masterData = [];
			for (var i = 1; i <= vm.rack.noofclm; i++) {
				for (var j = 1; j <= vm.rack.noofshlf; j++) {
					vm.slaveObj = {
							"columnName" : vm.rack.columnName + "-" + i,
							"shelfName" : vm.rack.shlfName + "-" + j
					}

					vm.rackSlave.push({
						"tagId": "",
						"cellName": vm.slaveObj.columnName + "/" + vm.slaveObj.shelfName,
						"description": ""
					})
					vm.masterData.push(vm.slaveObj);
				}
			}
		};

		vm.getLocation();

		//DockDoor select function
		vm.dockDoorSelect = function(){
			console.log("ddd");
			var modalInstance = $uibModal.open({
				backdrop : 'static',
				animation : $scope.animationsEnabled,
				templateUrl : 'resources/views/asset/dockDoorPop_up.html',
				controller : 'DockDoorPopUpController',
				controllerAs : 'dockdoorpopupController',
				/*resolve : {
											jw : function() {
												return angular.copy(d);
											}
										}*/
			});

			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				/*$log.info('Modal dismissed at: '
										+ new Date());*/
				/*
				 * $scope.jewellrylist =
				 * addJewellryService.query();
				 */
			});
		};

		//Tag select function
		vm.tagSelectFun = function() {
			var modalInstance = $uibModal
					.open({
						backdrop : 'static',
						animation : vm.animationsEnabled,
						templateUrl : 'resources/views/tag/tagSearch.html',
						controller : 'TagSearchController',
						controllerAs : 'tagSearchController',
						/*resolve : {
							jw : function() {
								return angular.copy(d);
							}
						}*/
					});

			modalInstance.result.then(function(selectedItem) {
				/*console.log("ddd" + selectedItem)*/
			}, function(t) {
				/*console.log;*/
			});
		};

		/* tag select value */
		$rootScope.$on("SendUp", function(evt, data) {
			vm.tag = JSON.parse(data);
			//vm.tagId = vm.tag.tagId;
			vm.rack.epcId = vm.tag.tagId;
		});

		vm.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}]);
})();

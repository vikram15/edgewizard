(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('LocationController',
			['$scope','$state', '$uibModal', '$log', 'MasterService', 'AuthService', '$rootScope',
			 function($scope, $state, $uibModal, $log,
					 MasterService, AuthService, $rootScope) {
				var vm = this;
				$rootScope.pageTitle = "Location | EdgeWizard";

				$scope.globalCompId = AuthService.getCompanyId();
				$scope.globalCompName = AuthService.getCompanyName();
				$scope.sortType     = 'name'; // set the default sort type
				$scope.sortReverse  = false;  // set the default sort order
				
				//for pickdrop
				$scope.loginType = AuthService.getSystemName();

				$scope.getAllLocation = function() {
					var requestparams = {
							"companyId" : $scope.globalCompId
					}
					MasterService.LocationListService(requestparams, $scope.onSuccess, $scope.onError);
				}
				$scope.onSuccess = function(response) {
					$scope.location = response.data;
				}
				$scope.onError = function() {
					console.log("error");
				}
				
				$scope.addLocationFun = function(d) {
					var modalInstance = $uibModal
					.open({
						backdrop : 'static',
						animation : $scope.animationsEnabled,
						templateUrl : 'resources/views/master/AddLocation.html',
						controller : 'LocationAddController',
						controllerAs : 'locationaddController',
						resolve : {
							jw : function() {
								return angular.copy(d);
							}
						}
					});
					
					modalInstance.result.then(function(
							selectedItem) {
						/* $scope.selected = selectedItem; */
					}, function() {
						$log.info('Modal dismissed at: '
								+ new Date());
						$scope.getAllLocation();
					});
				};
				
				$scope.editLocationFun = function(d) {
					var modalInstance = $uibModal
					.open({
						backdrop : 'static',
						animation : $scope.animationsEnabled,
						templateUrl : 'resources/views/master/EditLocation.html',
						controller : 'LocationEditController',
						controllerAs : 'locationeditController',
						resolve : {
							jw : function() {
								return angular.copy(d);
							}
						}
					});
					
					modalInstance.result.then(function(
							selectedItem) {
						/* $scope.selected = selectedItem; */
					}, function() {
						$log.info('Modal dismissed at: '
								+ new Date());
						$scope.getAllLocation();
					});
				};
				
				$scope.deleteLocationFun = function(d) {
					swal({	
						title : "Are you sure?",
						text : "Are you sure want to delete!",
						type : "warning",
						showCancelButton : true,
						confirmButtonColor : "#DD6B55",
						confirmButtonText : "Yes, delete it!",
						cancelButtonText : "No, cancel please!",
						closeOnConfirm : false,
						closeOnCancel : false
					},	
					function(isConfirm) {
						if (isConfirm) {
							var requestParams = {
									"locationId" : d.locationId,
									"locationName" : d.locationNm,
									"companyId" : $scope.globalCompId
							}
							MasterService.LocationDeleteService(requestParams, $scope.onDeleteSuccess, $scope.onDeleteError);
						} else {
							swal("Cancelled", "Your location record is safe :)", "error");
						}
					});
				}

				$scope.onDeleteSuccess = function(response) {
					if (response.data.resCode === "res0000") {
						swal("Deleted!", "Deleted Successfully.", "success");
					} else if (response.data.resCode === "res0100") {
						swal("Not deleted!", response.data.resMsg, "error");
					}
					$scope.getAllLocation();
				}
				$scope.onDeleteError = function() {
					console.log("error")
				}
				
				$scope.getAllLocation();
			}]);
})();

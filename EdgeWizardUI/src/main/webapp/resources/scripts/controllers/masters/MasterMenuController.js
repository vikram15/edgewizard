(function() {
	'use strict';
	angular.module('rfidDemoApp.controller').controller('MasterMenuController',
			[ '$scope', '$state', function($scope, $state) {
				var vm = this;
				vm.name = "Master Controller";
				$scope.companyTab = function() {
					alert("Good");
				};
				
				vm.href = function (state){
					console.log($state.$current);
					var url = $state.href('homeDashboard.masterMenu.company', {parameter: "parameter"});
					console.log(url);
					window.open(url);
				};
			} ]);
})();
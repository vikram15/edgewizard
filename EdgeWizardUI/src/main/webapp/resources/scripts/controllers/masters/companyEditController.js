(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
		.controller('CompanyEditController',['$scope', '$log', '$uibModalInstance', 'AuthService', 'jw', '$state', '$stateParams',
		                                     function($scope, $log, $uibModalInstance, AuthService, jw, $state, $stateParams) {

			var vm = this;
			$scope.company = jw;
			//console.log($scope.company, "GGGGG");
			vm.tableHeader = "Company Edit";
			$scope.globalCompId = AuthService.getCompanyId();
			$scope.globalCompName = AuthService.getCompanyName();

			$scope.addButton = false;
			$scope.updateButton = true;
			$scope.regEx = "/^[0-9]{10,10}$/;";

			$scope.getImage = function() {
				var requestParams = {
						"companyId" : $scope.globalCompId,
						"downloadFrom" : "COMPANY",
						"imageId" : $scope.globalCompId.toString(),
						"imageName" : $scope.company.companyLogo
				};
				AuthService.downloadLogo(requestParams, $scope.onGetLogoSuccess, $scope.onGetLogoError);
			};

			$scope.modify = function() {
				$scope.company = {
						"companyId" : $scope.globalCompId,
						"companyName" : $scope.globalCompName,
						"contPer" : $scope.company.contPer,
						"companyLogo" : $scope.company.companyLogo,
						"contNo" : $scope.company.contNo,
						"email" : $scope.company.email,
						"addr" : $scope.company.addr,
						"remark" : $scope.company.remark,
				};
				var requestParams = $scope.company;
				AuthService.CompanyModifyService(requestParams, $scope.onCompanyModifySuccess, $scope.onCompanyModifyError);
			}
			
			// $scope.getImage();
			$scope.onCompanyModifySuccess = function(response) {
				if (response.data.resCode === "res0000") {
					/*
					 * var file = $scope.image1.files[0];
					 */
					
					$scope.company = response.data;
					swal("Updated", "Company updated successfully!", "success");
					$uibModalInstance.dismiss('cancel');
					/*$state.transitionTo($state.current, $stateParams, {
						reload : true,
						inherit : false,
						notify : true
					});*/
					
					
					var requestParams = {
							"companyId" : $scope.globalCompId,
							"uploadTo" : "COMPANY",
							"imageId" : $scope.globalCompId.toString(),
					}
					if ($scope.companyLogo && $scope.companyLogo.name) {
						AuthService.uploadLogo(requestParams, $scope.companyLogo, $scope.onLogoSuccess, $scope.onLogoError)
					}
				}
			};
			$scope.validateEmail=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
			$scope.onLogoSuccess = function(response) {
				if (response.data.resCode === "res0000") {
					// swal("Updated!", "Logo updated
					// successfully.", "success");
				}
			};
			
			$scope.onLogoError = function(response) {
				console.log("error")
			}

			$scope.onGetLogoSuccess = function(response) {
				$scope.companyLogo = response.data.resData.image;
			};
			$scope.onGetLogoError = function(response) {
				console.log("error")
			}
			$scope.onCompanyModifyError = function(response) {
				console.log("error")
			}

			$scope.cancel = function() {
				$uibModalInstance.dismiss('cancel');
			};
			$scope.getImage();
		} ]);
})();
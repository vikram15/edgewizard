(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
			.controller('DivisionController',[ '$scope', '$uibModal', 'AuthService', 'MasterService', '$log', '$rootScope',
			                                   function($scope, $uibModal, AuthService,	MasterService, $log, $rootScope) {
				var vm = this;
								
				$scope.deleteMsg = "";
				$scope.globalCompId = AuthService.getCompanyId();
				$scope.globalCompName = AuthService.getCompanyName();
				$scope.sortType     = 'name'; // set the default sort type
				$scope.sortReverse  = false;  // set the default sort order
				
				//for pickdrop
				$scope.loginType = AuthService.getSystemName();
				
				vm.getDivisionDetails = function() {
					var requestParams = {
							"companyId" : $scope.globalCompId
					};
					MasterService.DivisionDetailService(requestParams, $scope.onDivisionInfoSuccess,$scope.onDivisionInfoError);
				};

				$scope.onDivisionInfoSuccess = function(response) {
					if (response.data.resCode === "res0000") {
						$scope.division = response.data;
					}
				};
				
				$scope.onDivisionInfoError = function(response) {
					console.log("error")
				};

				$scope.deleteDiv = function(d) {
					swal({
						title : "Are you sure?",
						text : "Are you sure want to delete!",
						type : "warning",
						showCancelButton : true,
						confirmButtonColor : "#DD6B55",
						confirmButtonText : "Yes, delete it!",
						cancelButtonText : "No, cancel please!",
						closeOnConfirm : false,
						closeOnCancel : false
					},
					function(isConfirm) {
						if (isConfirm) {
							$scope.division = {
									"divisionId" : d.divisionId,
									"companyId" : $scope.globalCompId
							}
							var requestParams = $scope.division;
							MasterService.DivisionDeleteService(requestParams,$scope.onDeleteSuccess, $scope.onDeleteError);
						} else {
							swal("Cancelled", "Your division record is safe :)","error");
						}
					});
				}
				
				$scope.onDeleteSuccess = function(response) {
					if (response.data.resCode === "res0000") {
						swal("Deleted!", "Deleted Successfully.", "success");
					} else if (response.data.resCode === "res0100") {
						swal("Not deleted!", response.data.resMsg, "error");
					}
					vm.getDivisionDetails();
				};
				
				$scope.onDeleteError = function() {
					console.log("error")
				};
				
				vm.adddivisionFun = function(d) {
					var modalInstance = $uibModal.open({
						backdrop : 'static',
						animation : $scope.animationsEnabled,
						templateUrl : 'resources/views/master/AddDivision.html',
						controller : 'DivisionAddController',
						controllerAs : 'divisionaddController',
						resolve : {
							jw : function() {
								return angular.copy(d);
							}
						}
					});

					modalInstance.result.then(function(
							selectedItem) {
						/* $scope.selected = selectedItem; */
					}, function() {
						vm.getDivisionDetails();
					});
				};

				vm.editDivisionFun = function(d) {
					var modalInstance = $uibModal.open({
							backdrop : 'static',
							animation : $scope.animationsEnabled,
							/*templateUrl : 'resources/views/master/EditDivision.html',
							controller : 'DivisionEditController',
							controllerAs : 'divisioneditController',*/
							templateUrl : 'resources/views/master/AddDivision.html',
							controller : 'DivisionAddController',
							controllerAs : 'divisionaddController',
							resolve : {
								jw : function() {
									return angular.copy(d);
								}
							}
					});

					modalInstance.result.then(function(selectedItem) {
						/* $scope.selected = selectedItem; */
					}, function() {
						vm.getDivisionDetails();
					});
				};

				vm.getDivisionDetails();
			} ]);
})()
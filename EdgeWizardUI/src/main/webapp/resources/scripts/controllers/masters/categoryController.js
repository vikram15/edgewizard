(function(){
  'use strict';
  angular.module('rfidDemoApp.controller')
  .controller('CategoryController',['$scope','$uibModal','AuthService','MasterService','$rootScope', function($scope, $uibModal, AuthService, MasterService, $rootScope){
      console.log("CategoryController");
      var vm = this ;
      vm.tableHeader = "Category Details";
      $rootScope.pageTitle = "Category | EdgeWizard";
      vm.comp_id = AuthService.getCompanyId();

      vm.dataLoaded = false;
      
      vm.getImage = function(){
  		vm.requestParams  = {
  			"companyId": vm.comp_id
  		};
  		AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
  	}
      /*Webcam.attach('#my_camera');

      $scope.take_snapshot = function () {
          Webcam.snap( function(data_uri) {
              document.getElementById('my_result').innerHTML = '<img src="'+data_uri+'"/>';
          } );
      }*/
      vm.onGetLogoSuccess = function(response){
  		vm.categoryLogo = response.data.resData.image;
  	};
  	vm.onGetLogoError = function(response){
  		console.log("error")
  	}
      //Get company asset
      vm.getCompanyAsset = function(){
    	vm.dataLoaded = false;
        vm.requestParams = {
          "categoryPrimery":{
              "category":0,
              "companyId":vm.comp_id
          }
        };
        MasterService.categoryGetDetails(vm.requestParams, vm.categoryGetSucc, vm.categoryGetError);
        MasterService.categoryGetINB(vm.requestParams, vm.catGetINBSuccm, vm.catGetINBErr);
      };

      vm.getCompanyUser = function(){
        vm.dataLoaded = false;
          vm.requestParams = {
            "categoryPrimery":{
                "category":2,
                "companyId":vm.comp_id
            }
          };
          MasterService.categoryGetDetails(vm.requestParams, vm.categoryGetSucc, vm.categoryGetError);
          MasterService.categoryGetINB(vm.requestParams, vm.catGetINBSuccm, vm.catGetINBErr);
          
      };

      vm.getNonCompanyUser = function() {
        vm.dataLoaded = false;
          vm.requestParams = {
            "categoryPrimery":{
                "category":3,
                "companyId":vm.comp_id
            }
          };
          MasterService.categoryGetDetails(vm.requestParams, vm.categoryGetSucc, vm.categoryGetError);
          MasterService.categoryGetINB(vm.requestParams, vm.catGetINBSuccm, vm.catGetINBErr);
          
      };

      vm.categoryGetSucc = function(response){
    	  vm.dataLoaded = true;
    	  vm.categoryList = response.data;
      };

      vm.categoryGetError = function(response){
        console.log("error log");
      };

      vm.catGetINBSuccm = function(response) {
          vm.catINB_list = response.data;
         //console.log(vm.catINB_list);
      };

      vm.categoryGetError = function(response) {
        console.log("error ");
      };


      vm.addCategoryFun = function(d){
        var modalInstance = $uibModal.open({
  				backdrop: 'static',
  				animation: vm.animationsEnabled,
  				templateUrl: 'resources/views/master/categoryAdd.html',
  				controller: 'CategoryAddController',
  				controllerAs : 'categoryAddController',
  				resolve: {
  					jw: function () {
  			    		  return angular.copy(d);
  					}
  				}
  			});

  			modalInstance.result.then(function (selectedItem) {
  				/*$scope.selected = selectedItem;*/
  			}, function () {
  				/*$log.info('Modal dismissed at: ' + new Date());
  			    */  /*		$scope.jewellrylist = addJewellryService.query();*/
  			});
      };

      vm.editCategoryListFun = function(category){
        var modalInstance = $uibModal.open({
  				backdrop: 'static',
  				animation: vm.animationsEnabled,
  				templateUrl: 'resources/views/master/categoryEdit.html',
  				controller: 'CategoryEditController',
  				controllerAs : 'categoryEditController',
  				resolve: {
  					category: function () {
  			    		  return angular.copy(category);
  					}
  				}
  			});

  			modalInstance.result.then(function (selectedItem) {
  				/*$scope.selected = selectedItem;*/
  			}, function () {
  				/*$log.info('Modal dismissed at: ' + new Date());
  			    */  /*		$scope.jewellrylist = addJewellryService.query();*/
  			});
      };
      
      vm.deleteCategory = function(cat){
    	  //console.log(cat);
          swal({
            title: "Are you sure?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false },
            function(isConfirm){
              if (isConfirm) {
                  vm.requestParams = cat;
                MasterService.categoryDeleteDetails(vm.requestParams, vm.catDelSucc, vm.catDelErr);
            }
          else {
        	  swal("Cancelled", "Your imaginary file is safe :)", "error");
           }
          });
      };

      vm.catDelSucc = function(response){
    	  if(response.data.resCode === "res0000"){
				swal("Deleted!", "Deleted Successfully.", "success");
			}
			else if(response.data.resCode === "res0100"){
				swal("Not deleted!", response.data.resMsg, "error");
			}
    	  vm.getCompanyAsset();
      };
      
      vm.catDelErr = function(response){
    	  console.log("error");
      };

      //click on company asset
      vm.compAsset = function(){
        vm.getCompanyAsset();
      }

      vm.compUser = function() {
        vm.getCompanyUser();
      }

      vm.compNonComp = function() {
        vm.getNonCompanyUser();
      }
      //vm.getImage();
      vm.getCompanyAsset();
  }]);
})();

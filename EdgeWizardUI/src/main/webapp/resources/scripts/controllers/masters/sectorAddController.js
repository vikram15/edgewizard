(function() {
	angular.module('rfidDemoApp.controller')
	.controller('SectorAddController',
			['$scope','$uibModalInstance', 'AuthService', 'MasterService', '$state', '$stateParams','CustomService','jw',
			 function($scope, $uibModalInstance, AuthService,
					 MasterService, $state, $stateParams, CustomService, jw) {
				var vm = this;
				vm.tableHeader = "Add Sector";
				$scope.sectorBtnNm = "SAVE";
				$scope.globalCompId = AuthService.getCompanyId();
				$scope.globalCompName = AuthService.getCompanyName();
				$scope.sector = {};
				$scope.division = {};
				
				$scope.getDivision = function() {
					var requestParams = {
							"companyId" : $scope.globalCompId
					};
					MasterService.DivisionDetailService(requestParams, $scope.onDivisionInfoSuccess, $scope.onDivisionInfoError);
				}
				$scope.onDivisionInfoSuccess = function(response) {
					$scope.division = response.data;
					$scope.sector.div = $scope.division.resData[0].divisionId;
					if(jw){
						$scope.sector.div = jw.divisionId;
					}
				}
				$scope.onDivisionInfoError = function() {
					console.log("error");
				}
								
				//Sector Edit functionality
				if(jw){
					vm.tableHeader = "Update Sector";
					$scope.sectorBtnNm = "UPDATE";
					$scope.sector = jw;
					var contNo = CustomService.validationService($scope.sector.contactNo);
					var email = CustomService.validationService($scope.sector.email);
					
					$scope.sector.contactNo = contNo;
					$scope.sector.email = email;
				}else{
					/*$scope.sector.div = $scope.division.resData[0].divisionId;*/
				}
				
				$scope.sectorAdd = function() {
					var requestParams = {
							"sectorNm" : $scope.sector.sectorNm,
							"contactPerson" : $scope.sector.contactPerson,
							"contactNo" : $scope.sector.contactNo,
							"email" : $scope.sector.email,
							"address" : $scope.sector.address,
							"remark" : $scope.sector.remark,
							"companyId" : $scope.globalCompId,
							"divisionId" : $scope.sector.div,
					}
					if($scope.sector.sectorId){
						requestParams.sectorId = $scope.sector.sectorId;
						MasterService.SectorUpdateService( requestParams ,$scope.onSectorUpdateSuccess, $scope.onSectorUpdateError);
					}else{
						MasterService.SectorAddService(requestParams, $scope.onSectorSuccess, $scope.onSectorError);	
					}
				}
				
				$scope.onSectorSuccess = function(response) {
					if (response.data.resCode === "res0000") {
						$scope.sector = response.data.resData;
						$uibModalInstance.dismiss('cancel');
						/*$state.transitionTo($state.current,
												$stateParams, {
													reload : true,
													inherit : false,
													notify : true
												});*/
						swal("Added!", "Sector added successfully.", "success");
					} else if (response.data.resCode === "res0100") {
						swal("Not added!", "Sector Name Already Exists.", "error");
					}
				}
				$scope.onSectorError = function() {
					console.log("error");
				}

				$scope.onSectorUpdateSuccess = function(response) {
					if (response.data.resCode === "res0000") {
						$scope.sector = response.data.resData;
						$uibModalInstance.dismiss('cancel');
						/*$state.transitionTo($state.current,
												$stateParams, {
													reload : true,
													inherit : false,
													notify : true
												});*/
										// console.log($scope.sector,"555555");
						swal("Updated!", "Sector updated successfully.", "success");
					} else if (response.data.resCode === "res0100") {
						swal("Not updated!", "Sector Name Already Exists.", "error");
					}
				}	

				$scope.onSectorUpdateError = function() {
					console.log("error");
				}
								
				$scope.cancel = function() {
					$uibModalInstance.dismiss('cancel');
				};
								
				$scope.getDivision();
			}]);
})();

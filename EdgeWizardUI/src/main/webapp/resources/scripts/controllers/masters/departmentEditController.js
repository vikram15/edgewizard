(function() {

	angular
			.module('rfidDemoApp.controller')
			.controller(
					'DepartmentEditController',
					[
							'$scope',
							'$state',
							'$uibModal',
							'$log',
							'MasterService',
							'AuthService',
							'jw',
							'$uibModalInstance',
							'$stateParams',
							'AssetService','CustomService',
							function($scope, $state, $uibModal, $log,
									MasterService, AuthService, jw,
									$uibModalInstance, $stateParams,
									AssetService,CustomService) {
								var vm = this;

								$scope.department = jw;
								vm.tableHeader = "Department Edit";
								$scope.globalCompId = AuthService.getCompanyId();
								$scope.globalCompName = AuthService.getCompanyName();

								$scope.divId = $scope.department.divisionId;
								$scope.secId = $scope.department.sectorId
								$scope.locId = $scope.department.locationId;

								var contNo = CustomService.validationService($scope.department.contactNumber);
								var email = CustomService.validationService($scope.department.email);

								$scope.department.contactNumber = contNo;
								$scope.department.email = email;

								$scope.departmentUpdate = function() {
									var requestParams = {
										"departmentId" : $scope.department.departmentId,
										"deptNm" : $scope.department.deptNm,
										"sectorId" : $scope.secId,
										"sectorNm" : $scope.department.sectorNm,
										"contactPerson" : $scope.department.contactPerson,
										"contactNumber" : $scope.department.contactNumber,
										"email" : $scope.department.email,
										"addr" : $scope.department.addr,
										"remark" : $scope.department.remark,
										"companyId" : $scope.globalCompId,
										"divisionId" : $scope.divId,
										"sectorId" : $scope.secId,
										"locationId" : $scope.locId
									}
									MasterService.DepartmentUpdateService(
											requestParams,
											$scope.onDepartmentSuccess,
											$scope.ondDepartmentError);
								};

								$scope.onDepartmentSuccess = function(response) {
									if (response.data.resCode === "res0000") {
										$scope.sector = response.data.resData;
										$uibModalInstance.dismiss('cancel');
										/*$state.transitionTo($state.current,
												$stateParams, {
													reload : true,
													inherit : false,
													notify : true
												});*/
										swal("Updated", "Department updated successfully!", "success");
									} else if (response.data.resCode === "res0100") {
										swal("Not updated!", "Department name already exists.", "error");
									}
								}
								$scope.ondDepartmentError = function() {
									console.log("error");
								}

								$scope.cancel = function() {
									$uibModalInstance.dismiss('cancel');
								};

								$scope.divisionList = function() {
									var requestParams = {
											"companyId" : $scope.globalCompId
									}
									MasterService.DivisionDetailService(requestParams, $scope.onDivisionSuccess, $scope.onDivisionError);
								}
								$scope.onDivisionSuccess = function(response) {
									if (response.data.resCode === "res0000") {
										$scope.division = response.data;
										$scope.divChange();
									}
								},

								$scope.onDivisionError = function(response) {
									console.log("error")
								},

								$scope.divChange = function() {
									$scope.divisionId = {
											"divisionId" : $scope.divId
									};
									AssetService.getSectDetailsByDiv($scope.divisionId, $scope.getSectDetBySucc, $scope.getSectDetByErr);
								};

								$scope.getSectDetBySucc = function(response) {
									$scope.sector = response.data;
									$scope.secId = $scope.sector.resData[0].sectorId;
									$scope.secChange();
								};

								$scope.getSectDetByErr = function(response) {
									console.log("error");
								};

								$scope.secChange = function() {
									$scope.sectorId = {
										"sectorId" : $scope.secId
									};
									AssetService.getLocDetailsBySecID($scope.sectorId, $scope.secChangeSuccess, $scope.secChangeError);
								};

								$scope.secChangeSuccess = function(response) {
									$scope.location = response.data;
									$scope.locId = $scope.location.resData[0].locationId;
								};

								$scope.secChangeError = function() {
									console.log("error");
									$scope.location = ""
								};

								$scope.divisionList();
							} ]);
})();

(function() {
	angular.module('rfidDemoApp.controller').controller(
			'LocationAddController',
			[
					'$scope',
					'$uibModalInstance',
					'AuthService',
					'MasterService',
					'$state',
					'$stateParams',
					'AssetService',
					function($scope, $uibModalInstance, AuthService,
							MasterService, $state, $stateParams, AssetService) {
						var vm = this;
						vm.tableHeader = "Add Location";
						$scope.globalCompId = AuthService.getCompanyId();
						$scope.globalCompName = AuthService.getCompanyName();
						$scope.location = {};
						$scope.location = {};

						$scope.getDivision = function() {
							var requestParams = {
								"companyId" : $scope.globalCompId
							};
							MasterService.DivisionDetailService(requestParams, $scope.onDivisionInfoSuccess, $scope.onDivisionInfoError);
						}
						$scope.onDivisionInfoSuccess = function(response) {
							$scope.division = response.data;
							$scope.location.div = $scope.division.resData[0].divisionId
							$scope.divChange();
						}
						$scope.onDivisionInfoError = function() {
							console.log("error");
						}

						$scope.divChange = function() {
							$scope.divisionId = {"divisionId":$scope.location.div}
							AssetService.getSectDetailsByDiv($scope.divisionId, $scope.getSectDetBySucc, $scope.getSectDetByErr);
						};

						$scope.getSectDetBySucc = function(response) {
							$scope.sector = response.data;
							$scope.location.sec = $scope.sector.resData[0].sectorId;
						};

						$scope.getSectDetByErr = function(response) {
							console.log("error");
						};

						$scope.cancel = function() {
							$uibModalInstance.dismiss('cancel');
						};

						$scope.locationAdd = function() {
							var requestParams = {
								"locationNm" : $scope.location.locationNm,
								"contPer" : $scope.location.contactPerson,
								"contactNumber" : $scope.location.contactNo,
								"email" : $scope.location.email,
								"address" : $scope.location.address,
								"remark" : $scope.location.remark,
								"companyId" : $scope.globalCompId,
								"divisionId" : $scope.location.div,
								"sectorId" : $scope.location.sec
							}
							MasterService.LocationAddService(requestParams,
									$scope.onLocationSuccess,
									$scope.onLocationError);
						}
						$scope.onLocationSuccess = function(response) {
							if (response.data.resCode === "res0000") {
								$scope.location = response.data.resData;
								$uibModalInstance.dismiss('cancel');
							/*	$state.transitionTo($state.current,
										$stateParams, {
											reload : true,
											inherit : false,
											notify : true
										});*/
								swal("Added!", "Location added successfully.",
										"success");
							} else if (response.data.resCode === "res0100") {
								swal("Not added!",
										"Location Name Already Exists.",
										"error");
							}
						}
						$scope.onLocationError = function() {
							console.log("error");
						}

						$scope.getDivision();
						// $scope.getSector();
					} ]);
})();

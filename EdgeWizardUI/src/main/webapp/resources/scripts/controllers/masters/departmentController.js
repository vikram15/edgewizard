(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('DepartmentController', ['$scope', '$state','$uibModal', '$log', 'MasterService', 'AuthService', '$rootScope',
	                                     function($scope, $state, $uibModal, $log, MasterService, AuthService, $rootScope) {
		var vm = this;
		$rootScope.pageTitle = "Department | EdgeWizard";
		$scope.globalCompId = AuthService .getCompanyId();
		$scope.globalCompName = AuthService .getCompanyName();
		$scope.deleteMsg = "";
		$scope.sortType     = 'name'; // set the default sort type
		$scope.sortReverse  = false;  // set the default sort order
		
		//for pickdrop
		$scope.loginType = AuthService.getSystemName();
		
		$scope.getAllDepartment = function() {
			var requestparams = {
					"companyId" : $scope.globalCompId
			}
				MasterService.DepartmentListService(requestparams, $scope.onSuccess, $scope.onError);
		}
								
		$scope.onSuccess = function(response) {
			$scope.department = response.data;
		}
		
		$scope.onError = function() {
			console.log("error");
		}

		$scope.addDepartmentFun = function(d) {
			var modalInstance = $uibModal
				.open({
					backdrop : 'static',
						animation : $scope.animationsEnabled,
						templateUrl : 'resources/views/master/AddDepartment.html',
						controller : 'DepartmentAddController',
						controllerAs : 'departmentAddController',
						resolve : {
							jw : function() {
								return angular.copy(d);
							}
						}
				});
			
			modalInstance.result.then(function(
					selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				$log.info('Modal dismissed at: ' + new Date());
				$scope.getAllDepartment();
			});
		};

		$scope.editDepartmentFun = function(d) {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : $scope.animationsEnabled,
				templateUrl : 'resources/views/master/EditDepartment.html',
				controller : 'DepartmentEditController',
				controllerAs : 'departmenteditController',
				resolve : {
					jw : function() {
						return angular.copy(d);
					}
				}
			});
			
			modalInstance.result.then(function(
					selectedItem) {
				console.log("1");
				/* $scope.selected = selectedItem; */
			}, function() {
				console.log("2");
				$log.info('Modal dismissed at: '
						+ new Date());
				$scope.getAllDepartment();
			});
		};
	
		$scope.deleteDepartmentFun = function(d) {
			swal({
				title : "Are you sure?",
				text : "Are you sure want to delete!",
				type : "warning",
				showCancelButton : true,
				confirmButtonColor : "#DD6B55",
				confirmButtonText : "Yes, delete it!",
				cancelButtonText : "No, cancel please!",
				closeOnConfirm : false,
				closeOnCancel : false
			},
			function(isConfirm) {
				if (isConfirm) {
					var requestParams = {
							"departmentId" : d.departmentId,
							"deptNm" : d.deptNm,
							"companyId" : $scope.globalCompId
					};
					MasterService.DepartmentDeleteService(requestParams, $scope.onDeleteSuccess, $scope.onDeleteError);
				} else {
					swal("Cancelled", "Your department	 record is safe :)", "error");
				}
			});
		}

		$scope.onDeleteSuccess = function(response) {
			if (response.data.resCode === "res0000") {
				swal("Deleted!", "Deleted Successfully.", "success");
			} else if (response.data.resCode === "res0100") {
				swal("Not deleted!", response.data.resMsg, "error");
			}
			$scope.getAllDepartment();
		}
		$scope.onDeleteError = function() {
			console.log("error")
		}
		
		$scope.getAllDepartment();
	}]);
})();

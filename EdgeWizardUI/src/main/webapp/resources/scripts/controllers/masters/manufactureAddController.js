(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
		.controller('ManufactureAddController', ['$scope', '$uibModalInstance', 'MasterService', 'AuthService', '$state', '$stateParams',
		                                         function($scope, $uibModalInstance, MasterService, AuthService, $state, $stateParams) {
			var vm = this;
			$scope.manufacture = {};
			$scope.tableHeader = "Add Manufacture";
			$scope.addButton = true;
			$scope.updateButton = false;
			$scope.manufactureData = {};
			
			$scope.manufactAddFun = function() {
				$scope.comp_Id = AuthService.getCompanyId();
				
				var requestParams = {
						"companyId" : $scope.comp_Id,
						"manufactureNm" : $scope.manufactureData.manufactureNm,
						"contPer" : $scope.manufactureData.contPer,
						"contactNo" : $scope.manufactureData.contactNo,
						"email" : $scope.manufactureData.email,
						"address" : $scope.manufactureData.address,
						"remark" : $scope.manufactureData.remark
				};
				MasterService.manufactureAddDetails(requestParams, $scope.onManufactureAddSuccess, $scope.onManufactureAddError);
			};

			$scope.onManufactureAddSuccess = function(response) {
				if (response.data.resCode === "res0000") {
					swal("Added", "Manufacturer Add Successfully!", "success")
					$uibModalInstance.dismiss('cancel');
				/*	$state.transitionTo($state.current,
							$stateParams, {
						reload : true,
						inherit : false,
						notify : true
					});*/
				} else if (response.data.resCode === "res0100") {
					swal("Not deleted!", response.data.resMsg, "error");
				} else {

				}
			};

			$scope.onManufactureAddError = function() {
				console.log("error");
			};
			
			$scope.cancel = function() {
				$uibModalInstance.dismiss('cancel');
			};

		} ]);
})();

(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('BatchLotController',['$scope', '$uibModal','AuthService','MasterService','$timeout','$rootScope',	
	                                  function($scope, $uibModal, AuthService, MasterService, $timeout, $rootScope) {
				var vm = this;
				vm.alerts = [];
				vm.closeAlert = function(index) {
					vm.alerts.splice(index, 1);
				};

				vm.tableHeader = "BatchLot Details";
				$rootScope.pageTitle = "BatchLot | EdgeWizard";
				vm.comp_Id = AuthService.getCompanyId();
				
				vm.sortType  = 'batchName'; // set the default sort type
				vm.sortReverse  = false;  // set the default sort order
				
				vm.getBatchDetails = function() {
					vm.requestParams = {
							"companyId" : vm.comp_Id
					}
					MasterService.getBatchLotData(vm.requestParams, vm.onSuccessBatchLot, vm.onErrorBatchLot);
				};

				vm.onSuccessBatchLot = function(response) {
					vm.batchLotList = response.data;
				};
				
				vm.onErrorBatchLot = function(response) {
					console.log("error block");
				};

				vm.addBatchLotFun = function(batchLots) {
					var modalInstance = $uibModal.open({
						backdrop : 'static',
						animation : vm.animationsEnabled,
						templateUrl : 'resources/views/master/batchLotAdd.html',
						controller : 'BatchLotAddController',
						controllerAs : 'batchLotAddController',
						resolve : {
							batchLots : function() {
								return angular.copy(batchLots);
							}
						}
					});
					
					modalInstance.result.then(function(
							selectedItem) {
						/* $scope.selected = selectedItem; */
					}, function() {
						vm.getBatchDetails();
					});
				};
				
				vm.editBatchLotFun = function(batchLots) {
					console.log(batchLots)
					var modalInstance = $uibModal
					.open({
						backdrop : 'static',
						animation : vm.animationsEnabled,
						templateUrl : 'resources/views/master/batchLotAdd.html',
						controller : 'BatchLotAddController',
						controllerAs : 'batchLotAddController',
						resolve : {
							batchLots : function() {
								return angular.copy(batchLots);
							}
						}
					});
					
					modalInstance.result.then(function(
							selectedItem) {
						/* $scope.selected = selectedItem; */
					}, function() {
						vm.getBatchDetails();
					});
				};

				vm.deleteBatchLotFun = function(batchLots) {
					swal({
						title : "Are you sure?",
						text : "Are you sure want to delete!",
						type : "warning",
						showCancelButton : true,
						confirmButtonColor : "#DD6B55",
						confirmButtonText : "Yes, delete it!",
						cancelButtonText : "No, cancel please!",
						closeOnConfirm : false,
						closeOnCancel : false
					},
					function(isConfirm) {
						if (isConfirm) {
							vm.requestParams = {
									"batchId" : batchLots.batchId,
									"batchName" : batchLots.batchName,
									"remark" : batchLots.remark,
									"companyId" : vm.comp_Id
							};
							MasterService .batchLotsDeleteDetails(vm.requestParams, vm.batchLtDltSucc, vm.batchLtDltError);
						} else {
							swal("Cancelled", "Your imaginary file is safe :)", "error");
							}
					});
				};
				
				vm.batchLtDltSucc = function(response) {
					if (response.data.resCode === "res0000") {
						swal("Deleted!", "Deleted Successfully.", "success");
					} else if (response.data.resCode === "res0100") {
						swal("Not deleted!", response.data.resMsg, "error");
					}
					vm.getBatchDetails();
				};
				
				vm.batchLtDltError = function(response) {
					console.log("error");
				};
		
				vm.getBatchDetails();
			}]);
})();

(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('DepartmentAddController', ['$scope', '$state', '$uibModal', '$log', 'MasterService', 'AuthService', '$uibModalInstance', '$stateParams', 'AssetService',
	                                        function($scope, $state, $uibModal, $log, MasterService, AuthService, $uibModalInstance, $stateParams, AssetService) {
		var vm = this;
		vm.tableHeader = "Add Department";
		$scope.globalCompId = AuthService.getCompanyId();
		$scope.globalCompName = AuthService.getCompanyName();
		$scope.department = {};

		$scope.getDivision = function() {
			var requestParams = {
					"companyId" : $scope.globalCompId
			};
			MasterService.DivisionDetailService(requestParams,$scope.onDivisionInfoSuccess,$scope.onDivisionInfoError);
		};
		
		$scope.onDivisionInfoSuccess = function(response) {
			$scope.division = response.data;
			$scope.department.division = $scope.division.resData[0].divisionId;
			$scope.divChange();
		}
		$scope.onDivisionInfoError = function() {
			console.log("error");
		}
		$scope.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
		
		$scope.divChange = function() {
			$scope.divisionId = {"divisionId" : $scope.department.division};
			AssetService.getSectDetailsByDiv($scope.divisionId,$scope.getSectDetBySucc, $scope.getSectDetByErr);
		};
		
		$scope.getSectDetBySucc = function(response) {
			if(response.data.resData){
				$scope.sector = response.data;	
				$scope.department.sector = $scope.sector.resData[0].sectorId;
				$scope.secChange();
			}else{
				$scope.sector = "";
			}
		};
		
		$scope.getSectDetByErr = function(response) {
			console.log("error");
		};

		$scope.secChange = function() {
			$scope.sectorId = {"sectorId" : $scope.department.sector};
			AssetService.getLocDetailsBySecID($scope.sectorId, $scope.secChangeSuccess,	$scope.secChangeError);
		};

		$scope.secChangeSuccess = function(response) {
			if(response.data.resData){
				$scope.location = response.data;
				$scope.department.location = $scope.location.resData[0].locationId;
			}else{
				$scope.location = ""
			}
		};
		
		$scope.secChangeError = function() {
			console.log("error");
			$scope.location = ""
		};
		
		$scope.departmentAdd = function() {
			
			var requestParams = {
					"deptNm" : $scope.sector.departmentNm,
					"contactPerson" : $scope.department.contactPerson,
					"contactNumber" : $scope.department.contactNumber,
					"email" : $scope.department.email,
					"addr" : $scope.department.address,
					"remark" : $scope.department.remark,
					"companyId" : $scope.globalCompId,
					"divisionId" : $scope.department.division,
					"sectorId" : $scope.department.sector,
					"locationId" : $scope.department.location,
			}
			
			MasterService.DepartmentAddService(requestParams, $scope.onDepartmentSuccess, $scope.onDepartmentError);
		}
		$scope.onDepartmentSuccess = function(response) {
			if (response.data.resCode === "res0000") {
				$scope.department = response.data.resData;
				$uibModalInstance.dismiss('cancel');
				/*$state.transitionTo($state.current,
						$stateParams, {	
					reload : true,
					inherit : false,
					notify : true
				});*/
				swal("Added!","Department added successfully.", "success");
			} else if (response.data.resCode === "res0100") {
				swal("Not added!","Department name already exists.", "error");
			}
		}
		$scope.onDepartmentError = function() {
			console.log("error");
		}

		$scope.getDivision();
		/*
		 * $scope.getSector(); $scope.getLocation();
		 */
	} ]);
})();
/** ********************************************** */

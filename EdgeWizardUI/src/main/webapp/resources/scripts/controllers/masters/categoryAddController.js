(function(){
  'use strict';
  angular.module('rfidDemoApp.controller')
  .controller('CategoryAddController',['$scope', '$uibModalInstance','Upload','AuthService','MasterService','$state','$stateParams','$timeout',
   function($scope, $uibModalInstance, Upload, AuthService, MasterService, $state, $stateParams, $timeout){
	  var vm = this ;
	  vm.tableHeader = "Add Category";
	  vm.comp_Id = AuthService.getCompanyId();
	  vm.alerts = [];
		vm.closeAlert = function(index) {
	         vm.alerts.splice(index, 1);
		};

	  vm.categoryAdd = function(file){
	          vm.requestParams = {
	            "categorynm":vm.categoryType,
	          	"categorydesc":vm.description,
	          	"catLogo":null,
	          	"categoryPrimery":{
	          		"category":vm.masterCategory,
	          		"companyId":vm.comp_Id
	          	}
	          };

	    MasterService.categoryAddDetails(vm.requestParams, vm.categoryAddDetSucc, vm.categoryAddDetErr);
    };

    vm.categoryAddDetSucc = function(response){
    	if(response.data.resCode === "res0000"){
    		//console.log(response.data.resData,"Categopry DATA");
    		if(response.data.resData[0].categoryPrimery.category == 0){
    			vm.category = "ASSET_CATEGORY";
    		}
    		else if(response.data.resData[0].categoryPrimery.category == 2){
    			vm.category = "USER_CATEGORY";
    		}
    		else if(response.data.resData[0].categoryPrimery.category == 3){
    			vm.category = "NONCUSERS_CATEGORY";
    		}
    		else{
    			vm.category = "CATEGORY";
    		}
    		vm.requestParams = {
		    		"companyId":vm.comp_Id,
		    		"uploadTo":vm.category,
		    		"imageId":response.data.resData[0].categoryPrimery.categoryId,
		    }

		    if(vm.categoryLogo && vm.categoryLogo.name){
		    	AuthService.uploadLogo(vm.requestParams,vm.categoryLogo, vm.categoryImgSucc, vm.categoryImgErr )
		    }
			swal("Added","Category Add Successfully!", "success")
				$uibModalInstance.dismiss('cancel');
				$state.transitionTo($state.current, $stateParams, {
					reload: true,
					inherit: false,
					notify: true
				});

		}else if(response.data.resCode === "res0100"){
			swal("Not deleted!", response.data.resMsg, "error");
		}else{

		}
    };

    vm.categoryImgSucc = function(response){

    }
    vm.categoryImgErr = function (response){

    }
	  vm.cancel = function(){
		  $uibModalInstance.dismiss('cancel');
	  };
  }]);
})();

(function() {
	'use strict';
	angular
			.module('rfidDemoApp.controller')
			.controller(
					'ManufactureController',
					[
							'$scope',
							'$uibModal',
							'$log',
							'MasterService',
							'AuthService',
							'$timeout',
							'$rootScope',
							function($scope, $uibModal, $log, MasterService,
									AuthService, $timeout, $rootScope) {
								var vm = this;
								$rootScope.pageTitle = "Manufacturer | EdgeWizard";
								vm.comp_Id = AuthService.getCompanyId();
								vm.alerts = [];
								vm.closeAlert = function(index) {
									vm.alerts.splice(index, 1);
								};
								$scope.sortType     = 'name'; // set the default sort type
								$scope.sortReverse  = false;  // set the default sort order

								vm.manufactureDetails = function() {
									vm.requestParams = {
										"companyId" : vm.comp_Id
									}
									MasterService.manufactureAllDetails(
											vm.requestParams,
											vm.onManufactureSuccess,
											vm.onManufactureError);
								}

								vm.onManufactureSuccess = function(response) {
									$log.debug('success block');
									vm.manufactureDt = response.data;
								};

								vm.onManufactureError = function() {
									$log.debug("error block");
								};

								vm.addManufactureFun = function(d) {
									var modalInstance = $uibModal
											.open({
												backdrop : 'static',
												animation : $scope.animationsEnabled,
												templateUrl : 'resources/views/master/AddManufacture.html',
												controller : 'ManufactureAddController',
												resolve : {
													jw : function() {
														return angular.copy(d);
													}
												}
											});

									modalInstance.result.then(function(
											selectedItem) {
										/* $scope.selected = selectedItem; */
									}, function() {
										$log.info('Modal dismissed at: '
												+ new Date());
										vm.manufactureDetails();
									});
								};

								vm.editManufactureFun = function(manFactRowData) {
									var modalInstance = $uibModal
											.open({
												backdrop : 'static',
												animation : $scope.animationsEnabled,
												templateUrl : 'resources/views/master/manufactureEdit.html',
												controller : 'ManufactureEditController',
												resolve : {
													manFactRowData : function() {
														return angular.copy(manFactRowData);
													}
												}
											});

									modalInstance.result.then(function(
											selectedItem) {

									}, function() {
										$log.info('Modal dismissed at: '
												+ new Date());
										vm.manufactureDetails();
									});
								};

								vm.deleteManufactureFun = function(
										manufactureDat) {

									swal({
												title : "Are you sure?",
												text : "Are you sure want to delete!",
												type : "warning",
												showCancelButton : true,
												confirmButtonColor : "#DD6B55",
												confirmButtonText : "Yes, delete it!",
												cancelButtonText : "No, cancel please!",
												closeOnConfirm : false,
												closeOnCancel : false
											},
											function(isConfirm) {
												if (isConfirm) {

													$scope.requestParams = {
														"companyId" : vm.comp_Id,
														"manufactureId" : manufactureDat.manufactureId
													};
													MasterService.manufactureDelete($scope.requestParams, $scope.onManFactDeleteSuccess, $scope.onManFactDeleteError);
												} else {
													swal("Cancelled", "Your imaginary file is safe :)", "error");
												}
											});
								};

								$scope.onManFactDeleteSuccess = function(response) {
									if (response.data.resCode === "res0000") {
										swal("Deleted!", "Deleted Successfully.", "success");
									} else if (response.data.resCode === "res0100") {
										swal("Not deleted!", response.data.resMsg, "error");
									}
									vm.manufactureDetails();
								}

								vm.manufactureDetails();
							}]);
})();

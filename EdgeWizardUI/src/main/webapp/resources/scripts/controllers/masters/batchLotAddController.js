(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('BatchLotAddController',
			['$scope', '$uibModalInstance', 'AuthService', 'MasterService', '$state',
					'$stateParams', '$timeout', 'batchLots',
					function($scope, $uibModalInstance, AuthService, MasterService, $state, $stateParams, $timeout,batchLots) {
						console.log("BatchLotAddController");
						var vm = this;
						vm.tableHeader = "Add BatchLot";
						vm.comp_Id = AuthService.getCompanyId();
						vm.alerts = [];
						vm.closeAlert = function(index) {
							vm.alerts.splice(index, 1);
						};

						if(batchLots){
							vm.tableHeader = "BatchLot Edit";
							vm.batchId = batchLots.batchId;
							vm.batchName = batchLots.batchName;
							vm.remark = batchLots.remark;
						}

						vm.batchLotAdd = function() {
							vm.requestParams = {
								"batchName" : vm.batchName,
								"remark" : vm.remark,
								"companyId" : vm.comp_Id
							};
							if(vm.batchId){
								vm.requestParams.batchId = vm.batchId;
								MasterService.batchLotUpdateDetails(vm.requestParams, vm.onBatchLtSucc,vm.onBatchLtErr);
							}
							MasterService.batchLostAddDetails(vm.requestParams, vm.onSuccessBathLostAddDetails, vm.onErrorBathLostAddDetails)
						};

						vm.onSuccessBathLostAddDetails = function(response) {
							if (response.data.resCode === "res0000") {
								swal("Added" ,"BatchLot added successfully!", "success")
								$uibModalInstance.dismiss('cancel');
							} else if (response.data.resCode === "res0100") {
								console.log("error res0100");
								swal("Not Added" ,response.data.resMsg, "error")
							} else {

							}
						};

						vm.onErrorBathLostAddDetails = function() {
							console.log("error block");
						};

						vm.onBatchLtSucc = function(response) {
							if (response.data.resCode === "res0000") {
								$uibModalInstance.dismiss('cancel');
								swal("Updated", "Batchlot Updated Successfully!", "success")
							}else{

							}
						};

						vm.onBatchLtErr = function(response) {
							console.log("error block");
						};

						vm.cancel = function() {
							$uibModalInstance.dismiss('cancel');
						};
					} ]);
})();

(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
			.controller('SectorController',
					['$scope','$state', '$uibModal', '$log', 'MasterService', 'AuthService', '$rootScope',
					 function($scope, $state, $uibModal, $log, MasterService, AuthService, $rootScope) {
						var vm = this;
						$scope.labeldata = {};
						$rootScope.pageTitle = "Sector | EdgeWizard";
						$scope.sortType     = 'name'; // set the default sort type
						$scope.sortReverse  = false;  // set the default sort order
						//for pickdrop
						$scope.loginType = AuthService.getSystemName();
						$scope.labeldata = AuthService.getLabelInfo();
						
						$scope.labeldata = JSON.parse($scope.labeldata);
						vm.tableHeader = "Sector Details";
						$scope.globalCompId = AuthService.getCompanyId();
						$scope.globalCompName = AuthService.getCompanyName();
						$scope.deleteMsg = "";
						
						$scope.getAllSector = function() {
							var requestparams = {
									"companyId" : $scope.globalCompId
							}
							MasterService.SectorListService(requestparams, $scope.onSuccess, $scope.onError);
						};
						
						$scope.onSuccess = function(response) {
							$scope.sector = response.data;
						}
						$scope.onError = function() {
							console.log("error");
						}

						vm.addSectorFun = function(d) {
							var modalInstance = $uibModal.open({
									backdrop : 'static',
									animation : $scope.animationsEnabled,
									templateUrl : 'resources/views/master/AddSector.html',
									controller : 'SectorAddController',
									controllerAs : 'sectorAddController',
									resolve : {
										jw : function() {
											return angular.copy(d);
										}
									}
							});
							
							modalInstance.result.then(function(
									selectedItem) {
								/* $scope.selected = selectedItem; */
							}, function() {
								$scope.getAllSector();
							});
						};

						vm.editSectorFun = function(d) {
							var modalInstance = $uibModal.open({
								backdrop : 'static',
								animation : $scope.animationsEnabled,
								templateUrl : 'resources/views/master/AddSector.html',
								controller : 'SectorAddController',
								controllerAs : 'sectorAddController',
								/*templateUrl : 'resources/views/master/sectorEdit.html',
								controller : 'SectorEditController',
								controllerAs : 'sectoreditdController',*/
								resolve : {
									jw : function() {
										return angular.copy(d);
									}
								}
							});

							modalInstance.result.then(function(
									selectedItem) {
								console.log("1");
								/* $scope.selected = selectedItem; */
							}, function() {
								$scope.getAllSector();
							});
						};

						$scope.deleteSector = function(d) {
							swal({
								title : "Are you sure?",
								text : "Are you sure want to delete!",
								type : "warning",
								showCancelButton : true,
								confirmButtonColor : "#DD6B55",
								confirmButtonText : "Yes, delete it!",
								cancelButtonText : "No, cancel please!",
								closeOnConfirm : false,
								closeOnCancel : false
							},
							function(isConfirm) {
								if (isConfirm) {
									$scope.division = {
											"sectorId" : d.sectorId,
											"sectorNm" : d.sectorNm,
											"companyId" : $scope.globalCompId
									}
									var requestParams = $scope.division;
									MasterService.SectorDeleteService(requestParams, $scope.onDeleteSuccess, $scope.onDeleteError);
								} else {
									swal("Cancelled", "Your sector record is safe :)", "error");
								}
							});
						}

						$scope.onDeleteSuccess = function(response) {
							if (response.data.resCode === "res0000") {
								swal("Deleted!", "Deleted Successfully.", "success");
							} else if (response.data.resCode === "res0100") {
								swal("Not deleted!", response.data.resMsg, "error");
							}
							$scope.getAllSector();
						}
						
						$scope.getAllSector();
					} ]);
})();
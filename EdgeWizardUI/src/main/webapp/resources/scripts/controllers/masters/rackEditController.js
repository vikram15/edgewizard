(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('RackEditController', ['$scope','MasterService','jw','$cookies','AssetService','$uibModalInstance', 
	                                   function($scope, MasterService, jw, $cookies, AssetService, $uibModalInstance) {
		var vm = this;
		vm.masterData = [];
		vm.tableHeader = "Rack Edit";
		vm.rack = {};
		vm.rack = jw;
		vm.totalCells = vm.rack.columnCount * vm.rack.shelfCount;
		
		var golObj = JSON.parse($cookies.get("globals"));
		vm.globalCompId = golObj.companyId;
		vm.globalCompName = golObj.CompanyName;
		
		if(vm.rack){
			for (var i = 1; i <= vm.rack.columnCount; i++) {
				for (var j = 1; j <= vm.rack.shelfCount; j++) {
					vm.slaveObj = {
							"columnName" : vm.rack.columnName + "-" + i,
							"shelfName" : vm.rack.shelfName + "-" + j
					}
					
					vm.masterData.push(vm.slaveObj);
				}
			}
		}
		
		vm.slaveData = vm.masterData;
		
		vm.getLocation = function() {
			var requestParams = {
					"companyId" : vm.globalCompId
			};
			MasterService.LocationListService( requestParams, vm.onLocInfoSuccess, vm.onLocInfoError);
		};
			
		vm.onLocInfoSuccess = function(response) {
			vm.location_data = response.data;
			vm.rack.location = response.data.resData[0];
			
			angular.forEach(response.data.resData , function(value){
				if(value.locationNm === vm.rack.locationName){
					vm.rack.location = value;
				}
			})
			
			vm.locChange();
		}
		vm.onLocInfoError = function() {
			console.log("error");
		}
		
		vm.locChange = function() {
			vm.locationId = {
					"locationId" : vm.rack.location.locationId
			};
			AssetService.getDepDetailsByLocID( vm.locationId, vm.locChangeSuccess, vm.locChangeError);
		};

		vm.locChangeSuccess = function(response) {
			vm.depart_data = response.data;
			vm.rack.department = vm.depart_data.resData[0];
		};
		
		vm.locChangeError = function() {
			console.log("error");
		};
		
		vm.rackUpdate = function(){
			console.log(vm.rack);
			vm.requestParams = vm.rack;
			MasterService.RackUpdateService(vm.requestParams, vm.onUpdateSuccess, vm.onUpdateError)
		};
		
		vm.onUpdateSuccess = function(response){
			console.log("update success" + response)
			if (response.data.resCode === "res0000") {
				$uibModalInstance.dismiss('cancel');
				swal("Updated",response.data.resMsg, "success");
			} else if (response.data.resCode === "res0100") {
				swal("Not updated!", response.data.resMsg, "error");
			}
		}, 
		
		vm.onUpdateError = function(){
			console.log("update Error");
		};
							
		vm.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
								
		vm.getLocation();						
	}]);
})();

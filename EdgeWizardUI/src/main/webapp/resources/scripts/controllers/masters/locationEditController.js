(function() {
	'use strict';

	angular
			.module('rfidDemoApp.controller')
			.controller(
					'LocationEditController',
					[
							'$scope',
							'$uibModalInstance',
							'jw',
							'AuthService',
							'MasterService',
							'$state',
							'$stateParams',
							'AssetService',
							'CustomService',
							function($scope, $uibModalInstance, jw,
									AuthService, MasterService, $state,
									$stateParams, AssetService, CustomService) {
								var vm = this;
								$scope.location = jw;
								vm.tableHeader = "Location Edit";
								$scope.globalCompId = AuthService .getCompanyId();
								$scope.globalCompName = AuthService .getCompanyName();

								$scope.divid = $scope.location.divisionId;
								$scope.secId = $scope.location.sectorId;

								var contNo = CustomService.validationService($scope.location.contactNumber);
								var email = CustomService.validationService($scope.location.email);
								
								$scope.location.contactNumber = contNo;
								$scope.location.email = email;
									
								$scope.locationUpdate = function() {
									var requestParams = {
										"locationId" : $scope.location.locationId,
										"locationNm" : $scope.location.locationNm,
										"contPer" : $scope.location.contPer,
										"contactNumber" : $scope.location.contactNumber,
										"email" : $scope.location.email,
										"address" : $scope.location.address,
										"remark" : $scope.location.remark,
										"companyId" : $scope.globalCompId,
										"divisionId" : $scope.divid,
										"sectorId" : $scope.secId
									}
									MasterService.LocationUpdateService( requestParams, $scope.onLocationSuccess, $scope.onLocationError);
								};

								$scope.onLocationSuccess = function(response) {
									if (response.data.resCode === "res0000") {
										$scope.location = response.data.resData;
										$uibModalInstance.dismiss('cancel');
										/*$state.transitionTo($state.current,
												$stateParams, {
													reload : true,
													inherit : false,
													notify : true
												});*/
										swal("Updated", "Location updated successfully!", "success");
									} else if (response.data.resCode === "res0100") {
										swal("Not updated!", response.data.resMsg, "error");
									}
								}
								$scope.onLocationError = function() {
									console.log("error");
								}

								$scope.cancel = function() {
									$uibModalInstance.dismiss('cancel');
								};

								$scope.divisionList = function() {
									var requestParams = {
										"companyId" : $scope.globalCompId
									}
									MasterService.DivisionDetailService(requestParams, $scope.onDivisionSuccess, $scope.onDivisionError);
								}
								$scope.onDivisionSuccess = function(response) {
									if (response.data.resCode === "res0000") {
										$scope.division = response.data;
										$scope.divChange();
									}
								}, $scope.onDivisionError = function(response) {
									console.log("error")
								}

								$scope.divChange = function() {
									$scope.divisionId = {"divisionId":$scope.divid}
									AssetService.getSectDetailsByDiv($scope.divisionId, $scope.getSectDetBySucc, $scope.getSectDetByErr);
								};

								$scope.getSectDetBySucc = function(response) {
									$scope.sector = response.data;
									$scope.secId = $scope.sector.resData[0].sectorId;
								};

								$scope.getSectDetByErr = function(response) {
									console.log("error");
								};

								$scope.divisionList();
								/* $scope.sectorList(); */

							} ]);
})();
(function() {
	'use strict';
	angular.module('rfidDemoApp.controller').controller(
			'BatchLotEditController',
			[
					'$scope',
					'batchLots',
					'$uibModalInstance',
					'AuthService',
					'MasterService',
					'$state',
					'$stateParams',
					function($scope, batchLots, $uibModalInstance, AuthService,
							MasterService, $state, $stateParams) {
						console.log("batchLotEditController");
						var vm = this;
						vm.tableHeader = "BatchLot Edit";
						vm.batchLotList = batchLots;
						vm.comp_Id = AuthService.getCompanyId();

						vm.batchLotEditFun = function() {
							vm.requestParams = {
								"batchId" : vm.batchLotList.batchId,
								"batchName" : vm.batchLotList.batchName,
								"remark" : vm.batchLotList.remark,
								"companyId" : vm.comp_Id
							};

							MasterService.batchLotUpdateDetails(
									vm.requestParams, vm.onBatchLtSucc,
									vm.onBatchLtErr);
						};

						vm.onBatchLtSucc = function(response) {
							if (response.data.resCode === "res0000") {
								$uibModalInstance.dismiss('cancel');
								swal("Updated", "Batchlot Updated Successfully!", "success")
								/*$state.transitionTo($state.current,
										$stateParams, {
											reload : true,
											inherit : false,
											notify : true
										});*/
							}else{
								
							}
						};

						vm.onBatchLtErr = function(response) {
							console.log("error block");
						};

						vm.cancel = function() {
							$uibModalInstance.dismiss('cancel');
						};
					} ]);
})();

(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
		.controller('DivisionAddController', [
					'$scope',
					'$uibModalInstance',
					'AuthService',
					'$state',
					'$stateParams',
					'MasterService',
					'$uibModal','jw','CustomService',
					function($scope, $uibModalInstance, AuthService, $state,
							$stateParams, MasterService, $uibModal, jw, CustomService) {
						var vm = this;
						vm.tableHeader = "Add Division";
						$scope.divBtnNm = "SAVE";
						
						$scope.addButton = true;
						$scope.updateButton = false;
						$scope.globalCompId = AuthService.getCompanyId();
						$scope.globalCompName = AuthService.getCompanyName();

						$scope.validateEmail=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
						//Division edit functionality
						if(jw){
							vm.tableHeader = "Edit Division";
							$scope.divBtnNm = "UPDATE";
							$scope.division = jw;
							var contNo = CustomService.validationService($scope.division.contactNo);
							var email = CustomService.validationService($scope.division.email);
							
							$scope.division.contactNo = contNo;
							$scope.division.email = email;
						};
						
						$scope.addDivisionDetails = function() {
							var requestParams = {
								"divisionNm" : $scope.division.divisionNm,
								"contactPer" : $scope.division.contactPer,
								"contactNo" : $scope.division.contactNo,
								"email" : $scope.division.email,
								"address" : $scope.division.address,
								"remark" : $scope.division.remark,
								"companyId" : $scope.globalCompId
							}
							if($scope.division.divisionId){
								requestParams.divisionId = $scope.division.divisionId;
								MasterService.DivisionUpdateService(requestParams, $scope.onDivisionUpdateSuccess, $scope.onDivisionUpdateError);
							}else{
								AuthService.DivisionAddService(requestParams, $scope.onDivisionInfoSuccess, $scope.onDivisionInfoError);
							}
						}

						$scope.onDivisionInfoSuccess = function(response) {

							if (response.data.resCode === "res0000") {
								$scope.division = response.data;
								$uibModalInstance.dismiss('cancel');
								/*$state.transitionTo($state.current,
										$stateParams, {
											reload : true,
											inherit : false,
											notify : true
										});*/

								swal("Added!", "Division added successfully.", "success");
							} else if (response.data.resCode === "res0100") {
								swal("Not added!",
										"Division Name Already Exists.",
										"error");
							}
						};

						$scope.onDivisionInfoError = function(response) {
							console.log("error")
						}
						
						$scope.onDivisionUpdateSuccess = function(response){
							if(response.data.resCode === "res0000"){
								$scope.division = response.data;
								$uibModalInstance.dismiss('cancel');
							    $state.transitionTo($state.current, $stateParams, {
							     reload: true,
							     inherit: false,
							     notify: true
							    });
							    swal("Updated!", "Division updated successfully.", "success");
							}
							else if(response.data.resCode === "res0100"){
								swal("Not updated!", "Division Name Already Exists.", "error");
							}
						};
						
						$scope.onDivisionUpdateError = function(response){
							console.log("error")
						},

						$scope.divisionList = function() {
							$scope.companyId = AuthService.getCompanyId();
							var requestParams = {
								"companyId" : $scope.companyId
							}
							MasterService.DivisionDetailService(requestParams,
									$scope.onDivisionSuccess,
									$scope.onDivisionError);
						}
						$scope.onDivisionSuccess = function(response) {
							if (response.data.resCode === "res0000") {
								$scope.division = response;
							}
						},

						$scope.onDivisionError = function(response) {
							console.log("error")
						},

						$scope.cancel = function() {
							$uibModalInstance.dismiss('cancel');
						};
						/*$scope.getCompany = function() {
							$scope.companyName = AuthService.getCompanyName();
							$scope.companyId = AuthService.getCompanyId();

							$scope.Company = {
								"companyName" : $scope.companyName,
								"companyId" : $scope.companyId
							}
						}
						$scope.getCompany();*/
						// $scope.divisionList();
					} ]);
})();
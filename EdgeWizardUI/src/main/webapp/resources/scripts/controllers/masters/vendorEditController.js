(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('VendorEditController',['$scope','$uibModalInstance','vendors','AuthService','MasterService','$state','$stateParams','CustomService',
	                                    function($scope, $uibModalInstance, vendors, AuthService, MasterService, $state, $stateParams, CustomService) {
		var vm = this;
		vm.tableHeader = "Vendor Edit";
		vm.vendorEditData = vendors;
		vm.comp_Id = AuthService.getCompanyId();
		
		var contNo = CustomService.validationService(vm.vendorEditData.contactNo);
		var email = CustomService.validationService(vm.vendorEditData.email);
						
		vm.vendorEditData.contactNo = contNo;
		vm.vendorEditData.email = email;

		vm.vendorUpdateFun = function() {
			vm.requestParams = {
					"vendorId" : vm.vendorEditData.vendorId,
					"vendorName" : vm.vendorEditData.vendorName,
					"contPer" : vm.vendorEditData.contPer,
					"contactNo" : vm.vendorEditData.contactNo,
					"email" : vm.vendorEditData.email,
					"address" : vm.vendorEditData.address,
					"remark" : vm.vendorEditData.remark,
					"companyId" : vm.comp_Id
			};
			MasterService.updateVendorDetails(vm.requestParams, vm.onSuccessUpdateVendorDet, vm.onErrorUpdateVendorDet);
		};
		
		vm.onSuccessUpdateVendorDet = function(response) {
			if (response.data.resCode === "res0000") {
				swal("Updated", "Vendor Updated Successfully!",	"success")
				$uibModalInstance.dismiss('cancel');
			}else{
				swal("Not Added!", response.data.resMsg, "error");
			}
		};

		vm.onErrorUpdateVendorDet = function() {
			console.log("error block");
		};

		vm.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}]);
})();

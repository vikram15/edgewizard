(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('IssuedAssetReportCtrl', ['$scope','$stateParams','ReportsService','DashboardService','$timeout','$cookies','AuthService','$localStorage','$rootScope','CustomService', 
	                                      function($scope, $stateParams, ReportsService, DashboardService, $timeout, $cookies, AuthService, $localStorage, $rootScope, CustomService){
		var vm = this ;
		var golObj = $cookies.get("globals")
	    if(golObj){
	    	vm.globalCompId = AuthService.getCompanyId();
	    	vm.globalCompName = AuthService.getCompanyName();
	    	vm.DefaultDockDoor = AuthService.getDockDoorName();
	    	vm.DefaultDockDoorId = JSON.parse($cookies.get("globals"));
	    	vm.userName = AuthService.getUserName();
	    }
		vm.assetData = [];
		
		vm.allIssuedAssetReportList = $localStorage.GAllIssReList;
	
		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		
		var dt = new Date();
		vm.currentDate = dt;
		vm.currDay = days[dt.getDay()];
		vm.currTime = dt.getTime();
		
    	vm.currentPage = 1;
	    vm.maxSize = 1;
	    vm.data = $stateParams.myParam;
	  
		vm.issuedAssetListLoad = function(){
			vm.requestParams = {
				"fromDate":vm.allIssuedAssetReportList.startDate,
				"toDate":vm.allIssuedAssetReportList.endDate,
				"orderBy":vm.allIssuedAssetReportList.ascOrDesc
			};
			ReportsService.getAllIssuedAssetList(vm.requestParams, vm.getIssuedAssetListSuccess, vm.getIssuedAssetListError);
		};
		
		vm.getIssuedAssetListSuccess = function(response){
			if(response.data.resData){
				vm.assetData = response.data;
				vm.totalItems = vm.assetData.resData.length;
		    	/*vm.itemsPerPage = 10;*/
			};
		}; 
		
		vm.getIssuedAssetListError = function(){
			
		};
		
		vm.receivedAssetListLoad = function(){
			vm.requestParams = {
					"fromDate":vm.allIssuedAssetReportList.startDate,
					"toDate":vm.allIssuedAssetReportList.endDate,
					"orderBy":vm.allIssuedAssetReportList.ascOrDesc
			}
			ReportsService.getAllReceivedAssetList(vm.requestParams, vm.getReceivedAssetListSuccess, vm.getReceivedAssetListError)
		};
		
		vm.getReceivedAssetListSuccess = function(response){
			if(response.data.resData){
				vm.assetData = response.data;
				vm.totalItems = vm.assetData.resData.length;
		    	/*vm.itemsPerPage = 10;*/
			};
		};
		
		vm.getReceivedAssetListError = function(){
			
		};
		
		vm.allAssetsIssuReceiveReport = function(){
			vm.requestParams = {
					"orderBy":vm.allIssuedAssetReportList.ascOrDesc
			}
			ReportsService.getAllIssuedReceivedAssetList(vm.requestParams, vm.getIssuedReceivedAssetListSuccess, vm.getIssuedReceivedAssetListError)
		};
		
		vm.getIssuedReceivedAssetListSuccess = function(response){
			if(response.data.resData){
				vm.assetData = response.data;
				vm.totalItems = vm.assetData.resData.length;
		    	/*vm.itemsPerPage = 10;*/
			};
		};
		
		vm.getIssuedReceivedAssetListError = function(){
			
		}
		
		vm.exportToExcel=function(tableId){ // ex: '#my-table'
            $scope.exportHref = DashboardService.tableToExcel(tableId,'ssst');

            /*$timeout(function(){location.href=$scope.exportHref;},100);*/ // trigger download
            
            var a = document.createElement('a');
            a.href = $scope.exportHref;
            a.download = vm.titleName + '.xls';
            a.click();
        };
        
       /* vm.exportAsExcel = function () {
            var blob = new Blob([document.getElementById('excelDiv').innerHTML], {
                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            });
            saveAs(blob, "All Issued Asset List.xls");
        };*/
        
        var imgToExport = document.getElementById('imgToExport');
        function getDataUri(url, callback) {
            var image = new Image();

            image.onload = function () {
                var canvas = document.createElement('canvas');
                canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
                canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

                canvas.getContext('2d').drawImage(this, 0, 0);

                // Get raw image data
              //  callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

                // ... or get as Data URI
                callback(canvas.toDataURL('image/png'));
            };
            image.src = url;
        }

        // Usage
        vm.dolpLogoBase64 = "";
        getDataUri(imgToExport.src, function(dataUri) {
        	vm.dolpLogoBase64 = dataUri;
            // Do whatever you'd like with the Data URI!
        });
  		
        vm.generatePDF = function (element) {
        	
        	var column = [];
        	var value = [];
        	var tdWidth = [30, '*', '*','*','*','*','*','*','*'];
        	
        	switch(vm.data) {
		    case "allIssuedAssetReport":// all issued asset report
		    	vm.titleName = "All Issued Asset Reports";
		    	column.push({text:'Sl.No', style: ['tableHeader','myTableHeader']}, 
						 {text: 'User Id', style: ['tableHeader','myTableHeader']},
						 {text: 'User Name', style: ['tableHeader','myTableHeader']},
						 {text: 'AssetId', style: ['tableHeader','myTableHeader']},
						 {text: 'Asset Name', style: ['tableHeader','myTableHeader']},
						 {text: 'Category', style: ['tableHeader','myTableHeader']},
						 {text: 'Issue Date Time', style: ['tableHeader','myTableHeader']},
						 {text: 'Receive Date', style: ['tableHeader','myTableHeader']},
						 {text: 'Issued By', style: ['tableHeader','myTableHeader']}
						 )
				
	        	break;
		    case "allReceivedAssetReport":// all received asset report
		    	vm.titleName = "All Received Asset Reports";
		    	column.push({text:'Sl.No', style: ['tableHeader','myTableHeader']}, 
						 {text: 'User Id', style: ['tableHeader','myTableHeader']},
						 {text: 'User Name', style: ['tableHeader','myTableHeader']},
						 {text: 'AssetId', style: ['tableHeader','myTableHeader']},
						 {text: 'Asset Name', style: ['tableHeader','myTableHeader']},
						 {text: 'Category', style: ['tableHeader','myTableHeader']}, 
						 {text: 'Receive Date', style: ['tableHeader','myTableHeader']},
						 {text: 'Return Date Time', style: ['tableHeader','myTableHeader']},
						 {text: 'Received By', style: ['tableHeader','myTableHeader']}
						 )
		    	break;
		    case "allReceivedExceReport":// all received exception report
		    	vm.titleName = "All Received Exception Reports";
			    break;
		    case "assetDetailsReport"://asset details report
		    	vm.titleName = "Asset Details Reports";
		    	break;
		    case "allAssetsIss_ReceiveReport"://asset details report
		    	vm.titleName = "All Issued/Received Asset List Reports";
		    	column.push({text:'Sl.No', style: ['tableHeader','myTableHeader']}, 
						 {text: 'User Id', style: ['tableHeader','myTableHeader']},
						 {text: 'User Name', style: ['tableHeader','myTableHeader']},
						 {text: 'AssetId', style: ['tableHeader','myTableHeader']},
						 {text: 'Asset Name', style: ['tableHeader','myTableHeader']},
						 {text: 'Category', style: ['tableHeader','myTableHeader']}, 
						 {text: 'Issue Date Time', style: ['tableHeader','myTableHeader']},
						 {text: 'Receive Date', style: ['tableHeader','myTableHeader']},
						 {text: 'Return Date Time', style: ['tableHeader','myTableHeader']},
						 {text: 'Issued By', style: ['tableHeader','myTableHeader']},
						 {text: 'Received By', style: ['tableHeader','myTableHeader']}
						 )
						 tdWidth.push('*','*');
		    	break;
        	};
        	
        	if(vm.assetData){
        		var k = 1;
        		for(var i = 0; i < vm.assetData.resData.length ; i++){
        			switch(vm.rowNm){
        			case 1 : 
        				value.push([{
            				text: k ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].userId ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].userName ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].assetId ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].assetName ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].category ,style : 'tableRowFont',
            			},{
            				text: moment(vm.assetData.resData[i].issueDateTime).format("DD-MMM-YYYY HH:mm:ss") ,style : 'tableRowFont',
            			},{
            				text: moment(vm.assetData.resData[i].returnDate).format("DD-MMM-YYYY HH:mm:ss") ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].issueBy ,style : 'tableRowFont',
            			}]);
        				break;
        			case 2 :
        				value.push([{
            				text: k ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].userId ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].userName ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].assetId ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].assetName ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].category ,style : 'tableRowFont',
            			},{
            				text: moment(vm.assetData.resData[i].returnDate).format("DD-MMM-YYYY HH:mm:ss") ,style : 'tableRowFont',
            			},{
            				text: moment(vm.assetData.resData[i].receiveDateTime).format("DD-MMM-YYYY HH:mm:ss") ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].receiveBy ,style : 'tableRowFont',
            			}]);
        				break;
        			case 5 :
        				value.push([{
            				text: k ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].userId ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].userName ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].assetId ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].assetName ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].category ,style : 'tableRowFont',
            			},{
            				text: moment(vm.assetData.resData[i].issueDateTime).format("DD-MMM-YYYY HH:mm:ss") ,style : 'tableRowFont',
            			},{
            				text: moment(vm.assetData.resData[i].returnDate).format("DD-MMM-YYYY HH:mm:ss") ,style : 'tableRowFont',
            			},{
            				text: moment(vm.assetData.resData[i].receiveDateTime).format("DD-MMM-YYYY HH:mm:ss") ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].issueBy ,style : 'tableRowFont',
            			},{
            				text: vm.assetData.resData[i].receiveBy ,style : 'tableRowFont',
            			}]);
        				break;
        			}
        				
        			k++;
        		}
        	}
        	
        	vm.otherData = {
        			"titleName" :vm.titleName,
        			"currDay" : vm.currDay,
        			"currentDate" : vm.currentDate,
        			"currTime" : vm.currTime,
        			"dolpLogoBase64" : vm.dolpLogoBase64,
        			"globalCompName" : vm.globalCompName,
        			"userName" : vm.userName,
        			"currTime" : vm.currTime,
        			
        	};
        	
        	CustomService.openPdfService(value,column, tdWidth ,vm.otherData);
        	
        	//   var pdf = new jsPDF('p','cm',[50, 55]);
        	/* pdf.addHTML($("#excelDiv"),1,0,options, function(){
            	pdf.setFontSize(50);
            	pdf.save(vm.titleName+'.pdf');
            });*/
        };
        
		// This executes when checkbox in table header is checked
	    $scope.selectAll = function () {
	    	// Loop through all the entities and set their isChecked property
	    	for (var i = 0; i < vm.items.length; i++) {
	    		vm.items[i].isChecked = vm.allItemsSelected;
	    		$scope.allSelect = true;
	    	}
	    };
	    
	    // This executes when entity in table is checked
	    $scope.selectEntity = function () {
	    	// If any entity is not checked, then uncheck the "allItemsSelected" checkbox
	    	for (var i = 0; i < vm.items.length; i++) {
	    		if (!vm.items[i].isChecked) {
	    			vm.allItemsSelected = false;
	    			return;
	    		}
	    	}
	    	//If not the check the "allItemsSelected" checkbox
	    	vm.allItemsSelected = true;
	    };
	    
	    switch(vm.data) {
		    case "allIssuedAssetReport":// all issued asset report
		    	vm.titleName = "All Issued Asset Reports";
		    	vm.rowNm = 1;
		    	vm.issuedAssetListLoad();
	        	break;
		    case "allReceivedAssetReport":// all received asset report
		    	vm.titleName = "All Received Asset Reports";
		    	vm.rowNm = 2;
		    	vm.receivedAssetListLoad();
		    	break;
		    case "allReceivedExceReport":// all received exception report
		    	vm.titleName = "All Received Exception Reports";
		    	vm.rowNm = 3;
		    	break;
		    case "assetDetailsReport"://asset details report
		    	vm.titleName = "Asset Details Reports";
		    	vm.rowNm = 4;
		    	break;
		    case "allAssetsIss_ReceiveReport"://asset details report
		    	vm.titleName = "All Issued/Received Asset List Reports";
		    	vm.rowNm = 5;
		    	vm.allAssetsIssuReceiveReport();
		    	break;
		}
		
	}])
})();
(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('MaintenanceReportCtrl', ['$uibModal','AuthService','ReportsService', 
	                                      function($uibModal, AuthService, ReportsService){
		console.log("MaintenanceReportCtrl");
		var vm = this;
		vm.comp_Id = AuthService.getCompanyId();
		vm.comp_name = AuthService.getCompanyName(); 
		vm.user_name = AuthService.getUserName();
		vm.ascordesc = "asc";
		vm.assetData = "";
		
		vm.hours = ['00', '01', '02','03', '04', '05','06', '07', '08','09', '10', '11','12', '13', '14','15', '16', '17','18', '19', '20','21', '22', '23']; 
		vm.minutes = ['00', '01', '02','03', '04', '05','06', '07', '08','09', '10', '11','12', '13', '14','15', '16', '17', '18', '19', '20','21', '22', '23','24', '25', '26','27', '28', '29','30', '31', '32', '33', '34', '35','36', '37', '38','39', '40', '41', '42', '43', '44','45', '46', '47','48', '49', '50','51', '52', '53','54', '55', '56','57', '58', '59']; 
		vm.seconds = ['00', '01', '02','03', '04', '05','06', '07', '08','09', '10', '11','12', '13', '14','15', '16', '17', '18', '19', '20','21', '22', '23','24', '25', '26','27', '28', '29','30', '31', '32', '33', '34', '35','36', '37', '38','39', '40', '41', '42', '43', '44','45', '46', '47','48', '49', '50','51', '52', '53','54', '55', '56','57', '58', '59']; 
		vm.shh =  vm.hours[0];
		vm.smm =  vm.hours[0];
		vm.sss =  vm.hours[0];
			
		vm.ehh =  vm.hours[0];
		vm.emm =  vm.hours[0];
		vm.ess =  vm.hours[0];
		
		/*open asset search box */
		vm.searchBtn = function(){
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/asset/assetSearch.html',
				controller : 'AssetSearchController',
				controllerAs : 'assetSearchController',
				resolve : {
					reportType : function() {
						return angular.copy(vm.reportType);
					}
				}
			});
			modalInstance.result.then(function(selectedItem) {
				
			}, function(data) {
				console.log(data);
				vm.assetData = data;
			});
		};
		
		vm.generateBtn = function(){
			vm.assetIdTemp = []; 
			vm.requestParams = {
					"companyId":vm.comp_Id,
					"companyName":vm.comp_name,
					"generatedBy":vm.user_name,
					"startDate": moment(vm.fromDate).format("DD-MMM-YYYY HH:ss:mm"),
					"endDate": moment(vm.toDate).format("DD-MMM-YYYY HH:ss:mm"),
					"ascOrDesc":vm.ascordesc
			}
			
			if(vm.reportType === "4"){
				vm.requestParams.assetId = vm.assetData[0].assetId,
				console.log(vm.requestParams);
				ReportsService.getMaintenanceHistoryReport(vm.requestParams, vm.maintenanceHistorySuccess, vm.maintenanceHistoryError);
			}
			
			if(vm.reportType === "5"){
				angular.forEach(vm.assetData, function(value){
					console.log(value)
					vm.assetIdTemp.push(value.assetId);
				});
				vm.requestParams.assetIdList = vm.assetIdTemp,
				console.log(vm.requestParams);
				ReportsService.getMaintenanceHistoryForAllAssets(vm.requestParams, vm.maintenanceHistoryforAllSuccess, vm.maintenanceHistoryforAllError);
			}
		};
		
		vm.maintenanceHistorySuccess = function(response){
			console.log(response.data);
			
			var blob = new Blob([response.data], {type: "application/*"});
	        var objectUrl = URL.createObjectURL(blob);
	       //window.open(objectUrl);
	       
			
			var a = document.createElement('a');
            a.href = objectUrl;
            a.download = "sample.pdf";
            a.click();
		};
		
 		vm.maintenanceHistoryError = function(response){
 			console.log("maintenanceHistoryError" + response);
 		};
		
 		vm.maintenanceHistoryforAllSuccess = function(response){
 			var blob = new Blob([response.data], {type: "application/*"});
	        var objectUrl = URL.createObjectURL(blob);
	       //window.open(objectUrl);
	       
			
			var a = document.createElement('a');
            a.href = objectUrl;
            a.download = "sample.pdf";
            a.click();
 		};
 		
 		vm.maintenanceHistoryforAllError = function(response){
 			console.log(response.data);
 		};
 		
 		/*========ANGULAR DATE PICKER============*/
		vm.today = function() {
			vm.fromDate = new Date();
			vm.toDate = new Date();
		};
		vm.today();

		vm.clear = function() {
			vm.fromDate = null;
			vm.toDate = null;
		};

		vm.dateOptions = {
				/*dateDisabled: disabled,*/
				formatYear: 'yy',
				maxDate: new Date(2020, 5, 22),
				minDate: new Date(2000, 5, 22),
				startingDay: 1
		};

		// Disable weekend selection
		function disabled(data) {
			var date = data.date,
			mode = data.mode;
			return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
		}

		vm.fromOpen = function(){
			vm.from.opened = true;
		};
	  
		vm.toOpen = function(){
			vm.to.opened = true;  
		};
	  
		vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		vm.format = vm.formats[0];
		vm.altInputFormats = ['M!/d!/yyyy'];
		
		vm.from = {
				opened: false
		};
		vm.to = {
				opened: false
		};
		
	  /*function getDayClass(data) {
		  var date = data.date,
		  mode = data.mode;
		  if (mode === 'day') {
			  var dayToCheck = new Date(date).setHours(0,0,0,0);
			  
			  for (var i = 0; i < $scope.events.length; i++) {
				  var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
		
				  if (dayToCheck === currentDay) {
					  return $scope.events[i].status;
				  }
			  }
		  }
		
		  return '';
	  }*/
	}])
})();
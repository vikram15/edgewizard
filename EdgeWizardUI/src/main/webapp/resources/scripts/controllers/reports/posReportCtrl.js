(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('PosReportCtrl',['$scope','CustomService','$uibModal','PosService','AuthService','$filter','_',
	                             function($scope, CustomService, $uibModal, PosService, AuthService, $filter, _){
		var vm = this;
		vm.usrData = {};
		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		var dt = new Date();
		vm.currentDate = dt;
		vm.currDay = days[dt.getDay()];
		vm.currTime = dt.getTime();
		vm.globalCompName = AuthService.getCompanyName();
		vm.userName = AuthService.getUserName();
		
		vm.userDetailsDailogue = function(){
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/POS/posUserSearch.html',
				controller : 'PosUserSearchCtrl',
				controllerAs : 'posUserSearchCtrl',
			});
			modalInstance.result.then(function(selectedItem) {
				
			}, function(usrD) {
				if(usrD !== 'cancel'){
					vm.usrData = JSON.parse(usrD);
				}
			});
		};
		
		var imgToExport = document.getElementById('imgToExport');
        function getDataUri(url, callback) {
            var image = new Image();

            image.onload = function () {
                var canvas = document.createElement('canvas');
                canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
                canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

                canvas.getContext('2d').drawImage(this, 0, 0);

                // Get raw image data
              //  callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

                // ... or get as Data URI
                callback(canvas.toDataURL('image/png'));
            };
            image.src = url;
        }

        // Usage
        vm.dolpLogoBase64 = "";
        getDataUri(imgToExport.src, function(dataUri) {
        	vm.dolpLogoBase64 = dataUri;
            // Do whatever you'd like with the Data URI!
        });
		
		vm.generate = function(){
			vm.requestParams = {
					"startDate": moment(vm.fromDate).format("DD-MM-YYYY"),
					"endDate" : moment(vm.toDate).format("DD-MM-YYYY"),
					/*"userID":vm.usrData.userId*/
			}
			PosService.getPosUsersReport(vm.requestParams, vm.onSuccessGetReportDetails, vm.onErrorGetReportDetails)
			//PosService.getUserTransactionReportDetails(vm.requestParams, vm.onSuccessGetReportDetails, vm.onErrorGetReportDetails)
		}
		
		vm.onSuccessGetReportDetails = function(response){
			
			var result = response.data.resData;
			if(result){
				var userTblWidth, userTblVal = [], userTblClm = [], totalCrCount = 0, totalParkCount = 0, totalPosCount = 0, totalCableCartCount = 0, totalCurreBalCount = 0;
				userTblWidth = [40,'*','*','*','*','*','*','*'];
				
				userTblClm.push({
					text:'Sl.No', style: ['tableHeader','myTableHeader']},{
					text:'Booking No', style: ['tableHeader','myTableHeader']},
					{text:'Name', style: ['tableHeader','myTableHeader']}, 
					{text: 'CR', style: ['tableHeader','myTableHeader']},
					{text: 'Park', style: ['tableHeader','myTableHeader']},
					{text: 'POS', style: ['tableHeader','myTableHeader']},
					{text: 'CableCart', style: ['tableHeader','myTableHeader']},
					{text: 'Refund', style: ['tableHeader','myTableHeader']}
					)
					
				var d = 1;
				_.each(result,function(res){
					
					if(res.totalCr === null || res.totalCr === "null" ){
						res.totalCr = 0;
					}
					if(res.parkReader === null || res.parkReader === "null" ){
						res.parkReader = 0;
					}
					if(res.pos === null || res.pos === "null" ){
						res.pos = 0;
					}
					if(res.cableCart === null || res.cableCart === "null" ){
						res.cableCart = 0;
					}
					if(res.currentBalance === null || res.currentBalance === "null" ){
						res.currentBalance = 0;
					}
					
					userTblVal.push([{
						text: d ,style : 'tableRowFont',
					},{
						text: res.bookingId ,style : 'tableRowFont',
					},{
						text: res.userName ,style : 'tableRowFont',
					},{
						text: "₹" + $filter('number')(res.totalCr, 2) ,style : 'tableRowFont',
					},{
						text: "₹" + $filter('number')(res.parkReader, 2) ,style : 'tableRowFont',
					},{
						text: "₹" + $filter('number')(res.pos, 2)  ,style : 'tableRowFont',
					},{
						text: "₹" + $filter('number')(res.cableCart, 2) ,style : 'tableRowFont',
					},{
						text: "₹" + $filter('number')(res.currentBalance, 2), style :'tableRowFont',
					}]);
					
					totalCrCount = totalCrCount + parseInt(res.totalCr);
					totalParkCount = totalParkCount + parseInt(res.parkReader);
					totalPosCount = totalPosCount + parseInt(res.pos);
					totalCableCartCount = totalCableCartCount + parseInt(res.cableCart);
					totalCurreBalCount = totalCurreBalCount + parseInt(res.currentBalance);
					d++;
				})
				
				vm.otherData = {
			    		"titleName" : "POS User Details",
			    		"currDay" : vm.currDay,
			    		"currentDate" : vm.currentDate,
			    		"currTime" : vm.currTime,
			    		"dolpLogoBase64" : vm.dolpLogoBase64,
			    		"globalCompName" : vm.globalCompName,
			    		"userName" : vm.userName,
			    		"totalCrCount" : totalCrCount,
			    		"totalParkCount" : totalParkCount,
			    		"totalPosCount" : totalPosCount,
			    		"totalCableCartCount" : totalCableCartCount,
			    		"totalCurreBalCount" : totalCurreBalCount
				};
				CustomService.posUserDetailsReport(userTblWidth, userTblVal, userTblClm, vm.otherData);
			}else{
				swal("Error", response.data.resMsg, "error");
			}
		};
		
		vm.onErrorGetReportDetails = function(response){
			console.log(response.data)
		};
		
		/*========ANGULAR DATE PICKER============*/
		vm.today = function() {
			vm.fromDate = new Date();
			vm.toDate = new Date();
		};
		vm.today();

		vm.clear = function() {
			vm.fromDate = null;
			vm.toDate = null;
		};

		vm.dateOptions = {
				/*dateDisabled: disabled,*/
				formatYear: 'yy',
				maxDate: new Date(2020, 5, 22),
				minDate: new Date(2000, 5, 22),
				startingDay: 1
		};

		// Disable weekend selection
		function disabled(data) {
			var date = data.date,
			mode = data.mode;
			return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
		}

		vm.fromOpen = function(){
			vm.from.opened = true;
		};
	  
		vm.toOpen = function(){
			vm.to.opened = true;  
		};
	  
		vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		vm.format = vm.formats[0];
		vm.altInputFormats = ['M!/d!/yyyy'];
		
		vm.from = {
				opened: false
		};
		vm.to = {
				opened: false
		};
		
	}]);
})();
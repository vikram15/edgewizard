(function() {
    'use strict';
    angular.module('rfidDemoApp.controller')
		.controller('IssueController', ['$uibModal','$scope' ,'$rootScope','AuthService','USERBGIMG','UserService','AssetService','_','IssueReceiveService','$state','$interval','$cookies','ReaderServices','$window','CustomService', 
		                                function($uibModal,$scope, $rootScope, AuthService, USERBGIMG, UserService, AssetService,_, IssueReceiveService, $state, $interval, $cookies, ReaderServices, $window, CustomService) {
        var vm = this;
        vm.userImg = false;
        var golObj = JSON.parse($cookies.get("globals"));
        vm.DefaultDockDoor = golObj.dockDoorName;
        
        if(golObj){
        	vm.globalCompId = AuthService.getCompanyId();
            vm.globalCompName = AuthService.getCompanyName();
            //vm.DefaultDockDoor = AuthService.getDockDoorName();
            vm.DefaultDockDoorId = JSON.parse($cookies.get("globals"));
			vm.userID = AuthService.getUserId();
		}
        
        vm.assetDataNew = [];
        vm.allSelect = true;
        var no = 1;
        vm.showUsrMsg = true;
        var promise;
        vm.issueType ="";
        vm.customDt = "";
        vm.usrId = 0;
        vm.grDetails;
        vm.usDetails;
        vm.listedAsset = 0;
        
        //print var
    	var assetNameList;
		var serNo;
		var temp = [];
		
        /*vm.asImg = "";
		vm.userImg = "";
        */
        /* user info from user select */
		$rootScope.$on("userInfo", function(evt, data) {
			vm.userInfo = JSON.parse(data);
			vm.usDetails = vm.userInfo;
			vm.compName = vm.globalCompName;
			vm.userDetails(vm.usDetails)
			
		});
		
		$rootScope.$on("assetInfo", function(evt, data) {
			vm.asset = "true";
	        vm.issueType = "M";
			/*console.log(data,"DADADAD")*/
			   for(var i=0; i< data.length; i++){
					if(i === data.length - 1){
						vm.assetInfo = data[i];
					}
				}
			   //vm.assetInfo = JSON.parse(data);
			   vm.asDetails = vm.assetInfo;
			   vm.compName = vm.globalCompName;
			   vm.assetDetails(data)
		});
		
		vm.userDetails = function(usrInfo){
			if(usrInfo && usrInfo.userId){
				vm.requestParams = {
						"userId" :usrInfo.userId,
						"companyId":vm.globalCompId
				};
				UserService.getUserDetails(vm.requestParams, vm.getUserDetSuccess, vm.getUserDetError);
			}
		};
			
		vm.getUserDetSuccess = function(response){
			vm.assetDataNew = [];
			if(response.data.resData !== null && response.data.resCode === "res0000"){
				vm.showUsrMsg = false;
				vm.showAssMsg = true;
				vm.usDetails = Object.assign(response.data.resData[0],response.data.resData[1]);
				 
				vm.compName = AuthService.getCompanyName();
				//if(!response.data.resData[0].userId){
				vm.getUserIssuedAsset(response.data.resData[0].userId)
				//}
				vm.userselected = true;
				if(response.data.resData[0].userPhoto){
					vm.requestParams = {  
							"companyId":response.data.resData[0].companyId,
							"downloadFrom":USERBGIMG,
							"imageId":response.data.resData[0].userId,
							"imageName":response.data.resData[0].userPhoto
					}; 
					AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
				}else{ 
					/* $scope.$on('$destroy', function(){
							    $interval.cancel(promise)
							});*/
				}
				//$interval.cancel(promise);
				//if(!response.data.resData.userId === undefined){
				promise =  $interval(function(){
					//vm.issueUserReadTag();
					vm.issueAssetReadTag();
				},1500);
				//}
			}
		};
			  
		vm.getUserDetError = function(){
			  
		};
		
		vm.assetDetails = function(assetInfo){
			if(assetInfo){
				for(var i=0; i <  assetInfo.length; i++){
					vm.requestParamsAss = {
							"assetId":assetInfo[i].assetId || assetInfo[i],
							"companyId": vm.globalCompId
					};
					/*AssetService.assetGetByID(vm.requestParams, vm.assSuccessBlock, vm.assErrorBlock)*/
					AssetService.getAssetIssueReceive(vm.requestParamsAss, vm.assSuccessBlock, vm.assErrorBlock);
				}
			}
		};
		
		vm.oldAssetData = [];
		
		vm.assSuccessBlock = function(response){
			if(response.data.resData !== null){
				vm.showAssMsg = false;
				/*vm.assetData = response.data.resData[0].concat(response.data.resData[1]);*/
				vm.assetData = Object.assign(response.data.resData[0], response.data.resData[1], response.data.resData[2]);
				/*vm.assetData = Object.assign(vm.assetData, response.data.resData[2])*/
				vm.asDetails = vm.assetData;
				//if(vm.assetData.assetId < 0){
				if(!_.any(vm.assetDataNew, _.matches({"assetId":vm.assetData.assetId}))){
					vm.assetData.no = no;
					vm.assetData.issueType = vm.issueType;
					vm.assetData.maintBgColor = "false";
					vm.assetData.expBgColor = "false";
					
					//vm.assetData.isChecked = true;
					vm.assetDataNew.push(vm.assetData);
					var countListedAs = 0;
					for (var i = 0; i < vm.assetDataNew.length; i++) {
						
						if(!vm.assetDataNew[i].from){
							countListedAs++;
						}
						vm.listedAsset = countListedAs ;
						/*vm.assetDataNew[i].isChecked = true; */
						if(vm.grDetails){
							vm.astMainDays = vm.grDetails.maintenanceDt;
							vm.astExpDays = vm.grDetails.expDt;
						};
							
						if(vm.assetDataNew[i].mainDt || vm.assetDataNew[i].expiryDt){
							/*vm.astMainDays = vm.grDetails.maintenanceDt;
								vm.astExpDays = vm.grDetails.expDt;
							 */
							/*vm.astMainDate = new Date(vm.assetDataNew[i].mainDt);
								vm.astExpDate = new Date(vm.assetDataNew[i].expiryDt);*/
							
							vm.astMainDate = moment(vm.assetDataNew[i].mainDt).format('YYYY-MM-DD');
							vm.astExpDate = moment(vm.assetDataNew[i].expiryDt).format('YYYY-MM-DD');
						};
							
						vm.astMaintDays = new Date();
						vm.astMaintDays.setDate(vm.astMaintDays.getDate() + vm.astMainDays);
						vm.astExpyDays = new Date();
						vm.astExpyDays.setDate(vm.astExpyDays.getDate() + vm.astExpDays);
						
						vm.astMaintDays = moment(vm.astMaintDays).format('YYYY-MM-DD');
						vm.astExpyDays = moment(vm.astExpyDays).format('YYYY-MM-DD');
							
						if(vm.assetDataNew[i].assetId === vm.assetData.assetId){
							vm.assetDataNew[i].isChecked = true; 
							$scope.allSelect = true;
							vm.allItemsSelected = true;
								
							//Maintenance date checking
							if(vm.astMainDays !== parseInt(-1)){
								/*if(vm.astMaintDays >= vm.astMainDate){*/
								if(vm.astMainDate <= vm.astMaintDays){
									/*vm.rowBgErrorId = vm.assetData.assetId;*/
									vm.assetDataNew[i].maintBgColor = "true"; 
								}else{
									console.log("false");
								}
							}
								
							//Expiry date checking
							if(vm.astExpDays !== parseInt(-1)){
								/*if(vm.astExpyDays >= vm.astExpDate){*/
								if(vm.astExpDate <= vm.astExpyDays){
									/*vm.rowBgExpErrorId = vm.assetData.assetId;*/
									vm.assetDataNew[i].expBgColor = "true"; 
								}else{
									console.log("false");
								}
							}
						}
					}
					no++;
				}else{
					/*swal("warning", "Asset already added", "error");*/
				}
				//}
				//$interval.cancel(promise);
				/*if (_.findWhere(vm.assetDataNew, vm.assetData) == null) {
					vm.assetDataNew.push(vm.assetData);
					}
				else{
					swal("warning", "Asset already added", "error");
				}*/
					
				vm.assetPhotoFunct(vm.asDetails);
			}
		};
		
		vm.assErrorBlock = function(){
			
		};
		
		vm.onGetLogoSuccess = function(response){
			if(response.data.resCode === "res0000"){
				if(response.data.resData){
					vm.userImg = true;
					vm.usrImg = response.data.resData.image;
				}
			}else{
				vm.userImg = false;
			};
		};

		vm.onGetLogoError = function(){
			
		};
		
		vm.userDetailsDailogue = function(){
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/user/userSearch.html',
				controller : 'UserSearchController',
				controllerAs : 'userSearchController',
			});
			modalInstance.result.then(function(selectedItem) {
				
			}, function(t) {
			});
		};
		
		vm.assetSearchDailogue = function(){
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/asset/assetIRSearch.html',
				controller : 'AssetIRSearchController',
				controllerAs : 'assetIRSearchController',
			});
			modalInstance.result.then(function(selectedItem) {
				
			}, function(t) {
			});
		};
		
		vm.getUserIssuedAsset = function(userId){
			vm.requestParamsUi = {
					"userId":userId,
					"companyId":vm.globalCompId
			}
			//op0048.11
			UserService.getUserIssuedAsset(vm.requestParamsUi, vm.issueAssetSucc, vm.issueAssetErr);
		}
		vm.cntData = "";
		vm.issueAssetSucc = function(response){
			if(response.data.resCode === "res0000"){
				if(Array.isArray(response.data.resData)){
					vm.showUsrMsg = false;
					vm.cntData = response.data.resData.length;
					vm.asDetails = response.data.resData[response.data.resData.length - 1]
					//vm.assetDataNew = response.data.resData;
					
					vm.assetPhotoFunct(vm.asDetails);
					
					for(var i= 0; i < response.data.resData.length; i++){
						
						response.data.resData[i].from = "UA";
						response.data.resData[i].no = no;
						/*if(vm.issueType ===""){
							response.data.resData[i].issueType = "M";
						}else{
							response.data.resData[i].issueType = vm.issueType;
						}*/
					
						/*if(!_.any(vm.assetDataNew, _.matches({"assetId":response.data.resData[i].assetId}))){
							vm.assetDataNew.push(response.data.resData[i]);
						}*/
						/*console.log("already asset issued to user..." + JSON.stringify(response.data.resData[i]));*/
						if(response.data.resData[i].assetIssueRF === "Default"){
							response.data.resData[i].customDt = response.data.resData[i].assetIssueDate;
							response.data.resData[i].issueType = "M";
							/*vm.customDt = response.data.resData[i].assetIssueDate;*/
						}else{
							response.data.resData[i].issueType = "R";
							response.data.resData[i].customDt = response.data.resData[i].assetIssueRF;
						}
						vm.assetDataNew.push(response.data.resData[i]);
						//vm.assetDataNew.reverse();
						no++;
					}
				}
				else{
					//vm.showUsrMsg = false;
					vm.asImg = 'resources/images/NoImage.gif';
					vm.assetImg = false;
					vm.asDetails={};
					vm.cntData = 0;
					vm.assetDataNew = [];
				}
			}else{
				swal("Error", "something went wrong","error")
			}
		}
		vm._selected_data = [];
		vm.allItemsSelected = false;
	       
		
		 vm.calculateChecked = function() {
		      var count = 0;
		      
		      angular.forEach(vm.assetDataNew, function(value) {
		        if(value.isChecked)
		          count++;
		      });
		      
		      return count;
		};
		
		vm.totalListedAsset = function() {
		      var count = 0;
		      angular.forEach(vm.assetDataNew, function(value) {
		    	  if(value.from !== "UA"){
		    		  count++;
		    	  }
		      });
		      return count;
		};
		
	    // This executes when entity in table is checked
	    $scope.selectEntity = function () {
	    	// If any entity is not checked, then uncheck the "allItemsSelected" checkbox
	    	for (var i = 0; i < vm.assetDataNew.length; i++) {
	    		if (!vm.assetDataNew[i].isChecked) {
	    			vm.allItemsSelected = false;
	    			return;
	    		}
	    	}
	    	//If not the check the "allItemsSelected" checkbox
	    	vm.allItemsSelected = true;
	    };
	
	    // This executes when checkbox in table header is checked
	    $scope.selectAll = function () {
	    	// Loop through all the entities and set their isChecked property
	    	for (var i = 0; i < vm.assetDataNew.length; i++) {
	    		vm.assetDataNew[i].isChecked = vm.allItemsSelected;
	    	}
	    };
			
		vm.getAssetInfo = function(data){
			vm.asDetails = data;
			vm.assetPhotoFunct(vm.asDetails);
		}
		
		vm.assetToUserIssues= [];
		/*vm.removeAsset = function(index){
			vm.assetDataNew.splice(index, 1);
			vm.asDetails = {};
		}*/
		vm.removeAsset = function(item){
			vm.assetDataNew.splice(vm.assetDataNew.indexOf(item), 1);
			vm.asDetails = {};
			vm.asImg = "";
			vm.assetImg = false;
		}
		vm.disabledIssueBtn = false;
		
		vm.verifyTransaction = function(){
			vm.disabledIssueBtn = true;
			
			//console.log(vm.usDetails,"dhsfhsd")
			if(vm.userInfo && vm.userInfo.userId || vm.usDetails && vm.usDetails.userId){
				vm.assetId = [];
				vm.selectedEle = [];
				vm.assetNameList = [];
				
				//only check asset will be send to server not already issued 
				for(var i=0; i < vm.assetDataNew.length; i++){
					if(vm.assetDataNew[i].isChecked === true && !vm.assetDataNew[i].from){
						vm.requestData= {
							"userId":vm.usDetails.userId,
							"assetId":vm.assetDataNew[i].assetId,
							"category":vm.assetDataNew[i].categoryId,
							"issueDockDoorId":vm.DefaultDockDoorId.dockDoorId,
							"issueBy":AuthService.getUserId(),
							"companyId":vm.globalCompId,
							"issueType":vm.issueType
						}
						vm.assetNameList.push(vm.assetDataNew[i].assetNm)
						vm.assetToUserIssues.push(vm.requestData);
					}
				}
				
				for(var i=0; i < vm.assetDataNew.length; i++){
					if(vm.assetDataNew[i].from){
						vm.assetNameList.push(vm.assetDataNew[i].assetNm)
					}
				};
				
				/*if(vm.allItemsSelected && vm.cntData > 0){
					var j =0 ;
					while(j < vm.cntData){
						vm.assetToUserIssues.splice(vm.assetDataNew[j], 1);
						j++
					}
				}else if(!vm.allItemsSelected && vm.cntData > 0 ){*/
					
				vm.requestParams = {
								"assetToUserIssues":vm.assetToUserIssues,
								"assetNameList" :vm.assetNameList,
								"userName":vm.usDetails.userName,
								"companyName":AuthService.getCompanyName(),
								"emailId":vm.usDetails.emailId,
								"mobileNo":vm.usDetails.mobileno,
								"empId":vm.userID 
							}
				
				if(vm.assetToUserIssues.length){
					IssueReceiveService.addAssetToUser(vm.requestParams, vm.assetTouserSucc, vm.assetTouserErr);
				}else{
					swal("warning", "Please select asset","warning");
					vm.disabledIssueBtn = false;
				}
			}else{
				swal("warning","Please select user first", "warning");
			}
		}
		
		vm.assetTouserSucc = function(response){
			vm.issueAssignCount = vm.assetToUserIssues.length;
			vm.userName = vm.usDetails.userName;
			if(response.data.resCode === "res0000"){
				/*$interval.cancel(promise);*/
				//vm.showUsrMsg = false;
				vm.printData = response.data.resData.printData;
				$state.reload();
				/*vm.asImg = "";*/
				/*vm.userImg = true;*/
				vm.disabledIssueBtn = false;
				vm.assetDataNew = [];
				vm.assetToUserIssues=[];
				vm.asDetails = {};
				vm.usDetails = {};
				vm.compName = "";
				vm.allItemsSelected =""
				vm.userselected = false;
				
				swal("Success", response.data.resData.issueMessage, "success");
				
				if(vm.printColor === 1){
					CustomService.issuePrintService(vm.printData);
				};
				  
			}
			else{
				vm.assetToUserIssues = [];
				vm.disabledIssueBtn = false;
				/*$state.reload();*/
				swal("Error", response.data.resData, "error");	
			}
		}
		vm.assetTouserErr = function(){
			console.log("Error in asset issue");
		}
		
		vm.issueUserReadTag = function(){
			
			vm.requestParams = {
					"dockDoorId":vm.DefaultDockDoorId.dockDoorId,
				    "companyId":vm.globalCompId,
				    "category":2
			}
			if(vm.usrId === 0 && vm.DefaultDockDoorId){ 
				//vm.showUsrMsg = true;
				IssueReceiveService.issueUserReadTag(vm.requestParams, vm.userReadSucc, vm.userReadErr);
			}
		}
		vm.userReadSucc = function(response){
			//response.data.resData.userId
			if(response.data.resData){
				if(response.data.resData.userId && vm.usrId === 0){
					vm.usrId++;
					vm.userDetails(response.data.resData);
				}
			}
			
			/*vm.showUsrMsg = false;*/
		}
		
		vm.userReadErr = function(){
			console.log("Error in read user");
		}
		
		vm.issueAssetReadTag = function(){
			vm.requestParams = {
					"dockDoorId":vm.DefaultDockDoorId.dockDoorId,
				      "companyId":vm.globalCompId,
			}
			
			IssueReceiveService.issueAssetReadTag(vm.requestParams, vm.assetReadSucc, vm.assetReadErr);
		}
		vm.assetReadSucc = function(response){
			if(response.data.resCode === "res0000"){
				vm.issueType = 'R';
				vm.assetDetails(response.data.resData)
			}
		}
		
		vm.assetReadErr = function(){
			console.log("Error in read user");
		}
		
		/*vm.start = function(){*/
			promise = $interval(function(){
				vm.issueUserReadTag();
				//vm.issueAssetReadTag();
			},1000);
		/*}*/
		
		vm.clearField = function(){
			$state.reload();
		};
		
		//get asset photo called 
		vm.assetPhotoFunct = function(photoData){
			/*console.log("photo called" + JSON.stringify(photoData.photo));*/
			if(photoData.photo){
				vm.requestParamsAs = {
						"companyId" : vm.globalCompId,
						"downloadFrom" : "ASSET",
						"imageId" : photoData.assetId.toString(),
						"imageName" : photoData.photo,
					};
					if(vm.requestParamsAs.imageName){
						AuthService.downloadLogo(vm.requestParamsAs, vm.onAssetImgSuccess, vm.onAssetImgError);
					}
			}else{
				console.log("2");
				vm.asImg = 'resources/images/NoImage.gif';
				vm.assetImg = false;
			}
		};
		
		vm.onAssetImgSuccess = function(response){
			if(response.data.resCode === "res0000"){
				if(response.data.resData){
					vm.assetImg = true;
					vm.asImg = response.data.resData.image;
				}
			}else{
				vm.assetImg = false;
			};
		};
		 
		vm.onAssetImgError = function(){
			 
		};
		
		vm.webDockSelectError = false;
		vm.dockDoorReaders = function(){
			vm.requestParams = {  
					"dockDoorId":vm.DefaultDockDoorId.dockDoorId,
					"companyId": vm.globalCompId
			};
			if(vm.DefaultDockDoorId.dockDoorId !== 0){
				ReaderServices.getDockDoorReaders(vm.requestParams, vm.getDockDoorSuccess, vm.getDockDoorError);
			}else{
				vm.webDockSelectError = true;
			}
		};
		
		vm.getDockDoorSuccess = function(response){
			/*console.log(response.data);*/
			if(response.data.resData){
				vm.dockDoorReaders_details = response.data.resData[0].dockDoorDetailsList;
			};
			if(_.any(response.data.resData[0].dockDoorDetailsList, _.matches({"status":"1"}))){
				vm.dockDoorBgColor = true;
			}else{
				vm.dockDoorBgColor = false;
			}
		};

		vm.getDockDoorError = function(response){l
			console.log(response.data);
		};
		
		vm.getUserDetailsField = function(){
			vm.requestParams = {
					"companyId":vm.globalCompId
			}
			IssueReceiveService.getUserDetailFields(vm.requestParams, vm.getUsrDetailSuccess, vm.getUsrDetailError)
		}
		
		vm.getUsrDetailSuccess = function(response){
			vm.userFields = response.data.resData;
		}
		vm.getUsrDetailError = function(){
			console.log("error in get user details field");
		};
		
		vm.printPDF = function(){
			if(vm.printColor === 0){
				vm.printColor = 1;
			}else{
				vm.printColor = 0;
			}
			vm.printOption(vm.printColor);
		};
		
		//Get GR Setting Details ex.expiry days, maintenance days print status
		vm.grsettingDetails = function(){
			IssueReceiveService.getGRSettingDetails(vm.getgrsuccess, vm.getgrerror);
		};
		
		vm.getgrsuccess = function(response){
			if(response.data.resData){
				vm.grDetails = response.data.resData;
				vm.printColor = vm.grDetails.isPrintOptionSelected;
			};
		};

		vm.getgrerror = function(response){
			console.log("error");
		};
		
		//Print status updated like 0 or 1
		vm.printOption = function(printColor){
			vm.requestParams = {
					"printoptionEnable":printColor
			};
			IssueReceiveService.updatePrintOption(vm.requestParams, vm.updatePrintSuccess, vm.updatePrintError);
		};
		
		vm.updatePrintSuccess = function(response){
			console.log("updatePrintSuccess" , response.data.resCode);
		};

		vm.updatePrintError = function(){
			console.log("updatePrintError");
		}
		
		vm.dockDoorReaders();
		vm.getUserDetailsField();
		vm.grsettingDetails();
		
		vm.cancel = function(){
			$state.go('homeDashboard.assetIssueRecieve')
		};
		
		vm.stop = function(){
			 $interval.cancel(promise)
		};
		
		/*$scope.$on('$destroy', function(){
			 if (angular.isDefined(promise)) {
				 $interval.cancel(promise)
			    }
		});*/
		
		$scope.$on('$destroy', function() {
			vm.stop();
		});
		
    }]);
})();

(function() {
    'use strict';
    angular.module('rfidDemoApp.controller')
		.controller('AssetIRstatusCtrl', ['StatusService','$scope','DashboardService','AuthService', 
		                                  function(StatusService, $scope, DashboardService, AuthService){
			var vm = this ;
			vm.selectedType = "issueDetails";
			vm.globalComp_name = AuthService.getCompanyName();
			vm.userName = AuthService.getUserName();
			vm.currDate = new Date();
			 $scope.minDate = new Date();
			
			vm.goFunction = function(){
				switch(vm.selectedType) {
				case "issueDetails":
					vm.issueDetailsFun();
					break;
				case "receiveDetails":
					vm.receiveDetailsFun();
					break;
				case "expiredDetails":
					if(vm.expiredSelect === "expIssueAss"){
						vm.expiredAssetIssue();
					}else{
						vm.expiredAssetReceived();
					}
					break;
				}
			};
			
			$scope.alerts = [];

			/*$scope.addAlert = function() {
				$scope.alerts.push({type: 'danger',msg: 'Another alert!'});
			};*/

			$scope.closeAlert = function(index) {
				$scope.alerts.splice(index, 1);
			};
			
			vm.issueDetailsFun = function(){
				vm.expiredSelect = "expIssueAss";
				vm.startDate = moment(vm.fromDate).format("DD-MMM-YYYY");
				vm.endDate = moment(vm.toDate).format("DD-MMM-YYYY");
				
				vm.startDate1 = moment(vm.fromDate).format("YYYY-MM-DD");
				vm.endDate1 = moment(vm.toDate).format("YYYY-MM-DD");
				vm.dateError = false;
				if(vm.startDate1 <= vm.endDate1){
					vm.requestParams = {
							"startDate":vm.startDate,
							"endDate"  :vm.endDate
					}
					StatusService.issueDetails(vm.requestParams, vm.issueDetailsSuccess, vm.issueDetailsError);
				
					vm.dateError = false;
					$scope.alerts =[];
				}else{
					vm.dateError = true;
					$scope.alerts.push({type: 'danger',msg: 'Start date should be less than end date!'});
				}
				
			};
			
			vm.issueDetailsSuccess = function(response){
				if(response.data.resCode === "res0000"){
					vm.issueDetails = response.data;
					vm.issueDetailsLength = vm.issueDetails.resData.length;
					
					vm.returnYellowDt = new Date();
					vm.returnYellowDt.setDate(vm.returnYellowDt.getDate() + 1);
					vm.returnDate1 = moment(vm.returnYellowDt).format("YYYY-MM-DD");
					
					
					for(var i = 0 ; i < vm.issueDetailsLength; i++){
						var issuedDate = moment(vm.issueDetails.resData[i].issuedDateTime).format("YYYY-MM-DD");
						var returnDate = moment(vm.issueDetails.resData[i].returnDate).format("YYYY-MM-DD");
						
						if(issuedDate === returnDate){
							vm.issueDetails.resData[i].redBgColor = true;
						};
						if(returnDate === vm.returnDate1){
							vm.issueDetails.resData[i].yellowBgColor = true;
						}
					}
					vm.issueDetails = response.data;
				}else{
					vm.issueDetails = "";
				}
			}
			
			vm.issueDetailsError = function(){
				console.log("error");
			}
			
			vm.receiveDetailsFun = function(){
				vm.expiredSelect = "expIssueAss";
				vm.startDate = moment(vm.fromDate).format("DD-MMM-YYYY");
				vm.endDate = moment(vm.toDate).format("DD-MMM-YYYY");
				
				vm.startDate1 = moment(vm.fromDate).format("YYYY-MM-DD");
				vm.endDate1 = moment(vm.toDate).format("YYYY-MM-DD");
				vm.dateError = false;
				if(vm.startDate1 <= vm.endDate1){
					vm.requestParams = {
							"startDate":vm.startDate,
							"endDate"  :vm.endDate
					}
					StatusService.receiveDetails(vm.requestParams, vm.receiveDetailsSuccess, vm.receiveDetailsError);
					
					vm.dateError = false;
					$scope.alerts =[];
				}else{
					vm.dateError = true;
					$scope.alerts.push({type: 'danger',msg: 'Start date should be less than end date!'});
				}
			
			};
			
			vm.receiveDetailsSuccess = function(response){
				if(response.data.resCode === "res0000"){
					vm.issueDetails = response.data;
					vm.issueDetailsLength = vm.issueDetails.resData.length;
				
					for(var i = 0 ; i < vm.issueDetailsLength; i++){
						var receiveDate = moment(vm.issueDetails.resData[i].receiveDateTime).format("YYYY-MM-DD");
						var returnDate = moment(vm.issueDetails.resData[i].returnDate).format("YYYY-MM-DD");
					
						if(receiveDate > returnDate){
							vm.issueDetails.resData[i].blueBgColor = true;
						};
					}
					vm.issueDetails = response.data;
				}else{
					vm.issueDetails = "";
				}
			};
			
			vm.receiveDetailsError = function(){
				
			};
			
			vm.expiredDetailsFun = function(){
				vm.expiredSelect = "expIssueAss";
			};
			
			vm.expiredAssetIssue = function(){
				vm.startDate = moment(vm.fromDate).format("DD-MMM-YYYY");
				vm.endDate = moment(vm.toDate).format("DD-MMM-YYYY");
				
				vm.startDate1 = moment(vm.fromDate).format("YYYY-MM-DD");
				vm.endDate1 = moment(vm.toDate).format("YYYY-MM-DD");
				vm.dateError = false;
				if(vm.startDate1 <= vm.endDate1){
					vm.requestParams = {
							"startDate":vm.startDate,
							"endDate"  :vm.endDate
					}
					StatusService.expiredAssetIssueDetails(vm.requestParams, vm.expiredDetailsSuccess, vm.expiredDetailsError);
				
					vm.dateError = false;
					$scope.alerts =[];
				}else{
					vm.dateError = true;
					$scope.alerts.push({type: 'danger',msg: 'Start date should be less than end date!'});
				}
				
			};
			
			vm.expiredDetailsSuccess = function(response){
				if(response.data){
					vm.issueDetails = response.data;
					vm.issueDetailsLength = vm.issueDetails.resData.length;
				
					vm.returnYellowDt = new Date();
					vm.returnYellowDt.setDate(vm.returnYellowDt.getDate() + 1);
					vm.returnDate1 = moment(vm.returnYellowDt).format("YYYY-MM-DD");
				
				
					for(var i = 0 ; i < vm.issueDetailsLength; i++){
						var issuedDate = moment(vm.issueDetails.resData[i].issuedDateTime).format("YYYY-MM-DD");
						var returnDate = moment(vm.issueDetails.resData[i].returnDate).format("YYYY-MM-DD");
					
						if(issuedDate === returnDate){
							vm.issueDetails.resData[i].redBgColor = true;
						};
						if(returnDate === vm.returnDate1){
							vm.issueDetails.resData[i].yellowBgColor = true;
						}
					}
					vm.issueDetails = response.data;
				}
			}
			
			vm.expiredDetailsError = function(){
				
			};
			
			vm.expiredAssetReceived = function(){
				vm.startDate = moment(vm.fromDate).format("DD-MMM-YYYY");
				vm.endDate = moment(vm.toDate).format("DD-MMM-YYYY");
				
				vm.startDate1 = moment(vm.fromDate).format("YYYY-MM-DD");
				vm.endDate1 = moment(vm.toDate).format("YYYY-MM-DD");
				vm.dateError = false;
				if(vm.startDate1 <= vm.endDate1){
					vm.requestParams = {
							"startDate":vm.startDate,
							"endDate"  :vm.endDate
					}
					StatusService.expiredAssetReceiveDetails(vm.requestParams, vm.expAssetReceieveSuccess, vm.expAssetReceieveError);
				
					vm.dateError = false;
					$scope.alerts =[];
				}else{
					vm.dateError = true;
					$scope.alerts.push({type: 'danger',msg: 'Start date should be less than end date!'});
				}
			}
			
			 vm.expAssetReceieveSuccess = function(response){
				 if(response.data.resData){
						vm.issueDetails = response.data;
						vm.issueDetailsLength = vm.issueDetails.resData.length;
					
						for(var i = 0 ; i < vm.issueDetailsLength; i++){
							var receiveDate = moment(vm.issueDetails.resData[i].receiveDateTime).format("YYYY-MM-DD");
							var returnDate = moment(vm.issueDetails.resData[i].returnDate).format("YYYY-MM-DD");
						
							if(receiveDate > returnDate){
								vm.issueDetails.resData[i].blueBgColor = true;
							};
						}
						vm.issueDetails = response.data;
					}
			 };
			 
			 vm.expAssetReceieveError = function(){
				 
			 }
			
			vm.exportToExcel=function(tableId){ // ex: '#my-table'
	            $scope.exportHref = DashboardService.tableToExcel(tableId,'ssst');

	            /*$timeout(function(){location.href=$scope.exportHref;},100);*/ // trigger download
	            
	            var a = document.createElement('a');
	            a.href = $scope.exportHref;
	            a.download = vm.selectedType + '.xls';
	            a.click();
	        };
			
	        var newStartDt = "2017-02-15" ;
	        vm.startDtChange = function(){
	        	console.log("startDtChange" + vm.fromDate)
	        	newStartDt = moment(vm.fromDate).format("YYYY-MM-DD");
	        	console.log(newStartDt);
	        	
	        }
	        
			/*========ANGULAR DATE PICKER============*/
		    vm.today = function() {
		    	vm.fromDate = new Date();
		    	vm.toDate = new Date();
		    };
		    vm.today();

		    vm.clear = function() {
		    	vm.fromDate = null;
				vm.toDate = null;
			};

			vm.dateOptions = {
					/*dateDisabled: disabled,*/
					formatYear: 'yy',
					maxDate: new Date(2020, 5, 22),
					minDate: new Date("2000-5-25"),
					startingDay: 1
			};
			
			
			// Disable weekend selection
			function disabled(data) {
				var date = data.date,
				mode = data.mode;
				return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			}

			vm.fromOpen = function(){
				vm.from.opened = true;
			};
		  
			vm.toOpen = function(){
				/*$scope.minDate = vm.fromDate;*/
				vm.to.opened = true;  
			};
		  
			vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			vm.format = vm.formats[0];
			vm.altInputFormats = ['M!/d!/yyyy'];
			
			vm.from = {
					opened: false
			};
			vm.to = {
					opened: false
			};
			
			function getDayClass(data) {
				var date = data.date,
				mode = data.mode;
				if (mode === 'day') {
					var dayToCheck = new Date(date).setHours(0,0,0,0);
					
					for (var i = 0; i < $scope.events.length; i++) {
						var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
						
						if (dayToCheck === currentDay) {
							return $scope.events[i].status;
						}
					}
				}
			
				return '';
			}
			
			vm.issueDetailsFun();
			
		}]);
})();
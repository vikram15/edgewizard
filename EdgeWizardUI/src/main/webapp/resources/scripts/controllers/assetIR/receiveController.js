(function() {
    'use strict';
    angular.module('rfidDemoApp.controller')
		.controller('ReceiveController', ['$uibModal' ,'$rootScope','AuthService','USERBGIMG','UserService','$state', 'IssueReceiveService','AssetService','$scope', '$interval', '$cookies','ReaderServices','CustomService', 
		                                function($uibModal, $rootScope, AuthService, USERBGIMG, UserService, $state, IssueReceiveService, AssetService, $scope, $interval, $cookies, ReaderServices, CustomService) {
       
			var vm = this;
	        vm.userImg = false;
	        vm.usrId = 0 ;
	        vm.customDt = "";
	        
	        var golObj = $cookies.get("globals")
	        if(golObj){
	        	vm.globalCompId = AuthService.getCompanyId();
		        vm.globalCompName = AuthService.getCompanyName();
		        vm.DefaultDockDoor = JSON.parse($cookies.get("globals"));
				vm.userID = AuthService.getUserId();
				vm.dockDoorName = AuthService.getDockDoorName();
				vm.userName = AuthService.getUserName();
			}
	        
	        vm.assetDataNew = [];
	        var promise;
	        vm.tableAssData = {};
	        vm.showUsrMsg = true;
	        
	        //print var
	    	var assetNameList;
			var serNo;
			var temp = [];

	        /* user info from user select */
			$rootScope.$on("userInfo", function(evt, data) {
				vm.userInfo = JSON.parse(data);
				vm.usDetails = vm.userInfo;
				vm.compName = vm.globalCompName;
				/*console.log(vm.userInfo);*/
				vm.userDetails(vm.userInfo)
				
			});
			
			/*$rootScope.$on("assetInfo", function(evt, data) {
				vm.assetInfo = JSON.parse(data);
				vm.asDetails = vm.assetInfo;
				vm.compName = vm.globalCompName;
				console.log(vm.assetInfo);
				vm.assetDetails(vm.assetInfo)
				
			});*/
			
			vm.userDetails = function(usrInfo){
				/*console.log(usrInfo);*/
				if(usrInfo.userId){
					vm.requestParams = {
							"userId" :usrInfo.userId,
							"companyId":vm.globalCompId
					};
					UserService.getUserDetails(vm.requestParams, vm.getUserDetSuccess, vm.getUserDetError);
				}
			};
				
			vm.getUserDetSuccess = function(response){ 
				if(response.data.resData !== null){
					vm.showUsrMsg = false;
					vm.showAssMsg = true;
					vm.getUserIssuedAsset(response.data.resData[0].userId)
					vm.userselected = true;
					
					vm.compName = vm.globalCompName;
					vm.usDetails = Object.assign(response.data.resData[0],response.data.resData[1]);
					if(response.data.resData[0].userPhoto){ 
						vm.requestParams = {  
								"companyId":response.data.resData[0].companyId,
								"downloadFrom":USERBGIMG,
								"imageId":response.data.resData[0].userId,
								"imageName":response.data.resData[0].userPhoto
						}; 
						AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
					}else{
					}
				}
			};
					  
			vm.getUserDetError = function(){
						  
			};
			
			vm.assetDetails = function(assetInfo){
				if(assetInfo){
					for(var i=0; i <  assetInfo.length; i++){
						vm.requestParamsAss = {
								"assetId":assetInfo[i].assetId || assetInfo[i],
								"companyId": vm.globalCompId
						};
						/*AssetService.assetGetByID(vm.requestParams, vm.assSuccessBlock, vm.assErrorBlock)*/
						AssetService.getAssetIssueReceive(vm.requestParamsAss, vm.assSuccessBlock, vm.assErrorBlock);
					}
				}
			}
			
			vm.sampleArr = [];
			vm.assSuccessBlock = function(response){
				vm.assetData = Object.assign(response.data.resData[0], response.data.resData[1], response.data.resData[2]);
				vm.sampleArr.push(vm.tableAssData);
				vm.bgColorId = vm.assetData.assetId; 
				for (var i = 0; i < vm.assetDataNew.length; i++) {
	            	if(vm.assetDataNew[i].assetId === vm.assetData.assetId){
	            		vm.asDetails = vm.assetData;
	            		vm.assetDataNew[i].isChecked = true;
	            		vm.allItemsSelected = true;
	            		vm.assetPhotoFunct(vm.assetData);
	            	}
	            }
			};
			
			vm.assErrorBlock = function(){
				
			};
			
			vm.onGetLogoSuccess = function(response){
				if(response.data.resCode === "res0000"){
					if(response.data.resData){
						vm.userImg = true;
						vm.usrImg = response.data.resData.image;
					}
				}else{
					vm.userImg = false;
				};
			};

			vm.onGetLogoError = function(){
				
			};
			
			vm.userDetailsDailogue = function(){
				var modalInstance = $uibModal
				.open({
					backdrop : 'static',
					animation : vm.animationsEnabled,
					templateUrl : 'resources/views/user/userSearch.html',
					controller : 'UserSearchController',
					controllerAs : 'userSearchController',
					/*
					 * resolve: { jw: function () {
					 * return angular.copy(d); } }
					 */
				});
				modalInstance.result.then(function(selectedItem) {
					
				}, function(t) {
				});
			};
			
			vm.assetSearchDailogue = function(){
				var modalInstance = $uibModal
				.open({
					backdrop : 'static',
					animation : vm.animationsEnabled,
					templateUrl : 'resources/views/asset/assetIRSearch.html',
					controller : 'AssetIRSearchController',
					controllerAs : 'assetIRSearchController',
					/*
					 * resolve: { jw: function () {
					 * return angular.copy(d); } }
					 */
				});
				modalInstance.result.then(function(selectedItem) {
					
				}, function(t) {
				});
			};
			
			vm.getUserIssuedAsset = function(userId){
				vm.requestParams = {
						"userId":userId,
						"companyId":vm.globalCompId
				}
				UserService.getUserIssuedAsset(vm.requestParams, vm.issueAssetSucc, vm.issueAssetErr);
			};
			
			vm.issueAssetSucc = function(response){
				/*console.log("gggggg");*/
				if(response.data.resCode === "res0000"){
					//vm.showUsrMsg = false;
					if(Array.isArray(response.data.resData)){
						vm.cntData = response.data.resData.length;
						vm.asDetails = response.data.resData[response.data.resData.length - 1]
						vm.assetDataNew = response.data.resData;
						for(var i=0; i < vm.assetDataNew.length ; i++){
							/*alert(vm.assetDataNew[i].assetIssueRF);*/
							if(vm.assetDataNew[i].assetIssueRF === "Default"){
								vm.assetDataNew[i].customDt = vm.assetDataNew[i].assetIssueDate;
								vm.assetDataNew[i].issueType = "M";
								/*vm.customDt = response.data.resData[i].assetIssueDate;*/
							}else{
								vm.assetDataNew[i].issueType = "R";
								vm.assetDataNew[i].customDt = vm.assetDataNew[i].assetIssueRF;
							}
						};
						
						
						vm.assetPhotoFunct(vm.asDetails);
					}
					else{
						vm.asImg = '';
						vm.assetImg = false;
						vm.asDetails={};
						vm.cntData = 0;
						vm.assetDataNew = [];
					}
				}
			};
			
			vm.getAssetInfo = function(data){
				vm.asDetails = data;
				vm.assetPhotoFunct(vm.asDetails);
			};
			
			vm.onAssetImgSuccess = function(response){
				if(response.data.resCode === "res0000"){
					if(response.data.resData){
						vm.assetImg = true;
						vm.asImg = response.data.resData.image;
					}
				}else{
					vm.assetImg = false;
				};
			};
			 
			vm.onAssetImgError = function(){
				 
			};
			
			vm.removeAsset = function(index){
				vm.assetDataNew.splice(index, 1);
			}

			vm.allItemsSelected = false;
	        // This executes when entity in table is checked
	        $scope.selectEntity = function () {
	            // If any entity is not checked, then uncheck the "allItemsSelected" checkbox
	        	for (var i = 0; i < vm.assetDataNew.length; i++) {
	        		if (!vm.assetDataNew[i].isChecked) {
	        			//console.log(vm.assetDataNew, "555555");
	        			vm.allItemsSelected = false;
	        			return;
	                }
	            }
	            //If not the check the "allItemsSelected" checkbox
	        	vm.allItemsSelected = true;
	        };
	
	        // This executes when checkbox in table header is checked
	        $scope.selectAll = function (){
	            // Loop through all the entities and set their isChecked property
	            for (var i = 0; i < vm.assetDataNew.length; i++) {
	            	vm.assetDataNew[i].isChecked = vm.allItemsSelected;
	            }
	        };
			
			vm.verifyTransaction = function(){
				vm.disabledIssueBtn = true;
				if(vm.userInfo && vm.userInfo.userId || vm.usDetails && vm.usDetails.userId){
					vm.assetId = [];
					vm.assetNameList = [];
					vm.selectedEle = [];
					
					for ( vm.property in vm.selected ) {
						if(vm.selected[vm.property] == true){
							vm.selectedEle.push(parseInt(vm.property)) ;
						}
					}
					
					for(var i=0; i < vm.assetDataNew.length; i++){
						if(vm.assetDataNew[i].isChecked === true){
							vm.assetId.push(vm.assetDataNew[i].assetId);
							vm.assetNameList.push(vm.assetDataNew[i].assetNm)
							
						}
						/*if(_.contains(vm.selectedEle,i)){
							vm.assetId.push(vm.assetDataNew[i].assetId)
						}*/
					}
					
					/*vm.requestParams = {
							"userId":vm.usDetails.userId,
							"assetId":vm.assetId,
							"companyId":vm.globalCompId
					}*/
					
					vm.requestParams = {
						"userId":vm.usDetails.userId,
						"userName":vm.usDetails.userName,
						"companyId":vm.globalCompId,
						"emailId":vm.usDetails.emailId,
						"issueBy":vm.userID,
						"receiveDockDoor":vm.DefaultDockDoor.dockDoorId,
						"assetNameList":vm.assetNameList,
						"assetIdList":vm.assetId
					}
					
					if(vm.assetId.length){
						IssueReceiveService.receiveAssetFromUser(vm.requestParams, vm.assetTouserSucc, vm.assetTouserErr);
					}
					else{
						vm.disabledIssueBtn = false;
						swal("warning", "Please select atleast one asset","warning")
					}
    			}else{
    				swal("warning","Please select user first", "warning");
    			}
			}
			
			vm.assetTouserSucc = function(response){
				vm.issueAssignCount = vm.assetId.length;
				vm.userName = vm.usDetails.userName;
				if(response.data.resCode === "res0000"){
					vm.printData = response.data.resData.printData;
					
					swal("Success",response.data.resData.assetReceiveMessage, "success");
					
					if(vm.printColor === 1){
						CustomService.receivePrintService(vm.printData);
					};
					$state.reload();
					vm.disabledIssueBtn = false;
					vm.assetImg = false;
					vm.usrImg = "";
					vm.asImg = "";
					/*swal("Success", vm.issueAssignCount+" "+ vm.userName+ " "+response.data.resData, "success");*/
					vm.assetDataNew = [];
					vm.asDetails = {};
					vm.usDetails = {};
					vm.compName = "";
					vm.allItemsSelected = "";
					vm.userselected = false;
				}
				else{
					vm.disabledIssueBtn = false;
				}
			}
			vm.assetTouserErr = function(){
				vm.disabledIssueBtn = false;
			}
			
			//get asset photo called 
			vm.assetPhotoFunct = function(data){
				if(data){
					vm.requestParamsAs = {
							"companyId" : vm.globalCompId,
							"downloadFrom" : "ASSET",
							"imageId" : data.assetId.toString(),
							"imageName" : data.photo,
						};
						if(vm.requestParamsAs.imageName){
							AuthService.downloadLogo(vm.requestParamsAs, vm.onAssetImgSuccess, vm.onAssetImgError);
						}
				}else{
					vm.asImg = 'resources/images/NoImage.gif';
					vm.assetImg = false;
				}
			};
			
			vm.issueUserReadTag = function(){
				vm.requestParams = {
						"dockDoorId":vm.DefaultDockDoor.dockDoorId,
					      "companyId":vm.globalCompId,
					         "category":2
				}
				if(vm.usrId === 0){
					IssueReceiveService.issueUserReadTag(vm.requestParams, vm.userReadSucc, vm.userReadErr);
				}
			}
			vm.userReadSucc = function(response){
				/*console.log(response.data.resData,"FFFF");*/
				//response.data.resData.userId
				/*if(response.data.resData){
					vm.showUsrMsg = false;
					vm.userDetails(response.data.resData);
					$interval.cancel(promise);
				}*/
				if(response.data.resData){
					if(response.data.resData.userId && vm.usrId === 0){
						vm.showUsrMsg = false;
						vm.usrId++;
						vm.userDetails(response.data.resData);
					}
				}
				
			};
			
			vm.userReadErr = function(){
				console.log("Error in read user");
			};
			
			vm.readAssetTags = function(asset){
				vm.tableAssData = asset;
				 promise =  $interval(function(){
					 vm.issueAssetReadTag();
				 },1000);
			};
			
			vm.issueAssetReadTag = function(){
				vm.requestParams = {
						"dockDoorId":vm.DefaultDockDoor.dockDoorId,
					      "companyId":vm.globalCompId,
				}
				IssueReceiveService.issueAssetReadTag(vm.requestParams, vm.assetReadSucc, vm.assetReadErr);
			}
			vm.assetReadSucc = function(response){
				if(response.data.resData){
					vm.assetDetails(response.data.resData)
				};
			};
			
			vm.assetReadErr = function(){
				console.log("Error in read user");
			};
			
			promise = $interval(function(){
				vm.issueUserReadTag();
			},1000);
			
			vm.dockDoorReaders = function(){
				vm.requestParams = {  
						"dockDoorId":vm.DefaultDockDoor.dockDoorId,
						"companyId": vm.globalCompId
				};
				ReaderServices.getDockDoorReaders(vm.requestParams, vm.getDockDoorSuccess, vm.getDockDoorError);
			};
			
			vm.getDockDoorSuccess = function(response){
				if(response.data.resData){
					vm.dockDoorReaders_details = response.data.resData[0].dockDoorDetailsList;
				};
				if(_.any(response.data.resData[0].dockDoorDetailsList, _.matches({"status":"1"}))){
					vm.dockDoorBgColor = true;
				}else{
					vm.dockDoorBgColor = false;
				}
			};

			vm.getDockDoorError = function(response){
				
			};
			
			vm.getUserDetailsField = function(){
				vm.requestParams = {
						"companyId":vm.globalCompId
				}
				IssueReceiveService.getUserDetailFields(vm.requestParams, vm.getUsrDetailSuccess, vm.getUsrDetailError)
			}
			
			vm.getUsrDetailSuccess = function(response){
				vm.userFields = response.data.resData;
			}
			vm.getUsrDetailError = function(){
				console.log("error in get user details field");
			}
			
			//Get GR Setting Details ex.expiry days, maintenance days print status
			vm.grsettingDetails = function(){
				IssueReceiveService.getGRSettingDetails(vm.getgrsuccess, vm.getgrerror);
			};
			
			vm.getgrsuccess = function(response){
				if(response.data.resData){
					vm.grDetails = response.data.resData;
					vm.printColor = vm.grDetails.isPrintOptionSelected;
				};
			};

			vm.getgrerror = function(response){
				console.log("error");
			};
			
			//Print status updated like 0 or 1
			vm.printOption = function(printColor){
				vm.requestParams = {
						"printoptionEnable":printColor
				};
				IssueReceiveService.updatePrintOption(vm.requestParams, vm.updatePrintSuccess, vm.updatePrintError);
			};
			
			vm.updatePrintSuccess = function(){
				console.log("updatePrintSuccess" , response.data.resCode);
			};

			vm.updatePrintError = function(){
				console.log("updatePrintError");
			}
			
			//Click on Print icon
			vm.printPDF = function(){
				if(vm.printColor === 0){
					vm.printColor = 1;
				}else{
					vm.printColor = 0;
				}
				vm.printOption(vm.printColor);
			};
			
			vm.dockDoorReaders();
			vm.getUserDetailsField();
			vm.grsettingDetails();
			
			vm.clearField = function(){
				$state.reload();
			}
			
			vm.cancel = function(){
				$state.go('homeDashboard.assetIssueRecieve')
			};
			
			vm.stop = function(){
				 $interval.cancel(promise)
			};
			
			$scope.$on('$destroy', function() {
			      vm.stop();
			      vm.issueUserReadTag();
			      vm.issueAssetReadTag();
			      vm.issueAssetReadTag();
			});
    }]);
})();

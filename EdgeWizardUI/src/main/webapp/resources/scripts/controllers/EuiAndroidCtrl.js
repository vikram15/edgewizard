(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('AndroidCtrl',['$scope','$stateParams', '$state','AuthService','AssetService', function($scope, $stateParams, $state, AuthService, AssetService){
		console.log("AndroidCtrl");
		var vm = this;
		$scope.isReqFrom = 'AND';
		
		vm.checkingTag = function(epcId){
			vm.requestParams = {
					'tagId' : epcId
			}
			if(epcId !== '0'){
				AssetService.assetGetByTagId(vm.requestParams, vm.viewSingleAssetSucc, vm.viewSingleAssetError);
			}
		};
		
		vm.viewSingleAssetSucc = function(response){
			var result = response.data.resData;
			if(result){
				loginSave($stateParams)
			}else{
				$state.go('error');
			}
		};
		
		vm.viewSingleAssetError = function(response){};
		
		//login method called
		function loginSave(stateData) {
			if($state.current.name === "euiAndroid" && $scope.isReqFrom === "AND"){
				$scope.requestParams = {
						"userId" : stateData.un,
						"password" : stateData.ps,
						"companyId" : stateData.cmpId,
						"solutionName" : stateData.sysname
				};
			}
			AuthService.loginService($scope.requestParams, $scope.onLoginSaveSuccess, $scope.onLoginSaveError);
		};
		
		$scope.onLoginSaveSuccess = function(response) {
			$scope.loginInfo = response.data;
			
			angular.forEach($scope.loginInfo.resData, function(value) {
				var loginInfoObj = value;
				AuthService.storeLoginInformation(loginInfoObj, $scope.isReqFrom);
			});
			
			if (response.data.resCode === "res0000") {
				if($scope.isReqFrom === "AND"){
					if($stateParams.activity === "AU"){
						$state.go('homeDashboard.asset.assetAddUpdateByTagId',{
							'tagId' : $stateParams.epcId,
							'activity' : $stateParams.activity
						})
					}
					//$state.go('login');
				}
			}else {
				if($scope.isReqFrom === "AND"){
					$state.go('login');
				};
			}
		};
	
		$scope.onLoginSaveError = function() {
			console.log("Error");
		};
		
		var isValid =_.isEmpty($stateParams);
		if(!isValid){
			//check tag already assigned to user
			vm.checkingTag($stateParams.epcId);
		}else{
			$state.go('login');
		}
		//=============
		
	}]);
})();
(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('LoginController',['$scope', 'AuthService', '$http', '$state', 'COMP_LIST', 'COMP_LIST_op0001', '$timeout', '$rootScope','DashboardService','_', '$stateParams',
	                               function($scope, AuthService, $http, $state, COMP_LIST, COMP_LIST_op0001, $timeout, $rootScope, DashboardService, _, $stateParams) {

		var vm = this;
		$scope.isReqFrom = 'WEB';

		$scope.alerts = [];
		$rootScope.pageTitle = "Login | EdgeWizard";
		
		$scope.closeAlert = function(index) {
			$scope.alerts.splice(index, 1);
		};

		$scope.licFileData = function() {
			AuthService.licFileDetails($scope.licFileDetailsSucc, $scope.licFileDetailsErr);
		};
		
		$scope.licFileDetailsSucc = function(response) {
			$scope.licFile_data = response.data;
			$scope.licCompanyName = $scope.licFile_data.resData;
			$scope.systemName = $scope.licFile_data.resData.SolutionList[0].solutionCode;
		};
		
		$scope.licFileDetailsErr = function(response) {
			console.log("error block");
		};
		
		$scope.load = function() {
			var requestParams = "";
			AuthService.companyList(requestParams, $scope.onCompanyListSuccess, $scope.onCompanyListError);
		};
		
		$scope.onCompanyListSuccess = function(response) {
			$scope.companyInfo = response.data;
			$scope.selectValue = $scope.companyInfo.resData[0].companyId;
			
			//get license data for ex : ATS,VTS or UTS
			$scope.getTtlAssets($scope.selectValue);
		};

		$scope.onCompanyListError = function(response) {
			console.log("error")
		};

		//click on login button
		$scope.loginSave = function(stateData) {
			if($state.current.name === "euiAndroid" && $scope.isReqFrom === "AND"){
				$scope.requestParams = {
						"userId" : stateData.un,
						"password" : stateData.ps,
						"companyId" : stateData.cmpId,
						"solutionName" : stateData.sysname
				};
			}else{
				$scope.requestParams = {
						"userId" : $scope.form.userName,
						"password" : $scope.form.password,
						"companyId" : $scope.selectValue,
						"solutionName" : $scope.systemName
				};
			}
			AuthService.loginService($scope.requestParams, $scope.onLoginSaveSuccess, $scope.onLoginSaveError);
		};
		$scope.onLoginSaveSuccess = function(response) {
			$scope.loginInfo = response.data;
			var requestParam = {
					"languageTypes" : "English"
			};
			AuthService.labelChange(requestParam, $scope.onLabelSuccess, $scope.onLabelError);
			angular.forEach($scope.loginInfo.resData, function(value) {
				var loginInfoObj = value;
				AuthService.storeLoginInformation(loginInfoObj, $scope.isReqFrom);
			});
			if (response.data.resCode === "res0000") {
				if($scope.isReqFrom === "AND"){
					if($stateParams.activity === "AU"){
						$state.go('homeDashboard.asset.assetAddUpdateByTagId',{
							'tagId' : $stateParams.epcId,
							'activity' : $stateParams.activity
						})
					}
					//$state.go('login');
				}else{
					if (AuthService.getSystemName() === "ATS") {
						$state.go('homeDashboard');
					} else if (AuthService.getSystemName() === "VTS") {
						$state.go('pickUpDropDashBoard');
						console.log("VTS Dashboard");
					} else if (AuthService.getSystemName() === "UTS") {
						console.log("UTS Dashboard");
					} else if (AuthService.getSystemName() === "LTS") {
						console.log("LTS Dashboard");
					} else {
						
					}
				}
			} else if (response.data.resCode === "res0100") {
				if($scope.isReqFrom === "AND"){
					$state.go('login');
				};
				
				/*
				 * $scope.errorMsg =
				 * response.data.resMsg;
				 */
				if (response.data.resMsg === "Please Enter Valid User Name") {
					$scope.userNmErr = response.data.resMsg;
					$scope.userPassErr = false;
					$timeout(function(){
						$scope.userNmErr = false;
					},1500);
					return;
				} else if (response.data.resMsg === "Please Enter Valid Password") {
					$scope.userPassErr = response.data.resMsg;
					$scope.userNmErr = false;
					$timeout(function(){
						$scope.userPassErr = false;
					},1500);
					return;
				}
			} else {
				
			}
		};
	
		$scope.onLabelSuccess = function(response) {
			AuthService.storeLabelInformation(response.data.resData);
			// $scope.labelnfo = response.data.resData;
		};

		$scope.onLabelError = function(response) {
			console.log("error")
		};
		
		$scope.onLoginSaveError = function() {
			console.log("Error");
		};
		
		$scope.getTtlAssets = function(compId){
			$scope.requestParams = {
					 "companyId": compId
			};
			DashboardService.getTtlAssetsFrmLicense($scope.requestParams, $scope.getTtlAssetSuccess, $scope.getTtlAssetError);
		};
		$scope.getTtlAssetSuccess = function(response){
			//console.log("getTtlAssetSuccess.."+ JSON.stringify(response.data));
		};
		
		$scope.getTtlAssetError = function(){
			console.log("error");
		};
		
								
		//get company list
		$scope.load();
	
		//get license data for ex : ATS,VTS or UTS
		$scope.licFileData();
		
		if($state.current.name === "euiAndroid"){
			$scope.isReqFrom = 'AND'
			var isValid =_.isEmpty($stateParams);
			if(!isValid){
				$scope.loginSave($stateParams);
			}else{
				$state.go('login');
			}
		};
		
		//get total and used asset from license copy
		//$scope.getTtlAssets();

	}]);
})();

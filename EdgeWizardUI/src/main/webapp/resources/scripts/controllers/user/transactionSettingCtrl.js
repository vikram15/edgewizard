(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('TransactionSettingCtrl',['$scope','$uibModalInstance','$uibModal','UserService','userData','AuthService','_',
	                                      function($scope, $uibModalInstance, $uibModal, UserService, userData, AuthService, _){
		var vm = this;
		vm.tableHeader = "Transaction Setting";
		vm.trPassword = "";
		vm.trConfirmPassword = "";
		var trChangeNo ;
		vm.paidType = "cash";
		vm.userId = AuthService.getUserId();
		vm.userName = AuthService.getUserName();
		vm.passwordRequired = 0;
		
		vm.trChange = function(data){
			trChangeNo = data;
		};
		
		vm.numBtn = function(data){
			if(data !== "clear"){
				if(trChangeNo === 1){
					vm.trPassword +=  data;
				}
				if(trChangeNo === 2){
					vm.trConfirmPassword += data;
				}
			}else{
				vm.trPassword =  "";
				vm.trConfirmPassword = "";
			}
		}
		
		vm.transactionSettingDetails = function(){
			vm.requestParams = {
					"userId": userData.userId
			}
			if(userData.userId){
				UserService.getTransactionSettingDetails(vm.requestParams, vm.getTransactionSettingDetailsSuccess, vm.getTransactionSettingDetailsError);
			}
		};
		
		vm.getTransactionSettingDetailsSuccess = function(response){
			if(response.data.resCode === "res0000"){
				vm.trDetails = response.data.resData;
				vm.currBalance = vm.trDetails.fromAmount;
				vm.passwordRequired = vm.trDetails.posPasswordAmtAcitvity.passwordRequired; 
				vm.paidType = vm.trDetails.paymentType;
				vm.cardNo = vm.trDetails.cardNumber;
				vm.cardHolderName = vm.trDetails.cardHolderName;
				if(vm.trDetails.posPasswordAmtAcitvity.operatedBy === "null"){
					vm.isPassword = false;
					vm.trDetails.posPasswordAmtAcitvity.operatedBy = null;
					vm.trDetails.posPasswordAmtAcitvity.modifiedDate = null;
					if(vm.cardHolderName = vm.trDetails.cardHolderName === "null"){
						vm.cardHolderName = vm.trDetails.cardHolderName = null;
					}
				}else{
					vm.isPassword = true;
				}
				/*if(vm.trDetails.passwordRequired === 1){
					vm.reqTransactionPass = true;
				}*/
			}else{
				console.log(response.data.resMsg);
			}
		};
		
		vm.getTransactionSettingDetailsError = function(response){
			console.log(response + "getTransactionSettingDetailsError");
		};
		
		vm.resetPasswordBtn = function(){
			//userData =  _.extend(userData, vm.passwordRequired);
			userData.passwordRequired = vm.passwordRequired;
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/user/resetPassword.html',
				controller : 'ResetPasswordCtrl',
				controllerAs : 'resetPasswordCtrl',
				resolve : {
					userData : function() {
						return angular.copy(userData);
					}
				}
			});
			
			modalInstance.result.then(function(selectedItem) {
				vm.transactionSettingDetails();
			}, function(baggageData) {
				
			});
		};
		
		vm.saveBtn = function(){
			/*if(vm.reqTransactionPass === false || vm.reqTransactionPass === undefined){
				vm.trPass = 0;
			}else{
				vm.trPass = 1;
			}*/
			
			/*if(vm.selectedValue === false || vm.selectedValue === undefined){
				vm.selectedValue = 0;
			}else{
				vm.selectedValue = 1;
			}*/
			vm.requestParams = {
					"userId":userData.userId,
					"toAmount":vm.toAmount,
					"modifiedBy":vm.userName,
					"modifiedById":vm.userId,
					"creditDate" : moment(new Date()).valueOf(),
					"posPasswordAmtAcitvity":{
						"passwordRequired": vm.passwordRequired,
						"password": vm.trPassword,
					}
			}
			
			if(vm.trPassword !== "null" && vm.trPassword !== null && vm.trPassword !== undefined || vm.passwordRequired === 1){
				/*"operatedBy": vm.userName*/
				vm.requestParams.posPasswordAmtAcitvity.operatedBy = vm.userName,
				vm.requestParams.posPasswordAmtAcitvity.creditDate = moment(new Date()).valueOf();
				vm.requestParams.posPasswordAmtAcitvity.modifiedDate = moment(new Date()).valueOf();
			}
			
			if(vm.paidType === "cash"){
				vm.requestParams.paymentType = vm.paidType;
			}else{
				vm.requestParams.paymentType = vm.paidType;
				vm.requestParams.cardNumber = vm.cardNo;
				vm.requestParams.cardHolderName = vm.cardHolderName;
			}
			
			$uibModalInstance.dismiss(vm.requestParams);
			//UserService.userTransactionSetting(vm.requestParams, vm.userTransactionSetSuccess, vm.userTransactionSetError)
		};
		
		vm.userTransactionSetSuccess = function(response){
			if(response.data.resCode === 'res0000'){
				 swal("Successfully", "Transaction saved successfully!", "success");
				 $uibModalInstance.dismiss();
			 }
			 if(response.data.resCode === 'res0100'){
				 console.log(response.data.resMsg);
			 }
		};
		
		vm.userTransactionSetError = function(response){
			console.log(response.data);
		};
		
		vm.cancel = function() {
			$uibModalInstance.close();
		};
		
		vm.transactionSettingDetails();
	}])
	
	//User recharge controller
	.controller('UserRechargeCtrl',['$uibModalInstance','UserService','userData','AuthService', function($uibModalInstance, UserService, userData, AuthService){
		var vm = this;
		vm.paidType = "cash";
		vm.userId = AuthService.getUserId();
		vm.userName = AuthService.getUserName();
		
		vm.saveBtn = function(){
			vm.requestParams = {
//					"userId":userData.userId,
					"toAmount":vm.toAmount,
					"modifiedBy":vm.userName,
					"modifiedById":vm.userId,
					"posPasswordAmtAcitvity":{
						"passwordRequired": vm.passwordRequired,  // 0 means not enable checkbox
						"password": vm.trPassword,
					}
			}
			if(vm.paidType === "cash"){
				vm.requestParams.paymentType = vm.paidType;
			}else{
				vm.requestParams.paymentType = vm.paidType;
				vm.requestParams.cardNumber = vm.cardNo;
				vm.requestParams.cardHolderName = vm.cardHolderName;
			}
			$uibModalInstance.dismiss(vm.requestParams);
		};
		
		vm.transactionSettingDetails = function(){
			vm.requestParams = {
					"userId": userData.userId
			}
			if(userData.userId){
				UserService.getTransactionSettingDetails(vm.requestParams, vm.getTransactionSettingDetailsSuccess, vm.getTransactionSettingDetailsError);
			}
		};
		
		vm.getTransactionSettingDetailsSuccess = function(response){
			if(response.data.resCode === "res0000"){
				vm.trDetails = response.data.resData;
				vm.currBalance = vm.trDetails.fromAmount;
			}
		};
		
		vm.getTransactionSettingDetailsError = function(response){
			console.log(response + "getTransactionSettingDetailsError");
		};
		
		
		
		vm.cancel = function() {
			$uibModalInstance.close();
		};
		vm.transactionSettingDetails();
	}]);
})();
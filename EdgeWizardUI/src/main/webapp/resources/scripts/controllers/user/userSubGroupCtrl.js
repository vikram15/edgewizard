(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('UserSubGroupCtrl',['$uibModalInstance','userData','$uibModal','_','subUsersList','AuthService','$cookies','UserService','MasterService','$timeout','userReadTag', 
	                                function($uibModalInstance, userData, $uibModal, _, subUsersList, AuthService, $cookies, UserService, MasterService, $timeout, userReadTag){
		var vm = this ;
		vm.usersTempData = [];
		vm.expenseType = "D";
		vm.userData = userData;
		vm.companyId = AuthService.getCompanyId();
		vm.dockDoorId = JSON.parse($cookies.get("globals")).dockDoorId;
		
		if(vm.userData.userId){
			vm.isDisabled = true;
		}else{
			vm.isDisabled = false;
		}
		
		if(subUsersList !== undefined && subUsersList !== null){
			vm.usersTempData = subUsersList;
		}else{
			vm.usersTempData = [];
		}
		
		vm.saveTemp = function(){
			if(!_.any(vm.usersTempData, _.matches({"epcID":vm.subGroupData.epcID}))){
				vm.usersTempData.push({
					"name" : vm.subGroupData.name,
					"gender" : vm.subGroupData.gender,
					"epcID" : vm.subGroupData.epcID,
					"categoryType" : vm.subGroupData.categoryType,
					"subUserCategoryId" : vm.subGroupData.categoryType.categoryPrimery.categoryId
				})
				vm.epcIdcheck = false;
				vm.subGroupData.name =  "";
				vm.subGroupData.epcID = "";
			}else{
				vm.epcIdcheck = true;
				$timeout(function(){
					vm.epcIdcheck = false;
				},2000)
			}
			
		};
		
		vm.addUserSubGroup = function(){
			var newArr = _.map(vm.usersTempData, function(o) { return _.omit(o, 'categoryType'); });
			
			vm.myObject = {
				"expenseType" : vm.expenseType,
				"subUserData" : newArr
			}
			$uibModalInstance.dismiss(vm.myObject);
		};
		
		vm.deleteUser = function(item){
			vm.usersTempData.splice(vm.usersTempData.indexOf(item), 1);
		};
		
		//tag select function
		  vm.tagSelectFun = function() {
				var modalInstance = $uibModal
				.open({
					backdrop : 'static',
					animation : vm.animationsEnabled,
					templateUrl : 'resources/views/tag/tagSearch.html',
					controller : 'TagSearchController',
					controllerAs : 'tagSearchController',
					resolve : {
						tagSelect : function() {
							return angular.copy(vm.userData.userTagId);
						}
					}
				});
				
				modalInstance.result.then(function(selectedItem) {
				}, function(data) {
					if(data){
						vm.tagInfo = JSON.parse(data);
						vm.subGroupData.epcID = vm.tagInfo.tagId;
					}
				});	
		  };
		  	
		  vm.readTag = function(){
			  vm.requestParams = {
					  "dockDoorId": vm.dockDoorId,
					  "companyId" : vm.companyId
			  };
			  UserService.getTagsByReaders(vm.requestParams, vm.onSuccessGetTags, vm.onErrorGetTags);
		  };
			 
		  vm.onSuccessGetTags = function(response){
			  if(response.data.resCode === "res0000"){
				  if(userReadTag !== null){
					  if(userReadTag === response.data.resData){
						  swal("Error" ,"Tag is already assigned" ,"error")
					  }else{
						  vm.subGroupData.epcID = response.data.resData;	
					  }
				  }else{
					  vm.subGroupData.epcID = response.data.resData;
				  }
			  }else{
				  swal("Error" ,response.data.resMsg ,"error")
			  }
		  };	
			 
		  vm.onErrorGetTags = function(response){
			  
		  };
		  
		  vm.categoryList = function() {
			  vm.requestParams = {
					  "categoryPrimery" : {
						  "category" : 2, // category list by user
						  "companyId" : vm.companyId
					  }
			  };
			  MasterService.getCategoryAll(vm.requestParams,vm.getCategoryAllSucc,vm.getCategoryAllError)
		  };
			 
		  vm.getCategoryAllSucc = function(response) {
			  vm.category_list = response.data;
			  vm.subGroupData.categoryType = vm.category_list.resData[1];
		  };
				
		  vm.getCategoryAllError = function(response) {
			  console.log("get CategoryAll Error");
		  };
		
		  vm.cancel = function(){
			  $uibModalInstance.close()	
		  };
		  
		  vm.categoryList();
	}]);
})();
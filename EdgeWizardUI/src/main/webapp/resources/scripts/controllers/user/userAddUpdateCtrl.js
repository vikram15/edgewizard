(function(){
  'use strict';
  angular.module('rfidDemoApp.controller')
  .controller('UserAddUpdateCtrl',['$stateParams','UserService','AuthService','MasterService', 'AssetService','$state','$uibModal','$rootScope','$timeout','PosService','$cookies','$scope','DTFORMAT','_','$interval', 
                                   function($stateParams, UserService, AuthService, MasterService, AssetService, $state, $uibModal, $rootScope, $timeout, PosService, $cookies, $scope, DTFORMAT, _, $interval){
	 var vm = this;
	 vm.userFrmData = {};
	 vm.tableHeader = "User Registration";
	 vm.userFrmData.gender = "Male";
	 vm.assetPostMode = "";
	 vm.userFrmData.companyId = AuthService.getCompanyId();
	 // vm.userFrmData.loginId = AuthService.getUserName();
	 vm.comp_name = AuthService.getCompanyName();
	 vm.submitBtnNm = "NEXT";
	 vm.baggageData = [];
	 vm.userAttachedFileNameData = [];
	 vm.userAttachedMultipartFileData = [];
	 vm.dockDoorId = JSON.parse($cookies.get("globals")).dockDoorId;
	 //for pickdrop
	 vm.loginType = AuthService.getSystemName();
	 vm.userFrmData.isUserPrimary = 'YES';
	 vm.userFrmData.actDate = new Date();
	 vm.userFrmData.deactDate = new Date();
	 vm.format = DTFORMAT;
	 vm.loc_data = [];
		 
	 vm.initialize = function() {
		 
		 
		 
		 vm.userId = $stateParams.userId;
		 if (vm.userId === null || vm.userId === '' || vm.userId === undefined) {
			 vm.userMode = 'NewMode';
			 vm.tableHeader = "User Registration";
			 $rootScope.pageTitle = "User | EdgeWizard";
			 vm.usrBtn = true;
			 
		 } else {
			 vm.userMode = 'EditMode';
			 vm.tableHeader = "User Update";
			 $rootScope.pageTitle = "User | EdgeWizard";
			 vm.usrBtn = false;
			 /*vm.getDivisionDetails();*/
			 vm.requestParams = {
					 'userId' : vm.userId,
					 'companyId' : vm.userFrmData.companyId
			 }
			 UserService.getUserDetails(vm.requestParams, vm.getUserDetailsSuccess, vm.getUserDetailsError);
		 }
	 };
	 
	 var locCount = 0;
	 vm.getUserDetailsSuccess = function(response){
		 if(response.data.resData){
			 vm.userFrmData = angular.extend(response.data.resData[0], response.data.resData[1], response.data.resData[2]);
			 vm.userTemData = angular.extend(response.data.resData[0], response.data.resData[1], response.data.resData[2]);
			 
			 vm.usrTypeCheck = vm.userFrmData.userType;
			 
			 if(vm.userFrmData.dob === -2209008600000){
				 vm.userFrmData.dob = null;
			 }	
			 vm.getImage(vm.userFrmData.userId, vm.userFrmData.userPhoto);
			 /*vm.timeZoneChange(vm.userFrmData.timeZoneId); */
			 //vm.getDivisionDetails();
			 /* vm.divChange();*/
			 if(vm.userFrmData.SubUserList.length > 0){
				 vm.userSubGroupTempdata =  vm.userFrmData.SubUserList;
			 }
			 vm.dateOptions = {
					 formatYear: 'yy',
					 minDate: new Date(moment(vm.userFrmData.deactDate).format("YYYY-MM-DD")),
					 startingDay: 1
			 };
			 
			
			
			//Division
				var divId = _.findIndex(vm.divisionList, {
		            'divisionId': vm.userFrmData.divisionId
				});
				if (divId > -1) {
					vm.userFrmData.divisionId = vm.divisionList[divId].divisionId;
					vm.divChange(vm.userFrmData.divisionId)
				} else {
					vm.userFrmData.divisionId = vm.divisionList[0].divisionId;
				}
			
			//locCount ++;
		 }else{
			 console.log(response.data.resMsg)
		 }
	 };	
	 
	 vm.getUserDetailsError = function(response){
		 console.log("get user details error");
	 };
	 
	 vm.getImage = function(userId, photo) {
		 vm.requestParams = {
				 "companyId" : vm.userFrmData.companyId,
				 "downloadFrom" : "USER",
				 "imageId" : userId.toString(),
				 "imageName" : photo,
		 };
		 if(vm.requestParams.imageName){
			 AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
		 }
	 }
	 
	 vm.assetImg;
	 vm.onGetLogoSuccess = function(response) {
		 vm.assetUpdImg = true;
		 vm.assetImg = response.data.resData.image;
		 
		//new code for image div
		vm.assetLogo = 'data:image/jpeg;base64,' + response.data.resData.image;
	 };
		
	 vm.onGetLogoError = function(response) {
		 /*console.log("error")*/
	 };
	 
	 vm.categoryList = function() {
		 vm.requestParams = {
				 "categoryPrimery" : {
					 "category" : 2, // category list by user
					 "companyId" : vm.userFrmData.companyId
				 }
		 };
		 MasterService.getCategoryAll(vm.requestParams,vm.getCategoryAllSucc,vm.getCategoryAllError)
	 };
	 
	 vm.getCategoryAllSucc = function(response) {
		 vm.category_list = response.data.resData;
		 var first = _.first(vm.category_list)
		 vm.userFrmData.categoryId = first.categoryPrimery.categoryId;
		 
		 vm.catcall = "called";
	 };
		
	 vm.getCategoryAllError = function(response) {};
	 
	 vm.getDivisionDetails = function() {
		 var requestParams = {
				 "companyId" : vm.userFrmData.companyId
		 };
		 MasterService.DivisionDetailService(requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
	 };	

	 vm.onDivisionInfoSuccess = function(response) {
//		 vm.divisionList = response.data;
//		 if (vm.divisionList.resData) {
//			 vm.userFrmData.divisionId = vm.divisionList.resData[0].divisionId;
//			
//			 vm.divChange(vm.userFrmData.divisionId);
//			 /* vm.userFrmData.sectorId = "";*/
//			}
		 
		 var result = response.data.resData;
		 if (result) {
			 vm.divisionList = result;
			 if(vm.assetPostMode === "EditMode"){
			 }else{
				 vm.userFrmData.divisionId = result[0].divisionId;
			 }
			 vm.divChange(vm.userFrmData.divisionId);
			 vm.divisioncall = "called";
		 }
	 };
		
	 vm.onDivisionInfoError = function(response) {
		 /*console.log("error");*/
	 };
	 
	 vm.divChange = function(divId, DIV_CHANGE) {
			vm.isDivChange = DIV_CHANGE;
			vm.divisionId = {
					"divisionId" : divId
			};
			if(divId){
				AssetService.getSectDetailsByDiv(vm.divisionId, vm.getSectDetBySucc, vm.getSectDetByErr);
			}else{
				vm.sect_data = [];
				vm.loc_data = [];
				vm.depart_data = [];
			}
	 };
		
	 vm.getSectDetBySucc = function(response) {
//		 
//		 vm.sect_data = response.data.resData;
//		 if (response.data.resData) {
//			 if(vm.userMode === "EditMode" && locCount === 1){
//				 var recId = _.findIndex(vm.sect_data, {
//					 'sectorId': vm.userTemData.sectorId 
//				 });
//				 if (recId > -1) {
//					 vm.userFrmData.sectorId  = vm.sect_data[recId].sectorId;
//				 }
//				 vm.secChange(vm.userFrmData.sectorId);
//				 locCount = 2;
//			 }else{
//				 vm.userFrmData.sectorId = vm.sect_data[0].sectorId;
//				 vm.secChange(vm.userFrmData.sectorId);
//			 }
//		 }else{
//			 vm.depart_data = "";
//			 vm.loc_data = "";
//		 }
		 
		 var result = response.data.resData;
		 if (result) {
			 vm.sect_data = result;
			 if(vm.userMode === "EditMode" && !vm.isDivChange){
				 var secId = _.findIndex(vm.sect_data, {
					 'sectorId': vm.userFrmData.sectorId
				 });
				 if (secId > -1) {
					 vm.userFrmData.sectorId = vm.sect_data[secId].sectorId;
				 }
				 vm.secChange(vm.userFrmData.sectorId);
			 }else{
				 vm.userFrmData.sectorId = vm.sect_data[0].sectorId;
				 vm.secChange(vm.userFrmData.sectorId);
			 }
		 }else{
			 vm.sect_data = [];
			 vm.loc_data = [];
			 vm.depart_data = [];
		 }
	 };

	 vm.getSectDetByErr = function(response) {
		 console.log("getSectDetByErr error");
	 };
	 
	 /*vm.secChange = function(secId) {
		 if(secId){
			 vm.sectorId = {
					 "sectorId" : secId
			 };
			 AssetService.getLocDetailsBySecID(vm.sectorId, vm.secChangeSuccess, vm.secChangeError);
		 }
	 };*/
	 
	 vm.secChange = function(secId, SEC_CHANGE) {
			vm.isSecChange = SEC_CHANGE;
			vm.sectorId = {
					"sectorId" : secId
			};
			if(secId){
				AssetService.getLocDetailsBySecID(vm.sectorId, vm.secChangeSuccess, vm.secChangeError);
			}else{
				vm.loc_data = [];
				vm.depart_data = [];
			}
	 };

//	 vm.secChangeSuccess = function(response) {
//		 vm.loc_data = response.data.resData;
//		 if (response.data.resData) {
//			 if(vm.userMode === "EditMode" && locCount === 2){
//				 var recId = _.findIndex(vm.loc_data, {
//					 'locationId': vm.userTemData.locationId 
//				 });
//				 if (recId > -1) {
//					 vm.userFrmData.locationId  = vm.loc_data[recId].locationId;
//				 }
//				 vm.locChange(vm.userFrmData.locationId);
//				 locCount = 3;
//			 }else{
//				 vm.userFrmData.locationId = vm.loc_data[0].locationId;
//				 vm.locChange(vm.userFrmData.locationId);
//			 }
//		 }else {
//			 vm.depart_data = "";
//			 vm.loc_data = "";
//		 }
//	 };
	 
	 vm.secChangeSuccess = function(response) {
		 var result = response.data.resData;
		 if (result){
			 vm.loc_data = result;
			 if(vm.userMode === "EditMode" && !vm.isSecChange){
				 if(!vm.isDivChange){
					 var locId = _.findIndex(vm.loc_data, {
						 'locationId': vm.userFrmData.locationId
					 });
					 if (locId > -1) {
						 vm.userFrmData.locationId = vm.loc_data[locId].locationId;
					 }
				 }else{
					 vm.userFrmData.locationId = vm.loc_data[0].locationId;
				 }
				 vm.locChange(vm.userFrmData.locationId);
			 }else{
				 vm.userFrmData.locationId = vm.loc_data[0].locationId;
				 vm.locChange(vm.userFrmData.locationId);
			 }
				
		 } else {
			 vm.loc_data = [];
			 vm.depart_data = [];
		 }
	 };

	 vm.secChangeError = function() {
		 console.log("secChangeError error");
	 };
	 
	 /*vm.locChange = function(locId) {
		 if(locId){
			 vm.locationId = {
					 "locationId" : locId
			 };
			 AssetService.getDepDetailsByLocID( vm.locationId, vm.locChangeSuccess, vm.locChangeError);
		 }
	 };*/
	 
	 vm.locChange = function(locId, LOC_CHANGE) {
		 vm.isLocChange = LOC_CHANGE;
		 vm.locationId = {
				 "locationId" : locId 
		 };
		 if(locId){
			 AssetService.getDepDetailsByLocID( vm.locationId, vm.locChangeSuccess, vm.locChangeError);
		 }
		 else{
			 vm.depart_data = [];
		 }
	 };

//	 vm.locChangeSuccess = function(response) {
//		 vm.depart_data = response.data.resData;
//		 if (response.data.resData){
//			 
//			 if(vm.userMode === "EditMode" && locCount === 3){
//				 var recId = _.findIndex(vm.depart_data, {
//					 'departmentId': vm.userTemData.departmentId 
//				 });
//				 if (recId > -1) {
//					 vm.userFrmData.locationId  = vm.depart_data[recId].departmentId;
//				 }
//				 vm.locChange(vm.userFrmData.departmentId);
//				 locCount = 4;
//			 }else{
//				 vm.userFrmData.departmentId = vm.depart_data[0].departmentId;
//			 }
//		 } 
//	 };
	 
	 vm.locChangeSuccess = function(response) {
		 var result = response.data.resData;
		 if (result){
			 vm.depart_data = response.data.resData;
			 vm.userFrmData.departmentId = vm.depart_data[0].departmentId;
		 }else{
			 vm.depart_data = [];
		 }
	 };

	 vm.locChangeError = function() {
		 console.log("locChangeError error");
	 };

	 vm.userTypeChange = function(userType){
		 if(userType === "User"){
			 vm.userFrmData.loginId = "";
			 vm.userFrmData.infoPassword = "";
			 vm.confrmPass = "";
		 }
	 };
	 
	 vm.userSubmitBtn = function(){
		 var actDate = moment(vm.userFrmData.actDate).format("DD-MMM-YYYY 00:00:00");
		 var deactDate = moment(vm.userFrmData.deactDate).format("DD-MMM-YYYY 23:59:59");
		 
		 vm.userFrmData.assetId = 0;
		 if(vm.userFrmData.dob){
			 vm.userFrmData.dob = moment(vm.userFrmData.dob).valueOf(); 
		 }
		 vm.userFrmData.actDate = moment(actDate).valueOf();
		 vm.userFrmData.deactDate = moment(deactDate).valueOf();
		 
		 if(vm.userMode === 'NewMode'){
			 vm.userFrmData.crDate = moment(new Date()).valueOf();
			 if(vm.baggageData === "cancel"){
				 vm.userFrmData.baggagesList = [];
			 }else{
				 vm.userFrmData.baggagesList = vm.baggageData;
			 }
			 if(vm.userSubGroupTempdata === undefined || vm.userSubGroupTempdata === null){
				 vm.userSubGroupTempdata = [];
			 }
			 vm.userFrmData.subUserDetails = vm.userSubGroupTempdata;
			 if(vm.expenseType === undefined || vm.expenseType === null){
				 vm.userFrmData.expenseType = "D";
			 }else{
				 vm.userFrmData.expenseType = vm.expenseType;
			 }
			 vm.userFrmData.systemLoginById = AuthService.getUserId();
			 vm.userFrmData.loginName = AuthService.getUserName();
			 
			 if(vm.transactionSettingData === undefined || vm.transactionSettingData === null || !parseInt(vm.transactionSettingData.toAmount) > 0){
				 swal({
					 title: "Please recharge your account",
					 //text: "Are you sure want to release tag!",
					 type: "warning",
					 showCancelButton: false,
					 confirmButtonColor: "#DD6B55",
					 confirmButtonText: "Yes, continue!",
					 /*cancelButtonText: "No, cancel please!",*/
					 closeOnConfirm: true,
					 closeOnCancel: false },
					 function(isConfirm){
						 if (isConfirm) {
							 vm.transactionSetting();
							 //vm.userRecharge();
						 }
					 });
			 }else{
				UserService.userAdd(vm.userFrmData, vm.userAddSuccess, vm.userAddError);
			}
			 
		 }else{
			 if(vm.baggageData === "cancel"){
				 vm.userFrmData.baggagesList = [];
			 }else{
				 vm.userFrmData.baggagesList = vm.baggageData;
			 }
			 vm.userFrmData.systemLoginById = AuthService.getUserId();
			 UserService.userUpdate(vm.userFrmData, vm.userUpdateSuccess, vm.userUpdateError);
		 }
	 };
	 
	 vm.userAddSuccess = function(response){
		 if (response.data.resCode === "res0000") {
				swal("Success", "User Added Successfully!", "success")
				
				//Image upload
				vm.requestParam = {
					"companyId" : response.data.resData.companyId,
					"uploadTo" : "USER",
					"imageId" : response.data.resData.userId,
				}
				if (vm.assetLogo && vm.assetLogo.name) {
					AuthService.uploadLogo(vm.requestParam, vm.assetLogo, vm.assetImgSucc, vm.assetImgErr)
				}
				
				//Attachment upload
				if(vm.userAttachedFileNameData !== "cancel" && vm.userAttachedMultipartFileData !== "cancel"){
					vm.requestParams ={
							"companyId": response.data.resData.companyId,
							"uploadTo":"USER",
							"id" : response.data.resData.userId,
							"fileName": vm.userAttachedFileNameData,
							"file" : vm.userAttachedMultipartFileData
					}
					AssetService.uploadAttach(vm.requestParams, vm.uploadAttachSuccess, vm.uploadAttachError);
				}
				
				if(vm.transactionSettingData){
					vm.transactionSettingData.userId = response.data.resData.userId;
					vm.transactionSettingData.posPasswordAmtAcitvity.mobileNumber = vm.userFrmData.mobileno;
					UserService.userTransactionSetting(vm.transactionSettingData, vm.userTransactionSetSuccess, vm.userTransactionSetError)
				};
				
				$state.go('homeDashboard.posUser');
				
				/*$state.transitionTo('homeDashboard.user', $stateParams, {
							reload : true,
							inherit : false,
							notify : true
						});*/
				
			} else if (response.data.resCode === "res0100") {
				swal("Not Added!", response.data.resMsg, "error");
			}
	 };
	 
	 vm.userAddError = function(response){
		console.log("user not added") 
	 };
	 
	 vm.userUpdateSuccess = function(response){
		 if (response.data.resCode === "res0000") {
				swal("Updated", response.data.resMsg, "success");
				vm.requestParam = {
						"companyId" : vm.userFrmData.companyId,
						"uploadTo" : "USER",
						"imageId" : $stateParams.userId,
				}
				
				if (vm.assetLogo && vm.assetLogo.name) {
					AuthService.uploadLogo(vm.requestParam, vm.assetLogo, vm.assetImgSucc, vm.assetImgErr)
				}
				
				//Attachment upload
				if(vm.userAttachedFileNameData !== "cancel" && vm.userAttachedMultipartFileData !== "cancel"){
					vm.requestParams ={
							"companyId": vm.userFrmData.companyId,
							"uploadTo":"USER",
							"id" : $stateParams.userId,
							"fileName": vm.userAttachedFileNameData,
							"file" : vm.userAttachedMultipartFileData
					}
					AssetService.uploadAttach(vm.requestParams, vm.uploadAttachSuccess, vm.uploadAttachError);
				}
				
				if(vm.transactionSettingData){
					vm.transactionSettingData.posPasswordAmtAcitvity.mobileNumber = vm.userFrmData.mobileno;
					UserService.userTransactionSetting(vm.transactionSettingData, vm.userTransactionSetSuccess, vm.userTransactionSetError)
				};
				
				$state.go('homeDashboard.posUser');
			}else{
				swal("Not Updated!", response.data.resMsg, "error");
			}
	 }; 
	 
	 vm.userUpdateError = function(response){
		 console.log("user not updated");
	 };
	 
	 vm.assetImgSucc = function(response) {
		 console.log("Image update success");
	 }
	 
	 vm.assetImgErr = function(response) {
		 console.log("Image update fail");
	 }
	 
	 vm.uploadAttachSuccess = function(response){
//		console.log(response.data.resMsg) 
	 };
	 
	 vm.uploadAttachError = function(response){
		 console.log(response.data.resMsg);
	 };
	 
	 vm.userTransactionSetSuccess = function(response){
		 if(response.data.resCode === 'res0000'){
			/* console.log(response.data.resMsg);*/
			 //swal("Successfully", response.data.resCode, "success");
			 //$uibModalInstance.dismiss();
		 }
		 if(response.data.resCode === 'res0100'){
			 console.log(response.data.resMsg);
		 }
	 };
		
	 vm.userTransactionSetError = function(response){
		 console.log(response.data);
	 };
	 
	 vm.cancel = function() {
		$state.go('homeDashboard.posUser');
	};
	
	/*========ANGULAR DATE PICKER============*/
	/*vm.today = function() {
		vm.dob = new Date();
		vm.actDate = new Date();
		vm.deactDate = new Date();
	};*/
	/*vm.today();*/

	vm.clear = function() {
		vm.dob = null;
		vm.actDate = null;
		vm.deactDate = null;
	};
		
	
	vm.dateOptionsForAct = {	
			/*dateDisabled: disabled,*/
			formatYear: 'yy',
			//maxDate: new Date(),
			//minDate: new Date(2000, 5, 22),
			startingDay: 1
	}
		
	vm.dateOptions = {
			/*dateDisabled: disabled,*/
			formatYear: 'yy',
//			minDate: new Date(moment(vm.userFrmData.actDate).format("YYYY-MM-DD")),
			startingDay: 1
	};
	
	vm.dobOptions = {
			/*dateDisabled: disabled,*/
			formatYear: 'yy',
			//minDate: new Date(),
			//maxDate: new Date(),
			startingDay: 1
	}
	
	vm.actDateChange = function(){
		vm.userFrmData.deactDate = null;
		vm.dateOptions = {
				/*dateDisabled: disabled,*/
				formatYear: 'yy',
				minDate: new Date(moment(vm.userFrmData.actDate).format("YYYY-MM-DD")),
				startingDay: 1
		};
	};
	
	vm.dobOpen = function() {
		vm.dobOp.opened = true;
	};
	  
	vm.activationDtOpen = function(){	
		vm.actDate.opened = true;
	}
	  
	vm.deActivationDtOpen = function(){	
		vm.deActDate.opened = true;
	}
	 
	vm.dobOp = {
			opened: false
	};
	  
	vm.actDate = {	
			opened: false  
	}
	  
	vm.deActDate = {
			opened: false
	}
	  
	//tag select function
	vm.tagSelectFun = function() {	
		var modalInstance = $uibModal
		.open({
			backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/tag/tagSearch.html',
				controller : 'TagSearchController',
				controllerAs : 'tagSearchController',
				resolve : {
					tagSelect : function() {
						return angular.copy(vm.userFrmData.userTagId);
					}
				}
		});
			
		modalInstance.result.then(function(selectedItem) {
		}, function(data) {
			if(data){
				vm.tagInfo = JSON.parse(data);
				vm.userFrmData.userTagId = vm.tagInfo.tagId;
			}
		});	
	};
	
	vm.timeZoneDetails = function(){
		vm.companyId = {	
				"companyId":vm.userFrmData.companyId
		}
		UserService.getAllTimeZones(vm.companyId, vm.getAllTimeZoneSuccess, vm.getAllTimeZoneError);
	};
	
	vm.getAllTimeZoneSuccess = function(response){
		//console.log(response);
		if(response.data.resData){
			vm.timezones = response.data.resData;
			var first = _.first(vm.timezones)
			vm.userFrmData.timeZoneId = first.timeId
			vm.timeZoneChange(first.timeId);  
			  
			if(vm.userMode === "EditMode"){
				vm.timeZoneChange(vm.userFrmData.timeZoneId);  
			}
		}
	};
	
	vm.getAllTimeZoneError = function(response){
		console.log("get all time zone error");
	};
	
	vm.timeZoneChange = function(timeId){
		vm.requestParams = {
				"timeId": timeId,
				"companyId":vm.userFrmData.companyId
		}
		UserService.getTimeZoneDetail(vm.requestParams, vm.getTimeZoneDetailsSuccess, vm.getTimeZoneDetailsError);
	};
	
	vm.getTimeZoneDetailsSuccess = function(response){
		if(response.data.resData){
			vm.timeZoneDetails = response.data.resData;
		}
	};
	 
	vm.getTimeZoneDetailsError = function(response){
		console.log("get Time Zone Details error") 
	};
		
	//USER ATTACHMENT
	vm.userAttach = function(pageData){
		
		var modalInstance = $uibModal
		.open({
			backdrop : 'static',
			 animation : vm.animationsEnabled,
			 templateUrl : 'resources/views/user/userAttachment.html',
			 controller : 'UserAttachmentController',
			 controllerAs : 'userAttachmentController',
			 resolve : {
				 userFrmData : function() {
					 return angular.copy(vm.userFrmData);
				 },
				 PageData : function() {
					 return angular.copy(pageData);
				 }
			 }
		});
		modalInstance.result.then(function(selectedItem) {
			/* $scope.selected = selectedItem; */	
		}, function(userFileData) {
			 
			if(userFileData !== 'cancel'){
				angular.forEach(userFileData, function(value){
					console.log(value);
					if(!value.userId){
						vm.userAttachedFileNameData.push(value.fileName);
						vm.userAttachedMultipartFileData.push(value.file);
					}
				});
			}else{
				vm.userAttachedFileNameData = "cancel";
				vm.userAttachedMultipartFileData = "cancel";
			}
			vm.initialize();
		});
	};
	
	//RELEASE TAG 
	vm.releaseTagFun = function(){
		swal({
			 title: "Click yes to continue ",
				text: "Are you sure want to release tag!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, continue!",
				/*cancelButtonText: "No, cancel please!",*/
				closeOnConfirm: false,
				closeOnCancel: false },
				function(isConfirm){
					if (isConfirm) {
						vm.requestParams = {
								 "userId" : $stateParams.userId,
								 "userTagId":vm.userFrmData.userTagId,
								 "companyId":vm.userFrmData.companyId
						 }
						 UserService.userReleaseTag(vm.requestParams, vm.userReleaseTagSuccess, vm.userReleaseTagError) 
					}
					else {
						swal("Cancelled", "Your asset is safe :)", "error");
					}	
				});
	 };
	 
	 vm.userReleaseTagSuccess = function(response){
		 if(response.data.resCode === 'res0000'){
			 swal("Successfully", "Tag released successfully!", "success");
			 $state.transitionTo('homeDashboard.posUser',
					 $stateParams, {
				 reload : true,
				 inherit : false,
				 notify : true
			 });
		 }
		 if(response.data.resCode === 'res0100'){
			 console.log("release tag not successfull");
		 }
	 };
	 
	 vm.userReleaseTagError = function(response){
		 console.log("error in release tag", response.data.resData);
	 };
	 
	 //BAGGAGE
	 vm.baggageFun = function(){
		 var modalInstance = $uibModal
		 .open({
			 backdrop : 'static',
			 animation : vm.animationsEnabled,
			 templateUrl : 'resources/views/user/baggage.html',
			 controller : 'BaggageCtrl',
			 controllerAs : 'baggageCtrl',
			 resolve : {
				 userData : function() {
					 return angular.copy(vm.userFrmData);
				 }
			 }
		 });
				
		 modalInstance.result.then(function(selectedItem) {
			 /* $scope.selected = selectedItem; */
		 }, function(baggageData) {
			 console.log(baggageData);
			 vm.baggageData = baggageData;
			 //vm.initialize();
		 });
	 };
	 
	 //TRANSACTION SETTING
	 vm.transactionSetting = function(){
		 var modalInstance = $uibModal
		 .open({
			 backdrop : 'static',
			 animation : vm.animationsEnabled,
			 templateUrl : 'resources/views/user/transactionSetting.html',
			 controller : 'TransactionSettingCtrl',
			 controllerAs : 'transactionSettingCtrl',
			 resolve : {
				 userData : function() {
					 return angular.copy(vm.userFrmData);
				 }
			 }
		 });
				
		 modalInstance.result.then(function(selectedItem) {
			 /* $scope.selected = selectedItem; */
		 }, function(data) {
			 vm.transactionSettingData = data;
			 vm.submitBtnNm = "SAVE";
			 if(!vm.userSubGroupTempdata.length > 0){
				 swal({
					 title: "Do you want to Add SubUser!",
					 //text: "Are you sure want to release tag!",
					 type: "warning",
					 showCancelButton: true,
					 confirmButtonColor: "#DD6B55",
					 confirmButtonText: "Yes, continue!",
					 /*cancelButtonText: "No, cancel please!",*/
					 closeOnConfirm: true,
					 closeOnCancel: true },
					 function(isConfirm){
						 if (isConfirm) {
							 vm.userSubGroup();
						 }else{
							 //vm.userSubmitBtn();
							// console.log("data saved from subgrrr");
							//UserService.userAdd(vm.userFrmData, vm.userAddSuccess, vm.userAddError);
						 }
					 });
			 }else{
				 //vm.userSubmitBtn();
			 }
		 });
	 };
	 
	 //Add Sub user
	 vm.userSubGroup = function(){
		 var modalInstance = $uibModal
		 .open({
			 backdrop : 'static',
			 animation : vm.animationsEnabled,
			 templateUrl : 'resources/views/user/userSubGroup.html',
			 controller : 'UserSubGroupCtrl',
			 controllerAs : 'userSubGroupCtrl',
			 size : 'md',
			 resolve : {
				 userData : function() {
					 return angular.copy(vm.userFrmData);
				 },
				 subUsersList : function(){
					 return vm.userSubGroupTempdata;
				 },
				 userReadTag : function(){
					 if(vm.userFrmData.userTagId){
						 return vm.userFrmData.userTagId;
					 }else{
						 return null;
					 }
				 }
			 }
		 });
				
		 modalInstance.result.then(function(selectedItem) {
		 }, function(selectedItem) {
			 if(selectedItem){
				 vm.userSubGroupTempdata = selectedItem.subUserData;
				 vm.expenseType = selectedItem.expenseType;
			 }
		 });
	 };
	 
	 //Webcam 
	 vm.webCamBtn = "Webcam";
	 vm.webcamClick = function() {
		 vm.webCamBtn = "Take";
		 vm.webCamDiv = true;
		 if(vm.webCamBtn === "Take"){
			 Webcam.snap(function(data_uri) {
				 vm.assetLogo = data_uri;
				 vm.webCamDiv = false;
					
				 /*var blob = new Blob([data_uri], {type: 'image/jpg'});
					
					var file = new File([blob], 'fileName.jpg');
					vm.webCamImg = data_uri;*/
					
				 /*var aa = data_uri.split(',');
					var blob = new Blob([aa[1]], {type: 'image/jpg'});
					var blobUrl = URL.createObjectURL(blob);
					var file = new File([blob], 'fileName.jpg' ,{type: 'image/jpg'});
					file.$ngfBlobUrl = blobUrl;
					vm.webCamImg = file;*/
				 document.getElementById('anchor').setAttribute('href', data_uri);// download
			 });
		 }	
		 
		 vm.showDiv = true;
		 $timeout(function(){
			 Webcam.attach('#my_camera');},0)	
	 };
	 
	 vm.myFun = function(){
		 vm.webImg = false;
		 vm.showDiv = false;
		 vm.assetUpdImg = false;	
	 };	
	 
	 vm.take_snapshot = function() {
		 vm.webImg = true;
		 vm.assetUpdImg = false
		 Webcam.snap(function(data_uri) {
			 vm.webCamImg = data_uri;
			 /*console.log(data_uri);
				var blob = new Blob([data_uri], {type: 'image/jpg'});
				console.log(blob);
				var file = new File([blob], 'imageFileName.jpg' ,{type: 'image/jpg'});
				vm.assetLogo = file;*/
			 document.getElementById('anchor').setAttribute('href', data_uri);// download
		 });	
	 };
		
	 //image select
	 vm.clickonChoose = function(){
		 vm.webCamDiv = false;
		 vm.webCamBtn = "Webcam";
		 document.getElementById('my_file').click();
	 };
		
	 vm.imgDelete = function(){
		 vm.assetLogo = "";
		 vm.webCamBtn = "Webcam";
	 };
	 
	 vm.getPosSettingDetails = function(){
		 vm.requestParams = {
				 "companyId" : vm.userFrmData.companyId
		 };	
		 PosService.getposSettingDetails(vm.requestParams, vm.onSuccessSettingDetails, vm.onErrorSettingDetails);
	 };
		
	 vm.onSuccessSettingDetails = function(response){
		 vm.possetData = response.data.resData;
	 };
		
	 vm.onErrorSettingDetails = function(response){
		 console.log(response.data.resMsg);
	 };
	 
	 vm.readTag = function(){
		 vm.requestParams = {
				 "dockDoorId": vm.dockDoorId,
				 "companyId" : vm.userFrmData.companyId
		 };
		 UserService.getTagsByReaders(vm.requestParams, vm.onSuccessGetTags, vm.onErrorGetTags);
	 };
	 
	 vm.onSuccessGetTags = function(response){
		 if(response.data.resCode === "res0000"){
			 vm.userFrmData.userTagId = response.data.resData;	
		 }else{
			 swal("Error",response.data.resMsg, "error");
		 }
	 };	
	 
	 vm.onErrorGetTags = function(response){};
	 
	 var asyncReqCheck = $interval(function(){
		 if(vm.catcall && vm.divisioncall){
			 vm.initialize();
			 $interval.cancel(asyncReqCheck)
		 }
	 },0)
	 
	 //vm.initialize();
	 vm.categoryList();
	 vm.timeZoneDetails();
	 vm.getDivisionDetails();
	 vm.getPosSettingDetails();
  }]);
})();
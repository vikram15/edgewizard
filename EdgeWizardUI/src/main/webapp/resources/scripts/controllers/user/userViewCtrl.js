(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('UserViewCtrl', ['$stateParams','AuthService','UserService','_','AssetService', function($stateParams, AuthService, UserService, _, AssetService){
		var vm = this;
		vm.userData = {};
		vm.userImage;
		vm.companyId = AuthService.getCompanyId();
		vm.companyName = AuthService.getCompanyName();
		
		vm.initialize = function() {
			vm.userId = $stateParams.userId;
			vm.requestParams = {
					'userId' : vm.userId,
					'companyId' : vm.companyId
			}
			UserService.getUserDetails(vm.requestParams, vm.getUserDetailsSuccess, vm.getUserDetailsError);
		}
		
		vm.getUserDetailsSuccess = function(response){
			if(response.data.resCode === 'res0000'){
				vm.userData = _.extend(response.data.resData[0], response.data.resData[1], response.data.resData[2]);
				vm.timeZoneChange(vm.userData.timeZoneId);
				if(vm.userData.userPhoto){
					vm.getImage(vm.userData);
				}
			}
		};
		
		vm.getUserDetailsError = function(response){
			console.log(response.data);
		};
		
		//GET USER IMAGE
		vm.getImage = function(data) {
			vm.requestParams = {
					"companyId" : vm.companyId,
					"downloadFrom" : "USER",
					"imageId" : data.userId.toString(),
					"imageName" : data.userPhoto,
			};
			if(vm.requestParams.imageName){
				AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
			}
		}
		 
		vm.onGetLogoSuccess = function(response) {
			vm.userImage = response.data.resData.image;
		 };
			
		 vm.onGetLogoError = function(response) {
			 /*console.log("error")*/
		 };
		
		vm.fileName = ""; 
		vm.downloadAttachment = function(data){
			vm.fileName = data; 
			vm.requestParams = {
					"searchBy":"USER",
					"searchValue":data,          
					"companyId":vm.companyId
			}
			AssetService.downloadAttachment(vm.requestParams, vm.assetgetFileSucc, vm.assetgetFileErr);
		};
		
		vm.assetgetFileSucc = function(response){
			var blob = new Blob([response.data], {type: "application/*"});
	        var objectUrl = URL.createObjectURL(blob);
			//vm.fileName = vm.fileName.slice(26);
			var a = document.createElement('a');
            a.href = objectUrl;
            a.download = vm.fileName;
            a.click();
		};
		
		vm.assetgetFileErr = function(){
			console.log("Error in download attachment");
		};
		
		vm.timeZoneChange = function(timeId){
			  vm.requestParams = {
					  "timeId": timeId,
					  "companyId":vm.companyId
			  }
			  UserService.getTimeZoneDetail(vm.requestParams, vm.getTimeZoneDetailsSuccess, vm.getTimeZoneDetailsError);
		  };
		  
		 vm.getTimeZoneDetailsSuccess = function(response){
			 if(response.data.resData){
				 vm.timeZoneDetails = response.data.resData
			 }
		 };
		 
		 vm.getTimeZoneDetailsError = function(response){
			console.log("get Time Zone Details error") 
		 };
		
		vm.initialize();
		
	}]);
})();
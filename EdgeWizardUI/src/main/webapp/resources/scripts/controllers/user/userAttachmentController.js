(function() {
	angular.module('rfidDemoApp.controller')
	.controller('UserAttachmentController',
			['$scope','$uibModalInstance', 'AuthService', '$state', '$stateParams', 'AssetService','PageData','userFrmData',
			 function($scope, $uibModalInstance, AuthService, $state, $stateParams, AssetService, PageData, userFrmData) {
				var vm = this;
				vm.tableHeader = "Attachment";
				vm.globalCompId = AuthService.getCompanyId();
				/*vm.globalCompName = AuthService.getCompanyName();
				*/
				vm.usrTempFileData = [];
				
				vm.userAttachBtn = function(){
					vm.usrTempFileData.push({
						"fileName": vm.attachedfile.name,
						"file":  vm.attachedfile
					});
				};
				
				vm.saveBtn = function(){
					$uibModalInstance.dismiss(vm.usrTempFileData);
				};
				
				vm.deleteBtn = function(item){
					console.log(item);
					if(item.userId){
						swal({
				            title: "Are you sure?",
				            text: "You want to delete permenently?",
				            type: "warning",
				            showCancelButton: true,
				            confirmButtonColor: "#DD6B55",
				            confirmButtonText: "Yes, delete it!",
				            cancelButtonText: "No, cancel please!",
				            closeOnConfirm: false,
				            closeOnCancel: false },
				            function(isConfirm){
				              if (isConfirm) {
				            	  vm.usrTempFileData.splice(vm.usrTempFileData.indexOf(item), 1);
				            	  vm.requestParams = {
											"searchBy":"ASSET",
											"searchValue":item.attaFullName,          
											"companyId":vm.globalCompId
									}
									if(vm.pageDataNew === "userAttach"){
										vm.requestParams.searchBy = "USER";
									}
									AssetService.deleteAttachment(vm.requestParams, vm.deleteAttSuccess, vm.deleteAttErr);
				              }
				              else {
				            	  swal("Cancelled", "Your imaginary file is safe :)", "error");
				              }
				            });
					}else{
						vm.usrTempFileData.splice(vm.usrTempFileData.indexOf(item), 1);
					}
					
				};
				
				vm.deleteAttSuccess = function(response){
					if (response.data.resCode === "res0000") {
						swal("Deleted!", response.data.resData,"success");
						//$uibModalInstance.dismiss('cancel');
					} else if (response.data.resCode === "res0100") {
						swal("Not deleted!", response.data.resData, "error");
					}else{
						
					}
				};
				
				vm.deleteAttErr = function(response){
					
				};
					
				vm.fileList = [];
				vm.getFile = function(){
					if(userFrmData.userAttachment && userFrmData.userAttachment.userAttachmentList){
						if(PageData === "userAttach"){
							vm.pageDataNew = PageData;
							vm.fileList = userFrmData.userAttachment.userAttachmentList;
							angular.forEach(vm.fileList ,function(value){
								var str = value.split('_');
								//vm.fileList.push(str[2]);
								vm.usrTempFileData.push({
									"userId" : str[0],
									"fileName" : str[2],
									"attaFullName" : value
								})
							});
						}
					}
				};
				
				vm.fileName = "";
				vm.downloadFile = function(data){
					vm.fileName = data.attaFullName; 
					vm.requestParams = {
							/*"searchBy":data.attaFullName,
							"searchValue":"ASSET", */    
							"searchBy":"ASSET",
							"searchValue":data.attaFullName,          
							"companyId":vm.globalCompId
					}
					if(vm.pageDataNew === "userAttach"){
						vm.requestParams.searchBy = "USER";
					}
					AssetService.downloadAttachment(vm.requestParams, vm.assetgetFileSucc, vm.assetgetFileErr);
				}
						
				vm.assetgetFileSucc = function(response){
					
					var blob = new Blob([response.data], {type: "application/*"});
			        var objectUrl = URL.createObjectURL(blob);
			       // window.open(objectUrl);
			       
					if(PageData === "AssetAttach"){
						vm.fileName = vm.fileName.slice(16);
					}
					if(PageData === "DocRefAttach"){
						vm.fileName = vm.fileName.slice(26);
					}
					
					var a = document.createElement('a');
		            a.href = objectUrl;
		            a.download = vm.fileName;
		            a.click();
		            
					/*var anchor = angular.element('<a/>');
					anchor.attr({
						href: 'data:attachment/*;charset=utf-8,' + encodeURI(response.data),
						target: '_blank',
						download: vm.fileName
					})[0].click();*/
					
				};
				
				vm.assetgetFileErr = function(){
					console.log("Error in download attachment");
				};
				
				vm.cancel = function() {
					$uibModalInstance.dismiss('cancel');
				};
						
				vm.getFile();
			}]);
})();

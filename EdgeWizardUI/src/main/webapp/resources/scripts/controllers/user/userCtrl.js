(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('UserCtrl',['UserService','$state','AuthService','$rootScope','MasterService','AssetService', 
	                        function(UserService, $state, AuthService, $rootScope, MasterService, AssetService){
		console.log("user controller");
		var vm = this;
		$rootScope.pageTitle = "User | EdgeWizard";
		vm.companyId = AuthService.getCompanyId();
		vm.comp_name = AuthService.getCompanyName();
		
		vm.selectUserType = "User Id";
		vm.currentPage = 1;
		vm.maxSize = 5;
		
		//for pickdrop
		 vm.loginType = AuthService.getSystemName();
		
		vm.userListLoad = function(){
			vm.requestParams = {
					"searchBy":vm.selectUserType,
					"searchValue":vm.searchValue,
					"sortBy":vm.sortBy,
					"currentPage":vm.currentPage,
					"companyId":vm.companyId
			}
			if(vm.selectUserType){
				UserService.getUserList(vm.requestParams, vm.userListSuccess, vm.userListError);
			}
		};
		
		vm.userListSuccess = function(response){
			if(response.data.resData){
				vm.userData = response.data.resData.userList;
				vm.totalItems = response.data.resData.totalUserSearch;
		    	vm.itemsPerPage = response.data.resData.RecordPerPage;
			}
		};
		
		vm.userListError = function(response){
			console.log("User list Error")
		};
		
		vm.userDeleteBtn = function(user){
			swal({
	            title: "Are you sure?",
	           /* text: "Are you sure want to delete!",*/
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Yes, delete it!",
	            cancelButtonText: "No, cancel please!",
	            closeOnConfirm: false,
	            closeOnCancel: false },
	            function(isConfirm){
	              if (isConfirm) {
	            	  vm.requestParams = {
	            			  "userId": user.userId,
	            			  "userType":user.userType,
	            			  "companyId":vm.companyId 
	            	  }
	            	  UserService.deleteUserList(vm.requestParams, vm.deleteUserListSuccess, vm.deleteUserListError);
	              }
	              else {
	            	  swal("Cancelled", "Your imaginary file is safe :)", "error");
	              }
	            });
		};
			
		vm.deleteUserListSuccess = function(response){
			if (response.data.resCode === "res0000") {
				swal("Deleted!", response.data.resMsg,"success");
				vm.userListLoad();
			} else if (response.data.resCode === "res0100") {
				swal("Not deleted!", response.data.resMsg, "error");
			}else{
				
			}
		};
		
		vm.deleteUserListError = function(){
			console.log("user not deleted");
		};
		
		 vm.categoryList = function() {
			 vm.requestParams = {
					 "categoryPrimery": {
						 "category": 2,
						 "companyId": vm.companyId
					 }
			 };
			 MasterService.getCategoryAll(vm.requestParams, vm.getCategoryAllSucc, vm.getCategoryAllError)
		 };

			vm.getCategoryAllSucc = function(response) {
				vm.category_list = response.data;
				vm.categoryType = vm.category_list.resData[0].categorynm;
			};

			vm.getCategoryAllError = function(response) {
				console.log("error");
			};

			vm.getDivisionDetails = function() {
				vm.requestParams = {
						"companyId" : vm.companyId
				};	
				MasterService.DivisionDetailService(vm.requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
			};

			vm.onDivisionInfoSuccess = function(response) {
				vm.divisionList = response.data;
				vm.divisionName = vm.divisionList.resData[0].divisionNm;
			};

			vm.onDivisionInfoError = function(response) {
				/*console.log("error");*/
			};
			
			vm.getSectorDetails = function(){
				vm.requestParams = {
						"companyId": vm.companyId
				}
				MasterService.SectorListService(vm.requestParams, vm.onSectorSuccess, vm.onSectorError);
			};

			vm.onSectorSuccess = function(response){
				vm.sect_data = response.data;
				vm.sectorName = vm.sect_data.resData[0].sectorNm
			};

			vm.onSectorError = function(){
				
			};

			vm.getLocationDetails = function(){
				vm.requestParams = {
						"companyId": vm.companyId
				};
				MasterService.LocationListService(vm.requestParams, vm.onLSuccess, vm.onLError);
			};

			vm.onLSuccess = function(response){
				vm.loc_data = response.data;
				vm.locationName = vm.loc_data.resData[0].locationNm;
			};

			vm.onLError = function(response){

			};

			vm.getDepartmentDetails = function(){
				vm.requestParams = {
						"companyId": vm.companyId
				};
				MasterService.DepartmentListService(vm.requestParams, vm.onDepSuccess, vm.onDepError);
			};

			vm.onDepSuccess = function(response){
				vm.depart_data = response.data;
				vm.departmentName = vm.depart_data.resData[0].deptNm;
			};

			vm.onDepError = function(response){	

			};
			
			/*========ANGULAR DATE PICKER============*/
			vm.today = function() {
				vm.fromDate = new Date();
				vm.toDate = new Date();
			};
			vm.today();
			
			vm.clear = function() {
				vm.fromDate = null;
				vm.toDate = null;
			};

			vm.dateOptions = {
					//dateDisabled: disabled,
					formatYear: 'yy',
					//maxDate: new Date(2020, 5, 22),
					minDate: new Date(2000, 5, 22),
					startingDay: 1
			};

			// Disable weekend selection
			function disabled(data) {
				var date = data.date,
				mode = data.mode;
				return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
			}

			vm.fromOpen = function(){
				vm.from.opened = true;
			};
				  
			vm.toOpen = function(){
				vm.to.opened = true;  
			};
				  
			vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
			vm.format = vm.formats[0];
			vm.altInputFormats = ['M!/d!/yyyy'];
			
			vm.from = {
					opened: false
			};
			vm.to = {
					opened: false
			};
			
			vm.viewMap = function(data){
				if(data.tagId){
					AssetService.getLocation(data.tagId, vm.onSuccessGetlocation, vm.onErrorGetLocation);
				}
			};
			
			vm.onSuccessGetlocation = function(response){
				if(response.data.resData){
					var result = response.data.resData;
					var aTag = document.createElement('a');
					aTag.setAttribute('href',"http://maps.google.com/maps?q=" + result.latitude + "," +result.longitude);
					aTag.setAttribute('target',"_blank");
					window.open(aTag);
				}
			};
			
			vm.onErrorGetLocation = function(){};
		
		vm.userListLoad();
	}]);
})();
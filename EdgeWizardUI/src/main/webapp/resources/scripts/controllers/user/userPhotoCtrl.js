(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('UserPhotoCtrl',['AuthService','IssueReceiveService','_','UserService','$interval','$scope','$cookies','PosService','$filter','CustomService', 
	                             function(AuthService, IssueReceiveService, _, UserService, $interval, $scope, $cookies, PosService, $filter, CustomService){
		var vm = this;
		var imgToExport;
		vm.userFields = [];
		vm.companyId = AuthService.getCompanyId();
		vm.companyName = AuthService.getCompanyName();
		vm.userName = AuthService.getUserName();
		vm.detectedUser = {};
		vm.dockDoorId = JSON.parse($cookies.get("globals")).dockDoorId;
		vm.dockDoorName = JSON.parse($cookies.get("globals")).dockDoorName;
		vm.dolpLogoBase64 = "";
		
		var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		var dt = new Date();
		vm.currentDate = dt;
		vm.currDay = days[dt.getDay()];
		vm.currTime = dt.getTime();
		
		
		vm.getUserslist = function(){
			vm.requestParams = {
					"companyId":vm.companyId,
					"dockDoorId":vm.dockDoorId
			}
			UserService.get10UsersList(vm.requestParams, vm.onSuccess, vm.onError);
		};
		
		vm.onSuccess = function(response){
			vm.userData = response.data.resData;
			var n = 0;
			_.each(vm.userData, function(user){
				if(n === 0){
					vm.detectedUser = user;
				}
				n++;
			});
		};
		
		vm.onError = function(response){
			
		};
		
		vm.getUserDetailsField = function(){
			vm.requestParams = {
					"companyId":vm.companyId
			}
			IssueReceiveService.getUserDetailFields(vm.requestParams, vm.getUsrDetailSuccess, vm.getUsrDetailError)
		};
		
		vm.getUsrDetailSuccess = function(response){
			vm.userFields = response.data.resData;
		};
		
		vm.getUsrDetailError = function(){
			console.log("error in get user details field");
		};
		
		
		
		vm.settlement = function(){
			vm.requestParams = {
					"startDate": moment(new Date()).format("DD-MM-YYYY"),
					"endDate" : moment(new Date()).format("DD-MM-YYYY"),
					"userID":9
			}
			PosService.getUserTransactionReportDetails(vm.requestParams, vm.onSuccessGetReportDetails, vm.onErrorGetReportDetails)
		}
		
		vm.onSuccessGetReportDetails = function(response){
			if(response.data.resData){
				var result = response.data.resData;
			}
			
			vm.otherData = {
		    		"titleName" : "User POS Transaction Activity Report",
		    		"currDay" : vm.currDay,
		    		"currentDate" : vm.currentDate,
		    		"currTime" : vm.currTime,
		    		"dolpLogoBase64" : vm.dolpLogoBase64,
		    		"globalCompName" : vm.companyName,
		    		"userName" : vm.userName,
		    		"userId":9,
		    		"tagId" : "EPC78787878787"
		    };
        	
		    //New code
		    //User Summary details
		    var userTblDataWidth, userTblDataClm = [], userTableDataValue = [], currentBalTotal = 0, totalCrAmount = 0, totalDrAmount = 0;
        	userTblDataWidth = [40,'*','*','*','*','*'];
        	userTblDataClm.push({
				text:'User Id', style: ['tableHeader','myTableHeader']},
				{text:'User Name', style: ['tableHeader','myTableHeader']}, 
				{text: 'EPC ID', style: ['tableHeader','myTableHeader']},
				{text: 'Current Balance', style: ['tableHeader','myTableHeader']},
				{text: 'CR', style: ['tableHeader','myTableHeader']},
				{text: 'DR', style: ['tableHeader','myTableHeader']}
        	)
        	
        	var mainUserDetails = {};
        	_.each(result.userSummary, function(res){
        		if(res.userId == res.groupId){
        			mainUserDetails = res;
        		}
        		userTableDataValue.push([{
 					text: res.userId, style :'tableRowFont',
 				},{
 					text: res.userName, style :'tableRowFont',
 				},{
 					text: res.userTagId, style :'tableRowFont',
 				},{
 					text: "₹" + $filter('number')(parseInt(res.currentBalance), 2), style :'tableRowFont',
 				},{
 					text: "₹" + $filter('number')(parseInt(res.totalCr), 2), style :'tableRowFont',
 				},{
 					text: "₹" + $filter('number')(parseInt(res.totalDr), 2), style :'tableRowFont',
 				}]);
        		if(res.currentBalance === "null" || res.currentBalance === null ){
        			res.currentBalance = 0
        		}
        		if(res.totalCr === "null" || res.totalCr === null ){
        			res.totalCr = 0
        		}
        		if(res.totalDr === "null" || res.totalDr === null ){
        			res.totalDr = 0
        		}
 				currentBalTotal = currentBalTotal + parseInt(res.currentBalance);
 				totalCrAmount = totalCrAmount + parseInt(res.totalCr);
 				totalDrAmount = totalDrAmount + parseInt(res.totalDr);
			});
        	
        	vm.userSummaryDetails = {
 					"userTblDataWidth" : userTblDataWidth,
 					"userTblDataClm" : userTblDataClm,
 					"userTableDataValue" : userTableDataValue,
 					"currentBalTotal" : $filter('number')(parseInt(currentBalTotal), 2),
 					"totalCrAmount" : $filter('number')(parseInt(totalCrAmount), 2) ,
 					"totalDrAmount" : $filter('number')(parseInt(totalDrAmount), 2) 
 			}
        	
        	//User CR details
        	var crTblWidth, crTblclm = [], crTblValue = [], crAmtTotal = 0, crCurrBalTotal = 0;
        	crTblWidth = [40,'*','*','*'];
        	crTblclm.push(
				{text:'SL.No', style: ['tableHeader','myTableHeader']}, 
				{text:'User Name', style: ['tableHeader','myTableHeader']}, 
				{text: 'Cr Date', style: ['tableHeader','myTableHeader']},
				{text: 'Recharge Amount', style: ['tableHeader','myTableHeader']}
        	)
        	
        	var crSlCount = 1;
        	_.each(result.CRDetils, function(res){
        		crTblValue.push([{
 					text: crSlCount, style :'tableRowFont',
 				},{
 					text: res.userName, style :'tableRowFont',
 				},{
 					text: moment(res.CrDate).format("DD-MM-YYYY : HH:mm:ss A"), style :'tableRowFont',
 				},{
 					text: res.topupAmount, style :'tableRowFont',
 				}]);
        		
        		if(res.topupAmount === "null" || res.topupAmount === null ){
        			res.topupAmount = 0
        		}
        		
 				crAmtTotal = crAmtTotal + res.topupAmount;
 				//crCurrBalTotal = crCurrBalTotal + res.currentBalance;
 				crSlCount++;
			});
        	
        	vm.crDetails = {
 					"crTblWidth" : crTblWidth,
 					"crTblclm" : crTblclm,
 					"crTblValue" : crTblValue,
 					"crAmtTotal" :  $filter('number')(parseInt(crAmtTotal ), 2)
 					//"crCurrBalTotal" : crCurrBalTotal
 			}
        	
        	//User DR details
        	var drTblWidth, drTblclm = [], drTblValue = [], drAmtTotal = 0;
        	drTblWidth = [40,70,'*','*','*','*','*'];
        	drTblclm.push(
				{text:'SL.No', style: ['tableHeader','myTableHeader']}, 
				{text:'Invoice No', style: ['tableHeader','myTableHeader']}, 
				{text:'User Name', style: ['tableHeader','myTableHeader']}, 
				{text: 'Date', style: ['tableHeader','myTableHeader']},
				{text: 'Kiosk Name', style: ['tableHeader','myTableHeader']},
				{text: 'Operator', style: ['tableHeader','myTableHeader']},
				{text: 'Amount', style: ['tableHeader','myTableHeader']})
        	
        	var drSlCount = 1;
        	_.each(result.DRDetils, function(res){
        		drTblValue.push([{
 					text: drSlCount, style :'tableRowFont',
 				},{
 					text: res.orderNumber, style :'tableRowFont',
 				},{
 					text: res.userName, style :'tableRowFont',
 				},{
 					text: moment(res.debitDate).format("DD-MM-YYYY : HH:mm:ss A"), style :'tableRowFont',
 				},{
 					text: vm.dockDoorName, style :'tableRowFont',
 				},{
 					text: vm.userName, style :'tableRowFont',
 				},{
 					text: res.debitAmount, style :'tableRowFont',
 				}]);
        		if(res.debitAmount === "null" || res.debitAmount === null ){
        			res.debitAmount = 0
        		}
 				drAmtTotal = drAmtTotal + res.debitAmount;
 				drSlCount++;
			});
        	
        	vm.drDetails = {
 					"drTblWidth" : drTblWidth,
 					"drTblclm" : drTblclm,
 					"drTblValue" : drTblValue,
 					"drAmtTotal" : $filter('number')(parseInt(drAmtTotal), 2) 
 			}
        	
			CustomService.posSettlementReport(vm.userSummaryDetails, vm.crDetails, vm.drDetails, vm.otherData, mainUserDetails);
		};
		
		vm.onErrorGetReportDetails = function(response){
			console.log(response.data)
		};
		
		vm.confirmBtn = function(){
			swal({
	            title: "Are you sure?",
	           text: "do you want to delete" + vm.detectedUser.userName ,
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Yes, delete it!",
	            cancelButtonText: "No, cancel please!",
	            closeOnConfirm: false,
	            closeOnCancel: false },
	            function(isConfirm){
	              if (isConfirm) {
	            	  if(vm.detectedUser.userId){
	            		  vm.requestParams = {
		            			  "userId": vm.detectedUser.userId,
		            			  "userType":vm.detectedUser.userType,
		            			  "companyId":vm.companyId 
		            	  }
		            	  UserService.deleteUserList(vm.requestParams, vm.deleteUserListSuccess, vm.deleteUserListError);
	            	  }
	              }
	              else {
	            	  swal("Cancelled", "Your imaginary file is safe :)", "error");
	              }
	            });
		};
		
		vm.deleteUserListSuccess = function(response){
			if (response.data.resCode === "res0000") {
				swal("Deleted!", response.data.resMsg,"success");
				vm.getUserslist();
			} else if (response.data.resCode === "res0100") {
				swal("Not deleted!", response.data.resMsg, "error");
			}else{
				
			}
		};
		
		vm.deleteUserListError = function(){
			console.log("user not deleted");
		};
		
		var promise = $interval(function(){
			vm.getUserslist();
		},500)
		
		vm.stop = function(){
			$interval.cancel(promise)
		};
		
		$scope.$on('$destroy', function() {
			vm.stop();
		});
		
		function getDataUri(url, callback) {
            var image = new Image();
            image.onload = function () {
                var canvas = document.createElement('canvas');
                canvas.width = this.naturalWidth; 
                canvas.height = this.naturalHeight; 
                canvas.getContext('2d').drawImage(this, 0, 0);
                callback(canvas.toDataURL('image/png'));
            };
            image.src = url;
        }

        /*vm.dolpLogoBase64 = "";
        getDataUri(imgToExport.src, function(dataUri) {
        	vm.dolpLogoBase64 = dataUri;
        });*/
        
		angular.element(document).ready(function () {
			imgToExport = document.getElementById('imgToExport');
			getDataUri(imgToExport.src, function(dataUri) {
	        	vm.dolpLogoBase64 = dataUri;
	        });
	    });
		
       /* addEventListener('load', load, false);
        function load(){
            var el = document.getElementById("imgToExport");
            alert(el);
        }*/
		
		vm.getUserslist();
		vm.getUserDetailsField();
	}]);
})();
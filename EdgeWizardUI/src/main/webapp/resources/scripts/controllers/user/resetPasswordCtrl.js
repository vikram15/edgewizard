(function(){
	'use strict';
	angular.module('rfidDemoApp').
	controller('ResetPasswordCtrl', ['$uibModalInstance','UserService','userData', 
	                                 function($uibModalInstance, UserService, userData){
		var vm = this ;
		vm.tableHeader = "Password Reset";
		vm.textBoxSelectedNum = ""
		vm.currentPassword = "";
		vm.newPassword = "";
		vm.confirmNewPassword = "";
		
		if(userData.passwordRequired === 1){
			vm.enablePassword = 1;
		}else{
			vm.enablePassword = 0;
		}
		
		vm.numberBtn = function(data){
			if(data !== "clear"){
				switch(vm.textBoxSelectedNum) {
			    case 1:
			    	vm.currentPassword += data;
			    	break;
			    case 2:
			    	vm.newPassword += data;
			    	break;
			    case 3:
			    	vm.confirmNewPassword += data;
			    	break;
				}
			}else{
				switch(vm.textBoxSelectedNum) {
			    case 1:
			    	vm.currentPassword = "";
			    	break;
			    case 2:
			    	vm.newPassword = "";
			    	break;
			    case 3:
			    	vm.confirmNewPassword = "";
			    	break;
				}
			}
		};
		
		vm.trChange = function(data){
			vm.textBoxSelectedNum = data;
		};
		
		vm.modifyBtn = function(){
			if(vm.setNewPass === false || vm.setNewPass === undefined){
				vm.setNewPass = false;
			}
			
			vm.requestParams = {
				 "posPasswordAmtAcitvity":{
				  "userId":userData.userId,
				  "password": vm.newPassword,
				  "isNewPasswordSet":vm.setNewPass,
				  "modifiedDate" : moment(new Date().valueOf())
				 }
			}
			if(vm.setNewPass === false){
				vm.requestParams.posPasswordAmtAcitvity.oldPassword = vm.currentPassword; 
			}
			UserService.modifyTransactionSettingPassword(vm.requestParams, vm.onSuccessModifyTransaction, vm.onErrorModifyTransaction)
		};
		
		vm.onSuccessModifyTransaction = function(response){
			if(response.data.resCode === "res0000"){
				swal("Successfully", "Modified Successfully", "success");
				$uibModalInstance.close();
			}else{
				swal("Error", response.data.resMsg, "error");
			}
		}; 
		
		vm.onErrorModifyTransaction = function(response){
			console.log(response);
		};
		
		vm.updateSettingBtn = function(){
			vm.requestParams = {
				 "posPasswordAmtAcitvity":{
				  "userId":userData.userId,
				  "passwordRequired":vm.enablePassword,
				  "modifiedDate" : moment(new Date().valueOf())
				 }
			}
			UserService.updateSettingForTransactionPassowrd(vm.requestParams, vm.onSuccessUpdateSetting, vm.onErrorUpdateSetting)
		};
		
		vm.onSuccessUpdateSetting = function(response){
			if(response.data.resCode === "res0000"){
				swal("Successfully", "Update setting successfully!", "success");
				$uibModalInstance.close();
			}else{
				swal("Error", response.data.resMsg, "error");
			}
		};
		
		vm.onErrorUpdateSetting = function(response){
			console.log(response);
		}
		
		vm.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}]);
})();
(function(){
  'use strict';
  angular.module('rfidDemoApp.controller')
  .controller('UserSearchController',['$uibModalInstance','UserService','AuthService','MasterService','$rootScope', 
                                      function($uibModalInstance, UserService, AuthService, MasterService, $rootScope){
    var vm = this ;
    vm.tableHeader = "User Search";
    vm.searchBy = "User Id";
    
    //pagination
    vm.currentPage = 1;
    vm.maxSize = 10;
    vm.selectAssetType = "EPC ID";
	vm.comp_Id = AuthService.getCompanyId();

    vm.userDetails = function(){
    	console.log("user details");
    	vm.requestParams = {
    		 "searchBy" : vm.searchBy,
    		 "searchType" : 1,
    		 "searchValue":	vm.searchValue,
    		 "currentPage":vm.currentPage,
    		 "companyId":vm.comp_Id
    	};
    	UserService.getUserSearchData(vm.requestParams, vm.getUserSearhSuccess, vm.getUserSearchError);
    };

    vm.getUserSearhSuccess = function(response){
    	vm.user_data = response.data;
    	vm.totalItems = vm.user_data.resData.totalUserSearch;
    	vm.itemsPerPage = vm.user_data.resData.RecordPerPage;
    };

    vm.getUserSearchError = function(response){

    };
    
    vm.userSelectFun = function(){
    	
    };
    
    vm.getDivisionDetails = function() {
		var requestParams = {
			"companyId" : vm.comp_Id
		};
		MasterService.DivisionDetailService(requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
	};

	vm.onDivisionInfoSuccess = function(response) {
		vm.divisionList = response.data;
		/*vm.categoryType = vm.category_list.resData[0].categoryPrimery.categoryId;*/
	};

	vm.onDivisionInfoError = function(response) {
		/*console.log("error");*/
	};
    
    
    vm.categoryList = function() {
        vm.requestParams = {
            "categoryPrimery": {
                "category": 2,
                "companyId": vm.comp_Id
            }
        };
        MasterService.getCategoryAll(vm.requestParams, vm.getCategoryAllSucc, vm.getCategoryAllError)
    };

    vm.getCategoryAllSucc = function(response) {
        vm.category_list = response.data;
        vm.categoryType = vm.category_list.resData[0].categoryPrimery.categoryId;
    };

    vm.getCategoryAllError = function(response) {
        console.log("error");
    };
    
    vm.getSectorDetails = function(){
        vm.requestParams = {
            "companyId": vm.comp_Id
        }
        MasterService.SectorListService(vm.requestParams, vm.onSectorSuccess, vm.onSectorError);
    }

    vm.onSectorSuccess = function(response){
      vm.sect_data = response.data;
    }

    vm.onSectorError = function(){

    };

    vm.getLocationDetails = function(){
    	vm.requestParams = {
    			"companyId": vm.comp_Id
    	}
    	MasterService.LocationListService(vm.requestParams, vm.onLSuccess, vm.onLError);
    };

    vm.onLSuccess = function(response){
      vm.loc_data = response.data;
    };

    vm.onLError = function(response){

    };

    vm.getDepartmentDetails = function(){
      vm.requestParams = {
    			"companyId": vm.comp_Id
    	}
    	MasterService.DepartmentListService(vm.requestParams, vm.onDepSuccess, vm.onDepError);
    };

    vm.onDepSuccess = function(response){
      vm.depart_data = response.data;
    };

    vm.onDepError = function(response){

    };
    
    vm.userSelectFun = function(){
    	if(vm.userSelect !== undefined){
			$uibModalInstance.dismiss(vm.userSelect);
			$rootScope.$broadcast("userInfo", vm.userSelect);
		}
    };
    
    /*========ANGULAR DATE PICKER============*/
	vm.today = function() {
		vm.fromDate = new Date();
		vm.toDate = new Date();
	};
	vm.today();

	vm.clear = function() {
		vm.fromDate = null;
		vm.toDate = null;
	};

	vm.dateOptions = {
			//dateDisabled: disabled,
			formatYear: 'yy',
			//maxDate: new Date(2020, 5, 22),
			minDate: new Date(2000, 5, 22),
			startingDay: 1
	};

	// Disable weekend selection
	function disabled(data) {
		var date = data.date,
		mode = data.mode;
		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	}

	vm.fromOpen = function(){
		vm.from.opened = true;
	};
  
	vm.toOpen = function(){
		vm.to.opened = true;  
	};
  
	vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	vm.format = vm.formats[0];
	vm.altInputFormats = ['M!/d!/yyyy'];
	
	vm.from = {
			opened: false
	};
	vm.to = {
			opened: false
	};
	
  function getDayClass(data) {
	  var date = data.date,
	  mode = data.mode;
	  if (mode === 'day') {
		  var dayToCheck = new Date(date).setHours(0,0,0,0);
		  
		  for (var i = 0; i < $scope.events.length; i++) {
			  var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
	
			  if (dayToCheck === currentDay) {
				  return $scope.events[i].status;
			  }
		  }
	  }
	
	  return '';
  }

  vm.clearFields = function(){
	 vm.searchValue = "";
  };
  
  vm.cancel= function(){
	  $uibModalInstance.dismiss('cancel');
  };
    
  vm.userDetails();
  }]);
})();

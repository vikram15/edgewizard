(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('DockDoorCtrl', ['$uibModal', function($uibModal){
		var vm = this;
		
		vm.dockDoorAddUpdate = function(data){
			var modalInstance = $uibModal.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/masterData/dockDoorAddUpdate.html',
				controller : 'DockDoorAddUpdateCtrl',
				controllerAs : 'dockDoorAddUpdateCtrl',
				resolve : {
					dockdoorData : function(){
						return angular.copy(data);
					}
				}
			});
	    			
			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				/*$log.info('Modal dismissed at: '+ new Date());*/
			});
		};
		
	}]);
})();
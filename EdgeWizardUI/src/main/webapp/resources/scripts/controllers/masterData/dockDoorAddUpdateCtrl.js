(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('DockDoorAddUpdateCtrl', ['$uibModalInstance','$scope','dockdoorData',
	                                      function($uibModalInstance, $scope, dockdoorData){
		var vm = this;
		console.log("dockDoorAddUpdateCtrl");
		console.log(dockdoorData);
		
		if(dockdoorData){
			vm.tableHeader = "Dockdoor Edit";
			vm.saveBtn = "Update";
		}else{
			vm.tableHeader = "Dockdoor Add";
			vm.saveBtn = "Add";
		}
		
		
		// array for dropped items
	    $scope.dropped = [];

	    // array of items for dragging
	    $scope.items = [
	        {id: 1, name: "Microwave"}, 
	        {id: 2, name: "Dishwasher" },
	        {id: 3, name: "Phone" },
	        {id: 4, name: "Punching Bag" }
	    ];

	    $scope.moveToBox = function(id) {

	        for (var index = 0; index < $scope.items.length; index++) {

	            var item = $scope.items[index];
	                
	            if (item.id == id) {
	                // add to dropped array
	                $scope.dropped.push(item);

	                // remove from items array
	                $scope.items.splice(index, 1);
	            }
	        }

	        $scope.$apply();
	    };
	    
	    $scope.moveTofirstDiv = function(id){
	    	for (var index = 0; index < $scope.dropped.length; index++) {

	            var dropped = $scope.dropped[index];
	                
	            if (dropped.id == id) {
	                // add to dropped array
	                $scope.items.push(dropped);
	                
	                // remove from items array
	                $scope.dropped.splice(index, 1);
	            }
	        }

	        $scope.$apply();
	    }
	    
	    vm.save = function(){
	    	 alert($scope.items.length + " items left.");
	    	 alert($scope.dropped.length + " items in drop-box.");
	    }

	    /*$scope.showItmesLeft = function () {
	        alert($scope.items.length + " items left.");
	    };
	    
	    $scope.showItmesDropped = function () {
	        alert($scope.dropped.length + " items in drop-box.");
	    };*/
		
		vm.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}]);
})();
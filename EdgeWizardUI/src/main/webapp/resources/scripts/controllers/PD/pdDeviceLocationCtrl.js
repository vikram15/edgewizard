(function(){
	'use strict';
	 angular.module('rfidDemoApp.controller')
	.controller('PdDeviceLocationCtrl',['$stateParams','PickUpDropService','$interval','$scope', 
	                                    function($stateParams, PickUpDropService, $interval, $scope){
		var vm = this;
		var map;
		var myCenter = new google.maps.LatLng(18.551516, 73.938830);
		vm.deviceName = "";
		var promise;
		
		//Map icons
		var redBusIcon = "resources/images/PD/bus_red.png";
		var infowindow = new google.maps.InfoWindow();
		
		vm.deviceInfo = function(){
			vm.deviceId = {
					"deviceId":$stateParams.deviceId
			}
			PickUpDropService.getDeviceInfo(vm.deviceId, vm.getDeviceSuccess, vm.getDeviceError);
		};
		
		vm.getDeviceSuccess = function(response){

			var position = new google.maps.LatLng(response.data.resData.latitude, response.data.resData.longitude);
			
			var contentString = '<b>Device Name : ' + vm.deviceName + '</b><br> Date :' + moment(response.data.resData.createdDttime).format("DD-MMM-YYYY");
			var marker = new google.maps.Marker({
				position: position,
				map: map,
				icon: redBusIcon,
				title: vm.deviceName,
				// zIndex: Math.round(latlng.lat() * -100000) << 5
			});

			google.maps.event.addListener(marker, 'click', function() {
				infowindow.setContent(contentString);
				infowindow.open(map, marker);
			});
		};
		
		vm.getDeviceError = function(response){
			console.log("Error in device info");
		};
		
		vm.initialize = function(){
			var myOptions = {
					zoom: 15,
					center: myCenter,
					mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			map = new google.maps.Map(document.getElementById("googleMap"),myOptions);
		};
		
		if($stateParams.deviceId){
			vm.deviceName =  $stateParams.deviceName;
			vm.deviceInfo();
			promise = $interval(function(){
				vm.deviceInfo();
			},2000)
		}
		
		vm.stop = function(){
			$interval.cancel(promise)
		};
		
		$scope.$on('$destroy', function() {
			console.log("$destroy called");
			vm.stop();
		});
		
	}]);
})();
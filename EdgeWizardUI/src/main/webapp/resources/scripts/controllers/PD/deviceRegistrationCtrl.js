(function(){
	angular.module('rfidDemoApp.controller')
	.controller('DeviceRegistrationCtrl' ,['$uibModal' , function($uibModal){
		var vm = this;
		console.log("DeviceRegistrationCtrl");
		
		vm.devicAddUpdate = function(data){
			var modalInstance = $uibModal.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/PD/deviceAddUpdate.html',
				controller : 'DeviceAddUpdateCtrl',
				controllerAs : 'deviceAddUpdateCtrl',
				resolve : {
					deviceData : function() {
						return angular.copy(data);
					}
				}
			});

			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				//vm.getDivisionDetails();
			});
		};
		
		
	}]);
})();
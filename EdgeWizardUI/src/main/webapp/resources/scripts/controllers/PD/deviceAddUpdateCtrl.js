(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('DeviceAddUpdateCtrl', ['deviceData','$uibModalInstance', function(deviceData, $uibModalInstance){
		var vm = this;
		vm.tableHeader = "Device Add";
		
		
		vm.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}]);
})();
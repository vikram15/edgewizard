(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('PdDashboardCtrl',['$scope','PickUpDropService','AuthService','$timeout','CustomService','$localStorage','$state','$rootScope','$uibModal', 
	                               function($scope, PickUpDropService, AuthService, $timeout, CustomService, $localStorage, $state, $rootScope, $uibModal){
		console.log("PdDashboard")
		var vm = this;
		$scope.globalUserName = AuthService.getUserName();
		vm.globalCompName = AuthService.getCompanyName();
		$scope.autoScroll = true;
		$scope.readerDlg = true;
		
		var map ;
		var waypoints = [];
        var idealStrtPoint;
        var idealEndPoint;
        var actualStrtPoint;
        var actualEndPoint;
        vm.staticIdealRoutes;
        vm.staticPickupDropList;
        vm.staticActularoutes;
        
		var myCenter = new google.maps.LatLng(18.551516, 73.938830);
		var dataUri ;
		
		
		//Map icons
		var redIcon = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
		var redBusIcon = "resources/images/PD/bus_red.png";
		
		var greenBusIcon = "resources/images/PD/bus_green.png";
		var greenIcon = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
		
		var yellowIcon = "http://www.google.com/mapfiles/marker_yellow.png";
		var journeyLocaionIcon = "resources/images/PD/journey_location.png";
		
		var pirpleIcon = "http://maps.google.com/mapfiles/ms/icons/purple-dot.png";
		var pirpleIconBase64;
		var userBlueIcon = "resources/images/PD/user_blue.png";
		
		var pinkIcon = "http://maps.google.com/mapfiles/ms/icons/pink-dot.png";
		var busIcon = "http://maps.google.com/mapfiles/ms/icons/bus.png";
		
		var orangeIcon = "http://maps.google.com/mapfiles/ms/icons/orange-dot.png";
		var orangeIconBase64;
		var userGreenIcon = "resources/images/PD/user_green.png";
		
		var img = new Image();
		img.crossOrigin="anonymous"
		img.src = orangeIcon;
		img.onload = function () {
        	 var canvas = document.createElement('canvas');
            canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
            canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

            canvas.getContext('2d').drawImage(this, 0, 0);

            // Get raw image data
          //  callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

            // ... or get as Data URI
            orangeIconBase64 = canvas.toDataURL('image/png');
          //  callback(canvas.toDataURL('image/png'));
        };
        
        var pirpleImg = new Image();
        pirpleImg.crossOrigin="anonymous"
        pirpleImg.src = pirpleIcon;
        pirpleImg.onload = function () {
        	 var canvas = document.createElement('canvas');
            canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
            canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

            canvas.getContext('2d').drawImage(this, 0, 0);

            // Get raw image data
          //  callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

            // ... or get as Data URI
            pirpleIconBase64 = canvas.toDataURL('image/png');
          //  callback(canvas.toDataURL('image/png'));
        };
		
		//this function called when page loads 
		vm.initialize = function(){
			var myOptions = {
					zoom: 15,
					center: myCenter,
					mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			map = new google.maps.Map(document.getElementById("googleMap"),myOptions);
		};
		
		var infowindow = new google.maps.InfoWindow();
        var directionsService = new google.maps.DirectionsService();

        //Journey locations with Ideal route map
        var directionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: {
                strokeColor: "#4c4cff",
            },
            //icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
            zoom: 8,
            map: map,
            suppressMarkers: true
        });
        
        //=== Actual route map display =====
        var actualDirectionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: {
                strokeColor: "red",
            },
            //icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
           // zoom: 2,
            map: map,
            suppressMarkers: true
        });
		
		//start device list function
		vm.deviceListFun = function(){
			PickUpDropService.getDeviceList(vm.onSuccessGetDeviceList, vm.onErrorGetDeviceList);
		};
		
		vm.onSuccessGetDeviceList = function(response){
			vm.deviceList = response.data;
			angular.forEach(vm.deviceList.resData,function(value){
				if(value.STATUS === 1){
					value.onof = "online";
				}else{
					value.onof = "offline";
				}
			})
		};

		vm.onErrorGetDeviceList = function(){ 
			
		};
		
		vm.currentPage = 1;
	    vm.maxSize = 5;
		
		//click on device list
	    var activeMenu;
	    vm.divSelect = false;
		vm.deviceSelect = function(d){
			activeMenu = d;
			vm.activeMenu = d;
			vm.journeyListByDeviceId(activeMenu.macId)
		};

		vm.searchJourneyList = function(){
			if(activeMenu){
				vm.divSelect = false;
				vm.journeyListByDeviceId(activeMenu.macId)
			}else{
				vm.divSelect = true;
				$timeout(function(){
					vm.divSelect = false;
				},1500);
				console.log("Please select device list");
			}
		};
			
		//start journey list function
		vm.journeyListByDeviceId = function(deviceId){
			vm.deviceId = {
					"deviceId":deviceId,
					"startSearchDate":moment(vm.fromDate).valueOf(), 
					"endSearchDate": moment(vm.toDate).valueOf(),
					"currentPage": vm.currentPage
			};
			PickUpDropService.getJourneyListByDeviceId(vm.deviceId, vm.onSuccessJourneyList, vm.onErrorJourneyList)
		};

		vm.onSuccessJourneyList = function(response){
			if(response.data.resData){
				vm.journeyList = response.data;
				vm.totalItems = vm.journeyList.resData[0].totalJourneySearch;
				vm.itemsPerPage = vm.journeyList.resData[0].recordPerPage;
			}else{
				vm.journeyList = response.data;
				console.log("ddddd....")
				$state.reload();
			}
			
			//vm.selectedJourney = vm.journeyList.resData[0];
            //vm.routeListFun($scope.selectedJourney);
		};

		vm.onErrorJourneyList = function(){
			console.log("journey list Error" + response);
		};
		
		//Get route list or Ideal routes when click on journey list 
		vm.globJourneyId = "";
		vm.routeListFun = function(journey){
			
			vm.globJourneyId = journey;
			vm.journeySelect = journey;
			vm.initialize();
			waypoints = [];
			actualStrtPoint = "";
			actualEndPoint = "";
			idealStrtPoint = "";
		    idealEndPoint = "";
			
            vm.routeId = {
            		"id":journey.routeId
            };
            vm.journeyId = {
            		"journeyId":journey.journeyid
            };
            PickUpDropService.getActualRouteDetails(vm.journeyId, vm.onSuccessActualrouteList, vm.onErrorActualrouteList)
            PickUpDropService.getRouteDetails(vm.routeId, vm.onSuccessrouteList, vm.onErrorrouteList);
            PickUpDropService.getPickupDropList(vm.journeyId, vm.onSuccessPickupDropList, vm.onErrorPickupDropList);
		};
		
		vm.onSuccessrouteList = function(response){
			if(response.data.resData){
				vm.staticIdealRoutes = response.data.resData;
				vm.staticMapDisplay();
				
				for (var i = 0; i < response.data.resData.length; i++) {
			        if(i === 0){
	                  idealStrtPoint = response.data.resData[i].latitude + "," + response.data.resData[i].longitude;
	                  var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                  createMarker(position, response.data.resData[i].geoFenceName, response.data.resData[i].routeName, greenBusIcon);
	                }
	                if(i != 0 && i != response.data.resData.length - 1){
	                  var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                  createMarker(position, response.data.resData[i].geoFenceName, response.data.resData[i].routeName, journeyLocaionIcon);
	                  waypoints.push({
	                      location: position,
	                      stopover: true
	                  });
	                }
	                if(i == response.data.resData.length - 1){
	                  idealEndPoint = response.data.resData[i].latitude + "," + response.data.resData[i].longitude;

	                  var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                  createMarker(position, response.data.resData[i].geoFenceName, response.data.resData[i].routeName, redBusIcon);

	                  // waypoints.push({
	                  //     location: position,
	                  //     stopover: true
	                  // });
	                }
	              }
				$timeout(function(){
					vm.displayIdealRoute();
				},200)
				
			}
		};
		
		vm.onErrorrouteList = function(response){
			
		};
		
		vm.onSuccessActualrouteList = function(response){
			if(response.data.resData){
				vm.staticActularoutes = response.data.resData;
				vm.staticMapDisplay();
				
				for (var i = 0; i < response.data.resData.length; i++) {
	                if(i === 0){
	                	actualStrtPoint = response.data.resData[i].latitude + "," + response.data.resData[i].longitude;
	                	var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                	createMarker(position, "device Name : " + response.data.resData[i].deviceName, moment(response.data.resData[i].createdDttime).format("DD-MM-YYYY"), greenBusIcon);
	                }
	                
	                if(i == response.data.resData.length - 1){
	                	actualEndPoint = response.data.resData[i].latitude + "," + response.data.resData[i].longitude;
	                	var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                	createMarker(position, "device Name : "+ response.data.resData[i].deviceName, moment(response.data.resData[i].createdDttime).format("DD-MM-YYYY"), redBusIcon);
	                }
				}
				vm.displayActualRoute();
			}
		};
		
		vm.onErrorActualrouteList = function(response){
			
		};
		
		vm.displayIdealRoute = function() {
			
			var IdealStartLatLng = idealStrtPoint.latitude + "," + idealStrtPoint.longitude;
            var IdealEndLatLng = idealEndPoint.latitude + "," + idealEndPoint.longitude;

            var selectedMode = 'DRIVING';
            var request = {
                // London Eye
                origin: idealStrtPoint,
                // Palace of Westminster
                destination: idealEndPoint,
                waypoints: waypoints, //an array of waypoints
                optimizeWaypoints: false,
                // Set our mode of travel - default: walking
                travelMode: google.maps.TravelMode[selectedMode]
            };
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    directionsDisplay.setMap(map);
                } else {
                    alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                }
            });
        };
        
        vm.displayActualRoute = function() {
            var selectedMode = 'WALKING';
            var request = {
                // London Eye
                origin: actualStrtPoint,
                // Palace of Westminster
                destination: actualEndPoint,
                //waypoints: waypoints, //an array of waypoints
                optimizeWaypoints: false,
                // Set our mode of travel - default: walking
                travelMode: google.maps.TravelMode[selectedMode]
            };
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    actualDirectionsDisplay.setDirections(response);
                    actualDirectionsDisplay.setMap(map);
                } else {
                    alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                }
            });
        };
        
        vm.onSuccessPickupDropList = function(response){
        	if(response.data.resData){
        		vm.staticPickupDropList = response.data.resData;
        		vm.staticMapDisplay();
        		for (var i = 0; i < response.data.resData.length; i++) {
	                var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                if(response.data.resData[i].pickupOrDrop === 1){
	                	createMarker(position, "User Name :"+response.data.resData[i].userName, moment(response.data.resData[i].pickDropDttime).format("DD-MM-YYYY"), userGreenIcon);
	                }else{
	                	createMarker(position, "User Name :"+response.data.resData[i].userName, moment(response.data.resData[i].pickDropDttime).format("DD-MM-YYYY"), userBlueIcon);
	                }	
				}
			}
        };
        
        vm.onErrorPickupDropList = function(){
        	
        };
        
        function createMarker(latlng, label, html, url) {
            var contentString = '<b>' + label + '</b><br>' + html;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: url,
                title: label,
                zIndex: Math.round(latlng.lat() * -100000) << 5
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            });
            
        };
		
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
		
		var dt = new Date();
		vm.currentDate = dt;
		vm.currDay = days[dt.getDay()];
		vm.currTime = dt.getTime();
		
		//Display static map
		vm.staticMapDisplay =function(){
			
			if(vm.staticIdealRoutes && vm.staticActularoutes && vm.staticPickupDropList){
        		var staticMapUrl = "https://maps.googleapis.com/maps/api/staticmap";
        		//staticMapUrl += "&markers=icon:"+ busIcon +"|18.54842,73.94643";
        		for(var i = 0;i<vm.staticIdealRoutes.length;i++){
            		if(i === 0){
                		staticMapUrl += "?path=" + vm.staticIdealRoutes[i].latitude + "," + vm.staticIdealRoutes[i].longitude + "|";
            		}
            		if(i !== 0 && i !== vm.staticIdealRoutes.length - 1 ){
            			staticMapUrl += vm.staticIdealRoutes[i].latitude + "," + vm.staticIdealRoutes[i].longitude + "|";
            		}
            		
            		if(i === vm.staticIdealRoutes.length - 1){
            			staticMapUrl += vm.staticIdealRoutes[i].latitude + "," + vm.staticIdealRoutes[i].longitude;
            		}
            	}
        		
        		for(var i = 0;i<vm.staticActularoutes.length;i++){
	        		if(i === 0){
	                //		staticMapUrl += "?path=" + vm.staticActularoutes[i].latitude + "," + vm.staticActularoutes[i].longitude + "|";
	                		staticMapUrl += "&path=color:red|" + vm.staticActularoutes[i].latitude + "," + vm.staticActularoutes[i].longitude + "|";
	        		}
	        		if(i === vm.staticIdealRoutes.length - 1){
	        			staticMapUrl += vm.staticActularoutes[i].latitude + "," + vm.staticActularoutes[i].longitude;
	        		}
            	}
            	
            	for(var i = 0;i<vm.staticIdealRoutes.length;i++){
            		if(i === 0){
                		staticMapUrl += "&markers=icon:"+ greenIcon +"|" + vm.staticIdealRoutes[i].latitude + "," + vm.staticIdealRoutes[i].longitude;
            		}
            		if(i !== 0 && i !== vm.staticIdealRoutes.length - 1 ){
            			staticMapUrl += "&markers=icon:"+ yellowIcon +"|" + vm.staticIdealRoutes[i].latitude + "," + vm.staticIdealRoutes[i].longitude;
            		}
            		if(i === vm.staticIdealRoutes.length - 1){
            			staticMapUrl += "&markers=icon:" + redIcon + "|" + vm.staticIdealRoutes[i].latitude + "," + vm.staticIdealRoutes[i].longitude;
            		}
            	}
            	
            	//Display static map using pickup drop list
            	for(var i = 0; i<vm.staticPickupDropList.length; i++){
            		if(vm.staticPickupDropList[i].pickupOrDrop === 1){
            			//latlangarr.push(vm.staticIdealRoutes[i].latitude + "," + vm.staticIdealRoutes[i].longitude + "|")
                		staticMapUrl += "&markers=icon:"+ orangeIcon +"|" + vm.staticPickupDropList[i].latitude + "," + vm.staticPickupDropList[i].longitude;
            		}
            		else{
            			staticMapUrl += "&markers=icon:"+ pirpleIcon +"|" + vm.staticPickupDropList[i].latitude + "," + vm.staticPickupDropList[i].longitude;
            			//18.54993,73.94117
            		}
            	}
            	
	        	for(var i = 0;i<vm.staticActularoutes.length;i++){
	        		if(i === 0){
	        			staticMapUrl += "&markers=color:green|label:S|" + vm.staticActularoutes[i].latitude + "," + vm.staticActularoutes[i].longitude;
	        		}
	        		if(i === vm.staticActularoutes.length - 1){
	        			staticMapUrl += "&markers=color:red|label:E|" + vm.staticActularoutes[i].latitude + "," + vm.staticActularoutes[i].longitude;
	        		}
	        	}
            		
                //Set the Google Map Size.
                staticMapUrl += "&size=800x800&senser=false";
         
                //Set the Google Map Type.
                staticMapUrl += "&maptype=" + google.maps.MapTypeId.ROADMAP;
                
                //Set the Google Map Zoom.
                staticMapUrl += "&zoom=" + 14;
                staticMapUrl += "&key=AIzaSyCpJ2D4XV_Sy7oxtUiO7azn7DA44E-8qkI";
               /* AIzaSyDtoK3oaTlalv3g3Bj2KwZF6lGELcGsHCM*/
                
                //Loop and add Markers.
                /* for (var i = 0; i < markers.length; i++) {
                    staticMapUrl += "&markers=color:red|" + markers[i].lat + "," + markers[i].lng;
                }*/
                //staticMapUrl += "&markers=color:red|" + "18.551516,73.938830 | 18.549172, 73.943181";
                //Display the Image of Google Map.
                /*vm.imgUrl = staticMapUrl;
                var imgMap = document.getElementById("imgMap");
                imgMap.src = staticMapUrl;
                imgMap.style.display = "block";*/
            	
                //vm.imgUrl = staticMapUrl;
                var image = new Image();
                image.crossOrigin="anonymous"
               	image.src = staticMapUrl;
                image.onload = function () {
                	 var canvas = document.createElement('canvas');
                    canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
                    canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

                    canvas.getContext('2d').drawImage(this, 0, 0);

                    // Get raw image data
                    //  callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));

                    // ... or get as Data URI
                    dataUri = canvas.toDataURL('image/png');
                    //  callback(canvas.toDataURL('image/png'));
                };
        	}
        };
        
        vm.generatePDF = function(journey){
        	var column = [];
        	var value = [];
        	var column1 = [];
        	var value1 = [];
        	var tdWidth = [30, '*', '*','*','*','*'];
        	
        	column.push({text: 'Sl.No', style: ['tableHeader','myTableHeader']},
        			{text: 'User Id', style: ['tableHeader','myTableHeader']},
        			{text: 'User Name', style: ['tableHeader','myTableHeader']},
        			{text: 'Pickup Latitude', style: ['tableHeader','myTableHeader']},
        			{text: 'Pickup Langitude', style: ['tableHeader','myTableHeader']},
        			{text: 'Pickup Time', style: ['tableHeader','myTableHeader']},
        			{text: 'markers', style: ['tableHeader','myTableHeader']}
        	);
        	
        	column1.push({text: 'Sl.No', style: ['tableHeader','myTableHeader']},
        			{text: 'User Id', style: ['tableHeader','myTableHeader']},
        			{text: 'User Name', style: ['tableHeader','myTableHeader']},
        			{text: 'Dropup Latitude', style: ['tableHeader','myTableHeader']},
        			{text: 'Dropup Langitude', style: ['tableHeader','myTableHeader']},
        			{text: 'Drop Time', style: ['tableHeader','myTableHeader']},
        			{text: 'markers', style: ['tableHeader','myTableHeader']}
        	);
					
        	if(vm.staticPickupDropList){
        		var k = 1; 
        		var l = 1;
         		angular.forEach(vm.staticPickupDropList, function(data){
         			if(data.pickupOrDrop === 1){
         				value.push([{
         	        		text: k ,style : 'tableRowFont',fillColor: 'green',color : 'white',
         	        	},{
         					text: data.userId ,style : 'tableRowFont',fillColor: 'green',color : 'white',
         				},{
         					text: data.userName ,style : 'tableRowFont',fillColor: 'green',color : 'white',
         				},{
         					text: data.latitude ,target : '_blank', link: 'http://maps.google.com/?q='+ data.latitude +"," + data.longitude,style : 'tableRowFont',fillColor: 'green',color : 'white',
         				},{
         					text: data.longitude, target : '_blank', link: 'http://maps.google.com/?q='+ data.latitude +"," + data.longitude, style : 'tableRowFont',fillColor: 'green',color : 'white',
         				},{
         					text: moment(data.pickDropDttime).format("DD-MM-YYYY") ,style : 'tableRowFont',fillColor: 'green',color : 'white',
         				},{
         					image: orangeIconBase64,width:20,
         				}]);
         				k++;
         			}else{
         				value1.push([{
         	        		text: l ,style : 'tableRowFont',fillColor: '#F08080',color : 'white',
         	        	},{
         					text: data.userId ,style : 'tableRowFont',fillColor: '#F08080',color : 'white',bold: true,
         				},{
         					text: data.userName ,style : 'tableRowFont',fillColor: '#F08080',color : 'white',
         				},{
         					text: data.latitude ,target : '_blank', link: 'http://maps.google.com/?q='+ data.latitude +"," + data.longitude,style : 'tableRowFont',fillColor: '#F08080',color : 'white',
         				},{
         					text: data.longitude ,target : '_blank', link: 'http://maps.google.com/?q='+ data.latitude +"," + data.longitude,style : 'tableRowFont',fillColor: '#F08080',color : 'white',
         				},{
         					text: moment(data.pickDropDttime).format("DD-MM-YYYY") ,style : 'tableRowFont',fillColor: '#F08080',color : 'white',
         				},{
         					image: pirpleIconBase64,width:20,
         				}]);
         				l++;
         			}
         		});
         	}else{
         		console.log("No static pickup drop list");
         	}
        	
        	vm.otherData = {
        			"titleName" :"Journey Report",
        			"currDay" : vm.currDay,
 	    			"currentDate" : vm.currentDate,
 	    			"currTime" : vm.currTime,
 	    			"base64img" : dataUri,
 	    			"globalCompName" : vm.globalCompName,
 	    			"userName" : $scope.globalUserName,
 	    			"journeyId" : journey.journeyid,
 	    			"noOfLocations" : vm.staticIdealRoutes.length,
 	    			"route":vm.staticIdealRoutes[0].routeName,
 	    			"jStartDate" : moment(journey.startDateTime).format("DD-MM-YYYY"),
 	    			"jEndDate":moment(journey.endDateTime).format("DD-MM-YYYY"),
 	    			"operatorName" : journey.userName,
 	    			"userPickupInJrny":k - 1,
 	    			"userDroppedInJrny":l - 1,
 	    	};
        	 
        	if(dataUri){
        		CustomService.pickupDropPdfService(column, value, tdWidth, vm.otherData,column1, value1);
        	}
        };
		
        /*========ANGULAR DATE PICKER============*/
    	vm.today = function() {
    		vm.fromDate = "SELECT";
    		vm.toDate = "SELECT";
    	};
    	vm.today();
    	
    	vm.clear = function() {
    		vm.fromDate = null;
    		vm.toDate = null;
    	};

    	vm.dateOptions = {
    			//dateDisabled: disabled,
    			formatYear: 'yy',
    			//maxDate: new Date(2020, 5, 22),
    			minDate: new Date(2000, 5, 22),
    			startingDay: 1
    	};

    	// Disable weekend selection
    	function disabled(data) {
    		var date = data.date,
    		mode = data.mode;
    		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    	}

    	vm.fromOpen = function(){
    		vm.from.opened = true;
    	};
			  
    	vm.toOpen = function(){
    		vm.to.opened = true;  
    	};
			  
    	vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    	vm.format = vm.formats[0];
    	vm.altInputFormats = ['M!/d!/yyyy'];
    	
    	vm.from = {
    		opened: false
    	};
    	vm.to = {
    			opened: false
    	};
				
    	function getDayClass(data) {
    		var date = data.date,
    		mode = data.mode;
    		if (mode === 'day') {
    			var dayToCheck = new Date(date).setHours(0,0,0,0);
						
    			for (var i = 0; i < $scope.events.length; i++) {
    				var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
							
    				if (dayToCheck === currentDay) {
    					return $scope.events[i].status;
    				}
    			}
    		}
				
    		return '';
    	}
    	
    	vm.tab = 1;
    	vm.setTab = function(tabId){
    		vm.tab = tabId;
    	};
    	
    	vm.isSet = function (tabId) {
            return vm.tab === tabId;
        };
    	
		//call function to get device list
        vm.deviceListFun();
        //call function to get journey list
        //vm.journeyListByDeviceId();
	}]);
})();
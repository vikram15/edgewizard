(function(){
	'use strict';
	 angular.module('rfidDemoApp.controller')
	.controller('PdFullMapCtrl',['$stateParams','PickUpDropService','$timeout', 
	                             function($stateParams,PickUpDropService, $timeout){
		console.log("PdFullMapCtrl");
		var vm = this;
		vm.journeyId = JSON.parse($stateParams.journeyID);
		vm.routeId = JSON.parse($stateParams.routeID);
		
		var map ;
		var myCenter = new google.maps.LatLng(18.551516, 73.938830);
		
		var waypoints = [];
		var idealStrtPoint;
		var idealEndPoint;
		var actualStrtPoint;
		var actualEndPoint;
		
		//Map icons
		var redIcon = "http://maps.google.com/mapfiles/ms/icons/red-dot.png";
		var redBusIcon = "resources/images/PD/bus_red.png";
		
		var greenBusIcon = "resources/images/PD/bus_green.png";
		var greenIcon = "http://maps.google.com/mapfiles/ms/icons/green-dot.png";
		
		var yellowIcon = "http://www.google.com/mapfiles/marker_yellow.png";
		var journeyLocaionIcon = "resources/images/PD/journey_location.png";
		
		var pirpleIcon = "http://maps.google.com/mapfiles/ms/icons/purple-dot.png";
		var userBlueIcon = "resources/images/PD/user_blue.png";
		
		var pinkIcon = "http://maps.google.com/mapfiles/ms/icons/pink-dot.png";
		var busIcon = "http://maps.google.com/mapfiles/ms/icons/bus.png";
		
		var orangeIcon = "http://maps.google.com/mapfiles/ms/icons/orange-dot.png";
		var userGreenIcon = "resources/images/PD/user_green.png";
		
		
		//this function called when page loads 
		vm.mapLaod = function(){
			var myOptions = {
					zoom: 15,
					center: myCenter,
					mapTypeId: google.maps.MapTypeId.ROADMAP
			};
			map = new google.maps.Map(document.getElementById("googleMap"),myOptions);
		};
		
		var infowindow = new google.maps.InfoWindow();
        var directionsService = new google.maps.DirectionsService();

        //Journey locations with Ideal route map
        var directionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: {
                strokeColor: "#4c4cff",
            },
            //icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
            zoom: 8,
            map: map,
            suppressMarkers: true
        });
        
        //=== Actual route map display =====
        var actualDirectionsDisplay = new google.maps.DirectionsRenderer({
            polylineOptions: {
                strokeColor: "red",
            },
            //icon: "http://maps.google.com/mapfiles/ms/icons/blue-dot.png",
           // zoom: 2,
            map: map,
            suppressMarkers: true
        });
		
		vm.initialize = function(){
			vm.routeId = {
            		"id":vm.routeId
            };
            vm.journeyId ={
            		"journeyId":vm.journeyId
            };
			PickUpDropService.getRouteDetails(vm.routeId, vm.onSuccessrouteList, vm.onErrorrouteList);
			PickUpDropService.getActualRouteDetails(vm.journeyId, vm.onSuccessActualrouteList, vm.onErrorActualrouteList)
			PickUpDropService.getPickupDropList(vm.journeyId, vm.onSuccessPickupDropList, vm.onErrorPickupDropList);
		}
		
		vm.onSuccessrouteList = function(response){
			if(response.data.resData){
				
				for (var i = 0; i < response.data.resData.length; i++) {
			        if(i === 0){
	                  idealStrtPoint = response.data.resData[i].latitude + "," + response.data.resData[i].longitude;
	                  var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                  createMarker(position, response.data.resData[i].geoFenceName, response.data.resData[i].routeName, greenBusIcon);
	                }
	                if(i != 0 && i != response.data.resData.length - 1){
	                  var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                  createMarker(position, response.data.resData[i].geoFenceName, response.data.resData[i].routeName, journeyLocaionIcon);
	                  waypoints.push({
	                      location: position,
	                      stopover: true
	                  });
	                }
	                if(i == response.data.resData.length - 1){
	                  idealEndPoint = response.data.resData[i].latitude + "," + response.data.resData[i].longitude;

	                  var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                  createMarker(position, response.data.resData[i].geoFenceName, response.data.resData[i].routeName, redBusIcon);
	                }
	              }
				$timeout(function(){
					vm.displayIdealRoute();
				},200)
			}
		}
		
		vm.onErrorrouteList = function(response){
			
		};
		
		vm.onSuccessActualrouteList = function(response){
			if(response.data.resData){
				for (var i = 0; i < response.data.resData.length; i++) {
	                if(i === 0){
	                	actualStrtPoint = response.data.resData[i].latitude + "," + response.data.resData[i].longitude;
	                	var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                	createMarker(position, "deviceId : " + response.data.resData[i].deviceId, moment(response.data.resData[i].createdDttime).format("DD-MM-YYYY"), greenBusIcon);
	                }
	                
	                if(i == response.data.resData.length - 1){
	                	actualEndPoint = response.data.resData[i].latitude + "," + response.data.resData[i].longitude;
	                	var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
	                	createMarker(position, "deviceId : "+ response.data.resData[i].deviceId, moment(response.data.resData[i].createdDttime).format("DD-MM-YYYY"), redBusIcon);
	                }
				}
				vm.displayActualRoute();
			}
		};
		
		vm.onErrorActualrouteList = function(response){
			
		};
		
		vm.onSuccessPickupDropList = function(response){
        	if(response.data.resData){
        		for (var i = 0; i < response.data.resData.length; i++) {
        			var position = new google.maps.LatLng(response.data.resData[i].latitude, response.data.resData[i].longitude);
        			if(response.data.resData[i].pickupOrDrop === 1){
        				createMarker(position, "userId :"+response.data.resData[i].userName, moment(response.data.resData[i].pickDropDttime).format("DD-MM-YYYY"), userGreenIcon);
        			}else{
        				createMarker(position, "userId :"+response.data.resData[i].userName, moment(response.data.resData[i].pickDropDttime).format("DD-MM-YYYY"), userBlueIcon);
        			}	
        		}
        	}
		};
        
		vm.onErrorPickupDropList = function(){
			
		};
		
        vm.displayIdealRoute = function() {
			
        	var selectedMode = 'DRIVING';
        	var request = {
                // London Eye
                origin: idealStrtPoint,
                // Palace of Westminster
                destination: idealEndPoint,
                waypoints: waypoints, //an array of waypoints
                optimizeWaypoints: false,
                // Set our mode of travel - default: walking
                travelMode: google.maps.TravelMode[selectedMode]
            };
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                    directionsDisplay.setMap(map);
                } else {
                    alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                }
            });
        };
        
        vm.displayActualRoute = function() {
            var selectedMode = 'WALKING';
            var request = {
                // London Eye
                origin: actualStrtPoint,
                // Palace of Westminster
                destination: actualEndPoint,
                //waypoints: waypoints, //an array of waypoints
                optimizeWaypoints: false,
                // Set our mode of travel - default: walking
                travelMode: google.maps.TravelMode[selectedMode]
            };
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    actualDirectionsDisplay.setDirections(response);
                    actualDirectionsDisplay.setMap(map);
                } else {
                    alert("Directions Request from " + start.toUrlValue(6) + " to " + end.toUrlValue(6) + " failed: " + status);
                }
            });
        };
        
        function createMarker(latlng, label, html, url) {
            var contentString = '<b>' + label + '</b><br>' + html;
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: url,
                title: label,
                zIndex: Math.round(latlng.lat() * -100000) << 5
            });

            google.maps.event.addListener(marker, 'click', function() {
                infowindow.setContent(contentString);
                infowindow.open(map, marker);
            });
            
        };
		
		if(vm.journeyId && vm.routeId){
			vm.initialize();
		}
	}]);
})();
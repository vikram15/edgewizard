(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('AreaCtrl',['$uibModal','AuthService','ReaderServices', function($uibModal, AuthService, ReaderServices){
		var vm = this;
		vm.companyId = AuthService.getCompanyId();
		vm.companyName = AuthService.getCompanyName();
		
		vm.getAreaList = function(){
			vm.requestParams = {
					"companyId": vm.companyId
			}
			ReaderServices.getAllAreaList(vm.requestParams, vm.onSuccessGetAreaList, vm.onErrorGetAreaList)
		};
		
		vm.onSuccessGetAreaList = function(response){
			if(response.data.resCode === 'res0000'){
				vm.areaList = response.data;
			}else{
				console.log(response.data.resMsg);
			}
		};
		
		vm.onErrorGetAreaList = function(response){
			console.log(response);
		};
		
		vm.areaAddBtn = function(data){
			var modalInstance = $uibModal.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/readers/areadAddUpdate.html',
				controller : 'AreadAddUpdateCtrl',
				controllerAs : 'areadAddUpdateCtrl',
				resolve : {
					areaRowData : function() {
						return angular.copy(data);
					}
				}
			});

			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				vm.getAreaList();
			});
		};
		
		vm.deleteAreaBtn = function(data){
			swal({
	            title: "Are you sure?",
	           /* text: "Are you sure want to delete!",*/
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Yes, delete it!",
	            cancelButtonText: "No, cancel please!",
	            closeOnConfirm: false,
	            closeOnCancel: false },
	            function(isConfirm){
	              if (isConfirm) {
	            	  ReaderServices.deleteAreaDetails(data, vm.onSuccessDelete, vm.onErrorDelete);
	              }
	              else {
	            	  swal("Cancelled", "Your imaginary file is safe :)", "error");
	              }
	            });
		};
		
		 vm.onSuccessDelete = function(response){
			 if (response.data.resCode === "res0000") {
				 swal("Deleted!", response.data.resData,"success");
				 vm.getAreaList();
			 } else{
				 swal("Not deleted!", response.data.resData, "error");
			 }
		 };

		 vm.onErrorDelete = function(response){
			 console.log("error");
		};
		
		vm.getAreaList();
		
	}]);
})();
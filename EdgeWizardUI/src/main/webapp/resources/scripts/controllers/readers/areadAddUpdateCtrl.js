(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('AreadAddUpdateCtrl', ['areaRowData','$uibModalInstance','AuthService','ReaderServices','CustomService',
	                                   function(areaRowData, $uibModalInstance, AuthService,ReaderServices, CustomService){
		var vm = this;
		vm.areaInfo = {};
		vm.companyName = AuthService.getCompanyName();
		vm.areaInfo.companyId = AuthService.getCompanyId();
		if(areaRowData){
			vm.tableHeader = "Area Update";
			vm.saveBtn = "Update";
			vm.areaInfo = areaRowData;
			vm.areaInfo.email = CustomService.validationService(vm.areaInfo.email);
			vm.areaInfo.address = CustomService.validationService(vm.areaInfo.address);
			vm.areaInfo.contactNumber = CustomService.validationService(vm.areaInfo.contactNumber);
			vm.areaInfo.contactPerson = CustomService.validationService(vm.areaInfo.contactPerson);
			vm.areaInfo.remark = CustomService.validationService(vm.areaInfo.remark);

		}else{
			vm.tableHeader = "Area Add";
			vm.saveBtn = "Add";
		}

		vm.save = function(){
			if(vm.saveBtn === "Add"){
				ReaderServices.addAreaDetails(vm.areaInfo, vm.onSuccessAddArea, vm.onErrorAddArea);
			}else{
				ReaderServices.updateAreaDetails(vm.areaInfo, vm.onSuccessUpdateArea, vm.onErrorUpdateArea)
			}
		};

		vm.onSuccessAddArea = function(response){
			if (response.data.resCode === "res0000") {
				swal("Success", "Area Add Successfully!", "success");
				$uibModalInstance.dismiss('cancel');
			}else{
				swal("Not Added", response.data.resMsg, "error");
				console.log(response.data.resMsg);
			}
		};

		vm.onErrorAddArea = function(response){
			console.log(response);
		};

		vm.onSuccessUpdateArea = function(response){
			if (response.data.resCode === "res0000") {
				swal("Success", "Area Updated Successfully!", "success");
				$uibModalInstance.dismiss('cancel');
			}else{
				console.log(response.data.resMsg);
			}
		};

		vm.onErrorUpdateArea = function(response){
			console.log(response);
		};

		vm.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};

	}]);
})();

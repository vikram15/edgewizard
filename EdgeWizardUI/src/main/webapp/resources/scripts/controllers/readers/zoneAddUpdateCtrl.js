(function() {
    'use strict';
    angular.module('rfidDemoApp.controller')
        .controller('ZoneAddUpdateCtrl', ['areaRowData', '$uibModalInstance', 'AuthService', 'ReaderServices',
            function(areaRowData, $uibModalInstance, AuthService, ReaderServices) {
                var vm = this;
                vm.zoneInfo = {};
                vm.zoneInfo.companyId = AuthService.getCompanyId();
                //vm.zoneInfo.companyName = AuthService.getCompanyName();

                if (areaRowData) {
                    vm.tableHeader = "Zone Edit";
                    vm.saveBtn = "Update";
                } else {
                    vm.tableHeader = "Zone Add";
                    vm.saveBtn = "Add";
                }

                vm.getAllReaders = function(){
                	vm.requestParams = {
                		"companyId" : vm.zoneInfo.companyId
                	}
                	ReaderServices.getAllAreaList(vm.requestParams,vm.onSuccessAreas, vm.onErrorAreas)
                };
                
                vm.onSuccessAreas = function(response){
                	vm.areaData = response.data;
                };
                
                vm.onErrorAreas = function(response){
                	
                }

                vm.save = function() {
                    console.log(vm.zoneInfo.color.slice(1));
                    vm.zoneInfo.readerAreaId = vm.readerArea.readerAreaId;
                    vm.zoneInfo.readerAreaName = vm.readerArea.readerAreaName;
                    vm.zoneInfo.color = vm.zoneInfo.color.slice(1);
                    var aa = parseInt(vm.zoneInfo.color, 16);
                    alert(aa);
                    if (vm.saveBtn === 'Add') {
                        ReaderServices.addZoneDetails(vm.zoneInfo, vm.onSuccessAddZone, vm.onErrorAddZone);
                    } else {

                    }

                };

                vm.onSuccessAddZone = function(response) {
                    if (response.data.resCode === "res0000") {
                        swal("Success", "Area Add Successfully!", "success");
                        $uibModalInstance.dismiss('cancel');
                    } else {
                        console.log(response.data.resMsg);
                    }
                };

                vm.onErrorAddZone = function(response) {
                    console.log(response.data.resMsg);
                };

                vm.cancel = function() {
                    $uibModalInstance.dismiss('cancel');
                };

                vm.getAllReaders();

            }
        ]);
})();

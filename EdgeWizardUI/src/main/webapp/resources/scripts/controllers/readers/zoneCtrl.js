(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('ZoneCtrl',['$uibModal','AuthService','ReaderServices', function($uibModal, AuthService, ReaderServices){
		var vm = this;
		vm.companyId = AuthService.getCompanyId();


		vm.getAllZones = function(){
			vm.requestParams = {
				'companyId' : vm.companyId
			}
			ReaderServices.getAllZoneDetails(vm.requestParams, vm.onSuccessZoneDetails, vm.onErrorZoneDetails)
		};

		vm.onSuccessZoneDetails = function(response){
			if(response.data.resCode === 'res0000'){
				vm.zoneList = response.data;
			}else{
				console.log(response.data.resMsg);
			}
		};

		vm.onErrorZoneDetails = function(response){
			console.log(response);
		};

		vm.zoneAddBtn = function(data){
			var modalInstance = $uibModal.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/readers/zoneAddUpdate.html',
				controller : 'ZoneAddUpdateCtrl',
				controllerAs : 'zoneAddUpdateCtrl',
				resolve : {
					areaRowData : function() {
						return angular.copy(data);
					}
				}
			});

			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				//vm.getDivisionDetails();
			});
		};

		vm.getAllZones();

	}]);
})();

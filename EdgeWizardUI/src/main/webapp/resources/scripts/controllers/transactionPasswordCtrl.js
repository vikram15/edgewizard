(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('TransactionPasswordCtrl',['$scope','$uibModalInstance','AuthService','userId', 
	                                       function($scope, $uibModalInstance, AuthService, userId){
		var vm = this;
		vm.password = "";
		
		vm.numberBtn = function(data){
			if(data !== "clear"){
				vm.password += data;
			}else{
				vm.password = "";
			}
		};
		
		vm.okBtn = function(){
			vm.requestParams = {
					"userId":userId,
					"password":vm.password
			};
			AuthService.checkTransactionPassword(vm.requestParams, vm.onSuccessCheck, vm.onErrorCheck);
		};
		
		vm.onSuccessCheck = function(response){
			if(response.data.resCode === 'res0000'){
				$uibModalInstance.dismiss();	
			}else{
				swal("Invalid", response.data.resMsg, "error")
			}
		};
		
		vm.onErrorCheck = function(response){
			
		};
		
		vm.cancel = function(){
			$uibModalInstance.close();
		};
	}]);
})();
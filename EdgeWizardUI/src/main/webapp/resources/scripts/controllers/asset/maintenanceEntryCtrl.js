(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('MaintenanceEntryCtrl',['$rootScope', function($rootScope){
		console.log("maintenanceEntryCtrl");
		var vm = this;
		$rootScope.maintTableName = "Maintenance Entry";
		
		vm.nextMaintDtOpen = function(){
			vm.nextMaintDt.opened = true;
		};
		
		vm.nextMaintDt = {
				opened: false
		}
	}]);
})();
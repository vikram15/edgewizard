(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('DockDoorPopUpController',['AuthService','dockDoorService','AssetService','$uibModalInstance','$rootScope','$state','$stateParams','$cookies','$timeout',
	            function( AuthService, dockDoorService, AssetService, $uibModalInstance, $rootScope, $state,$stateParams, $cookies, $timeout){
		
		var vm = this ;
		vm.comp_Id  = AuthService.getCompanyId();
	    vm.comp_name = AuthService.getCompanyName();
	    vm.div = {};
	    vm.id = JSON.parse($cookies.get("globals"));
	    //JSON.parse(vm.id);
	    vm.div.dockDoorId = JSON.stringify(vm.id.dockDoorId);
	    //console.log(vm.id.dockDoorId, AuthService.getDockDoorId());
	    vm.getAllDockDoor = function(){
			vm.requestParams  = {
				"companyId": vm.comp_Id
				/*"dockDoorId":1*/
			};
			dockDoorService.getAllDockDoor(vm.requestParams, vm.onGetDockDoorSuccess, vm.onDockDoorError);
		}
	    vm.selectDockDoor ="";
	    vm.onGetDockDoorSuccess = function(response){
	    	//console.log(response.data,"hhjkguyjhuy")
			vm.dockDoorData = response.data;
		};
		vm.onDockDoorError = function(response){
			console.log("error")
		}
		
		
		vm.dockDoor = function(){
			
			vm.requestParam = {
					"dockDoorId":parseInt(vm.selectDockDoor),
					"companyId" : vm.comp_Id
			}
			//AssetService.readTag(vm.requestParam,vm.readTagSucc, vm.readTagErr)
			//$rootScope.$broadcast("DookDoor", {selectedDockDoor : vm.selectDockDoor});
		}
		
		vm.readTagSucc = function(response) {
			if (response.data.resCode === "res0000") {
				vm.tagId = response.data.resData.readTagId;
				$rootScope.$broadcast("TagId", {tagId : vm.tagId});
				$uibModalInstance.dismiss('cancel');
			}
			if (response.data.resCode === "res0100") {
				swal("Tag error", "Tag is not register", "warning")
			}
		}
		
		vm.setDockDoor = function(){
			
//			$cookies.put('globals', vm.div);
			/*vm.dockDoorId = JSON.parse(vm.div);
			console.log(vm.selectDockDoor,"LLL");*/
			//$rootScope.$broadcast("DookDoorName", {dockDoorName : vm.div.dockDoorName});
			vm.requestParams = {
					 "userName": AuthService.getUserName(),
					 "dockDoorId":vm.div.dockDoorId,
					 "companyId":vm.comp_Id
					}
			dockDoorService.setDefaultDockDoor(vm.requestParams, vm.setDockSuccess, vm.setDockErr)
		}
		
		vm.setDockSuccess = function(response){
			/*console.log(response.data,"hghgjhg");*/
			/*$rootScope.$broadcast("DookDoorName", {dockDoorName : response.data.resData.dockdoorName});
			$rootScope.$broadcast("DookDoor", {selectedDockDoor : vm.div.dockDoorId});*/
			/*vm.setdock = response.data.resData;*/
			var kittensFromLocalStorage = JSON.parse($cookies.get("globals"));
			kittensFromLocalStorage.dockDoorName = response.data.resData.dockdoorName;
			kittensFromLocalStorage.dockDoorId = response.data.resData.doockdoorId;
			$cookies.put("globals",JSON.stringify(kittensFromLocalStorage));
			/*vm.dockDoorName = {'globals':$cookies.get("globals")};*/
			vm.div.dockDoorId = response.data.resData.dockdoorId;
			$uibModalInstance.dismiss('cancel');
			/*$timeout(function(){*/
			$state.reload();
			/*},1000)*/
		}
		vm.setDockErr = function(){
			console.log("Error in set dock door");
		}
		vm.readTagErr = function() {
			console.log("Error in read tag");
		}
		
		vm.cancel = function(){
			$uibModalInstance.dismiss('cancel');
		}
		vm.getAllDockDoor();
		//vm.getDockdoor();
	}]);
})();

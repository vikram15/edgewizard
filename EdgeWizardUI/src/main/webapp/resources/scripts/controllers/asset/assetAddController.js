(function() {
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('AssetAddController', ['$rootScope','AuthService','MasterService','AssetService',	'$state','$stateParams','$uibModal', '$timeout','$scope', '$cookies','_',
							function($rootScope, AuthService, MasterService, AssetService, $state, $stateParams, $uibModal, $timeout, $scope, $cookies, _) {
		
		var vm = this;
		vm.tableHeader = {};
		vm.comp_Id = AuthService.getCompanyId();
		vm.comp_name = AuthService.getCompanyName();
		/*vm.dockDoorName = AuthService.getDockDoorName();*/
		vm.selectDockDoor = AuthService.getDockDoorId();
		//vm.showImage = false;
		vm.dockDoorName = JSON.parse($cookies.get('globals'));
		/*vm.dockDoorName = vm.asss.dockDoorName;*/
		vm.attchment = "";
		vm.grpMasterError = false;
		vm.assetPostMode = '';
		var groupMasID;
		vm.photo = "";
		vm.rack_data = [];
		vm.single_ass;
		
		vm.assetAttachedFileNameData = [];
		vm.assetAttachedMultipartFileData = [];
		var selectedAttachment;
			
		//TEST GROUP MASTER
		vm.myGroupArr = [];
		vm.mySubGroupArr = [];
		
		vm.initialize = function() {
			vm.asset_data = $stateParams.assetID;
			if (vm.asset_data === null || vm.asset_data === '' || vm.asset_data === undefined) {
				vm.assetPostMode = 'NewMode';
				vm.tableHeader = "Asset Registration";
				vm.astBtn = true;
				vm.newLogo = true;
			} else {
				/*console.log("EDIT MODE")*/
				vm.assetPostMode = 'EditMode';
				vm.tableHeader = "Asset Update";
				vm.astBtn = false;
				vm.newLogo = true;
				// vm.editLogo = true;
				vm.requestParams = {
						'assetId' : vm.asset_data,
						'companyId' : vm.comp_Id
				}
				AssetService.assetGetByID(vm.requestParams, vm.viewSingleAssetSucc, vm.viewSingleAssetError);
			}
		}

		vm.getImage = function(photo) {
			vm.requestParams = {
					"companyId" : vm.comp_Id,
					"downloadFrom" : "ASSET",
					"imageId" : $stateParams.assetID.toString(),
					"imageName" : photo,
			};
			if(vm.requestParams.imageName){
				AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
			}
		}
								
		vm.assetImg;
		vm.onGetLogoSuccess = function(response) {
			vm.assetUpdImg = true;
			vm.assetImg = response.data.resData.image;
			
			//new code for image div
			vm.assetLogo = 'data:image/jpeg;base64,' + response.data.resData.image; 
		};
		
		vm.onGetLogoError = function(response) {
			/*console.log("error")*/
		}
		
		vm.viewSingleAssetSucc = function(response) {
			
			
			vm.single_ass = response.data;
			vm.assetAttachment = vm.single_ass.resData[1].assetAttachments.assetDocumentCount;
			vm.docRefAttachment = vm.single_ass.resData[1].assetAttachments.assetDocRefCount;
			vm.totAttachment = vm.single_ass.resData[1].assetAttachments.totalCount;
			
			vm.assetName = vm.single_ass.resData[0][0].assetNm;
			vm.tagId = vm.single_ass.resData[0][0].tagId;
			vm.groupMasterType = vm.single_ass.resData[1].groupMaster;
			vm.subGroupType = vm.single_ass.resData[1].subGroupType;
			
			//TEST Group Master and subGroup Master
			if(vm.single_ass.resData[1].groupMaster === "null" || vm.single_ass.resData[1].groupMaster === null){
				vm.single_ass.resData[1].groupMaster = "";
			}
			vm.myGroupArr = [{
				"gpId" : vm.single_ass.resData[1].groupMasterType,
				"gpNm" : vm.single_ass.resData[1].groupMaster
			}]
			vm.myGroupMst = vm.myGroupArr[0].gpId;

			if(vm.single_ass.resData[1].subGroupType === "null" || vm.single_ass.resData[1].subGroupType === null){
				vm.single_ass.resData[1].subGroupType = "";
			}
			if(vm.single_ass.resData[1].subGroupMaster === "null" || vm.single_ass.resData[1].subGroupMaster === null){
				vm.single_ass.resData[1].subGroupMaster = 0;
			}

			vm.mySubGroupArr = [{
				"subGpId" : vm.single_ass.resData[1].subGroupMaster,
				"subGpNm" : vm.single_ass.resData[1].subGroupType
			}]
			vm.mySubGroupMst = vm.mySubGroupArr[0].subGpId;
			//End
			
			vm.productNo = vm.single_ass.resData[0][0].serialNo;
			
			vm.columnShelf = vm.single_ass.resData[0][0].slaveId;
									
			vm.categoryType = vm.single_ass.resData[0][0].categoryId;
			vm.batchLot = vm.single_ass.resData[0][0].batchId;
			vm.manufacturer = vm.single_ass.resData[0][0].manufactureId;
			vm.vendor = vm.single_ass.resData[0][0].vendorId;
			vm.division = vm.single_ass.resData[0][0].divisionId;
			vm.docrefNo = vm.single_ass.resData[0][0].inDocRefNo;
			vm.assetQuantity = vm.single_ass.resData[0][0].assetQty
			vm.photo = vm.single_ass.resData[0][0].photo;
			vm.unitCost = vm.single_ass.resData[0][0].cost
			vm.description = vm.single_ass.resData[0][0].assetDesc;
			vm.tax = vm.single_ass.resData[0][0].taxValue;
			vm.warrantyStDt = new Date(vm.single_ass.resData[0][0].warrantStDt);
			var warrStDt = moment(vm.warrantyStDt).format("DD-MM-YYYY");
			if(warrStDt === "01-01-1900"){
				vm.warrantyStDt = "";
			}
			vm.warrantyEtDt = new Date(vm.single_ass.resData[0][0].warrantEdDt);
			var warrEtDt = moment(vm.warrantyEtDt).format("DD-MM-YYYY");
			if(warrEtDt === "01-01-1900"){
				vm.warrantyEtDt = "";
			}
			vm.acquisitionDate = new Date(
					vm.single_ass.resData[0][0].aqsDate);
			var acqtDt = moment(vm.acquisitionDate).format("DD-MM-YYYY");
			if(acqtDt === "01-01-1900"){
				vm.acquisitionDate = "";
			}
			vm.expiryDate = new Date(
					vm.single_ass.resData[0][0].expiryDt);
			var expiryDt = moment(vm.expiryDate).format("DD-MM-YYYY");
			if(expiryDt === "01-01-1900"){
				vm.expiryDate = "";
			}
			vm.maintenanceDate = new Date(
					vm.single_ass.resData[0][0].mainDt);
			var maintDt = moment(vm.maintenanceDate).format("DD-MM-YYYY");
			if(maintDt === "01-01-1900"){
				vm.maintenanceDate = "";
			}
			//vm.divChange();
				
			vm.getImage(vm.photo);
		};
								
		vm.viewSingleAssetError = function() {
			/*console.log("error");*/
		};
		
		/* asset edit functions */
		/*
		 * if(vm.asset_data !== null){
		 * console.log(vm.asset_data);
		 * vm.selectedAssetData(vm.asset_data); };
		 */
		
		/* tag select value */
		$rootScope.$on("SendUp", function(evt, data) {
			vm.tag = JSON.parse(data);
			vm.tagId = vm.tag.tagId;
		});
		
		/* group master select value */
		vm.grpMst = {};
		$rootScope.$on("grpMstValue", function(evt,	data) {
			vm.grpMst = JSON.parse(data);
			/*console.log(vm.grpMst.assetNm);*/
			vm.groupMasterType = vm.grpMst.assetNm;
			
			//testing with selectbox
			vm.myGroupArr = [{
				"gpId" : vm.grpMst.assetId,
				"gpNm" : vm.grpMst.assetNm
			}]
			vm.myGroupMst = vm.myGroupArr[0].gpId;
				
		});
		
		/* sub group master select value */
		vm.subGrpMst = {};
		$rootScope.$on("subGrpMstValue", function(evt, data) {
			vm.subGrpMst = JSON.parse(data);
			/*console.log(vm.subGrpMst.assetId);*/
			
			vm.selectedAssetData = function() {
				vm.requestParams = {
						'assetId' : vm.subGrpMst.assetId,
						'companyId' : vm.comp_Id
				}
				AssetService.assetGetByID(vm.requestParams, vm.assGetByIDSuccess, vm.assGetByIDError);
			};	
			
			vm.assGetByIDSuccess = function(response) {
				/*console.log("gododo");*/
				
				vm.myGroupArr = [{
					"gpId" : response.data.resData[1].groupMasterType,
					"gpNm" : response.data.resData[1].groupMaster
				}]
				vm.myGroupMst = vm.myGroupArr[0].gpId;
				
				vm.groupMasterType = response.data.resData[1].groupMaster;
				groupMasID = response.data.resData[1].groupMasterType;
				/*console.log(groupMasID);*/
			}
			
			vm.assGetByIDError = function(response) {
				/*console.log("error");*/
			};
			
			vm.mySubGroupArr = [{
				"subGpId" : vm.subGrpMst.assetId,
				"subGpNm" : vm.subGrpMst.assetNm
			}]
			vm.mySubGroupMst = vm.mySubGroupArr[0].subGpId;
				
			vm.subGroupType = vm.subGrpMst.assetNm;
			vm.selectedAssetData();
		});
		
		/*vm.catValue = function() {
									console.log(vm.categoryType);
										if (vm.categoryType === 1) {
										vm.subGroupType = "";
										vm.groupMasterType = vm.assetName;
										
									} else if (vm.categoryType === 0) {
										vm.groupMasterType = "";
										vm.subGroupType = "";
									} else if (vm.categoryType === 2) {
										vm.groupMasterType = "";
										vm.subGroupType = vm.assetName;
									} else if (vm.categoryType > 4) {
										
									}
								};*/
								
		vm.catValue = function() {
			/*console.log(vm.categoryType);*/
			if (vm.categoryType === 1) {
				vm.mySubGroupMst = "";
				vm.mySubGroupArr =[];
				vm.groupMasterType = vm.assetName;
				
				//testing with selectbox
				vm.myGroupArr = [{
					"gpId" : 0,
					"gpNm" : vm.assetName
				}]
				vm.myGroupMst = vm.myGroupArr[0].gpId;
				vm.mySubGroupMst = 0;

			} else if (vm.categoryType === 0) {
					vm.groupMasterType = "";
					vm.subGroupType = "";
					
					vm.myGroupArr = [];
					vm.myGroupMst = "";
					vm.mySubGroupArr = [];
					vm.mySubGroupMst = "";	
						
			} else if (vm.categoryType === 2) {
				
				vm.grpMasterError = true;
				/*$timeout(function() {
					vm.grpMasterError = false;
				}, 1000)*/
				
				vm.groupMasterType = "";
				vm.subGroupType = vm.assetName;
				vm.myGroupArr = [];
				vm.myGroupMst = "";
				
				vm.mySubGroupArr = [{
					"subGpId" : 0,
					"subGpNm" : vm.assetName
				}]
				vm.mySubGroupMst = vm.mySubGroupArr[0].subGpId;
				
			} else if (vm.categoryType > 4) {
				vm.myGroupMst = 0;
				vm.mySubGroupMst = 0;
				/*vm.groupMasterType = 0;
										vm.subGroupType = 0;*/
			}
		};	
		
		vm.categoryList = function() {
			vm.requestParams = {
					"categoryPrimery" : {
						"category" : 0, // category list by asset
						"companyId" : vm.comp_Id
					}
			};
			MasterService.getCategoryAll(vm.requestParams,vm.getCategoryAllSucc,vm.getCategoryAllError)
		};

		vm.getCategoryAllSucc = function(response) {
			vm.category_list = response.data;
			vm.categoryType = vm.category_list.resData[0].categoryPrimery.categoryId;
		};
		
		vm.getCategoryAllError = function(response) {
			/*console.log("error");*/
		};
		
		vm.getBatchDetails = function() {
			vm.requestParams = {
					"companyId" : vm.comp_Id
			}
			MasterService.getBatchLotData(vm.requestParams, vm.onSuccessBatchLot, vm.onErrorBatchLot);
		};
		
		vm.onSuccessBatchLot = function(response) {
			vm.batchLotList = response.data;
			vm.batchLot = vm.batchLotList.resData[0].batchId;
		};
		
		vm.onErrorBatchLot = function(response) {
			//console.log("error block");
		};
		
		vm.manufactureDetails = function() {
			vm.requestParams = {
					"companyId" : vm.comp_Id
			}
			MasterService.manufactureAllDetails(vm.requestParams, vm.onManufactureSuccess, vm.onManufactureError);
		};

		vm.onManufactureSuccess = function(response) {
			vm.manufactureDt = response.data;
			vm.manufacturer = vm.manufactureDt.resData[0].manufactureId;
		};

		vm.onManufactureError = function() {
			/*console.log("error");*/
		};
			
		vm.getVendorDetails = function() {
			vm.requestParams = {
					"companyId" : vm.comp_Id
			};
			MasterService.getVendorData(vm.requestParams, vm.onSuccessVendorData, vm.onErrorVendorData);
		};
									
		vm.onSuccessVendorData = function(response) {
			vm.vendordetails = response.data;
			vm.vendor = vm.vendordetails.resData[0].vendorId;
		};
		
		vm.onErrorVendorData = function() {
			/*console.log("error block");*/
		};

		vm.getDivisionDetails = function() {
			var requestParams = {
					"companyId" : vm.comp_Id
			};
			MasterService.DivisionDetailService(requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
		};
		
		vm.onDivisionInfoSuccess = function(response) {
			vm.divisionList = response.data;
			if (vm.divisionList.resData) {
				vm.division = vm.divisionList.resData[0].divisionId;
				vm.divChange();
				vm.sector = "";
			}
		};
		
		vm.onDivisionInfoError = function(response) {
			/*console.log("error");*/
		};
								
		vm.divChange = function() {
			vm.divisionId = {
					"divisionId" : vm.division
			};
			AssetService.getSectDetailsByDiv( vm.divisionId, vm.getSectDetBySucc, vm.getSectDetByErr);
		};
		
		vm.getSectDetBySucc = function(response) {
			vm.sect_data = response.data;
			if (vm.sect_data.resData) {
				vm.sector = vm.sect_data.resData[0].sectorId;
				vm.secChange();
				vm.location = "";
			}
			
		};
		
		vm.getSectDetByErr = function(response) {
			console.log("getSectDetByErr error");
		};
		
		vm.secChange = function() {
			vm.sectorId = {
					"sectorId" : vm.sector
			};
			AssetService.getLocDetailsBySecID( vm.sectorId, vm.secChangeSuccess, vm.secChangeError);
		};
		
		vm.secChangeSuccess = function(response) {
			vm.loc_data = response.data;
			if (vm.loc_data.resData) {
				/*
				 * vm.locChange(); }else{
				 */
				vm.location = vm.loc_data.resData[0].locationId;
				vm.locChange();
				
			} else {
				vm.department = "";
			}
		};
		
		vm.secChangeError = function() {
			console.log("secChangeError error");
			vm.location = "";
			vm.department = ""
		};
		
		vm.locChange = function() {
			vm.locationId = {
					"locationId" : vm.location
			};
			AssetService.getDepDetailsByLocID( vm.locationId, vm.locChangeSuccess, vm.locChangeError);
		};
		
		vm.locChangeSuccess = function(response) {
			vm.depart_data = response.data;
			if (vm.depart_data.resData)
				vm.department = vm.depart_data.resData[0].departmentId;
		};

		vm.locChangeError = function() {
			console.log("locChangeError error");
		};
			
		vm.getAllTaxMasters = function() {
			MasterService.taxMastersDetails( vm.taxmasterSuccess, vm.taxmasterError);
		};
		
		vm.taxmasterSuccess = function(response) {
			vm.taxDetails = response.data;
		};
		
		vm.taxmasterError = function(response) {
			/*console.log("error");*/
		};
		
		
		vm.assetAddFun = function() {
			if(vm.warrantyStDt === "" || vm.warrantyStDt === undefined  || vm.warrantyStDt === null){
				vm.warrantyStDt = moment(-2209008600000).format("DD-MM-YYYY");
			}
			if(vm.warrantyEtDt === "" || vm.warrantyEtDt === undefined  || vm.warrantyEtDt === null){
				vm.warrantyEtDt =  moment(-2209008600000).format("DD-MM-YYYY");
			}
			if(vm.maintenanceDate === "" || vm.maintenanceDate === undefined  || vm.maintenanceDate === null){
				vm.maintenanceDate = moment(-2209008600000).format("DD-MM-YYYY"); 
			}
			if(vm.acquisitionDate === "" || vm.acquisitionDate === undefined  || vm.acquisitionDate === null){
				vm.acquisitionDate =  moment(-2209008600000).format("DD-MM-YYYY");
			}
			if(vm.expiryDate === "" || vm.expiryDate === undefined  || vm.expiryDate === null){
				vm.expiryDate = moment(-2209008600000).format("DD-MM-YYYY");
			}
			/* p	roduct field checking */
			if (vm.productNo === undefined || vm.productNo === null) {
				vm.productNo = 0;
			}
									
			if(vm.categoryType === 0){
				if(vm.myGroupMst === undefined || vm.myGroupMst === "") {
					vm.myGroupMst = 0;
				}
				if(vm.mySubGroupMst === undefined || vm.mySubGroupMst === "") {
					vm.mySubGroupMst = 0;
				}
			}
			if (vm.categoryType === 2 && vm.myGroupMst === undefined || vm.myGroupMst === "") {
				vm.grpMasterError = true;
				$timeout(function() {
					vm.grpMasterError = false;
				}, 1000)
			}
				
			if(vm.rackName){
				vm.rackID = vm.rackName.rackId;
			}else{
				vm.rackID = 0;
			}
				
			vm.requestParams = {
					"assetNm" : vm.assetName,
					"tagId" : vm.tagId,
					// "groupMaster" : vm.groupMasterType,
					"groupMaster" : vm.myGroupMst,
					"groupSubMaster" : vm.mySubGroupMst,
					// "groupSubMaster" : vm.subGroupType,
					"companyId" : vm.comp_Id,
					"divisionId" : vm.division,
					"sectorId" : vm.sector,
					"locationId" : vm.location,
					"departmentId" : vm.department,
					"manufactureId" : vm.manufacturer,
					"vendorId" : vm.vendor,
					"inDocRefNo" : vm.docrefNoSelect.docrefNo,
					/* "outDocRefNo": 5, */
					"categoryId" : vm.categoryType,
					"batchId" : vm.batchLot,
					"serialNo" : vm.productNo,
					"cost" : vm.unitCost,
					"warrantStDt" : Date.parse(vm.warrantyStDt),
					"warrantEdDt" : Date.parse(vm.warrantyEtDt),
					"mainDt" : Date.parse(vm.maintenanceDate),
					"aqsDate" : Date.parse(vm.acquisitionDate),
					"expiryDt" : Date.parse(vm.expiryDate),
					"assetDesc" : vm.description,
					"assetQty" : vm.assetQuantity,
					
					"rackId" : vm.rackID,
					"slaveId" : vm.columnShelf,
					"taxValue" : vm.tax,
			};
			if (vm.assetPostMode === "NewMode") {
				/*console.log("NEW MODE" + JSON.stringify(vm.requestParams));*/
				AssetService.assetAddDetails(vm.requestParams, vm.assetAddSuccess, vm.assetAddError);
			} else {
				/*vm.requestParams = angular.extend(vm.requestParams,
						{
					"assetId" : $stateParams.assetID,
					"photo" : vm.photo
						});*/
				vm.assetData.currentDetectedDateTime = moment(vm.assetData.currentDetectedDateTime).valueOf();
				vm.requestParams.assetId = $stateParams.assetID;
				vm.requestParams.photo = vm.photo;
				
				AssetService.AssetUpdateDetails(vm.requestParams, vm.assetUpdateSucc, vm.assetUpdateError);
			}
			//										}
		};
		
		vm.assetAddSuccess = function(response) {
			if (response.data.resCode === "res0000") {
				swal("Success", "Asset Add Successfully!", "success")
				
				vm.requestParam = {
					"companyId" : vm.comp_Id,
					"uploadTo" : "ASSET",
					"imageId" : response.data.resData.assetId,
				}
				if (vm.assetLogo && vm.assetLogo.name) {
					AuthService.uploadLogo(vm.requestParam, vm.assetLogo, vm.assetImgSucc, vm.assetImgErr)
				}
				/*else{
					AuthService.uploadLogo(vm.requestParam, vm.webCamImg, vm.assetImgSucc, vm.assetImgErr)
				}*/
				
				//Asset and Doc Attachments
				if(selectedAttachment !== undefined && selectedAttachment !== null){
					if(vm.assetAttachedFileNameData !== "cancel" && vm.assetAttachedMultipartFileData !== "cancel"){
						if(selectedAttachment === "AssetAttach"){
							vm.requestParams = {
								"companyId": vm.comp_Id,
								"uploadTo":"ASSET",
								"id":response.data.resData.assetId,
								"fileName": vm.assetAttachedFileNameData,
								"file": vm.assetAttachedMultipartFileData
							}
						}
						if(selectedAttachment === "DocRefAttach"){
							vm.requestParams ={
									"companyId": vm.comp_Id,
									"uploadTo":"ASSET",
									"id":vm.docrefNo.docRefName,
									"fileName": vm.assetAttachedFileNameData,
									"file":  vm.assetAttachedMultipartFileData
							}
						}
						AssetService.uploadAttach(vm.requestParams, vm.assetDocattachSucc, vm.assetDocattachErr);
					}
				}
				
				$state.transitionTo('homeDashboard.asset',
						$stateParams, {
						reload : true,
						inherit : false,
						notify : true
				});
			} else if (response.data.resCode === "res0100") {
				swal("Not Added!", response.data.resMsg, "error");
				$state.reload();
			} else {
				
			}
		};
		
		vm.assetAddError = function() {
			/*console.log("error");*/
		};
		vm.assetImgSucc = function(response) {
			/*console.log("Image added successfully..!!");*/
		}
		vm.assetImgErr = function(response) {
			/*console.log("Image added failed..!!");*/
		}
		
		vm.assetUpdateSucc = function(response) {
			/*console.log(response.data);*/
			if (response.data.resCode === "res0000") {
				swal("Updated", response.data.resData, "success");
				if(response.data.resData === "" || response.data.resData === null ){
					swal("Updated", "Asset Updated Successfully", "success");
				}
				vm.requestParam = {
						"companyId" : vm.comp_Id,
						"uploadTo" : "ASSET",
						"imageId" : $stateParams.assetID.toString(),
				}
				
				if (vm.assetLogo && vm.assetLogo.name) {
					AuthService.uploadLogo(vm.requestParam, vm.assetLogo, vm.assetImgSucc1, vm.assetImgErr1)
				}
				
				//Asset and Doc Attachments
				if(selectedAttachment !== undefined && selectedAttachment !== null){
					if(vm.assetAttachedFileNameData !== "cancel" && vm.assetAttachedMultipartFileData !== "cancel"){
						if(selectedAttachment === "AssetAttach"){
							vm.requestParams = {
								"companyId": vm.comp_Id,
								"uploadTo":"ASSET",
								"id":$stateParams.assetID,
								"fileName": vm.assetAttachedFileNameData,
								"file": vm.assetAttachedMultipartFileData
							}
						}
						if(selectedAttachment === "DocRefAttach"){
							vm.requestParams = {
									"companyId": vm.comp_Id,
									"uploadTo":"ASSET",
									"id":vm.docrefNo.docRefName,
									"fileName": vm.assetAttachedFileNameData,
									"file":  vm.assetAttachedMultipartFileData
							}
						}
						AssetService.uploadAttach(vm.requestParams,selectedAttachment, vm.assetDocattachSucc, vm.assetDocattachErr);
					}
				}
				
				/*$state.go('homeDashboard.asset');*/
				$state.transitionTo('homeDashboard.asset',
						$stateParams, {
					reload : true,
					inherit : false,
					notify : true
				});
			}else{
				swal("Not Added!", response.data.resMsg, "error");
			}
		};
		
		vm.assetUpdateError = function(response) {
			console.log("error");
		};
		
		vm.assetImgSucc1 = function(response) {
			console.log("Image update success");
		}
		vm.assetImgErr1 = function(response) {
			console.log("Image update fail");
		}
		
		vm.assetDocattachSucc = function(response){
			console.log(response.data.resMsg);
		};
		
		vm.assetDocattachErr = function(response){
			console.log(response.data.resMsg);
		};
		
		vm.tagSelectFun = function() {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/tag/tagSearch.html',
				controller : 'TagSearchController',
				controllerAs : 'tagSearchController',
				resolve : {
					tagSelect : function() {
						return angular.copy(2);
					}
				}
			});
			
			modalInstance.result.then(function(selectedItem) {
			}, function(t) {
				console.log('Modal dismissed at: ' + new Date());
			});	
		};	
								
		vm.groupMasterDailog = function() {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/asset/groupMaster.html',
				controller : 'GroupMasterController',
				controllerAs : 'groupMasterController',
				/*
				 * resolve: { jw: function () {
				 * return angular.copy(d); } }
				 */
			});
				
			modalInstance.result.then(function(
					selectedItem) {
				/*console.log("ddd" + selectedItem)*/
			}, function(t) {
				/*console.log(t);*/
			});
		};
								
		vm.subGroupMasterDailogue = function() {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/asset/subGroupMaster.html',
				controller : 'SubGroupMasterController',
				controllerAs : 'subGroupMasterController',
				resolve : {
					groupMasterID : function() {
						return angular.copy(vm.grpMst);
					}
				}
			});
		};
		
		$scope.$on('TagId', function(event, args) {
			vm.tagId = JSON.parse(args.tagId);
		});
		
		vm.readTag = function() {
			vm.requestParam = {
					"dockDoorId":parseInt(vm.dockDorId),
					"companyId" : vm.comp_Id
			}
			AssetService.readTag(vm.requestParam, vm.readTagSucc, vm.readTagErr)
		}
		
		vm.readTagSucc = function(response) {
			if (response.data.resCode === "res0000") {
				vm.tagId = response.data.resData.readTagId;
			}
			if (response.data.resCode === "res0100") {
				swal("Tag error", response.data.resMsg, "warning")
			}
		}

		vm.readTagErr = function() {
			console.log("Error in read tag");
		}
		
		vm.readTagByDock = function(){
			var modalInstance = $uibModal.open({
				backdrop : 'static',
					animation : $scope.animationsEnabled,
					templateUrl : 'resources/views/asset/dockDoorPop_up.html',
					controller : 'DockDoorPopUpController',
					controllerAs : 'dockdoorpopupController',
			});
			
			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				/*$log.info('Modal dismissed at: '+ new Date());*/
			});
		}
		
		vm.getInOutDocRef = function() {
			vm.requestParams = {
					"status" : 0,
					"companyId" : vm.comp_Id
			}
			AssetService.getInOutDocRef(vm.requestParams,vm.inOutDocrefSucc,vm.inOutDocrefErr);
		}
		
		vm.inOutDocrefSucc = function(response) {
			vm.docRef_data = response.data;
			vm.docrefNoSelect = response.data.resData[0];
		}
		
		vm.inOutDocrefErr = function() {
			console.log("ERROR in doc ref fetch");
		}
		
		vm.getRackData = function() {
			vm.requestParams = {
					"status" : 0,
					"companyId" : vm.comp_Id
			}
			MasterService.RackListService(vm.rackSucc, vm.rackErr);
		}
		
		vm.rackSucc = function(response) {
			vm.rack_data = response.data.resData;
			if(vm.astBtn === false){
				_.each(vm.rack_data, function(data){
					if(vm.single_ass){
						if(data.rackId === vm.single_ass.resData[0][0].rackId){
							vm.rackName = data;
						}
					}
				});
			}else{
				vm.rackName = vm.rack_data[0];
			}
			vm.rackChange(vm.rackName);
		};
		
		vm.rackErr = function() {
			console.log("ERROR in doc ref fetch");
		};
		
		vm.rackChange = function(data){
			if(data){
				vm.columnShelf = data.rackSlave[0].slaveId;
			}
		};
		
		vm.cancel = function() {
			$state.go('homeDashboard.asset');
		};
		/**
		 * ********************WEB
		 * CAM***************************************
		 */
		//vm.showDiv = true;
		
		vm.webCamBtn = "Webcam";
		vm.webcamClick = function() {
			vm.webCamBtn = "Take";
			vm.webCamDiv = true;
			if(vm.webCamBtn === "Take"){
				Webcam.snap(function(data_uri) {
					vm.assetLogo = data_uri;
					vm.webCamDiv = false;
					vm.assetImg = data_uri;
					/*var blob = new Blob([data_uri], {type: 'image/jpg'});
					
					var file = new File([blob], 'fileName.jpg');
					vm.webCamImg = data_uri;*/
					
					/*var aa = data_uri.split(',');
					var blob = new Blob([aa[1]], {type: 'image/jpg'});
					var blobUrl = URL.createObjectURL(blob);
					var file = new File([blob], 'fileName.jpg' ,{type: 'image/jpg'});
					file.$ngfBlobUrl = blobUrl;
					vm.webCamImg = file;*/
					document.getElementById('anchor').setAttribute('href', data_uri);// download
				});
			}
			
			//old code for image div	
			vm.showDiv = true;
			$timeout(function(){
			Webcam.attach('#my_camera');},0)
		};
		
		vm.take_snapshot = function() {
			vm.webImg = true;
			vm.assetUpdImg = false
			Webcam.snap(function(data_uri) {
				vm.webCamImg = data_uri;
				var blob = new Blob([data_uri], {type: 'image/jpg'});
				var file = new File([blob], 'imageFileName.jpg' ,{type: 'image/jpg'});
				vm.assetLogo = file;
				document.getElementById('anchor').setAttribute('href', data_uri);// download
				
			});
		};
		
		vm.myFun = function(){
			vm.webImg = false;
			vm.showDiv = false;
			vm.assetUpdImg = false;	
		};	
								
		vm.splitAssetFun = function(){
			var modalInstance = $uibModal.open({
				backdrop: 'static',
				animation: vm.animationsEnabled,
				templateUrl: 'resources/views/asset/splitAsset.html',
				controller: 'SplitAssetController',
				controllerAs : 'splitAssetController',
				resolve: {
					singleAssData: function () {
						return angular.copy(vm.single_ass);
					}
				}
			});
			
			modalInstance.result.then(function (selectedItem) {
			}, function () {
			});
		};
		
		vm.outOfStockFun = function (){
			swal({
				title: "Click yes to continue ",
				text: "Are you sure want to out of stock!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, continue!",
							            /*cancelButtonText: "No, cancel please!",*/
				closeOnConfirm: false,
				closeOnCancel: false },
				function(isConfirm){
					if (isConfirm) {
						vm.requestParams = {
								"assetId":$stateParams.assetID,
								"companyId":vm.comp_Id,
								"assetDesc":"no desc"		
						}
						AssetService.outOfstock(vm.requestParams, vm.outOfStcSucc, vm.outOfStcErr)
					}	
					else {
						swal("Cancelled", "Your asset is safe :)", "error");
					}
				});
		}
		vm.outOfStcSucc = function(response){
			swal("Updated", "Asset Out of Stock successfully!", "success");
			$state.go('homeDashboard.asset');
		}
		
		vm.outOfStcErr = function(response){
			console.log("Error in out of stock");
		}
		
		vm.releaseTagFun = function (){
			swal({
				title: "Click yes to continue ",
				text: "Are you sure want to release tag!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, continue!",
				/*cancelButtonText: "No, cancel please!",*/
				closeOnConfirm: false,
				closeOnCancel: false },
				function(isConfirm){
					if (isConfirm) {
						vm.requestParams = {
								"assetId":$stateParams.assetID,
								"companyId":vm.comp_Id,
								"tagId":vm.tagId,
								"assetDesc":"tagRelease"		
						}	
						AssetService.releaseTag(vm.requestParams, vm.releaseTagSucc, vm.releaseTagErr)
					}
					else {
						swal("Cancelled", "Your asset is safe :)", "error");
					}	
				});
		}
		
		vm.releaseTagSucc = function(response){
			swal("Successfully", "Release tag successfully!", "success");
			$state.transitionTo('homeDashboard.asset',
					$stateParams, {
				reload : true,
				inherit : false,
				notify : true
			});
		}
		vm.releaseTagErr = function(response){
			console.log("error in release tag", response.data.resData);
		}
		
		vm.assetAttach = function(pageData){
			//$stateParams = $stateParams.pageData;
			selectedAttachment = pageData;
			
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : $scope.animationsEnabled,
				templateUrl : 'resources/views/asset/assetAttachment.html',
				controller : 'AssetAttachmentController',
				controllerAs : 'assetAttachmentController',
				resolve : {
					assetData : function() {
						return angular.copy(vm.single_ass);
					},
					PageData : function() {
						return angular.copy(pageData);
					}
				}
			});
				
			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function(attachedFileData) {
				if(attachedFileData !== 'cancel'){
					 angular.forEach(attachedFileData, function(value){
							if(!value.assetId){
								vm.assetAttachedFileNameData.push(value.originName);
								vm.assetAttachedMultipartFileData.push(value.fileData);
							}
					 });
				 }else{
					vm.assetAttachedFileNameData = "cancel";
					vm.assetAttachedMultipartFileData = "cancel";
				 }
				vm.initialize();
			});
		}
		
		vm.getCount = function(){
			vm.requestParams = {
					"companyId":vm.comp_Id,
					"inDocRef":vm.single_ass.resData[1].docRefName,
					"assetId":$stateParams.assetID
			}
			AssetService.getAllUploadDetails(vm.requestParams, vm.getCountSucc, vm.getCountErr);
		}
		vm.getCountSucc = function(response){
			vm.assetAttachment = response.data.resData.docCountById;
			vm.docRefAttachment = response.data.resData.docCountByDocRefName;
			vm.totAttachment = response.data.resData.totalCount;
		};
		vm.getCountErr = function(){
			console.log("Error in get count");
		};
		
		$timeout(function(){
			if($stateParams.assetID){
				//vm.getCount();
			}
		},1000)
		
		/*========ANGULAR DATE PICKER============*/
		vm.today = function() {
			vm.maintenanceDate = new Date();
			vm.expiryDate = new Date();
			vm.warrantyStDt = new Date();
			vm.warrantyEtDt = new Date();
			vm.acquisitionDate = new Date();
		};
		/*vm.today();*/
		
		vm.clear = function() {
			vm.maintenanceDate = null;
		};
		
		vm.dateOptions = {
				/*dateDisabled: disabled,*/
				formatYear: 'yy',
				//maxDate: new Date(),
				minDate: new Date(2000, 5, 22),
				startingDay: 1
		};
		
		// Disable weekend selection
		function disabled(data) {
			var date = data.date,
			mode = data.mode;
			return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
		}
		
		vm.open2 = function() {
			vm.popup2.opened = true;
		};
		
		vm.expiryDtOpen = function(){
			vm.expiryDt.opened = true;
		};
		
		vm.wrntyetDtOpen = function(){
			vm.wrntyetDt.opened = true;  
		};
		
		vm.wrntystDtOpen = function(){
			vm.wrntystDt.opened = true;  
		};
		
		vm.aquDateOpen = function(){
			vm.aquDate.opened = true;  
		};
		
		vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		vm.format = vm.formats[0];
		vm.altInputFormats = ['M!/d!/yyyy'];
		
		vm.wrntystDt = {
				opened: false
		};
		vm.wrntyetDt = {
				opened: false
		};
		vm.aquDate = {
				opened : false
		}
		vm.expiryDt = {
				opened: false
		};
		
		vm.popup2 = {
				opened: false
		};
		
		function getDayClass(data) {
			var date = data.date,
			mode = data.mode;
			if (mode === 'day') {
				var dayToCheck = new Date(date).setHours(0,0,0,0);
				
				for (var i = 0; i < $scope.events.length; i++) {
					var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
					
					if (dayToCheck === currentDay) {
						return $scope.events[i].status;
					}
				}
			}
			return '';
		}
		
		//image select
		vm.clickonChoose = function(){
			vm.webCamDiv = false;
			vm.webCamBtn = "Webcam";
			document.getElementById('my_file').click();
		};
		
		vm.imgDelete = function(){
			vm.assetLogo = "";
			vm.webCamBtn = "Webcam";
		};
		
		vm.initialize();
		vm.getBatchDetails();
		vm.manufactureDetails();
		vm.getVendorDetails();
		vm.getDivisionDetails();
		vm.getAllTaxMasters();
		vm.getRackData();
		vm.getInOutDocRef();
		vm.categoryList();
	}]);
})();
	
(function() {
	angular.module('rfidDemoApp.controller')
	.controller('AssetAttachmentController',
			['$scope','$uibModalInstance','AuthService', 'MasterService', '$state', '$stateParams', 'AssetService', 'assetData', 'PageData',
			 function($scope, $uibModalInstance, AuthService, MasterService, $state, $stateParams, AssetService, assetData, PageData) {
				var vm = this;
				vm.tableHeader = "Attachment";
				vm.globalCompId = AuthService.getCompanyId();
				vm.globalCompName = AuthService.getCompanyName();
				vm.attachList = [];
				vm.pageDataNew = "";
				
				if(assetData !== undefined){
					getFile();
				}
				
				//click on save button
				vm.saveBtn = function(){
					$uibModalInstance.dismiss(vm.attachList);
				};
				
				vm.AssetAttach  = function(){
					vm.attachList.push({
						"originName" : vm.attachedfile.name,
						"fileData" : vm.attachedfile	
					})
					
					/*if(PageData === "AssetAttach"){
						vm.requestParams = {
								"companyId": vm.globalCompId,
								"uploadTo":"ASSET",
								"id":$stateParams.assetID,
								"fileName": vm.attachedfile.name,
								"file":  vm.attachedfile
						}
					}
					if(PageData === "DocRefAttach"){
						vm.requestParams ={
								"companyId": vm.globalCompId,
								"uploadTo":"ASSET",
								"id":assetData.resData[1].docRefName,
								"fileName": vm.attachedfile.name,
								"file":  vm.attachedfile
						}
					}
					AssetService.uploadAttach(vm.requestParams, vm.assetDocattachSucc, vm.assetDocattachErr);*/
				};
						
				vm.fileList = [];
				vm.assetDocattachSucc = function(response){
					vm.date = new Date();
					if(response.data.resCode == "res0000"){
						/*vm.requestParams = {
								"companyId":vm.globalCompId,
								"inDocRef":assetData.resData[1].docRefName,
								"assetId":$stateParams.assetID
						}*/
						//AssetService.getAllUploadDetails(vm.requestParams, vm.getAlluploadSucc, vm.getAlluploadErr);
						
						$uibModalInstance.dismiss('cancel');
						/*$state.transitionTo($state.current,
								$stateParams, {
							reload : true,
							inherit : false,
							notify : true
						});*/
					}else if(response.data.resCode == "res0100"){
						swal("Error", response.data.resData ,"error");
					}
				}
				vm.assetDocattachErr = function(response){
					console.log(response.data.resData,"Error in asset doc attach");
				}
				
				/*vm.getAlluploadSucc = function(response){
					console.log(response.data,"JJJJJJJ");
				}
				vm.getAlluploadErr = function(response){
					console.log(response.data.resData,"Error in get upload details");
				}*/
				
				function getFile(){
					
					//assetData.resData[1].attachment.docRefName = [];
					if(PageData === "DocRefAttach"){
						vm.pageDataNew = PageData;
						if(assetData.assetAttachments){
							vm.fileList = assetData.assetAttachments.assetDocRefAttachmentList;
							angular.forEach(vm.fileList ,function(value){
								var str = value.split('_');
								vm.fileList.push(str[2]);
								vm.attachList.push({
									"inDocRefNo" : assetData.inDocRefNo,
									"assetId" : "000",
									"originName" : str[2],
									"attaFullName" : value
								})
							});
						}
					}
					if(PageData === "AssetAttach"){
						vm.pageDataNew = PageData;
						if(assetData.assetAttachments){
							vm.fileList = assetData.assetAttachments.assetAttachmentList;
							angular.forEach(vm.fileList ,function(value){
								var str = value.split('_');
								vm.fileList.push(str[2]);
								vm.attachList.push({
									"assetId" : str[0],
									"originName" : str[2],
									"attaFullName" : value
								})
							});
						}
					}
				}
				
				vm.fileName = "";
				vm.downloadFile = function(data){
					vm.fileName = data.attaFullName; 
					vm.requestParams = {
							/*"searchBy":data.attaFullName,
							"searchValue":"ASSET", */    
							"searchBy":"ASSET",
							"searchValue":data.attaFullName,          
							"companyId":vm.globalCompId
					}
					if(vm.pageDataNew === "userAttach"){
						vm.requestParams.searchBy = "USER";
					}
					AssetService.downloadAttachment(vm.requestParams, vm.assetgetFileSucc, vm.assetgetFileErr);
				}
						
				vm.assetgetFileSucc = function(response){
					
					var blob = new Blob([response.data], {type: "application/*"});
			        var objectUrl = URL.createObjectURL(blob);
			       // window.open(objectUrl);
			       
					if(PageData === "AssetAttach"){
						vm.fileName = vm.fileName.slice(16);
					}
					if(PageData === "DocRefAttach"){
						vm.fileName = vm.fileName.slice(26);
					}
					
					var a = document.createElement('a');
		            a.href = objectUrl;
		            a.download = vm.fileName;
		            a.click();
		            
					/*var anchor = angular.element('<a/>');
					anchor.attr({
						href: 'data:attachment/*;charset=utf-8,' + encodeURI(response.data),
						target: '_blank',
						download: vm.fileName
					})[0].click();*/
					
				};
				
				vm.assetgetFileErr = function(){
					console.log("Error in download attachment");
				};
				
				vm.deleteAttachFun = function(data){
					if(data.assetId){
						swal({
				            title: "Are you sure?",
				            text: "You want to delete permenently?",
				            type: "warning",
				            showCancelButton: true,
				            confirmButtonColor: "#DD6B55",
				            confirmButtonText: "Yes, delete it!",
				            cancelButtonText: "No, cancel please!",
				            closeOnConfirm: true,
				            closeOnCancel: false },
				            function(isConfirm){
				              if (isConfirm) {
				            	  vm.attachList.splice(vm.attachList.indexOf(data), 1);
				            	  vm.requestParams = {
											"searchBy":"ASSET",
											"searchValue":data.attaFullName,          
											"companyId":vm.globalCompId
									}
									AssetService.deleteAttachment(vm.requestParams, vm.deleteAttSuccess, vm.deleteAttErr);
				              }
				              else {
				            	  swal("Cancelled", "Your imaginary file is safe :)", "error");
				              }
				            });
					}else{
						vm.attachList.splice(vm.attachList.indexOf(data), 1);
					}
				};
				
				vm.deleteAttSuccess = function(response){
					if (response.data.resCode === "res0000") {
						swal("Deleted!", response.data.resData,"success");
						//$uibModalInstance.dismiss('cancel');
					} else if (response.data.resCode === "res0100") {
						swal("Not deleted!", response.data.resData, "error");
					}else{
						
					}
				};
				
				vm.deleteAttErr = function(response){
					
				};
				
				$scope.cancel = function() {
					$uibModalInstance.dismiss('cancel');
				};
						
				//vm.getFile();
			}]);
})();

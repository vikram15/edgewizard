(function() {
    angular.module('rfidDemoApp.controller')
        .controller('GroupMasterController', ['$uibModalInstance', 'AssetService', 'AuthService', '$rootScope', 'MasterService',
            function($uibModalInstance, AssetService, AuthService, $rootScope, MasterService) {
                var vm = this;
                vm.tableHeader = "Group Asset Search";
                vm.searchBy = "Asset Id";
                vm.filterBy = "Asset Id";
                vm.comp_Id = AuthService.getCompanyId();


                vm.groupMasterSearchData = function() {
                    vm.requestParams = {
                        "searchBy": vm.searchBy,
                        "searchValue": vm.searchValue,
                        "searchField": vm.searchField,
                        "assetType": 99,
                        "categoryId": 1,
                        "groupId": 0,
                        "searchByTwo": vm.filterBy,
                        "searchValueTwo": vm.searchValueTwo,
                        "companyId": vm.comp_Id,
                        "startDate":Date.parse(vm.fromDate),
                        "endDate":Date.parse(vm.toDate),
                    }
                    AssetService.groupMasterDetails(vm.requestParams, vm.groupMasterDetSuccess, vm.groupMasterDetError);
                };
                
                vm.groupMasterDetSuccess = function(response) {
                    vm.groupMaster_data = response.data;
                };

                vm.groupMasterDetError = function(response) {
                    console.log("error");
                };

                vm.categoryList = function() {
                    vm.requestParams = {
                        "categoryPrimery": {
                            "category": 0,
                            "companyId": vm.comp_Id
                        }
                    };
                    MasterService.getCategoryAll(vm.requestParams, vm.getCategoryAllSucc, vm.getCategoryAllError)
                };

                vm.getCategoryAllSucc = function(response) {
                    vm.category_li = response.data;
                    vm.category_list = vm.category_li.resData.splice(0,3)
                };

                vm.getCategoryAllError = function(response) {
                    console.log("error");
                };

                vm.getBatchDetails = function() {
                    vm.requestParams = {
                        "companyId": vm.comp_Id
                    }
                    MasterService.getBatchLotData(vm.requestParams, vm.onSuccessBatchLot, vm.onErrorBatchLot);
                };

                vm.onSuccessBatchLot = function(response) {
                    vm.batchLotList = response.data;

                };

                vm.onErrorBatchLot = function(response) {
                    console.log("error block");
                };

                vm.getSectorDetails = function(){
                    vm.requestParams = {
                        "companyId": vm.comp_Id
                    }
                    MasterService.SectorListService(vm.requestParams, vm.onSectorSuccess, vm.onSectorError);
                }

                vm.onSectorSuccess = function(response){
                  vm.sect_data = response.data;
                }

                vm.onSectorError = function(){

                };

                vm.getLocationDetails = function(){
                	vm.requestParams = {
                			"companyId": vm.comp_Id
                	}
                	MasterService.LocationListService(vm.requestParams, vm.onLSuccess, vm.onLError);
                };

                vm.onLSuccess = function(response){
                  vm.loc_data = response.data;
                };

                vm.onLError = function(response){

                };

                vm.getDepartmentDetails = function(){
                  vm.requestParams = {
                			"companyId": vm.comp_Id
                	}
                	MasterService.DepartmentListService(vm.requestParams, vm.onDepSuccess, vm.onDepError);
                };

                vm.onDepSuccess = function(response){
                  vm.depart_data = response.data;
                };

                vm.onDepError = function(response){

                };

                vm.getManufacturerDetails = function(){
                  vm.requestParams = {
                			"companyId": vm.comp_Id
                	}
                	MasterService.manufactureAllDetails(vm.requestParams, vm.onManSuccess, vm.onManError);
                };

                vm.onManSuccess = function(response){
                  vm.manufactureDt = response.data;
                };

                vm.onManError = function(response){

                };

                vm.getVendorDetails = function(){
                  vm.requestParams = {
                      "companyId": vm.comp_Id
                  }
                  MasterService.getVendorData(vm.requestParams, vm.onVenSuccess, vm.onVenError);
                };

                vm.onVenSuccess = function(response){
                  vm.vendordetails = response.data;
                };

                vm.onVenError = function(){

                };

                vm.groupMasterSelect = function() {
                    if (vm.grpMstSelect !== undefined) {
                        $uibModalInstance.dismiss(vm.grpMstSelect);
                        $rootScope.$broadcast("grpMstValue", vm.grpMstSelect);
                    }
                };

                vm.getDivisionDetails = function() {
                    vm.requestParams = {
                        "companyId": vm.comp_Id
                    };
                    MasterService.DivisionDetailService(vm.requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
                };

                vm.onDivisionInfoSuccess = function(response) {
                    vm.divisionList = response.data;
                };

                vm.onDivisionInfoError = function(response) {
                    console.log("error");
                };
                
                /*========ANGULAR DATE PICKER============*/
            	vm.today = function() {
            		vm.fromDate = new Date();
            		vm.toDate = new Date();
            	};
            	vm.today();

            	vm.clear = function() {
            		vm.fromDate = null;
            		vm.toDate = null;
            	};

            	vm.dateOptions = {
            			//dateDisabled: disabled,
            			formatYear: 'yy',
            			//maxDate: new Date(2020, 5, 22),
            			minDate: new Date(2000, 5, 22),
            			startingDay: 1
            	};

            	// Disable weekend selection
            	function disabled(data) {
            		var date = data.date,
            		mode = data.mode;
            		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
            	}

            	vm.fromOpen = function(){
            		vm.from.opened = true;
            	};
              
            	vm.toOpen = function(){
            		vm.to.opened = true;  
            	};
              
            	vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            	vm.format = vm.formats[0];
            	vm.altInputFormats = ['M!/d!/yyyy'];
            	
            	vm.from = {
            			opened: false
            	};
            	vm.to = {
            			opened: false
            	};
            	
              function getDayClass(data) {
            	  var date = data.date,
            	  mode = data.mode;
            	  if (mode === 'day') {
            		  var dayToCheck = new Date(date).setHours(0,0,0,0);
            		  
            		  for (var i = 0; i < $scope.events.length; i++) {
            			  var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
            	
            			  if (dayToCheck === currentDay) {
            				  return $scope.events[i].status;
            			  }
            		  }
            	  }
            	
            	  return '';
              }

                vm.cancel = function() {
                    $uibModalInstance.close();
                };

                vm.groupMasterSearchData();

            }
        ]);
})();

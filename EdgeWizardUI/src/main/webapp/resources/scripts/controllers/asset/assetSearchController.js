(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('AssetSearchController',['AssetService','AuthService','$uibModalInstance','MasterService','reportType','$timeout', 
	                                     function(AssetService, AuthService, $uibModalInstance, MasterService, reportType, $timeout){
		var vm = this;
		vm.tableHeader = "Asset Search";
		vm.currentPage = 1;
		vm.maxSize = 5;
		vm.searchBy = "Asset Id";
		vm.comp_Id = AuthService.getCompanyId();
		vm.assetDataNew = [];
		console.log(reportType);
		
		
		vm.assetSearchData = function() {
			vm.requestParams = {
				      "searchBy": vm.searchBy,
				      "searchValue" :vm.searchValue,
				      "currentPage":vm.currentPage,
				      "companyId":vm.comp_Id,
				      "categoryType":vm.categoryType,
				      "batchName": vm.batchName,
				      "divisionName":vm.divisionName,
				      "sectorName":vm.sectorName,
				      "locationName":vm.locationName,
				      "departmentName":vm.departmentName,
				      "manufactureName":vm.manufactureName,
				      "vendorName":vm.vendorName,
				      "startDate":vm.fromDate,
					  "endDate":vm.toDate,
			};
			AssetService.searchAssetDetails(vm.requestParams, vm.searchAsssuccess, vm.searchAssError);
	    };

	    vm.searchAsssuccess = function(response){
	    	vm.ass_data = response.data;
	    	if(vm.ass_data.resData){
	    		vm.assetDataNew.push(vm.ass_data.resData);
	    		vm.totalItems = vm.ass_data.resData[0].totalAssetSearch;
		    	vm.itemsPerPage = vm.ass_data.resData[0].RecordPerPage;
	    	}
	    	
	    };
	    vm.searchAssError = function(response){
	    	console.log("error");
	    };
	    
	    vm.categoryList = function() {
    		vm.requestParams = {
    				"categoryPrimery": {
    					"category": 0,
    					"companyId": vm.comp_Id
    				}
    		};
    		MasterService.getCategoryAll(vm.requestParams, vm.getCategoryAllSucc, vm.getCategoryAllError)
    	};

    	vm.getCategoryAllSucc = function(response) {
    		vm.category_list = response.data;
    		vm.categoryType = vm.category_list.resData[0].categorynm;
    	};

    	vm.getCategoryAllError = function(response) {
    		console.log("error");
    	};
    	
    	vm.getDivisionDetails = function() {
    		vm.requestParams = {
    				"companyId" : vm.comp_Id
    		};	
    		MasterService.DivisionDetailService(vm.requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
    	};

    	vm.onDivisionInfoSuccess = function(response) {
    		vm.divisionList = response.data;
    		/*vm.categoryType = vm.category_list.resData[0].categoryPrimery.categoryId;*/
    	};

    	vm.onDivisionInfoError = function(response) {
    		/*console.log("error");*/
    	};
    	
    	vm.getBatchDetails = function() {
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		}
    		MasterService.getBatchLotData(vm.requestParams, vm.onSuccessBatchLot, vm.onErrorBatchLot);
    	};

    	vm.onSuccessBatchLot = function(response) {
    		vm.batchLotList = response.data;
    		vm.batchName = vm.batchLotList.resData[0].batchName;
    	};

    	vm.onErrorBatchLot = function(response) {
    		console.log("error block");
    	};
			
    	vm.getSectorDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		}
    		MasterService.SectorListService(vm.requestParams, vm.onSectorSuccess, vm.onSectorError);
    	};

    	vm.onSectorSuccess = function(response){
    		vm.sect_data = response.data;
    		vm.sectorName = vm.sect_data.resData[0].sectorNm
    	};

    	vm.onSectorError = function(){
    		
    	};

    	vm.getLocationDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		};
    		MasterService.LocationListService(vm.requestParams, vm.onLSuccess, vm.onLError);
    	};

    	vm.onLSuccess = function(response){
    		vm.loc_data = response.data;
    		vm.locationName = vm.loc_data.resData[0].locationNm;
    	};

    	vm.onLError = function(response){

    	};

    	vm.getDepartmentDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		};
    		MasterService.DepartmentListService(vm.requestParams, vm.onDepSuccess, vm.onDepError);
    	};

    	vm.onDepSuccess = function(response){
    		vm.depart_data = response.data;
    		vm.departmentName = vm.depart_data.resData[0].deptNm;
    	};

    	vm.onDepError = function(response){	

    	};

    	vm.getManufacturerDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		}
    		MasterService.manufactureAllDetails(vm.requestParams, vm.onManSuccess, vm.onManError);
    	};

    	vm.onManSuccess = function(response){
    		vm.manufactureDt = response.data;
    		vm.manufactureName = vm.manufactureDt.resData[0].manufactureNm;
    	};

    	vm.onManError = function(response){

    	};

    	vm.getVendorDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		}
    		MasterService.getVendorData(vm.requestParams, vm.onVenSuccess, vm.onVenError);
    	};

    	vm.onVenSuccess = function(response){
    		vm.vendordetails = response.data;
    		vm.vendorName = vm.vendordetails.resData[0].vendorName;
    	};

    	vm.onVenError = function(){

    	};
	    
    	vm.assetTempData = [];
    	vm.okBtn = function(){
    		vm.ass_data.resData.splice(0, 1);
    		angular.forEach(vm.ass_data.resData, function(value){
    			if(value.isChecked === true){
    				vm.assetTempData.push(value);
    			}
    		})
    		
    		if(reportType === "4"){
    			if(vm.assetTempData.length === 1){
    				$uibModalInstance.dismiss(vm.assetTempData);
    			}else{
    				vm.selectError = true;
    				$timeout(function(){
    					vm.selectError = false;
    					vm.assetTempData = [];
    				},1500);
    				console.log("please select one record")
    			}
    		}else{
    			$uibModalInstance.dismiss(vm.assetTempData);
    		}
    		//$uibModalInstance.dismiss(vm.assetTempData);
    	};
    	
	    vm.cancel = function() {
    		$uibModalInstance.dismiss('cancel');
    	};
    	
    	vm.allItemsSelected = false;
    	vm.selectAll = function(){
    		
    		angular.forEach(vm.ass_data.resData, function(value){
    			value.isChecked = vm.allItemsSelected;
    		});
    	};
    	
    	vm.selectEntity = function(){
    		for (var i = 0; i < vm.ass_data.resData.length; i++) {
    			if (!vm.ass_data.resData[i].isChecked) {
    				vm.allItemsSelected = false;
    				return;
    			}
    		}
    		vm.allItemsSelected = true;
    	};
    	
    	/*========ANGULAR DATE PICKER============*/
    	vm.today = function() {
    		vm.fromDate = new Date();
    		vm.toDate = new Date();
    	};
    	vm.today();
    	
    	vm.clear = function() {
    		vm.fromDate = null;
    		vm.toDate = null;
    	};

    	vm.dateOptions = {
    			//dateDisabled: disabled,
    			formatYear: 'yy',
    			//maxDate: new Date(2020, 5, 22),
    			minDate: new Date(2000, 5, 22),
    			startingDay: 1
    	};

    	// Disable weekend selection
    	function disabled(data) {
    		var date = data.date,
    		mode = data.mode;
    		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    	}

    	vm.fromOpen = function(){
    		vm.from.opened = true;
    	};
			  
    	vm.toOpen = function(){
    		vm.to.opened = true;  
    	};
			  
    	vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    	vm.format = vm.formats[0];
    	vm.altInputFormats = ['M!/d!/yyyy'];
    	
    	vm.from = {
    			opened: false
    	};
    	vm.to = {
    			opened: false
    	};
				
    	/*function getDayClass(data) {
    		var date = data.date,
    		mode = data.mode;
    		if (mode === 'day') {
    			var dayToCheck = new Date(date).setHours(0,0,0,0);
						
    			for (var i = 0; i < $scope.events.length; i++) {
    				var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
							
    				if (dayToCheck === currentDay) {
    					return $scope.events[i].status;
    				}
    			}
    		}
				
    		return '';
    	}*/
	    
	    vm.assetSearchData();
	}]);
})();
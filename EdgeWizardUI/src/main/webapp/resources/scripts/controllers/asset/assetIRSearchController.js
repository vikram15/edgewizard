(function() {
    'use strict';
    angular.module('rfidDemoApp.controller')
    .controller('AssetIRSearchController', ['$uibModalInstance','AuthService','AssetService','$rootScope', '$scope','MasterService', 
                                          function($uibModalInstance, AuthService, AssetService, $rootScope, $scope, MasterService) {
			
    	var vm = this ;
    	vm.tableHeader = "Asset Search";
    	vm.searchBy = "Asset Id";
    	//pagination
    	vm.currentPage = 1;
    	vm.maxSize = 10;
    	vm.comp_Id = AuthService.getCompanyId();
    	
    	vm.assetDetails = function(){
    		vm.requestParams = {
    				"searchBy" : vm.searchBy,
    				"categoryType":vm.categoryType,
    				"batchName":vm.batchName,
    				"divisionName":vm.divisionName,
    				"sectorName":vm.sectorName,
    				"locationName" : vm.locationName,
    				"departmentName":vm.departmentName,
    				"manufactureName" : vm.manufactureName,
    				"vendorName":vm.vendorName,
    				
    				"searchValue" :vm.searchValue,
    				"currentPage":vm.currentPage,
    				"companyId":vm.comp_Id,
    				"startDate":moment(vm.fromDate).valueOf(),
    				"endDate":moment(vm.toDate).valueOf(),
    		};	
    		AssetService.assetSearchData(vm.requestParams, vm.onAssetSearchSuccess, vm.onAssetSearchError);
    	};
    	
    	vm.onAssetSearchSuccess = function(response){
    		if(response.data.resData){
    			vm.ass_data = response.data;
    			vm.totalItems = vm.ass_data.resData[0].totalAssetSearch;
    			vm.itemsPerPage = vm.ass_data.resData[0].RecordPerPage;
    		}
    	};
    	
    	vm.onAssetSearchError= function(){
    		
    	};
			
    	vm.getDivisionDetails = function() {
    		vm.requestParams = {
    				"companyId" : vm.comp_Id
    		};	
    		MasterService.DivisionDetailService(vm.requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
    	};

    	vm.onDivisionInfoSuccess = function(response) {
    		vm.divisionList = response.data;
    		/*vm.categoryType = vm.category_list.resData[0].categoryPrimery.categoryId;*/
    	};

    	vm.onDivisionInfoError = function(response) {
    		/*console.log("error");*/
    	};
			
    	vm.categoryList = function() {
    		vm.requestParams = {
    				"categoryPrimery": {
    					"category": 0,
    					"companyId": vm.comp_Id
    				}
    		};
    		MasterService.getCategoryAll(vm.requestParams, vm.getCategoryAllSucc, vm.getCategoryAllError)
    	};

    	vm.getCategoryAllSucc = function(response) {
    		vm.category_list = response.data;
    		vm.categoryType = vm.category_list.resData[0].categorynm;
    	};

    	vm.getCategoryAllError = function(response) {
    		console.log("error");
    	};
		    
    	vm.getBatchDetails = function() {
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		}
    		MasterService.getBatchLotData(vm.requestParams, vm.onSuccessBatchLot, vm.onErrorBatchLot);
    	};

    	vm.onSuccessBatchLot = function(response) {
    		vm.batchLotList = response.data;
    		vm.batchName = vm.batchLotList.resData[0].batchName;
    	};

    	vm.onErrorBatchLot = function(response) {
    		console.log("error block");
    	};
			
    	vm.getSectorDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		}
    		MasterService.SectorListService(vm.requestParams, vm.onSectorSuccess, vm.onSectorError);
    	};

    	vm.onSectorSuccess = function(response){
    		vm.sect_data = response.data;
    		vm.sectorName = vm.sect_data.resData[0].sectorNm
    	};

    	vm.onSectorError = function(){
    		
    	};

    	vm.getLocationDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		};
    		MasterService.LocationListService(vm.requestParams, vm.onLSuccess, vm.onLError);
    	};

    	vm.onLSuccess = function(response){
    		vm.loc_data = response.data;
    		vm.locationName = vm.loc_data.resData[0].locationNm;
    	};

    	vm.onLError = function(response){

    	};

    	vm.getDepartmentDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		};
    		MasterService.DepartmentListService(vm.requestParams, vm.onDepSuccess, vm.onDepError);
    	};

    	vm.onDepSuccess = function(response){
    		vm.depart_data = response.data;
    		vm.departmentName = vm.depart_data.resData[0].deptNm;
    	};

    	vm.onDepError = function(response){	

    	};

    	vm.getManufacturerDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		}
    		MasterService.manufactureAllDetails(vm.requestParams, vm.onManSuccess, vm.onManError);
    	};

    	vm.onManSuccess = function(response){
    		vm.manufactureDt = response.data;
    		vm.manufactureName = vm.manufactureDt.resData[0].manufactureNm;
    	};

    	vm.onManError = function(response){

    	};

    	vm.getVendorDetails = function(){
    		vm.requestParams = {
    				"companyId": vm.comp_Id
    		}
    		MasterService.getVendorData(vm.requestParams, vm.onVenSuccess, vm.onVenError);
    	};

    	vm.onVenSuccess = function(response){
    		vm.vendordetails = response.data;
    		vm.vendorName = vm.vendordetails.resData[0].vendorName;
    	};

    	vm.onVenError = function(){

    	};
			
    	/*vm.allSelect = true;*/
    	vm.allItemsSelected = false;
			
		// This executes when entity in table is checked
    	$scope.selectEntity = function () {
    		for (var i = 0; i < vm.ass_data.resData.length; i++) {
    			/*console.log(vm.ass_data.resData, "44444");*/
    			if (!vm.ass_data.resData[i].isChecked) {
    				/*console.log(vm.ass_data.resData, "555555");*/
    				vm.allItemsSelected = false;
    				return;
    			}
	            }
	            /*console.log(vm.ass_data.resData, "DDDD3333");*/
    		vm.allItemsSelected = true;
    	};
	
    	// This executes when checkbox in table header is checked
    	$scope.selectAll = function () {
    		// Loop through all the entities and set their isChecked property
    		for (var i = 0; i < vm.ass_data.resData.length; i++) {
    			vm.ass_data.resData[i].isChecked = vm.allItemsSelected;
    			
	    	       }
    		/* console.log(vm.ass_data.resData, "DDDD");*/
    	};
    	/*vm.selectChange = function(t){
				console.log("sdfsdf" +JSON.stringify(vm.ass_data.resData));
				
			};*/
			
    	vm.assetSelectFun = function(){
    		/*console.log(vm.ass_data.resData + "BBBBBB");*/
    		vm.newData = [];
    		for(var i= 0; i< vm.ass_data.resData.length; i++){
    			if(vm.ass_data.resData[i].isChecked === true){
    				vm.newData.push(vm.ass_data.resData[i]);
    			}
    		}
				
    		/*	if(vm.assetSelect !== undefined){*/

    		$uibModalInstance.dismiss('cancel');
    		$rootScope.$broadcast("assetInfo", vm.newData);
    		/*}*/
    		/*if(vm.assetSelect !== undefined){
						$uibModalInstance.dismiss('cancel');
						$rootScope.$broadcast("assetInfo", vm.assetSelect);
					}*/
    	};		
			    
    	/*========ANGULAR DATE PICKER============*/
    	vm.today = function() {
    		vm.fromDate = new Date();
    		vm.toDate = new Date();
    	};
    	vm.today();
    	
    	vm.clear = function() {
    		vm.fromDate = null;
    		vm.toDate = null;
    	};

    	vm.dateOptions = {
    			//dateDisabled: disabled,
    			formatYear: 'yy',
    			//maxDate: new Date(2020, 5, 22),
    			minDate: new Date(2000, 5, 22),
    			startingDay: 1
    	};

    	// Disable weekend selection
    	function disabled(data) {
    		var date = data.date,
    		mode = data.mode;
    		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    	}

    	vm.fromOpen = function(){
    		vm.from.opened = true;
    	};
			  
    	vm.toOpen = function(){
    		vm.to.opened = true;  
    	};
			  
    	vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    	vm.format = vm.formats[0];
    	vm.altInputFormats = ['M!/d!/yyyy'];
    	
    	vm.from = {
    			opened: false
    	};
    	vm.to = {
    			opened: false
    	};
				
    	function getDayClass(data) {
    		var date = data.date,
    		mode = data.mode;
    		if (mode === 'day') {
    			var dayToCheck = new Date(date).setHours(0,0,0,0);
						
    			for (var i = 0; i < $scope.events.length; i++) {
    				var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
							
    				if (dayToCheck === currentDay) {
    					return $scope.events[i].status;
    				}
    			}
    		}
				
    		return '';
    	}
    	
    	vm.clearFields = function(){
    		vm.searchValue = "";
    	};
			  
    	vm.cancel = function() {
    		$uibModalInstance.dismiss('cancel');
    	};
			
    	vm.assetDetails();
    }]);
})();
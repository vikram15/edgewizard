(function(){
  'use strict';
  angular.module('rfidDemoApp.controller')
  .controller('AssetController', ['$scope','$uibModal','AssetService','AuthService','$rootScope','$state','$window','$anchorScroll','MasterService', 
                                  function($scope, $uibModal, AssetService, AuthService, $rootScope, $state, $window, $anchorScroll, MasterService){
    var vm = this ;
    
    vm.currentPage = 1;
    vm.maxSize = 5;
    vm.selectAssetType = "Asset Id";
    $rootScope.pageTitle = "Asset | EdgeWizard";
    vm.assetDataNew = [];

    vm.comp_Id = AuthService.getCompanyId();

  	vm.pageChanged = function() {
  		vm.requestParams = {
  				"searchBy": vm.selectAssetType,
  				"searchValue" :vm.searchValue,
  				"currentPage":vm.currentPage,
  				"companyId":vm.comp_Id,
  				"categoryType":vm.categoryType, 
  				"batchName":vm.batchName,
  				"divisionName":vm.divisionName,
  				"sectorName":vm.sectorName,
  				"locationName":vm.locationName,
  				"departmentName":vm.departmentName,
  				"manufactureName": vm.manufactureName,
  				"vendorName":vm.vendorName,
  				"inDocRef":vm.docrefNo,
  				"outDocRef" : vm.outDocrefNo,
  				"startDate":moment(vm.fromDate).valueOf(),
  				"endDate":moment(vm.toDate).valueOf(),
  		};
  		if(vm.selectAssetType){
  			AssetService.searchAssetDetails(vm.requestParams, vm.searchAsssuccess1, vm.searchAssError1);
  		}
    };

    vm.searchAsssuccess1 = function(response){
    	vm.result = response.data.resData;
    	if(vm.result){
    		vm.assetPag_data = response.data.resData.assetList;
        	vm.assetDataNew.push(vm.assetPag_data);
        	vm.totalItems = response.data.resData.totalAssetSearch;
        	vm.itemsPerPage = response.data.resData.RecordPerPage;
    	}
    	
    };
    vm.searchAssError1 = function(response){
    	console.log("error");
    };

    vm.editAssetFun = function(assetRowData){
    	/*$state.go('homeDashboard.asset.assetAdd',{
    		'assetID' : assetRowData.assetId,
    	});*/
    	
    	$state.go('homeDashboard.asset.assetAddUpdate',{
    		'assetID' : assetRowData.assetId,
    		'from':'WEB'
    	})
    	
//    	$state.go('euiAndroid',{
//    		'un' : "Vikram",
//    		'ps':'123',
//    		'cmpId' :1,
//    		'sysname' : 'ATS',
//    		'activity':'AU',
//    		'epcId':'EPOMMDFDI2343'
//    	})
    	
    	
//    	$state.go('homeDashboard.asset.assetAddUpdateByTagId',{
//    		'tagId' : assetRowData.tagId,
//    		'from' :'AND'
//    	})
    	/*$state.go('homeDashboard.asset.assetEdit',{
    		'assetData' : JSON.stringify(assetRowData),
    	});*/
    };

    vm.deleteAssetFun = function(assetPag){
    	swal({
            title: "Are you sure?",
           /* text: "Are you sure want to delete!",*/
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel please!",
            closeOnConfirm: false,
            closeOnCancel: false },
            function(isConfirm){
              if (isConfirm) {

                vm.requestParams = {
                		"assetId":assetPag.assetId,
                		"companyId": AuthService.getCompanyId()
                };

                AssetService.assetDeleteDetails(vm.requestParams, vm.assetDeleteSucc, vm.assetDeleteError);
              }
              else {
            	  swal("Cancelled", "Your imaginary file is safe :)", "error");
              }
            });
    };

    vm.assetDeleteSucc = function(response){
    	console.log(response.data.resCode);
    	if (response.data.resCode === "res0000") {
			swal("Deleted!", response.data.resData,"success");
			 vm.pageChanged();
		} else if (response.data.resCode === "res0100") {
			swal("Not deleted!", response.data.resData, "error");
		}else{
			
		}
    };

    vm.assetDeleteError = function(response){
    	console.log("error");
    };

    /*---click on row with arrow down mark---*/
    vm.assetRowData = function(asse){
    
    	for(var i=0; i < vm.assetDataNew[0].length; i++){
    		if(vm.assetDataNew[0][i].assetId === asse.assetId){
    			vm.assetDataNew[0][i].expanded = true;
    		}else{
    			vm.assetDataNew[0][i].expanded = false;
    			vm.assetLogo = "";
    		}
    		/*vm.assetDataNew[i].expanded = false;*/
    	}
    	
    	vm.requestParams = {
				   'assetId' :asse.assetId,
				   'companyId':vm.comp_Id
		   }
    	AssetService.assetGetByID(vm.requestParams, vm.assGetByIDSuccess, vm.assGetByIDError);
    };

    vm.assGetByIDSuccess = function (response){
    	vm.single_ass = response.data;
        vm.assetId = vm.single_ass.resData[0][0].assetId;
        vm.photo = vm.single_ass.resData[0][0].photo;
        
        
        if(vm.photo){
        	vm.getImage(vm.photo);
        	vm.assImg = false;
        }else{
        	vm.assImg = true;
        }
    };

    vm.assGetByIDError = function(response){
      console.log("error");
    };
    /*---end---*/
    
    vm.assetRowDataEmpt = function(asse){
    	vm.photo = "";
    };
    
    vm.getImage = function(photo) {
		vm.requestParams = {
			"companyId" : vm.comp_Id,
			"downloadFrom" : "ASSET",
			"imageId" : vm.assetId.toString(),
			"imageName" : photo,
		};
		AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
	}

	vm.onGetLogoSuccess = function(response) {
		vm.assetLogo = response.data.resData.image;
	};
	vm.onGetLogoError = function(response) {
		console.log("error")
	}

    vm.categoryList = function() {
		vm.requestParams = {
				"categoryPrimery": {
					"category": 0,
					"companyId": vm.comp_Id
				}
		};
		MasterService.getCategoryAll(vm.requestParams, vm.getCategoryAllSucc, vm.getCategoryAllError)
	};

	vm.getCategoryAllSucc = function(response) {
		vm.category_list = response.data;
		vm.categoryType = vm.category_list.resData[0].categorynm;
	};

	vm.getCategoryAllError = function(response) {
		console.log("error");
	};
	
	vm.getBatchDetails = function() {
		vm.requestParams = {
				"companyId": vm.comp_Id
		}
		MasterService.getBatchLotData(vm.requestParams, vm.onSuccessBatchLot, vm.onErrorBatchLot);
	};

	vm.onSuccessBatchLot = function(response) {
		vm.batchLotList = response.data;
		vm.batchName = vm.batchLotList.resData[0].batchName;
	};

	vm.onErrorBatchLot = function(response) {
		console.log("error block");
	};

	vm.getDivisionDetails = function() {
		vm.requestParams = {
				"companyId" : vm.comp_Id
		};	
		MasterService.DivisionDetailService(vm.requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
	};

	vm.onDivisionInfoSuccess = function(response) {
		vm.divisionList = response.data;
		vm.divisionName = vm.divisionList.resData[0].divisionNm;
	};

	vm.onDivisionInfoError = function(response) {
		/*console.log("error");*/
	};
	
	vm.getSectorDetails = function(){
		vm.requestParams = {
				"companyId": vm.comp_Id
		}
		MasterService.SectorListService(vm.requestParams, vm.onSectorSuccess, vm.onSectorError);
	};

	vm.onSectorSuccess = function(response){
		vm.sect_data = response.data;
		vm.sectorName = vm.sect_data.resData[0].sectorNm
	};

	vm.onSectorError = function(){
		
	};

	vm.getLocationDetails = function(){
		vm.requestParams = {
				"companyId": vm.comp_Id
		};
		MasterService.LocationListService(vm.requestParams, vm.onLSuccess, vm.onLError);
	};

	vm.onLSuccess = function(response){
		vm.loc_data = response.data;
		vm.locationName = vm.loc_data.resData[0].locationNm;
	};

	vm.onLError = function(response){

	};

	vm.getDepartmentDetails = function(){
		vm.requestParams = {
				"companyId": vm.comp_Id
		};
		MasterService.DepartmentListService(vm.requestParams, vm.onDepSuccess, vm.onDepError);
	};

	vm.onDepSuccess = function(response){
		vm.depart_data = response.data;
		vm.departmentName = vm.depart_data.resData[0].deptNm;
	};

	vm.onDepError = function(response){	

	};

	vm.getManufacturerDetails = function(){
		vm.requestParams = {
				"companyId": vm.comp_Id
		}
		MasterService.manufactureAllDetails(vm.requestParams, vm.onManSuccess, vm.onManError);
	};

	vm.onManSuccess = function(response){
		vm.manufactureDt = response.data;
		vm.manufactureName = vm.manufactureDt.resData[0].manufactureNm;
	};

	vm.onManError = function(response){

	};

	vm.getVendorDetails = function(){
		vm.requestParams = {
				"companyId": vm.comp_Id
		}
		MasterService.getVendorData(vm.requestParams, vm.onVenSuccess, vm.onVenError);
	};

	vm.onVenSuccess = function(response){
		vm.vendordetails = response.data;
		vm.vendorName = vm.vendordetails.resData[0].vendorName;
	};

	vm.onVenError = function(){

	};
	
	vm.getInOutDocRef = function() {
		vm.requestParams = {
				"status" : 0,
				"companyId" : vm.comp_Id
		}
		AssetService.getInOutDocRef(vm.requestParams, vm.inOutDocrefSucc, vm.inOutDocrefErr);
	}
	
	vm.inOutDocrefSucc = function(response) {
		vm.docRef_data = response.data;
		vm.docrefNo = response.data.resData[0].docrefName;
	}
	
	vm.inOutDocrefErr = function() {
		console.log("ERROR in doc ref fetch");
	}
	
	vm.getOutDocRef = function(){
		vm.requestParams = {
				"status" : 1,
				"companyId" : vm.comp_Id
		}
		AssetService.getInOutDocRef(vm.requestParams, vm.outDocrefSucc, vm.outDocrefErr);
	};
	
	vm.outDocrefSucc = function(response){
		vm.outDocrefData = response.data;
		vm.outDocrefNo = response.data.resData[0].docRefName;
	};
	
	vm.outDocrefErr = function(response){
		
	};
	
	/*========ANGULAR DATE PICKER============*/
	vm.today = function() {
		vm.fromDate = new Date();
		vm.toDate = new Date();
	};
	vm.today();
	
	vm.clear = function() {
		vm.fromDate = null;
		vm.toDate = null;
	};

	vm.dateOptions = {
			//dateDisabled: disabled,
			formatYear: 'yy',
			//maxDate: new Date(2020, 5, 22),
			minDate: new Date(2000, 5, 22),
			startingDay: 1
	};

	// Disable weekend selection
	function disabled(data) {
		var date = data.date,
		mode = data.mode;
		return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
	}

	vm.fromOpen = function(){
		vm.from.opened = true;
	};
		  
	vm.toOpen = function(){
		vm.to.opened = true;  
	};
		  
	vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
	vm.format = vm.formats[0];
	vm.altInputFormats = ['M!/d!/yyyy'];
	
	vm.from = {opened: false};
	vm.to = {opened: false};
	
	vm.viewMap = function(data){
		if(data.tagId){
			AssetService.getLocation(data.tagId, vm.onSuccessGetlocation, vm.onErrorGetLocation);
		}
	};
	
	vm.onSuccessGetlocation = function(response){
		if(response.data.resData){
			var result = response.data.resData;
			var aTag = document.createElement('a');
			aTag.setAttribute('href',"http://maps.google.com/maps?q=" + result.latitude + "," +result.longitude );
			aTag.setAttribute('target',"_blank");
			window.open(aTag);
		}
	};
	
	vm.onErrorGetLocation = function(){};
	
	
    vm.pageChanged();
  }]);
})();

(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
		.controller('SplitAssetController',['$uibModalInstance','singleAssData','MasterService','AuthService','AssetService','$state','$uibModal','$rootScope',
		                                    function($uibModalInstance, singleAssData, MasterService, AuthService, AssetService, $state, $uibModal, $rootScope){
			var vm = this ;
			vm.ass_data = singleAssData;
			
			vm.comp_Id  = AuthService.getCompanyId();
			vm.comp_name = AuthService.getCompanyName();
			vm.categoryType = vm.ass_data.categoryId;
			vm.division = vm.ass_data.divisionId;
			vm.sector = vm.ass_data.sectorId;
			vm.location = vm.ass_data.locationId;
			vm.department = vm.ass_data.departmentId;

			vm.categoryList = function(){
				vm.requestParams = {
						"categoryPrimery":{
							"category":0,
							"companyId":vm.comp_Id
						}
				};
				MasterService.getCategoryAll(vm.requestParams, vm.getCategoryAllSucc, vm.getCategoryAllError)
			};

			vm.getCategoryAllSucc = function(response){
				vm.category_list = response.data;
			};

			vm.getCategoryAllError= function(response){
				console.log("error");
			};

			vm.getDivisionDetails = function (){
				var requestParams  = {
						"companyId": vm.comp_Id
				};
				MasterService.DivisionDetailService(requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
			};

			vm.onDivisionInfoSuccess = function(response){
				vm.divisionList = response.data;
				if (vm.divisionList.resData) {
					vm.division = vm.divisionList.resData[0].divisionId;
					vm.divChange();
					vm.sector = "";
				}
			};

			vm.onDivisionInfoError = function(response){
				console.log("error");
			};

			vm.divChange = function(){
				vm.divisionId = {
						"divisionId" : vm.division
				};
				AssetService.getSectDetailsByDiv(vm.divisionId, vm.getSectDetBySucc, vm.getSectDetByErr);
			};	

			vm.getSectDetBySucc = function(response){
				vm.sect_data = response.data;
				if (vm.sect_data.resData) {
					vm.sector = vm.sect_data.resData[0].sectorId;
					vm.secChange();
					vm.location = "";
				}
			};

			vm.getSectDetByErr = function(response){
				console.log("error");
			};

			vm.secChange = function(){
				vm.sectorId = {
						"sectorId" : vm.sector
				};
				AssetService.getLocDetailsBySecID(vm.sectorId, vm.secChangeSuccess, vm.secChangeError);
			};

			vm.secChangeSuccess = function(response){
				vm.loc_data = response.data;
				if (vm.loc_data.resData) {
					vm.location = vm.loc_data.resData[0].locationId;
					vm.locChange();
				} else {
					vm.department = "";
				}
			};

			vm.secChangeError = function(){
				console.log("error");
			};

			vm.locChange = function(){
				vm.locationId = {
						"locationId" : vm.location
				};
				AssetService.getDepDetailsByLocID(vm.locationId, vm.locChangeSuccess, vm.locChangeError);
			};

			vm.locChangeSuccess = function(response){
				vm.depart_data = response.data;
				if (vm.depart_data.resData)
					vm.department = vm.depart_data.resData[0].departmentId;
			};

			vm.locChangeError = function(){
				console.log("error");
			};

			vm.assetError = false;
			vm.splitAssetFun = function(){
				if(parseInt(vm.assetQty) <= (vm.ass_data.assetQty)){
					vm.assetError = false;
					vm.ass_data.rackId = vm.ass_data.rackId.rackId; 
					vm.ass_data.tagId = vm.tagId;
					vm.ass_data.divisionId = vm.division;
					vm.ass_data.sectorId = vm.sector;
					vm.ass_data.locationId = vm.location;
					vm.ass_data.departmentId = vm.department;
					vm.ass_data.oldSplitQuantity = parseInt(vm.ass_data.assetQty) - (vm.assetQty),
					vm.ass_data.assetQty = vm.assetQty;
					
					/*vm.requestParams = {
							"assetId":vm.ass_data.resData[0][0].assetId,
							"assetNm": vm.ass_data.resData[0][0].assetNm,
							"tagId": vm.tagId,
							"groupMaster": vm.ass_data.resData[0][0].groupMaster,
							"groupSubMaster": vm.ass_data.resData[0][0].groupSubMaster,
							"companyId": vm.ass_data.resData[0][0].companyId,
							"divisionId":vm.division,
							"sectorId":vm.sector,
							"locationId":vm.location,
							"departmentId" :vm.department,
							"manufactureId":vm.ass_data.resData[0][0].manufactureId,
							"vendorId": vm.ass_data.resData[0][0].vendorId,
							"inDocRefNo": vm.ass_data.resData[0][0].inDocRefNo,
							"outDocRefNo": vm.ass_data.resData[0][0].outDocRefNo,
							"categoryId": vm.ass_data.resData[0][0].categoryId,
							"batchId": vm.ass_data.resData[0][0].batchId,
							"photo": vm.ass_data.resData[0][0].photo,
							"serialNo": vm.ass_data.resData[0][0].serialNo,
							"cost": vm.ass_data.resData[0][0].cost,
							"warrantStDt": vm.ass_data.resData[0][0].warrantStDt,
							"warrantEdDt": vm.ass_data.resData[0][0].warrantEdDt,
							"mainDt": vm.ass_data.resData[0][0].mainDt,
							"aqsDate": vm.ass_data.resData[0][0].aqsDate,
							"expiryDt": vm.ass_data.resData[0][0].expiryDt,
							"assetDesc": vm.ass_data.resData[0][0].assetDesc,
							"creationDate": vm.ass_data.resData[0][0].creationDate,
							"modDt": vm.ass_data.resData[0][0].modDt,
							"assetQty": vm.assetQty,
							"oldSplitQuantity":vm.ass_data.resData[0][0].assetQty - vm.assetQty,
							"inRegBy": vm.ass_data.resData[0][0].inRegBy,
							"inRegistrationDate": vm.ass_data.resData[0][0].inRegistrationDate,
							"rackId": vm.ass_data.resData[0][0].rackId,
							"slaveId": vm.ass_data.resData[0][0].slaveId,
							"taxValue": vm.ass_data.resData[0][0].taxValue,
							"tagHist":vm.ass_data.resData[0][0].tagHist,
							"status":vm.ass_data.resData[0][0].status,
							"firstDateTime":vm.ass_data.resData[0][0].firstDateTime,
							"inVerifyBy":vm.ass_data.resData[0][0].inVerifyBy,
							"inVerifyDate":vm.ass_data.resData[0][0].inVerifyDate,
							"outVerifyBy":vm.ass_data.resData[0][0].outVerifyBy,
							"outVerifyDt":vm.ass_data.resData[0][0].outVerifyDt
					}*/
					AssetService.splitAddDetails(vm.ass_data, vm.splitSuccess, vm.splitError);
					
				}else{
					vm.assetError = true;
				};
			};

			vm.splitSuccess = function(response){
				if(response.data.resCode === "res0000"){
					$uibModalInstance.dismiss('cancel');
					swal("Saved", "Split saved successfully!", "success");
				}
			};

			vm.splitError = function(response){
				console.log("error");
			}
		
			vm.tagSelectFun = function(){
				var modalInstance = $uibModal.open({
					backdrop: 'static',
					animation: vm.animationsEnabled,
					templateUrl: 'resources/views/tag/tagSearch.html',
					controller: 'TagSearchController',
					controllerAs : 'tagSearchController',
					resolve: {
						tagSelect: function () {
							return angular.copy(vm.tagId);
						}
					}
				});

			modalInstance.result.then(function (selectedItem) {
				console.log("ddd"+selectedItem);
			}, function (data) {
				vm.tag = JSON.parse(data);
				vm.tagId = vm.tag.tagId;
			});
			};
	    
	    vm.cancel = function () {
	    	$uibModalInstance.dismiss('cancel');
	    };

	    vm.categoryList();
	    vm.getDivisionDetails();

	}]);
})();

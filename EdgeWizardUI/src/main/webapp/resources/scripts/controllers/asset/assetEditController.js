(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('AssetEditController',['AuthService','MasterService','AssetService','$state','$stateParams','$uibModal','$localStorage','$timeout','$rootScope',
	            function( AuthService, MasterService, AssetService, $state, $stateParams, $uibModal, $localStorage, $timeout, $rootScope ){
		
		var vm = this ;
		vm.comp_Id  = AuthService.getCompanyId();
	    vm.comp_name = AuthService.getCompanyName();
	    vm.asset_data = JSON.parse($stateParams.assetData);
	    console.log(vm.asset_data,"678678967");
	    	    
	    $rootScope.$on("SendUp", function (evt, data) {
	        vm.tag = JSON.parse(data);
	        vm.tagId = vm.tag.tagId;
	    });
	    
	    vm.getImage = function(){
			vm.requestParams  = {
				"companyId": vm.comp_Id
			};
			AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
		}
	    
	    vm.onGetLogoSuccess = function(response){
			vm.assetLogo = response.data.resData.image;
		};
		vm.onGetLogoError = function(response){
			console.log("error")
		}
		
	   vm.selectedAssetData = function(){
		   vm.requestParams = {
				   'assetId' :vm.asset_data.assetId,
				   'companyId':vm.comp_Id
		   }
		   AssetService.assetGetByID(vm.requestParams, vm.assGetByIDSuccess, vm.assGetByIDError);
	   };
		 
	   vm.assGetByIDSuccess = function (response){
		   console.log(response.data.resData[0][0],"%$$$");
		   vm.single_ass = response.data;
		   vm.tagId = vm.single_ass.resData[0][0].tagId;
		   vm.categoryType = vm.single_ass.resData[0][0].categoryId;
		   vm.batchLot = vm.single_ass.resData[0][0].batchId;
		   vm.manufacturer = vm.single_ass.resData[0][0].manufactureId;
		   vm.vendor = vm.single_ass.resData[0][0].vendorId;
		   vm.division = vm.single_ass.resData[0][0].divisionId;
		   vm.sector = vm.single_ass.resData[0][0].sectorId;
		   vm.location = vm.single_ass.resData[0][0].locationId;
		   vm.department = vm.single_ass.resData[0][0].departmentId;
		   vm.photo = vm.single_ass.resData[0][0].photo;
		   vm.warrantStDt = new Date(vm.single_ass.resData[0][0].warrantStDt);
		   vm.warrantEdDt = new Date(vm.single_ass.resData[0][0].warrantEdDt);
		   vm.aqsDate = new Date(vm.single_ass.resData[0][0].aqsDate);
		   vm.expiryDt = new Date(vm.single_ass.resData[0][0].expiryDt);
		   vm.maintenanceDt = new Date(vm.single_ass.resData[0][0].mainDt);
		   
		   vm.getImage(vm.photo);
	   };
	   vm.getImage = function(photo){
	    	vm.requestParams  = {
	    			"companyId": vm.comp_Id,
	    			"downloadFrom":"ASSET",
	    			"imageId":vm.asset_data.assetId.toString(),
	    			"imageName":photo,
	    		};
			AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
		}
	   
	   vm.onGetLogoSuccess = function(response){
			vm.assetLogo = response.data.resData.image;
		};
		vm.onGetLogoError = function(response){
			console.log("error")
		}
	   vm.assGetByIDError = function(response){
		   console.log("error");
	   };
		
	   vm.categoryList = function(){
		   vm.requestParams = {
				   "categoryPrimery":{
					   "category":0,
					   "companyId":vm.comp_Id
				   }
		   };
		   MasterService.getCategoryAll(vm.requestParams, vm.getCategoryAllSucc, vm.getCategoryAllError)
	   };
		    
	   vm.getCategoryAllSucc = function(response){
		   vm.category_list = response.data;
	   };
		    
	   vm.getCategoryAllError= function(response){
		   console.log("error");
	   };
		
	   vm.getBatchDetails = function(){
		   vm.requestParams = {
				   "companyId": vm.comp_Id
		   }
		   MasterService.getBatchLotData(vm.requestParams, vm.onSuccessBatchLot, vm.onErrorBatchLot);
	   };

	   vm.onSuccessBatchLot = function(response){
		   vm.batchLotList = response.data;
	   };

	   vm.onErrorBatchLot= function(response) {
		   console.log("error block");
	   };

	   vm.manufactureDetails = function(){
		   vm.requestParams  = {
				   "companyId" : vm.comp_Id
		   }
		   MasterService.manufactureAllDetails(vm.requestParams, vm.onManufactureSuccess, vm.onManufactureError);
	   };

	   vm.onManufactureSuccess = function(response){
		   vm.manufactureDt = response.data;
	   };

	   vm.onManufactureError = function(){
		   console.log("error");
	   };

		vm.getVendorDetails = function(){
			vm.requestParams = {
					"companyId" : vm.comp_Id
			};
			MasterService.getVendorData(vm.requestParams, vm.onSuccessVendorData, vm.onErrorVendorData);
		};

		vm.onSuccessVendorData = function(response){
			vm.vendordetails = response.data;
		};

		vm.onErrorVendorData = function(){
			console.log("error block");
		};

		vm.getDivisionDetails = function (){
			var requestParams  = {
						"companyId": vm.comp_Id
					};
			MasterService.DivisionDetailService(requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
		};

		vm.onDivisionInfoSuccess = function(response){
			vm.divisionList = response.data;
			$timeout(function(){
				vm.divChange();
			},1000)
		};
	
		vm.onDivisionInfoError = function(response){
			console.log("error");
		};

		vm.divChange = function(){
			vm.divisionId = vm.division;
			AssetService.getSectDetailsByDiv(vm.divisionId, vm.getSectDetBySucc, vm.getSectDetByErr);
		};

		vm.getSectDetBySucc = function(response){
			vm.sect_data = response.data;
			vm.secChange();
		};

		vm.getSectDetByErr = function(response){
			console.log("error");
		};

		vm.secChange = function(){
			vm.sectorId = vm.sector;
			AssetService.getLocDetailsBySecID(vm.sectorId, vm.secChangeSuccess, vm.secChangeError);
		};

		vm.secChangeSuccess = function(response){
			vm.loc_data = response.data;
			vm.locChange();
		};

		vm.secChangeError = function(){
			console.log("error");
		};

		vm.locChange = function(){
			vm.locationId = vm.location;
			AssetService.getDepDetailsByLocID(vm.sectorId, vm.locChangeSuccess, vm.locChangeError);
		};

		vm.locChangeSuccess = function(response){
			vm.depart_data = response.data;
		};

		vm.locChangeError = function(){
			console.log("error");
		};
		vm.newLogo1 = "imageName (5).jpg";
		/*************************WEBCAM ***************************************/
		vm.showDiv = true;
		 vm.webcamClick = function (){
			   	//vm.showDiv = true;
				  Webcam.attach('#my_camera');
			  }
			  //setTimeout(function(){  }, 1000);
		vm.reset = function () {
			   vm.showDiv = false;
			   Webcam.reset('#my_camera');
		}

	    vm.take_snapshot = function () {
	    	  vm.showImage = true;
	          Webcam.snap( function(data_uri) {
	        	  //vm.assetLogo = data_uri;
	        	  
	        	  if(data_uri){
	        		  vm.assetLogo = data_uri.split(",").pop();
	        		  /***********************base64 to file**********************************/
	            	  var imageBase64 = vm.assetLogo;
	            	  var blob = new Blob([imageBase64], {type: 'image/jpg'});
	            	 
	            	  vm.file = new File([blob], 'test123.jpg', {type: 'image/jpg'});
	            	  //vm.file.type = "image/jpeg"
	            	  console.log(vm.file,"filefilefilefilefilefilefile");
	            	  //vm.assetLogo = data_uri;
	            	  	vm.assetLogo = data_uri.split(",").pop();
	            	  vm.result = data_uri;
	            	  document.getElementById('anchor').setAttribute( 'href',data_uri);//download image
	        	  }
	        	  //console.log(vm.assetLogo,"vm.assetLogovm.assetLogovm.assetLogovm.assetLogo");
	        	  
	              //document.getElementById('my_result').innerHTML = '<img src="'+data_uri+'"/>';
	          } );
	    }
	      	/*****************************************************************************************************/
	      
		vm.assetEditSaveFun = function(){
			console.log("edit called");
			vm.requestParams = {
					"assetId": vm.single_ass.resData[0][0].assetId,
					"assetNm": vm.single_ass.resData[0][0].assetNm,
					"tagId": vm.tagId,
					"groupMaster": 0,
					"groupSubMaster": 0,
					"companyId": vm.comp_Id,
					"divisionId":vm.division,
					"sectorId":vm.sector,
					"locationId":vm.location,
					"departmentId" : vm.department,
					"manufactureId": vm.manufacturer,
					"vendorId": vm.vendor,
					"inDocRefNo": 1,
					
					"categoryId": vm.categoryType,
					"batchId": vm.batchLot,
					"photo": vm.single_ass.resData[0][0].photo,
					"serialNo": vm.single_ass.resData[0][0].serialNo,   
					"cost": vm.single_ass.resData[0][0].cost,
					"warrantStDt": Date.parse(vm.warrantStDt),
				    "warrantEdDt": Date.parse(vm.warrantEdDt),
				    "mainDt": Date.parse(vm.maintenanceDt),
				    "aqsDate": Date.parse(vm.aqsDate),  
				    "expiryDt": Date.parse(vm.expiryDt),
					"assetDesc": vm.single_ass.resData[0][0].assetDesc,
					
					"modDt": 1468308061000,
					"assetQty": vm.single_ass.resData[0][0].assetQty,
					"inRegBy": "0",
				    "inRegistrationDate": 1468308061000,
				    "rackId": 1,
				    "slaveId": 1,
				    "taxValue":0
			}
			console.log(vm.requestParams);
			AssetService.AssetUpdateDetails(vm.requestParams, vm.assetUpdateSucc, vm.assetUpdateError);
		};

		vm.assetUpdateSucc = function(response){
			console.log("updated successfull");
			if(response.data.resCode === "res0000"){
				swal("Updated", "Asset updated successfully!", "success");
				vm.requestParam = {
			    		"companyId":vm.comp_Id,
			    		"uploadTo":"ASSET",
			    		"imageId":vm.single_ass.resData[0][0].assetId.toString(),
			    		"imageData":vm.result
			    }
				if(vm.assetLogo && vm.assetLogo.name){
			    	AuthService.uploadLogo(vm.requestParam,vm.assetLogo, vm.assetImgSucc, vm.assetImgErr )
			    }
				$state.go('homeDashboard.asset');
			}
			
		};
		vm.assetImgSucc = function(response){
	    	console.log("Image update success");
	    }
	    vm.assetImgErr = function (response){
	    	console.log("Image update fail");
	    }
		vm.assetUpdateError = function(response){
			console.log("error");
		};

		vm.cancel = function () {
			$state.go('homeDashboard.asset');
		};
		
		vm.splitAssetFun = function(){
			var modalInstance = $uibModal.open({
  				backdrop: 'static',
  				animation: vm.animationsEnabled,
  				templateUrl: 'resources/views/asset/splitAsset.html',
  				controller: 'SplitAssetController',
  				controllerAs : 'splitAssetController',
  				resolve: {
  					singleAssData: function () {
  			    		  return angular.copy(vm.single_ass);
  					}
  				}
  			});

  			modalInstance.result.then(function (selectedItem) {
  				/*$scope.selected = selectedItem;*/
  			}, function () {
  				/*$log.info('Modal dismissed at: ' + new Date());
  			    */  /*		$scope.jewellrylist = addJewellryService.query();*/
  			});
		};
		
		vm.outOfStockFun = function (){
			//console.log(vm.single_ass.resData[0][0].assetId,"HHHH");
			swal({
	            title: "Click yes to continue ",
	            text: "Are you sure want to out of stock!",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonColor: "#DD6B55",
	            confirmButtonText: "Yes, continue!",
	            /*cancelButtonText: "No, cancel please!",*/
	            closeOnConfirm: false,
	            closeOnCancel: false },
	            function(isConfirm){
	              if (isConfirm) {
	            	  vm.requestParams = {
	  						"assetId":vm.single_ass.resData[0][0].assetId,
	  						"companyId":vm.comp_Id,
	  						"assetDesc":"no desc"		
	            	  }
	            	 AssetService.outOfstock(vm.requestParams, vm.outOfStcSucc, vm.outOfStcErr)
	              }
	              else {
	            	  swal("Cancelled", "Your asset is safe :)", "error");
	              }
	            });
			
		}
		vm.outOfStcSucc = function(response){
			swal("Updated", "Out of stock successfully!", "success");
			//console.log(response.data.resData,"OUTTTTTT")
		}
		vm.outOfStcErr = function(response){
			console.log("Error in out of stock");
		}
		 vm.tagSelectFun = function(d){
		    	var modalInstance = $uibModal.open({
					backdrop: 'static',
					animation: vm.animationsEnabled,
					templateUrl: 'resources/views/tag/tagSearch.html',
					controller: 'TagSearchController',
					controllerAs : 'tagSearchController',
					resolve: {
						jw: function () {
				    		  return angular.copy(d);
						}
					}
				});

				modalInstance.result.then(function (selectedItem) {
					console.log("ddd"+selectedItem)
				}, function (t) {
					console.log(t);
					console.log('Modal dismissed at: ' + new Date());
				});
		    };
		
		
		vm.categoryList();
  		vm.selectedAssetData();
		vm.getBatchDetails();
		vm.manufactureDetails();
		vm.getVendorDetails();
		vm.getDivisionDetails();
		//vm.getImage();
		
	}]);
})();

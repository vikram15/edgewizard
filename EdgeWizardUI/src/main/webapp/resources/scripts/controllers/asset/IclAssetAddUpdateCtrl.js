(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('IclAssetAddUpdateCtrl',['$stateParams','AuthService','MasterService','AssetService','$uibModal','$cookies','$state','$scope','_','$interval','$rootScope', 
	                                     function($stateParams, AuthService, MasterService, AssetService, $uibModal, $cookies, $state, $scope, _, $interval, $rootScope){
		var vm = this ;
		vm.assetData = {};
		vm.myGroupArr = [];
		vm.companyId = AuthService.getCompanyId();
		vm.companyName = AuthService.getCompanyName();
		vm.dockDoorId = JSON.parse($cookies.get("globals")).dockDoorId;
		vm.dockDoorName = JSON.parse($cookies.get("globals")).dockDoorName;
		var selectedAttachment;
		vm.assetAttachedFileNameData = [];
		vm.assetAttachedMultipartFileData = [];
		vm.rack_data = "";
		vm.isReqFrom = $cookies.get('isReqFrom');
		
		
		vm.initialize = function() {
			vm.paramsData = $stateParams;
		
			//Asset add 
			if(vm.isReqFrom === "AND" && vm.paramsData.activity === "AC"){
				vm.assetPostMode = 'NewMode';
				vm.tableHeader = "Asset Registration";
				vm.astBtn = true;
				vm.newLogo = true;
			};
			
			//Asset Update
			if(vm.isReqFrom === "AND" && vm.paramsData.activity === "AU"){
				vm.assetPostMode = 'EditMode';
				vm.tableHeader = "Asset Update";
				vm.astBtn = false;
				vm.newLogo = true;
				
				vm.requestParams = {
						'tagId' : vm.paramsData.tagId
				}
				if(vm.paramsData.tagId !== '0'){
					AssetService.assetGetByTagId(vm.requestParams, vm.viewSingleAssetSucc, vm.viewSingleAssetError);
				}
			}
			
			
			
//			if (vm.isReqFrom === "AND" && vm.paramsData.activity !== "AU") {
//				vm.assetPostMode = 'NewMode';
//				vm.tableHeader = "Asset Registration";
//				vm.astBtn = true;
//				vm.newLogo = true;
//			} else {
//				vm.assetPostMode = 'EditMode';
//				vm.tableHeader = "Asset Update";
//				vm.astBtn = false;
//				vm.newLogo = true;
//				
//				vm.requestParams = {
//						'tagId' : vm.paramsData.tagId
//				}
//				if(vm.paramsData.tagId !== '0'){
//					AssetService.assetGetByTagId(vm.requestParams, vm.viewSingleAssetSucc, vm.viewSingleAssetError);
//				}
//				else{
//					vm.requestParams = {
//							'assetId' : assetId,
//							'companyId' : vm.companyId
//					}
//					AssetService.assetGetByID(vm.requestParams, vm.viewSingleAssetSucc, vm.viewSingleAssetError);
//				}
			//}
		};
		
		vm.viewSingleAssetSucc = function(response) {
			if(response.data.resData){
				var result = response.data.resData;
				vm.assetData = _.extend(result[0][0],result[1]);
				vm.assetAttachment = vm.assetData.assetAttachments.assetDocumentCount;
				vm.docRefAttachment = vm.assetData.assetAttachments.assetDocRefCount;
				vm.totAttachment = vm.assetData.assetAttachments.totalCount;
				vm.groupMasterType = vm.assetData.groupMaster;
				vm.subGroupType = vm.assetData.subGroupType;
				
				//TEST Group Master and subGroup Master
				if(vm.assetData.groupMaster === "null" || vm.assetData.groupMaster === null){
					vm.assetData.groupMaster = "";
				}
				vm.myGroupArr = [{
					"gpId" : vm.assetData.groupMasterType,
					"gpNm" : vm.assetData.groupMaster
				}]
				vm.myGroupMst = vm.myGroupArr[0].gpId;

				if(vm.assetData.subGroupType === "null" || vm.assetData.subGroupType === null){
					vm.assetData.subGroupType = "";
				}
				if(vm.assetData.subGroupMaster === "null" || vm.assetData.subGroupMaster === null){
					vm.assetData.subGroupMaster = 0;
				}
				vm.mySubGroupArr = [{
					"subGpId" : vm.assetData.subGroupMaster,
					"subGpNm" : vm.assetData.subGroupType
				}]
				vm.mySubGroupMst = vm.mySubGroupArr[0].subGpId;
				vm.assetData.categoryType =  vm.assetData.categoryId;
				
				vm.photo = vm.assetData.photo;
				
				if(vm.assetData.warrantStDt === -2208966800000){
					vm.assetData.warrantStDt = "";
				};
				if(vm.assetData.warrantEdDt === -2208966800000){
					vm.assetData.warrantEdDt = "";
				}
				if(vm.assetData.aqsDate === -2208966800000){
					vm.assetData.aqsDate = "";
				}
				if(vm.assetData.expiryDt === -2208966800000){
					vm.assetData.expiryDt = "";
				}
				if(vm.assetData.mainDt === "01-01-1900"){
					vm.assetData.mainDt = "";
				}
				
				//Division
				var divId = _.findIndex(vm.divisionList, {
		            'divisionId': vm.assetData.divisionId
				});
				if (divId > -1) {
					vm.assetData.divisionId = vm.divisionList[divId].divisionId;
					//vm.divObj = {"divisionId" : vm.assetData.divisionId};
					
					vm.divChange(vm.assetData.divisionId);
					
					//AssetService.getSectDetailsByDiv(vm.divObj, vm.sectorSuccess, vm.sectorError);
				} else {
					vm.assetData.divisionId = vm.divisionList[0].divisionId;
				}
				
				//rack 
				var recId = _.findIndex(vm.rack_data, {
		            'rackId': vm.assetData.rackId
				});
				if (recId > -1) {
					vm.assetData.rackId = vm.rack_data[recId];
				} else {
					vm.assetData.rackId = vm.rack_data[0];
				}
				
				vm.getImage(vm.photo, vm.assetData.assetId);
			}else{
				if(vm.isReqFrom === "AND"){
					$state.go('error');
				}
			}
		};
								
		vm.viewSingleAssetError = function() {
			/*console.log("error");*/
		};
		
		vm.categoryList = function() {
			vm.requestParams = {
					"categoryPrimery" : {
						"category" : 0,
						"companyId" : vm.companyId
					}
			};
			MasterService.getCategoryAll(vm.requestParams,vm.getCategoryAllSucc,vm.getCategoryAllError)
		};

		vm.getCategoryAllSucc = function(response) {
			vm.category_list = response.data;
			vm.assetData.categoryType = vm.category_list.resData[0].categoryPrimery.categoryId;
			
			vm.catcall = "called";
		};
		
		vm.getCategoryAllError = function(response) {
			/*console.log("error");*/
		};
		
		vm.getBatchDetails = function() {
			vm.requestParams = {
					"companyId" : vm.companyId
			}
			MasterService.getBatchLotData(vm.requestParams, vm.onSuccessBatchLot, vm.onErrorBatchLot);
		};
		
		vm.onSuccessBatchLot = function(response) {
			vm.batchLotList = response.data;
			vm.assetData.batchId = vm.batchLotList.resData[0].batchId;
			
			vm.batchcall = "called";
		};
		
		vm.onErrorBatchLot = function(response) {};
		
		vm.manufactureDetails = function() {
			vm.requestParams = {
					"companyId" : vm.companyId
			}
			MasterService.manufactureAllDetails(vm.requestParams, vm.onManufactureSuccess, vm.onManufactureError);
		};

		vm.onManufactureSuccess = function(response) {
			vm.manufactureDt = response.data;
			vm.assetData.manufactureId = vm.manufactureDt.resData[0].manufactureId;
			
			vm.manufacturecall = "called";
		};

		vm.onManufactureError = function() {
			/*console.log("error");*/
		};
			
		vm.getVendorDetails = function() {
			vm.requestParams = {
					"companyId" : vm.companyId
			};
			MasterService.getVendorData(vm.requestParams, vm.onSuccessVendorData, vm.onErrorVendorData);
		};
									
		vm.onSuccessVendorData = function(response) {
			vm.vendordetails = response.data;
			vm.assetData.vendorId = vm.vendordetails.resData[0].vendorId;
			vm.vendorcall = "called";
		};
		
		vm.onErrorVendorData = function() {};
		
		//====== Start EditBlock =========
//		vm.sectorSuccess = function(response){
//			var result = response.data.resData;
//			if(result){
//				vm.sect_data = response.data.resData;
//				var secId = _.findIndex(vm.sect_data, {
//		            'sectorId': vm.assetData.sectorId
//				});
//				if (secId > -1) {
//					vm.assetData.sectorId = vm.sect_data[secId].sectorId;
//				} 
//				else {
//					vm.assetData.sectorId = vm.sect_data[0].sectorId;
//				}
//				vm.secObj = {"sectorId" : vm.assetData.sectorId	};
//				AssetService.getLocDetailsBySecID(vm.secObj, vm.locSuccess, vm.locError);
//			}
//		}; 
//		vm.sectorError = function(){};
//		
//		vm.locSuccess = function(response){
//			var result = response.data.resData;
//			if(result){
//				vm.loc_data = result;
//				var locId = _.findIndex(vm.loc_data, {
//		            'locationId': vm.assetData.locationId
//				});
//				if (locId > -1) {
//					vm.assetData.locationId = vm.loc_data[locId].locationId;
//				} else {
//					vm.assetData.locationId = vm.loc_data[0].locationId;
//				}
//			}
//			vm.locObj = {"locationId" : vm.assetData.locationId}
//			AssetService.getDepDetailsByLocID( vm.locObj, vm.departSuccess, vm.departError);
//		};
//		vm.locError = function() {};
//		vm.departSuccess = function(response){
//			var result = response.data.resData;
//			if(result){
//				vm.depart_data = response.data.resData;
//				var depId = _.findIndex(vm.depart_data, {
//		            'departmentId': vm.assetData.departmentId
//				});
//				if (depId > -1) {
//					vm.assetData.departmentId = vm.depart_data[depId].departmentId;
//				} else {
//					vm.assetData.departmentId = vm.depart_data[0].departmentId;
//				}
//			}
//		};
//		vm.departError = function(){};
		//======== End =======
		
		//Get division details
		vm.getDivisionDetails = function() {
			var requestParams = {
					"companyId" : vm.companyId
			};
			MasterService.DivisionDetailService(requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
		};
		
		vm.onDivisionInfoSuccess = function(response) {
			var result = response.data.resData;
			if (result) {
				vm.divisionList = result;
				if(vm.assetPostMode === "EditMode"){
				}else{
					vm.assetData.divisionId = result[0].divisionId;
				}
				vm.divChange(vm.assetData.divisionId);
				vm.divisioncall = "called";
			}
		};
		
		vm.onDivisionInfoError = function(response) {};
								
		vm.divChange = function(divId, DIV_CHANGE) {
			vm.isDivChange = DIV_CHANGE;
			vm.divisionId = {
					"divisionId" : divId
			};
			if(divId){
				AssetService.getSectDetailsByDiv(vm.divisionId, vm.getSectDetBySucc, vm.getSectDetByErr);
			}else{
				vm.sect_data = [];
				vm.loc_data = [];
				vm.depart_data = [];
			}
		};
		
		vm.getSectDetBySucc = function(response) {
			var result = response.data.resData;
			if (result) {
				vm.sect_data = result;
				if(vm.assetPostMode === "EditMode" && !vm.isDivChange){
					var secId = _.findIndex(vm.sect_data, {
			            'sectorId': vm.assetData.sectorId
					});
					if (secId > -1) {
						vm.assetData.sectorId = vm.sect_data[secId].sectorId;
					}
					vm.secChange(vm.assetData.sectorId);
				}else{
					vm.assetData.sectorId = vm.sect_data[0].sectorId;
					vm.secChange(vm.assetData.sectorId);
				}
			}else{
				vm.sect_data = [];
				vm.loc_data = [];
				vm.depart_data = [];
			}
		};
		
		vm.getSectDetByErr = function(response) {
			console.log("getSectDetByErr error");
		};
		
		vm.secChange = function(secId, SEC_CHANGE) {
			vm.isSecChange = SEC_CHANGE;
			vm.sectorId = {
					"sectorId" : secId
			};
			if(secId){
				AssetService.getLocDetailsBySecID(vm.sectorId, vm.secChangeSuccess, vm.secChangeError);
			}else{
				vm.loc_data = [];
				vm.depart_data = [];
			}
		};
		
		vm.secChangeSuccess = function(response) {
			var result = response.data.resData;
			if (result){
				vm.loc_data = result;
				if(vm.assetPostMode === "EditMode" && !vm.isSecChange){
					if(!vm.isDivChange){
						var locId = _.findIndex(vm.loc_data, {
				            'locationId': vm.assetData.locationId
						});
						if (locId > -1) {
							vm.assetData.locationId = vm.loc_data[locId].locationId;
						}
					}else{
						vm.assetData.locationId = vm.loc_data[0].locationId;
					}
					vm.locChange(vm.assetData.locationId);
				}else{
					vm.assetData.locationId = vm.loc_data[0].locationId;
					vm.locChange(vm.assetData.locationId);
				}
				
			} else {
				vm.loc_data = [];
				vm.depart_data = [];
			}
		};
		
		vm.secChangeError = function() {
			console.log("secChangeError error");
		};
		
		vm.locChange = function(locId, LOC_CHANGE) {
			vm.isLocChange = LOC_CHANGE;
			vm.locationId = {
					"locationId" : locId 
			};
			if(locId){
				AssetService.getDepDetailsByLocID( vm.locationId, vm.locChangeSuccess, vm.locChangeError);
			}
			else{
				vm.depart_data = [];
			}
		};
		
		vm.locChangeSuccess = function(response) {
			var result = response.data.resData;
			if (result){
				vm.depart_data = response.data.resData;
				vm.assetData.departmentId = vm.depart_data[0].departmentId;
			}else{
				vm.depart_data = [];
			}
		};

		vm.locChangeError = function() {
			console.log("locChangeError error");
		};
			
		vm.getAllTaxMasters = function() {
			MasterService.taxMastersDetails( vm.taxmasterSuccess, vm.taxmasterError);
		};
		
		vm.taxmasterSuccess = function(response) {
			vm.taxDetails = response.data;
		};
		
		vm.taxmasterError = function(response) {};
		
		vm.getInOutDocRef = function() {
			vm.requestParams = {
					"status" : 0,
					"companyId" : vm.companyId
			}
			AssetService.getInOutDocRef(vm.requestParams,vm.inOutDocrefSucc,vm.inOutDocrefErr);
		}
		
		vm.inOutDocrefSucc = function(response) {
			vm.docRef_data = response.data;
			vm.assetData.inDocRefNo = response.data.resData[0].docrefNo;
			
			vm.docrefcall = "called";
		}
		
		vm.inOutDocrefErr = function() {
			console.log("ERROR in doc ref fetch");
		}
		
		vm.getRackData = function() {
			vm.requestParams = {
					"status" : 0,
					"companyId" : vm.companyId
			}
			MasterService.RackListService(vm.rackSucc, vm.rackErr);
		}
		
		vm.rackSucc = function(response) {
			vm.rack_data = response.data.resData;
			vm.rackcall = "called";
			
			if (vm.assetPostMode === 'EditMode') {
	            var recId = _.findIndex(vm.rack_data, {
	              'rackId': vm.assetData.rackId
	            });
	            if (recId > -1) {
	            	vm.assetData.rackId  = vm.rack_data[racId];
	            } else {
//	            	vm.assetData.rackId  = vm.rack_data[0];
	            }
			}else{
				vm.assetData.rackId  = vm.rack_data[0];
			}
			
			vm.rackChange(vm.assetData.rackId);
		};
		
		vm.rackErr = function() {
			console.log("ERROR in doc ref fetch");
		};
		
		vm.rackChange = function(data){
			if(data){
				vm.assetData.slaveId = data.rackSlave[0].slaveId;
			}
		}
		
		vm.assetAttach = function(pageData){
			//$stateParams = $stateParams.pageData;
			selectedAttachment = pageData;
			
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : $scope.animationsEnabled,
				templateUrl : 'resources/views/asset/assetAttachment.html',
				controller : 'AssetAttachmentController',
				controllerAs : 'assetAttachmentController',
				resolve : {
					assetData : function() {
						return angular.copy(vm.assetData);
					},
					PageData : function() {
						return angular.copy(pageData);
					}
				}
			});
				
			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function(attachedFileData) {
				if(attachedFileData !== 'cancel'){
					 angular.forEach(attachedFileData, function(value){
							if(!value.assetId){
								vm.assetAttachedFileNameData.push(value.originName);
								vm.assetAttachedMultipartFileData.push(value.fileData);
							}
					 });
				 }else{
					vm.assetAttachedFileNameData = "cancel";
					vm.assetAttachedMultipartFileData = "cancel";
				 }
				vm.initialize();
			});
		}
	
		vm.assetAddFun = function() {
			if(vm.assetData.warrantStDt === "" || vm.assetData.warrantStDt === undefined  || vm.assetData.warrantStDt === null){
				vm.assetData.warrantStDt = moment(-2209008600000).valueOf();
			}else{
				vm.assetData.warrantStDt = moment(vm.assetData.warrantStDt).valueOf();
			}
			if(vm.assetData.warrantEdDt === "" || vm.assetData.warrantEdDt === undefined  || vm.assetData.warrantEdDt === null){
				vm.assetData.warrantEdDt =  moment(-2209008600000).valueOf();
			}else{
				vm.assetData.warrantEdDt = moment(vm.assetData.warrantEdDt).valueOf();
			}
			if(vm.assetData.mainDt === "" || vm.assetData.mainDt === undefined  || vm.assetData.mainDt === null){
				vm.assetData.mainDt = moment(-2209008600000).valueOf(); 
			}else{
				vm.assetData.mainDt = moment(vm.assetData.mainDt).valueOf();
			}
			if(vm.assetData.aqsDate === "" || vm.assetData.aqsDate === undefined  || vm.assetData.aqsDate === null){
				vm.assetData.aqsDate =  moment(-2209008600000).valueOf();
			}else{
				vm.assetData.aqsDate = moment(vm.assetData.aqsDate).valueOf();
			}
			if(vm.assetData.expiryDt === "" || vm.assetData.expiryDt === undefined  || vm.assetData.expiryDt === null){
				vm.assetData.expiryDt = moment(-2209008600000).valueOf();
			}else{
				vm.assetData.expiryDt = moment(vm.assetData.expiryDt).valueOf();
			}
						
			if(vm.assetData.categoryType === 0){
				if(vm.myGroupMst === undefined || vm.myGroupMst === "") {
					vm.myGroupMst = 0;
				}
				if(vm.mySubGroupMst === undefined || vm.mySubGroupMst === "") {
					vm.mySubGroupMst = 0;
				}
			}
			if (vm.assetData.categoryType === 2 && vm.myGroupMst === undefined || vm.myGroupMst === "") {
				vm.grpMasterError = true;
				$timeout(function() {
					vm.grpMasterError = false;
				}, 1000)
			}
			
			vm.assetData.rackId = vm.assetData.rackId.rackId;
			vm.assetData.groupMaster = vm.myGroupMst;
			vm.assetData.groupSubMaster = vm.mySubGroupMst;
			vm.assetData.companyId = vm.companyId;
			vm.assetData.categoryId = vm.assetData.categoryType;
			vm.requestParams = vm.assetData;
			
			if (vm.assetPostMode === "NewMode") {
				AssetService.assetAddDetails(vm.requestParams, vm.assetAddSuccess, vm.assetAddError);
			} else {
				
				vm.requestParams.assetId = vm.assetData.assetId; 
				vm.requestParams.photo = vm.photo;
				vm.requestParams.currentDetectedDateTime = moment(vm.requestParams.currentDetectedDateTime).valueOf();
				vm.requestParams.lastDetectedDateTime = moment(vm.requestParams.lastDetectedDateTime).valueOf();
				/*vm.requestParams = angular.extend(vm.requestParams,
						{
						"assetId" : $stateParams.assetID,
						"photo" : vm.photo
						});*/
				AssetService.AssetUpdateDetails(vm.requestParams, vm.assetUpdateSucc, vm.assetUpdateError);
			}
		};
		
		vm.assetAddSuccess = function(response) {
			if (response.data.resCode === "res0000") {
				swal("Success", "Asset Add Successfully!", "success")
				
				vm.requestParam = {
					"companyId" : vm.companyId,
					"uploadTo" : "ASSET",
					"imageId" : response.data.resData.assetId,
				}
				if (vm.assetLogo && vm.assetLogo.name) {
					AuthService.uploadLogo(vm.requestParam, vm.assetLogo, vm.assetImgSucc, vm.assetImgErr)
				}
				
				//Asset and Doc Attachments
				if(selectedAttachment !== undefined && selectedAttachment !== null){
					if(vm.assetAttachedFileNameData !== "cancel" && vm.assetAttachedMultipartFileData !== "cancel"){
						if(selectedAttachment === "AssetAttach"){
							vm.requestParams = {
								"companyId": vm.companyId,
								"uploadTo":"ASSET",
								"id":response.data.resData.assetId,
								"fileName": vm.assetAttachedFileNameData,
								"file": vm.assetAttachedMultipartFileData
							}
						}
						if(selectedAttachment === "DocRefAttach"){
							vm.requestParams ={
									"companyId": vm.companyId,
									"uploadTo":"ASSET",
									"id":vm.assetData.inDocRefNo,
									"fileName": vm.assetAttachedFileNameData,
									"file":  vm.assetAttachedMultipartFileData
							}
						}
						AssetService.uploadAttach(vm.requestParams, vm.assetDocattachSucc, vm.assetDocattachErr);
					}
				}
				
				$state.transitionTo('homeDashboard.asset',
						$stateParams, {
						reload : true,
						inherit : false,
						notify : true
				});
			} else if (response.data.resCode === "res0100") {
				swal("Not Added!", response.data.resMsg, "error");
				$state.reload();
			} else {
				
			}
		};
		
		vm.assetAddError = function() {
			/*console.log("error");*/
		};
		
		vm.assetUpdateSucc = function(response) {
			/*console.log(response.data);*/
			if (response.data.resCode === "res0000") {
				swal("Updated", "Asset Updated Successfully", "success");
				vm.requestParam = {
						"companyId" : vm.companyId,
						"uploadTo" : "ASSET",
						"imageId" : vm.assetData.assetId.toString(),
				}
				
				if (vm.assetLogo && vm.assetLogo.name) {
					AuthService.uploadLogo(vm.requestParam, vm.assetLogo, vm.assetImgSucc, vm.assetImgErr)
				}
				
				//Asset and Doc Attachments
				if(selectedAttachment !== undefined && selectedAttachment !== null){
					if(vm.assetAttachedFileNameData !== "cancel" && vm.assetAttachedMultipartFileData !== "cancel"){
						if(selectedAttachment === "AssetAttach"){
							vm.requestParams = {
								"companyId": vm.companyId,
								"uploadTo":"ASSET",
								"id":vm.assetData.assetId,
								"fileName": vm.assetAttachedFileNameData,
								"file": vm.assetAttachedMultipartFileData
							}
						}
						if(selectedAttachment === "DocRefAttach"){
							vm.requestParams = {
									"companyId": vm.companyId,
									"uploadTo":"ASSET",
									"id":vm.assetData.inDocRefNo,
									"fileName": vm.assetAttachedFileNameData,
									"file":  vm.assetAttachedMultipartFileData
							}
						}
						AssetService.uploadAttach(vm.requestParams,selectedAttachment, vm.assetDocattachSucc, vm.assetDocattachErr);
					}
				}
				$state.reload();
				/*$state.go('homeDashboard.asset');*/
				/*$state.transitionTo('homeDashboard.asset',
						$stateParams, {reload : true,
					inherit : false,
					notify : true
				});*/
			}else{
				swal("Not Added!", response.data.resMsg, "error");
			}
		};
		
		vm.assetUpdateError = function(response) {
			console.log("error");
		};
		
		vm.assetImgSucc = function(response){};
		vm.assetImgErr = function(response) {};
		
		vm.assetDocattachSucc = function(response){};
		vm.assetDocattachErr = function(response){};
		
		vm.readTag = function() {
			vm.requestParam = {
					"dockDoorId":vm.dockDoorId,
					"companyId" : vm.companyId
			}
			AssetService.readTag(vm.requestParam, vm.readTagSucc, vm.readTagErr)
		};
		
		vm.readTagSucc = function(response) {
			if (response.data.resCode === "res0000") {
				vm.assetData.tagId = response.data.resData.readTagId;
			}
			if (response.data.resCode === "res0100") {
				swal("Tag error", response.data.resMsg, "warning")
			}
		};

		vm.readTagErr = function() {
			console.log("Error in read tag");
		};
		
		vm.readTagByDock = function(){
			var modalInstance = $uibModal.open({
				backdrop : 'static',
					animation : vm.animationsEnabled,
					templateUrl : 'resources/views/asset/dockDoorPop_up.html',
					controller : 'DockDoorPopUpController',
					controllerAs : 'dockdoorpopupController',
			});
			
			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				/*$log.info('Modal dismissed at: '+ new Date());*/
			});
		};
		
		vm.tagSelectFun = function() {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/tag/tagSearch.html',
				controller : 'TagSearchController',
				controllerAs : 'tagSearchController',
				resolve : {
					tagSelect : function() {
						return angular.copy(2);
					}
				}
			});
			
			modalInstance.result.then(function(selectedItem) {
			}, function(data) {
				vm.tag = JSON.parse(data);
				vm.assetData.tagId = vm.tag.tagId;
			});	
		};
		
		vm.groupMasterDailog = function() {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/asset/groupMaster.html',
				controller : 'GroupMasterController',
				controllerAs : 'groupMasterController'
			});
				
			modalInstance.result.then(function(selectedItem) {
				/*console.log("ddd" + selectedItem)*/
			}, function(data) {
				vm.grpMst = JSON.parse(data);
				vm.groupMasterType = vm.grpMst.assetNm;
				vm.myGroupArr = [{
					"gpId" : vm.grpMst.assetId,
					"gpNm" : vm.grpMst.assetNm
				}]
				vm.myGroupMst = vm.myGroupArr[0].gpId;
			});
		};
		
		vm.subGroupMasterDailogue = function() {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/asset/subGroupMaster.html',
				controller : 'SubGroupMasterController',
				controllerAs : 'subGroupMasterController',
				resolve : {
					groupMasterID : function() {
						return angular.copy(vm.grpMst);
					}
				}
			});
			
			modalInstance.result.then(function(selectedItem) {
				/*console.log("ddd" + selectedItem)*/
			}, function(data) {
				vm.subGrpMst = JSON.parse(data);
				
				vm.selectedAssetData = function() {
					vm.requestParams = {
							'assetId' : vm.subGrpMst.assetId,
							'companyId' : vm.companyId
					}
					AssetService.assetGetByID(vm.requestParams, vm.assGetByIDSuccess, vm.assGetByIDError);
				};	
				
				vm.assGetByIDSuccess = function(response) {
					vm.myGroupArr = [{
						"gpId" : response.data.resData[1].groupMasterType,
						"gpNm" : response.data.resData[1].groupMaster
					}]
					vm.myGroupMst = vm.myGroupArr[0].gpId;
					vm.groupMasterType = response.data.resData[1].groupMaster;
				/*	groupMasID = response.data.resData[1].groupMasterType;*/
					/*console.log(groupMasID);*/
				}
				
				vm.assGetByIDError = function(response) {
					/*console.log("error");*/
				};
				
				vm.mySubGroupArr = [{
					"subGpId" : vm.subGrpMst.assetId,
					"subGpNm" : vm.subGrpMst.assetNm
				}]
				vm.mySubGroupMst = vm.mySubGroupArr[0].subGpId;
					
				vm.subGroupType = vm.subGrpMst.assetNm;
				vm.selectedAssetData();
			});
		};
		
		//Click on release tag
		vm.releaseTagFun = function (){
			swal({
				title: "Click yes to continue ",
				text: "Are you sure want to release tag!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, continue!",
				/*cancelButtonText: "No, cancel please!",*/
				closeOnConfirm: false,
				closeOnCancel: false },
				function(isConfirm){
					if (isConfirm) {
						vm.requestParams = {
								"assetId":vm.assetData.assetId,
								"companyId":vm.companyId,
								"tagId":vm.tagId,
								"assetDesc":"tagRelease"		
						}	
						AssetService.releaseTag(vm.requestParams, vm.releaseTagSucc, vm.releaseTagErr)
					}
					else {
						swal("Cancelled", "Your asset is safe :)", "error");
					}	
				});
		}
		
		vm.releaseTagSucc = function(response){
			swal("Successfully", "Release tag successfully!", "success");
			$state.transitionTo('homeDashboard.asset',
					$stateParams, {
				reload : true,
				inherit : false,
				notify : true
			});
		}
		vm.releaseTagErr = function(response){
			console.log("error in release tag", response.data.resData);
		}
		
		vm.catValue = function() {
			if (vm.assetData.categoryType === 1) {
				vm.mySubGroupMst = "";
				vm.mySubGroupArr =[];
				vm.groupMasterType = vm.assetData.assetNm;
				
				//testing with selectbox
				vm.myGroupArr = [{
					"gpId" : 0,
					"gpNm" : vm.assetData.assetNm
				}]
				vm.myGroupMst = vm.myGroupArr[0].gpId;
				vm.mySubGroupMst = 0;

			} else if (vm.assetData.categoryType === 0) {
					vm.groupMasterType = "";
					vm.subGroupType = "";
					
					vm.myGroupArr = [];
					vm.myGroupMst = "";
					vm.mySubGroupArr = [];
					vm.mySubGroupMst = "";	
						
			} else if (vm.assetData.categoryType === 2) {
				
				vm.grpMasterError = true;
				vm.groupMasterType = "";
				vm.subGroupType = vm.assetData.assetNm;
				vm.myGroupArr = [];
				vm.myGroupMst = "";
				
				vm.mySubGroupArr = [{
					"subGpId" : 0,
					"subGpNm" : vm.assetData.assetNm
				}]
				vm.mySubGroupMst = vm.mySubGroupArr[0].subGpId;
				
			} else if (vm.assetData.categoryType > 4) {
				vm.myGroupMst = 0;
				vm.mySubGroupMst = 0;
			}
		};	
		
		vm.getImage = function(photo, assetId) {
			vm.requestParams = {
					"companyId" : vm.companyId,
					"downloadFrom" : "ASSET",
					"imageId" : assetId.toString(),
					"imageName" : photo,
			};
			
			if(vm.requestParams.imageName){
				AuthService.downloadLogo(vm.requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
			}
		};
								
		vm.assetImg;
		vm.onGetLogoSuccess = function(response) {
			vm.assetUpdImg = true;
			vm.assetImg = response.data.resData.image;
			vm.assetLogo = 'data:image/jpeg;base64,' + response.data.resData.image; 
		};
		
		vm.onGetLogoError = function(response) {
			/*console.log("error")*/
		};
		
		vm.clear = function() {
			vm.maintenanceDate = null;
		};
		
		vm.dateOptions = {
				/*dateDisabled: disabled,*/
				formatYear: 'yy',
				//maxDate: new Date(),
				//minDate: new Date(2000, 5, 22),
				startingDay: 1
		};
		
		vm.open2 = function() {
			vm.popup2.opened = true;
		};
		
		vm.expiryDtOpen = function(){
			vm.expiryDt.opened = true;
		};
		
		vm.wrntyetDtOpen = function(){
			vm.wrntyetDt.opened = true;  
		};
		
		vm.wrntystDtOpen = function(){
			vm.wrntystDt.opened = true;  
		};
		
		vm.aquDateOpen = function(){
			vm.aquDate.opened = true;  
		};
		
		vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		vm.format = vm.formats[0];
		vm.altInputFormats = ['M!/d!/yyyy'];
		
		vm.wrntystDt = {
				opened: false
		};
		vm.wrntyetDt = {
				opened: false
		};
		vm.aquDate = {
				opened : false
		}
		vm.expiryDt = {
				opened: false
		};
		
		vm.popup2 = {
				opened: false
		};
		
		vm.webCamBtn = "Webcam";
		vm.webcamClick = function() {
			vm.webCamBtn = "Take";
			vm.webCamDiv = true;
			if(vm.webCamBtn === "Take"){
				Webcam.snap(function(data_uri) {
					vm.assetLogo = data_uri;
					vm.webCamDiv = false;
					vm.assetImg = data_uri;
					document.getElementById('anchor').setAttribute('href', data_uri);// download
				});
			}
			
			//old code for image div	
			vm.showDiv = true;
			$timeout(function(){
			Webcam.attach('#my_camera');},0)
		};
		
		vm.take_snapshot = function() {
			vm.webImg = true;
			vm.assetUpdImg = false
			Webcam.snap(function(data_uri) {
				vm.webCamImg = data_uri;
				var blob = new Blob([data_uri], {type: 'image/jpg'});
				var file = new File([blob], 'imageFileName.jpg' ,{type: 'image/jpg'});
				vm.assetLogo = file;
				document.getElementById('anchor').setAttribute('href', data_uri);// download
				
			});
		};
		
		vm.myFun = function(){
			vm.webImg = false;
			vm.showDiv = false;
			vm.assetUpdImg = false;	
		};	
								
		vm.splitAssetFun = function(){
			var modalInstance = $uibModal.open({
				backdrop: 'static',
				animation: vm.animationsEnabled,
				templateUrl: 'resources/views/asset/splitAsset.html',
				controller: 'SplitAssetController',
				controllerAs : 'splitAssetController',
				resolve: {
					singleAssData: function (){
						return angular.copy(vm.assetData);
					}
				}
			});
			
			modalInstance.result.then(function (selectedItem) {
			}, function () {
			});
		};
		
		vm.outOfStockFun = function (){
			swal({
				title: "Click yes to continue ",
				text: "Are you sure want to out of stock!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, continue!",
							            /*cancelButtonText: "No, cancel please!",*/
				closeOnConfirm: false,
				closeOnCancel: false },
				function(isConfirm){
					if (isConfirm) {
						vm.requestParams = {
								"assetId":vm.assetData.assetId,
								"companyId":vm.companyId,
								"assetDesc":"no desc"		
						}
						AssetService.outOfstock(vm.requestParams, vm.outOfStcSucc, vm.outOfStcErr)
					}	
					else {
						swal("Cancelled", "Your asset is safe :)", "error");
					}
				});
		}
		vm.outOfStcSucc = function(response){
			swal("Updated", "Asset Out of Stock successfully!", "success");
			$state.go('homeDashboard.asset');
		}
		
		vm.outOfStcErr = function(response){
			console.log("Error in out of stock");
		}
			
		//image select
		vm.clickonChoose = function(){
			vm.webCamDiv = false;
			vm.webCamBtn = "Webcam";
			document.getElementById('my_file').click();
		};
		
		vm.imgDelete = function(){
			vm.assetLogo = "";
			vm.webCamBtn = "Webcam";
		};
		
		vm.cancel = function() {
			$state.go('homeDashboard.asset');
		};
		
		var asyncReqCheck = $interval(function(){
			if(vm.catcall && vm.batchcall && vm.manufacturecall && vm.vendorcall && vm.divisioncall && vm.docrefcall && vm.rackcall){
				vm.initialize();
				$interval.cancel(asyncReqCheck)
			}
		},0)
		
		vm.categoryList();
		vm.getDivisionDetails();
		vm.getBatchDetails();
		vm.manufactureDetails();
		vm.getVendorDetails();
		vm.getInOutDocRef();
		vm.getRackData();
	}])
})();
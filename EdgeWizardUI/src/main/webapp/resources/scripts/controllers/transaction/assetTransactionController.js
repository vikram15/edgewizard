(function(){
  'use strict';
  angular.module('rfidDemoApp.controller')
  .controller('AssetTransactionController', ['$scope','$uibModal','TransactionService','AuthService','$interval', 
                                             function($scope, $uibModal, TransactionService, AuthService, $interval){

	  var vm = this ;
	  vm.globalCompId = AuthService.getCompanyId();
    
	  vm.globalCompName = AuthService.getCompanyName();
	  vm.requestParams = {"companyId":vm.globalCompId}
	
	  vm.getAllAssetTransaction = function(){
		  TransactionService.getAssetTransaction(vm.requestParams, vm.successCall, vm.errorCall);
	  }
    
	  vm.successCall = function(response){
		  vm.assetTransaction_data = response.data;
	  };

	  vm.errorCall = function(response){
		  console.log("error");
	  };
	  
	  var promise = $interval(function () {
		  vm.getAllAssetTransaction();  
	  }, 1000);
	  
	  vm.stop = function(){
		  $interval.cancel(promise)
	  };
		
	  $scope.$on('$destroy', function() {
		  console.log("$destroy called");
		  vm.stop();
	  });
	  
	  vm.getAllAssetTransaction();
  	}]);
})();
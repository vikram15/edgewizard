(function(){
  'use strict';
  angular.module('rfidDemoApp.controller')
  .controller('AllTransactionController', ['$scope','$uibModal','TransactionService','$interval','AuthService', 
                                           function($scope, $uibModal, TransactionService, $interval, AuthService){

    var vm = this ;
    console.log("All Transactions");
    vm.globalCompId = AuthService.getCompanyId();
    
    vm.getAllEVentTransaction = function(){
    	vm.requestParams = {
    			"companyId":vm.globalCompId
    	}
    	TransactionService.getEventTransaction(vm.requestParams, vm.successCall, vm.errorCall);
    }
    
    vm.successCall = function(response){
        vm.eventTransaction_data = response.data;
    };

	vm.errorCall = function(response){
	    console.log("error");
	  };
	  
	 var promise = $interval(function () {
		  vm.getAllEVentTransaction();  
      }, 1000);
	  
	  vm.stop = function(){
			 $interval.cancel(promise)
		};
		
		$scope.$on('$destroy', function() {
			console.log("$destroy called");
		      vm.stop();
		});
	  
	  vm.getAllEVentTransaction();
  }]);
})();
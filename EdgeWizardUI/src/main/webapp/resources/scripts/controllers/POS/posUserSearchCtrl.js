(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('PosUserSearchCtrl',['$uibModalInstance','AuthService','PosService', 
	                                 function($uibModalInstance, AuthService, PosService){
		var vm = this ;
		vm.tableHeader = "User Search";
		vm.searchBy = "User Id";
		
		//pagination
	    vm.currentPage = 1;
	    vm.maxSize = 10;
	    vm.selectAssetType = "EPC ID";
		vm.comp_Id = AuthService.getCompanyId();
		
		vm.userDetails = function(){
	    	vm.requestParams = {
	    		 "searchBy" : vm.searchBy,
	    		 "searchValue":	vm.searchValue,
	    		 "currentPage":vm.currentPage,
	    		 "companyId":vm.comp_Id
	    	};
	    	PosService.getUserData(vm.requestParams, vm.getUserSearhSuccess, vm.getUserSearchError);
	    };

	    vm.getUserSearhSuccess = function(response){
	    	vm.user_data = response.data;
	    	vm.totalItems = vm.user_data.resData[0].totalUserSearch;
	    	vm.itemsPerPage = vm.user_data.resData[0].RecordPerPage;
	    };

	    vm.getUserSearchError = function(response){
	    	
	    };
	    
	    vm.userSelectFun = function(){
	    	if(vm.userSelect !== undefined){
				$uibModalInstance.dismiss(vm.userSelect);
			}
	    };
		
		vm.cancel= function(){
			$uibModalInstance.dismiss('cancel');
		};
		
		vm.userDetails();
		    
	}]);
})();
(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('CheckOutCtrl',['$uibModalInstance','selectedOrders', 'CustomService','usrData','$filter', 'AuthService','$uibModal','PosService','$state', 
	                            function($uibModalInstance, selectedOrders, CustomService, usrData, $filter, AuthService, $uibModal, PosService, $state){
		var vm = this;
		vm.companyId = AuthService.getCompanyId();
		vm.userName = AuthService.getUserName();
		vm.userId = AuthService.getUserId();
		vm.companyName = AuthService.getCompanyName();
		
		vm.subTotal = ""
		//vm.taxDetails = taxDetails;
		vm.usrOtherData = usrData;
		
		if(selectedOrders){
			vm.selectedOrders = selectedOrders;
			angular.forEach(selectedOrders, function(value){
				vm.subTotal =+ vm.subTotal + value.cost *  value.purchaseQuantity;
				value.totalCost = value.cost *  value.purchaseQuantity;
			});
			
			vm.tax = vm.usrOtherData.taxFromDB;
			vm.totalTax =  vm.subTotal * vm.usrOtherData.taxFromDB / 100;
			vm.deliveryCharges = vm.usrOtherData.fixedAmount;
			vm.grandTotal = vm.subTotal + vm.deliveryCharges + vm.totalTax;
			vm.discount = vm.grandTotal * vm.usrOtherData.discountDivisionPercentage / 100;
			vm.yearDiscount = vm.grandTotal * vm.usrOtherData.yearDiscountValue / 100;
			
			if(vm.usrOtherData.yearDiscountValue || vm.usrOtherData.yearDiscountValue > 0){
				vm.yearDiscount = vm.grandTotal * vm.usrOtherData.yearDiscountValue / 100;
				vm.yearDiscountValue  = vm.yearDiscount;
				vm.netTotal = vm.grandTotal - vm.yearDiscountValue;
			}else{
				vm.discount = vm.grandTotal * vm.usrOtherData.discountDivisionPercentage / 100;
				vm.discountValue = vm.discount;
				vm.netTotal = vm.grandTotal - vm.discountValue;
			};
			vm.availableBalance = vm.usrOtherData.currentBalanceValue - vm.netTotal;
			
			vm.otherDetails = {
					'userName' : vm.usrOtherData.userName,
					'tax' : vm.tax,
					'totalTax' : $filter('number')(vm.totalTax, 2),
					'totalAmt' : $filter('number')(vm.subTotal, 2),
					'deliveryCharges' : $filter('number')(vm.deliveryCharges, 2),
					'grandTotal' : $filter('number')(vm.grandTotal, 2),
					'discount' : $filter('number')(vm.discount, 2),
					'netTotal' : $filter('number')(vm.netTotal, 2),
					'availBalance' : $filter('number')(vm.availableBalance, 2)
			};
		};
		
		vm.confirmOrder = function(){
			vm.assetIdTemp = [];
			vm.isConfirm = true;
			angular.forEach(vm.selectedOrders, function(val){
				vm.assetIdTemp.push(val.assetId);
			});
			
			vm.requestParams = {
					"companyId":vm.companyId,
					"userId":vm.usrOtherData.userId,
					"userName":vm.usrOtherData.userName,
					"grossTotal":vm.subTotal,                            
					"taxValue":vm.totalTax,
					"deliveryChages":vm.deliveryCharges,
					"grandTotal":vm.grandTotal,           
					"availableBalance":vm.availableBalance,
					"createdBy":vm.userName,
					"operatedId":vm.userId,
					"holdOrderId":vm.usrOtherData.holdOrderId,
					"netPayTotal":vm.netTotal,
					"disCount":vm.discount,
					"yearlyDiscount":vm.yearDiscount,
					"createdId":0,
					"companyName":vm.companyName,
					"invoiceBOList": vm.selectedOrders,
					"orderedAssetIdList": vm.assetIdTemp,
					"email" : vm.usrOtherData.emailId,
					"mobileNumber" : vm.usrOtherData.mobileNo,
					"orderedDate" : moment(new Date()).valueOf()
			}
			
			if(vm.usrOtherData.passwordRequired === true){
				var modalInstance = $uibModal
				.open({
					backdrop : 'static',
					size : 'sm',
					animation : vm.animationsEnabled,
					templateUrl : 'resources/views/transactionPassword.html',
					controller : 'TransactionPasswordCtrl',
					controllerAs : 'transactionPasswordCtrl',
					resolve : {
						userId : function(){
							return vm.usrOtherData.userId
						}
					}
				});
				modalInstance.result.then(function(selectedItem) {
					vm.isConfirm = false;
				},function(usrD) {
					PosService.saveOrderDetails(vm.requestParams, vm.onSuccessSaveOrders ,vm.onErrorSaveOrders)
					vm.isConfirm = true;
				});
			}else{
				PosService.saveOrderDetails(vm.requestParams, vm.onSuccessSaveOrders ,vm.onErrorSaveOrders)
			}
		};
		
		vm.onSuccessSaveOrders = function(response){
			if(response.data.resCode === "res0000"){
				if(vm.usrOtherData.printStatus === 1){
					 vm.otherDetails.orderNo = response.data.resData.orderId;
					CustomService.printCartOrders(vm.selectedOrders, vm.otherDetails);
					swal("Success", response.data.resData.orderStatusMessage,"success")
					$uibModalInstance.close();
				}else{
					swal("Success", response.data.resData.orderStatusMessage, "success")
					$uibModalInstance.close();
				}
				vm.isConfirm = true;
			}else{
				swal("Not Added", response.data.resMsg ,"error");
				vm.isConfirm = false;
			}
		};	
		
		vm.onErrorSaveOrders = function(response){
			console.log(response.data);
		};
		
		function myfunction() {
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				size : 'sm',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/transactionPassword.html',
				controller : 'TransactionPasswordCtrl',
				controllerAs : 'transactionPasswordCtrl',
				resolve : {
					userId : function(){
						return vm.usrOtherData.userId
					}
				}
			});
			modalInstance.result.then(function(selectedItem) {
				console.log("11111", selectedItem);
			},function(usrD) {
				conosole.log(usrD);
				/*if(usrD === "success"){
					PosService.saveOrderDetails(vm.requestParams, vm.onSuccessSaveOrders ,vm.onErrorSaveOrders)
				}*/
			});	
		};
		
		vm.cancel = function() {
			$uibModalInstance.dismiss('cancel');
		};
	}]);
})();
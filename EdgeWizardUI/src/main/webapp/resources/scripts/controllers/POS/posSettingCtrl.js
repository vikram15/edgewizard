(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('PosSettingCtrl',['$timeout','AuthService','PosService','$state', function($timeout, AuthService, PosService, $state){
		var vm = this ;
		vm.companyId = AuthService.getCompanyId();
		vm.companyName = AuthService.getCompanyName();
		vm.saveBtn = "save";
		
		vm.getSettingDetails = function(){
			vm.requestParams = {
					"companyId" : vm.companyId
			};
			PosService.getposSettingDetails(vm.requestParams, vm.onSuccessSettingDetails, vm.onErrorSettingDetails);
		};
		
		vm.onSuccessSettingDetails = function(response){
			vm.possetData = response.data.resData;
			vm.deliverySelect = vm.possetData.fixedAmountStatus;
		};
		
		vm.onErrorSettingDetails = function(response){
			console.log(response.data.resMsg);
		};
		
		vm.getAllCommGroupDetails = function(){
			vm.requestParams = {
					"companyId" : vm.companyId
			};
			PosService.getAllCommGroupMaseter(vm.requestParams, vm.onSuccessGroupMaster, vm.onErrorGroupMaster);
		};
		
		vm.onSuccessGroupMaster = function(response){
			vm.groupMasterData = response.data;
		};
		
		vm.onErrorGroupMaster = function(response){
			console.log(response.data.resMsg);
		};
		
		vm.save = function () {
			vm.saveBtn = "saving...";
			
			if(vm.deliverySelect === 1){
				 vm.possetData.fixedAmountStatus = 1;
				 vm.possetData.invoicePercentStatus = 0;
			 }else{
				 vm.possetData.fixedAmountStatus = 0;
				 vm.possetData.invoicePercentStatus = 1;
			 }
			 
			PosService.updatePosSetting(vm.possetData, vm.onSuccessUpdate, vm.onErrorUpdate);
			$timeout(function(){
				vm.saveBtn = "save";
			}, 2000);
		};
		
		vm.onSuccessUpdate = function(response){
			vm.saveBtn = "save";
			if(response.data.resCode === "res0000"){
				swal("Success", response.data.resMsg , "success");
				$state.reload();
			}else{
				swal("Not Updated", response.data.resMsg , "error");
			}
		};
		
		vm.onErrorUpdate = function(response){
			console.log(response.data);
		};
		
		vm.getSettingDetails();
		vm.getAllCommGroupDetails();
		
	}]);
})();
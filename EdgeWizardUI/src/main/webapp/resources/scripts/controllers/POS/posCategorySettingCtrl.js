(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('PosCategorySettingCtrl',['PosService','AuthService','$state', function(PosService, AuthService, $state){
		var vm = this;
		vm.categoryUpdateDetails = "";
		vm.companyId = AuthService.getCompanyId();
		vm.userName = AuthService.getUserName();
		vm.assetListArray = [];
		
		vm.getCategoryList = function() {
			vm.requestParams = {
				"categoryPrimery" : {
					"companyId" : vm.companyId
				}
			}
			PosService.getPosCategoryList(vm.requestParams, vm.onSuccessCategoryListm, vm.onErrorCategoryList)
		};
		
		vm.onSuccessCategoryListm = function(response){
			vm.categoryList = response.data.resData[1];
			vm.categoryUpdateDetails = response.data.resData[0];
		};
		
		vm.onErrorCategoryList = function(response){
			
		};
		
		vm.changeCategorySelect = function(catData){
			if(catData){
				vm.requestParams = catData;
				PosService.getAssetsByCategory(vm.requestParams, vm.onSuccessGetAssetsBy, vm.onErrorGetAssetsBy)
			}
		};
		
		vm.onSuccessGetAssetsBy = function(response){
			vm.assetData = response.data;
			var keepGoing = true;
			if(vm.assetData.resData){
				vm.assetListArray = vm.assetData.resData.assetInfoList;
				angular.forEach(vm.assetData.resData.assetInfoList, function(value){
					angular.forEach(vm.assetData.resData.checkAssetListKey, function(v){
						if(value.assetId === v.checkdId){
							value.checked = true;
							var keepGoing = false;
						}
					});
				})
			}
		};
		
		vm.onErrorGetAssetsBy = function(response){
			
		};
		
		vm.saveBtn = function(){
			var assetIdArr = [];
			angular.forEach(vm.assetData.resData.assetInfoList, function(value){
				if(value.checked){
					assetIdArr.push(value.assetId);
				}
			})
			vm.requestParams = {
					"categoryId":vm.categorySelect.categoryPrimery.categoryId,
					"assetIdList" : assetIdArr,
					"dbUserName":vm.userName,
					"companyId":vm.companyId
			};
			PosService.saveAssetSelection(vm.requestParams, vm.onSuccessSaveAssets, vm.onErrorSaveAssets);
		};
		
		vm.onSuccessSaveAssets = function(response){
			if(response.data.resCode === "res0000"){
				swal("Success", response.data.resMsg , "success");
				$state.reload();
				//vm.getCategoryList();
			}else{
				swal("Not Updated", response.data.resMsg , "error");
			}
		};
		
		vm.onErrorSaveAssets = function(response){
			console.log(response.data);
		};
		
		vm.selectAll = function(check){
			if(check === true){
				angular.forEach(vm.assetListArray, function(val){
					val.checked = true;
				})
			}else{
				angular.forEach(vm.assetListArray, function(val){
					val.checked = false;
				})
			}
		}
		
		vm.selectEntity = function(){
			if(_.any(vm.assetListArray, _.matches({"checked":false}))){
				vm.checkAll = false;
				return;
			}else{
				vm.checkAll = true;
			}
		};
		
		vm.getCategoryList();
	}]);
})();
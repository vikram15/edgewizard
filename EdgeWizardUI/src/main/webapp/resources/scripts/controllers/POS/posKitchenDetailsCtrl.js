(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('PosKitchenDetailsCtrl', ['$scope', function($scope){
		var vm = this;
		
		vm.tab = 1;
    	vm.setTab = function(tabId){
    		vm.tab = tabId;
    	};
    	
    	vm.isSet = function (tabId) {
            return vm.tab === tabId;
        };
        
        vm.ordersList = [{
        	"userId" : 1,
        	"userName" : "vikram"
        },
        {
        	"userId" : 1,
        	"userName" : "vikram"
        },
        {
        	"userId" : 1,
        	"userName" : "vikram"
        },
        {
        	"userId" : 1,
        	"userName" : "vikram"
        }];
	}]);
})();
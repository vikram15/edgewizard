(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('PosOrderDetailsCtrl',['$scope','$timeout','$uibModal','AuthService','PosService','$cookies','$interval','$state', function($scope, $timeout, $uibModal, AuthService, PosService, $cookies, $interval, $state){
		var vm = this ;
		vm.successMsg = false;
		vm.companyId = AuthService.getCompanyId();
		vm.companyName = AuthService.getCompanyName();
		vm.userId = AuthService.getUserId();
		vm.userName = AuthService.getUserName();
		vm.orderCategories = [{"categorynm": "ALL CATEGORY","assetId" : "default"}];
		vm.addToCart = [];
		vm.orderList = [];
		vm.usrData = ""; 
		vm.crDate = new Date();
		vm.dockDoorId = JSON.parse($cookies.get("globals")).dockDoorId;
		vm.dockDoorName = JSON.parse($cookies.get("globals")).dockDoorName;
		var promise ;

		vm.getCompanyDetails = function() {
			var requestParams = {
					"companyId" : vm.companyId
			};
			AuthService.CompanyDetailService(requestParams, vm.onCompanyInfoSuccess, vm.onCompanyInfoError);
		};
		
		vm.onCompanyInfoSuccess = function(response) {
			vm.companyInfo = response.data;
			if (response.data.resCode === "res0000") {
				vm.company = response.data.resData;
				vm.getCompanyImg(vm.company);
			}
		};
		
		vm.onCompanyInfoError = function(response) {
			console.log("error")
		};
		
		vm.getOrderCategory = function(){
			vm.requestParams = {
					"categoryPrimery":{
						"companyId": vm.companyId
					}
			}
			PosService.getOrderCategoryList(vm.requestParams, vm.onSuccessOrderCat, vm.onErrorOrderCat)
		};
		
		vm.onSuccessOrderCat = function(response){
			if(response.data.resData){
				angular.forEach(response.data.resData, function(value){
					vm.orderCategories.push(value);
				})
			}
		};
		
		vm.onErrorOrderCat = function(response){
			
		};
		
		vm.getOrdersByCat = function(data){
			vm.activeCategory = data;
			if(data.categorynm === "ALL CATEGORY" && data.assetId === "default"){
				vm.requestParams = {	
						"companyId":vm.companyId,
				}
			}else{
				vm.requestParams = {	
						"companyId":vm.companyId,
						"categoryName":data.categorynm
				}
			}
			PosService.getOrdersByCategory(vm.requestParams, vm.onSuccessGetOrderByCat, vm.onErrorGetOrderByCat);
		};
		
		vm.onSuccessGetOrderByCat = function(response){
			vm.orderList = response.data.resData;
			angular.forEach(vm.orderList, function(val){
				val.purchaseQuantity = '0';
			});
		};
		
		vm.onErrorGetOrderByCat = function(response){
			
		};
		
		vm.saveToCart = function(ordersData){
			if(vm.usrTraData){
				if(parseInt(ordersData.purchaseQuantity) > 0 && ( vm.possetData.crossCheckQuantity === 1 && parseInt(ordersData.purchaseQuantity) <= ordersData.assetQuantity ) || vm.possetData.crossCheckQuantity === 0){
					vm.successMsg = true;
					vm.addcartAssNm = ordersData.assetName;
					
					if(_.any(vm.addToCart, _.matches({"assetId":ordersData.assetId}))){
						/*angular.forEach(vm.addToCart, function(val){
							val.assetQuantity = ordersData.assetQuantity;
						});*/
						for(var i = 0 ; i < vm.addToCart.length; i++){
							if(vm.addToCart[i].assetId === ordersData.assetId){
								vm.addToCart[i].assetQuantity = ordersData.assetQuantity;
							}
						}
					}else{
						vm.addToCart.push(ordersData);
					}
					
					for(var i = 0; i < vm.orderList.length; i++){
						if(_.any(vm.addToCart, _.matches({"assetId":vm.orderList[i].assetId}))){
							vm.orderList[i].removeCrt = true;
	 					}
					}
					
					$timeout(function(){
						vm.successMsg =false;
					},1500)
				}else{
					vm.qtyErrorMsg = true;
					$timeout(function(){
						vm.qtyErrorMsg =false;
					},1500)
				}
			}
			else{
				swal("Error","Please select user", "error");
			}
		};
		
		vm.removeCartMsg = false;
		vm.removeCart = function(data){
			for(var i = 0; i < vm.orderList.length; i++){
				if(vm.orderList[i].assetId === data.assetId){
					vm.orderList[i].removeCrt = false;
					vm.orderList[i].purchaseQuantity = '0';
				}
			}	
			
			if(vm.addToCart.length > 0){
				vm.addToCart.splice(vm.addToCart.indexOf(data), 1);
				vm.removeCartMsg = true;
				vm.removedAssNm = data.assetName;
				
				$timeout(function(){
					vm.removeCartMsg =false;
				},1500)
			}
		};
		
		vm.proceed = function(){	
			var sum =  0;
			angular.forEach(vm.addToCart, function(val){
				sum = sum + val.cost * val.purchaseQuantity
			})
			
			if(vm.usrTraData.currentBalanceValue > sum){
				var otherData = Object.assign(vm.taxDetails, vm.usrTraData, vm.usrData);
				var modalInstance = $uibModal.open({
					backdrop : 'static',
					animation : vm.animationsEnabled,
					templateUrl : 'resources/views/POS/checkOut.html',
					controller : 'CheckOutCtrl',
					controllerAs : 'checkOutCtrl',
					resolve : {
						selectedOrders : function() {
							return angular.copy(vm.addToCart);
						},
//						taxDetails : function(){
//							return angular.copy(vm.taxDetails);
//						},
//						usrBalance : function(){
//							return angular.copy(vm.usrTraData);
//						},
						usrData : function(){
							return angular.copy(otherData);
						}
					}
				});

				modalInstance.result.then(function(selectedItem) {
					$state.reload();
				}, function() {
					//vm.getDivisionDetails();
				});
			}else{
				vm.availableBalanceError = true;
				$timeout(function(){
					vm.availableBalanceError = false;
				},1500)
			}
		};
		
		vm.headerPanel = false;
		vm.userAccord = function(){
			vm.headerPanel = !vm.headerPanel;
		};
		
		vm.companyPanel = false;
		vm.companyAccord = function(){
			vm.companyPanel = !vm.companyPanel;
		};
		
		/*vm.increment = function(){
			vm.text01 += 1;
		};
		
		vm.decrement = function(){
			vm.text01 += -1;
		};*/
		
		vm.userDetailsDailogue = function(){
			 $interval.cancel(promise)
			var modalInstance = $uibModal
			.open({
				backdrop : 'static',
				animation : vm.animationsEnabled,
				templateUrl : 'resources/views/POS/posUserSearch.html',
				controller : 'PosUserSearchCtrl',
				controllerAs : 'posUserSearchCtrl',
			});
			modalInstance.result.then(function(selectedItem) {
				
			}, function(usrD) {
				if(usrD !== 'cancel'){
					vm.usrData = JSON.parse(usrD);
					vm.getUserBaanceAndTaxDetails(vm.usrData);
				}
			});
		};
		
		vm.getOrderTaxDetails = function(){
			vm.requestParams = {
				"companyId" : vm.companyId	 
			}
			PosService.getOrderTaxDetails(vm.requestParams, vm.onSuccessTaxDetails, vm.onErrorTaxDetails)
		};
		
		vm.onSuccessTaxDetails = function(response){
			vm.taxDetails = response.data.resData;
		};
		
		vm.onErrorTaxDetails = function(response){
			console.log(response.data);
		};
		
		/*Get User Balance*/
		vm.getUserBaanceAndTaxDetails = function(usrInfo){
			var usrData= usrInfo;
			vm.requestParams = {
					"companyId":vm.companyId,
					/*"divisionName":usrData.divisionName,*/
					"userId":usrData.userId,
					"operatedBy" : AuthService.getUserId()
			}
			PosService.getUserTransactionBalance(vm.requestParams, vm.onSuccessUserTra, vm.onErrorUserTra)
		};
		
		vm.onSuccessUserTra = function(response){
			vm.usrTraData  = response.data.resData;
		};
		
		vm.onErrorUserTra = function(response){
			console.log(response);
		};
		
		/*Get company image*/
		vm.getCompanyImg = function(data) {
			var requestParams = {
					"companyId" : data.companyId,
					"downloadFrom" : "COMPANY",
					"imageId" : data.companyId.toString(),
					"imageName" : data.companyLogo
			};
			if(data.companyLogo){
				AuthService.downloadLogo(requestParams, vm.onGetLogoSuccess, vm.onGetLogoError);
			}
		};
		
		vm.onGetLogoSuccess = function(response){
			vm.companyImg = response.data.resData.image;
		};
		
		vm.onGetLogoError = function(response){
			console.log(response.data);
		};
		
		//Read user using Readers 
		vm.getUsersByReaders = function(){
			vm.requestParams = {
					"companyId": vm.companyId,
					"operatedBy": vm.userId,
					"category":2,
					"dockDoorIdList":[vm.dockDoorId]
			}
			PosService.getposUsersByReaders(vm.requestParams, vm.onSuccessGetUsersByReaders, vm.onErrorGetUsersByReaders)
		};
		
		vm.onSuccessGetUsersByReaders = function(response){
			var count = 0;
			if(response.data.resData){
				count++;
				vm.getUserBaanceAndTaxDetails(response.data.resData);
				if(count <= 1){
					$interval.cancel(promise)
				}
			}
		};
		
		vm.onErrorGetUsersByReaders = function(response){};
		
		promise = $interval(function(){
			vm.getUsersByReaders();
		},200)
		
		vm.clear = function(){
			$state.reload();
		};
		
		vm.stop = function(){
			 $interval.cancel(promise)
		};
		
		$scope.$on('$destroy', function() {
			vm.stop();
		});
		
		vm.openDockdoor = function(){
			var modalInstance = $uibModal.open({
				backdrop : 'static',
					animation : $scope.animationsEnabled,
					templateUrl : 'resources/views/asset/dockDoorPop_up.html',
					controller : 'DockDoorPopUpController',
					controllerAs : 'dockdoorpopupController',
			});
			
			modalInstance.result.then(function(selectedItem) {
				/* $scope.selected = selectedItem; */
			}, function() {
				/*$log.info('Modal dismissed at: '+ new Date());*/
			});
		};
		
		vm.getSettingDetails = function(){
			vm.requestParams = {
					"companyId" : vm.companyId
			};
			PosService.getposSettingDetails(vm.requestParams, vm.onSuccessSettingDetails, vm.onErrorSettingDetails);
		};
		
		vm.onSuccessSettingDetails = function(response){
			vm.possetData = response.data.resData;
		};
		
		vm.onErrorSettingDetails = function(response){
			console.log(response.data.resMsg);
		};
		
		vm.getOrderCategory();
		vm.getCompanyDetails();
		vm.getOrderTaxDetails();
		vm.getSettingDetails();
	}]);
})();
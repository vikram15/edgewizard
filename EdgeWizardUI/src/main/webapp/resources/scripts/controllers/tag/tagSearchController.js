(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('TagSearchController',['$rootScope','$uibModalInstance','AuthService','TagService','$state','$stateParams','MasterService','tagSelect',
	function($rootScope, $uibModalInstance, AuthService, TagService, $state, $stateParams, MasterService, tagSelect){
		var vm = this;
		vm.tableHeader = "Tag Search";
		
		//pagination
	    vm.currentPage = 1;
	   /* vm.itemsPerPage = 25;*/
	    vm.maxSize = 10;
	    vm.selectAssetType = "EPC ID";
		vm.comp_Id = AuthService.getCompanyId();

		vm.tagSearchData = function(){
			vm.requestParams = {
					"searchBy":vm.selectAssetType,
	                "searchValue":vm.searchValue,
					"companyId":AuthService.getCompanyId(),
					"category":vm.catType,
					"currentPage":vm.currentPage,
					
					"tagType":vm.sortBy,
					"tagMemory":vm.sortByTagMem,
					"tagFreq":vm.sortByTagFre,
					"categoryName":vm.catType,
					"divisionName":vm.sortByDivision,
					"sectorName":vm.sortBySector,
					"locationName":vm.sortByLocName,
					"departmentName":vm.sortByDepartmentName,
					"manufactureName":vm.sortByManufacture,
					"vendorName":vm.sortByVendorNm,
					"fromDate":vm.fromDate,
	              	"toDate":vm.toDate,
			};
			TagService.tagSearchDetails(vm.requestParams, vm.tagSearchDetSuccess, vm.tagSearchDetError);
		};
			
		vm.tagData = [];
		vm.tagSearchDetSuccess = function(response){
			vm.allTags = response.data;
			if(vm.allTags.resData[1].length > 0){
				if(tagSelect !== 2 && tagSelect !== undefined){
					if(_.any(vm.allTags.resData[1], _.matches({"tagId":tagSelect}))){
						_.each(vm.allTags.resData[1], function(data){
							if(data.tagId === tagSelect){
								vm.tagData = _.without(vm.allTags.resData[1], data);
							}
						});
						//console.log(_.without(data, 'a3'));
					}else{
						vm.tagData = vm.allTags.resData[1];
					}
				}else{
					vm.tagData = vm.allTags.resData[1]
				}
			}
			vm.totalItems = vm.allTags.resData[0].totalRecord;
	    	vm.itemsPerPage = vm.allTags.resData[0].recordPerPage;
		};

		vm.tagSearchDetError = function(response){
			console.log("error");
		};
		
		 vm.categoryList = function() {
             vm.requestParams = {
                 "categoryPrimery": {
                     "category": 0,
                     "companyId": vm.comp_Id
                 }	
             };
             MasterService.getCategoryAll(vm.requestParams, vm.getCategoryAllSucc, vm.getCategoryAllError)
         };

         vm.getCategoryAllSucc = function(response) {
             vm.category_list = response.data;
             vm.categoryType = vm.category_list.resData[0].categoryPrimery.categoryId;
         };

         vm.getCategoryAllError = function(response) {
             console.log("error");
         };
         
         vm.getSectorDetails = function(){
             vm.requestParams = {
                 "companyId": vm.comp_Id
             }
             MasterService.SectorListService(vm.requestParams, vm.onSectorSuccess, vm.onSectorError);
         };

         vm.onSectorSuccess = function(response){
           vm.sect_data = response.data;
         };

         vm.onSectorError = function(){

         };

         vm.getLocationDetails = function(){
         	vm.requestParams = {
         			"companyId": vm.comp_Id
         	}
         	MasterService.LocationListService(vm.requestParams, vm.onLSuccess, vm.onLError);
         };

         vm.onLSuccess = function(response){
           vm.loc_data = response.data;
         };

         vm.onLError = function(response){

         };

         vm.getDepartmentDetails = function(){
           vm.requestParams = {
         			"companyId": vm.comp_Id
         	}
         	MasterService.DepartmentListService(vm.requestParams, vm.onDepSuccess, vm.onDepError);
         };

         vm.onDepSuccess = function(response){
           vm.depart_data = response.data;
         };

         vm.onDepError = function(response){

         };

         vm.getManufacturerDetails = function(){
           vm.requestParams = {
         			"companyId": vm.comp_Id
         	}
         	MasterService.manufactureAllDetails(vm.requestParams, vm.onManSuccess, vm.onManError);
         };

         vm.onManSuccess = function(response){
           vm.manufactureDt = response.data;
         };

         vm.onManError = function(response){

         };

         vm.getVendorDetails = function(){
           vm.requestParams = {
               "companyId": vm.comp_Id
           }
           MasterService.getVendorData(vm.requestParams, vm.onVenSuccess, vm.onVenError);
         };

         vm.onVenSuccess = function(response){
           vm.vendordetails = response.data;
         };

         vm.onVenError = function(){

         };
         
         vm.getDivisionDetails = function() {
             vm.requestParams = {
                 "companyId": vm.comp_Id
             };
             MasterService.DivisionDetailService(vm.requestParams, vm.onDivisionInfoSuccess, vm.onDivisionInfoError);
         };

         vm.onDivisionInfoSuccess = function(response) {
             vm.divisionList = response.data;
         };

         vm.onDivisionInfoError = function(response) {
             console.log("error");
         };

         
		vm.tagSelectFun = function(){
			if(vm.tagselect !== undefined){
				$uibModalInstance.dismiss(vm.tagselect);
				$rootScope.$broadcast("SendUp", vm.tagselect);
			}
		};	
		

	    /*========ANGULAR DATE PICKER============*/
		vm.today = function() {
			vm.fromDate = new Date();
			vm.toDate = new Date();
		};
		vm.today();

		vm.clear = function() {
			vm.fromDate = null;
			vm.toDate = null;
		};

		vm.dateOptions = {
				//dateDisabled: disabled,
				formatYear: 'yy',
				//maxDate: new Date(2020, 5, 22),
				minDate: new Date(2000, 5, 22),
				startingDay: 1
		};

		// Disable weekend selection
		function disabled(data) {
			var date = data.date,
			mode = data.mode;
			return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
		}

		vm.fromOpen = function(){
			vm.from.opened = true;
		};
	  
		vm.toOpen = function(){
			vm.to.opened = true;  
		};
	  
		vm.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
		vm.format = vm.formats[0];
		vm.altInputFormats = ['M!/d!/yyyy'];
		
		vm.from = {
				opened: false
		};
		vm.to = {
				opened: false
		};
		
	  function getDayClass(data) {
		  var date = data.date,
		  mode = data.mode;
		  if (mode === 'day') {
			  var dayToCheck = new Date(date).setHours(0,0,0,0);
			  
			  for (var i = 0; i < $scope.events.length; i++) {
				  var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);
		
				  if (dayToCheck === currentDay) {
					  return $scope.events[i].status;
				  }
			  }
		  }
		
		  return '';
	  }
		
		vm.cancel = function () {
			$uibModalInstance.close();
		};
		
		vm.tagSearchData();
		/*vm.getCategoryList();*/	
	}]);
})();

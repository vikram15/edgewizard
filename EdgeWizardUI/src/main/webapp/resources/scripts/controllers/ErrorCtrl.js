(function(){
	'use strict';
	angular.module('rfidDemoApp.controller')
	.controller('ErrorCtrl',['$state', function($state){
		$state.go('error');
	}]);
})(); 
(function(){
	'use strict';
	angular.module('rfidDemoApp.services')
	.service('PosService',['$http','HTTP', function($http, HTTP){
		return {
			getposSettingDetails : function(companyId, successCallback, errorCallback){
				$http({
					method : 'POST',
					url : HTTP + '/op1006.11',
					data : companyId,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			updatePosSetting : function(requestParams, successCallback, errorCallback){
				$http({
					method : 'POST',
					url : HTTP + '/op1006.32',
					data : requestParams,
					headers : { 
						'Content-type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function(response){
					errorCallback(response);
				})
			},
			
			getAllCommGroupMaseter : function(companyId, successCallback, errorCallback){
				$http({
					method : 'POST',
					url : HTTP + '/op1009.13',
					data : companyId,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response){
					errorCallback(response);
				})
			},
			
			getPosCategoryList : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.12',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			getAssetsByCategory : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.13',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			saveAssetSelection : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.21',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			getOrderCategoryList : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.14',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			getOrdersByCategory : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.15',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			getUserData : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.16',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			getUserTransactionBalance : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.17',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			getOrderTaxDetails : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.18',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			saveOrderDetails : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.110',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})	
			},
			//http://localhost:8888/EdgeWizard/op1006.51?startDate=22-07-2007&endDate=19-10-2017&userID=12
			getUserTransactionReportDetails : function(requestParams ,successCallback, errorCallback){
				$http({
					method :'GET',
					url : HTTP + '/op1006.51'+'?startDate=' + requestParams.startDate +'&endDate='+ requestParams.endDate +'&userID='+requestParams.userID,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			getposUsersByReaders : function(requestParams, successCallback, errorCallback){
				$http({
					method :'POST',
					url : HTTP + '/op1006.111',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})	
			},
			
			getPosUsersReport : function(requestParams, successCallback, errorCallback){
				$http({
					method :'GET',
					url : HTTP + '/op1006.511?activationDate=' + requestParams.startDate + '&deactivationDate=' + requestParams.endDate ,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response){
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})	
			},
			
			
		}
	}]);
})()
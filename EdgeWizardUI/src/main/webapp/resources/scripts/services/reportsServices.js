(function(){
  'use strict';
  angular.module('rfidDemoApp.services')
  .service('ReportsService', ['$http','HTTP','GET_ISSUEDASSETSLIST','GET_RECEIVEDASSETLIST','GET_ISSUEDRECEIVEDASSETLIST', 
                              function($http, HTTP, GET_ISSUEDASSETSLIST, GET_RECEIVEDASSETLIST,GET_ISSUEDRECEIVEDASSETLIST){
    return {
    	/*Issue recieve user search*/
    	getIssuedAssetsReport : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/getAllIssuedAssetList',
    			data : requestParams,
    			/*headers : {
    				'Content-Type' : 'application/json'
    			},*/
    			responseType: 'arraybuffer'
    		})
	        .then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	getReceivedAssetsReport : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/getAllReceivedAssetList',
    			data : requestParams,
    			responseType: 'arraybuffer'
    		})
	        .then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	getAllReceivedExceptionReport : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/getAllReceivedExceptionReport',
    			data : requestParams,
    			responseType: 'arraybuffer'
    		})
	        .then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	getAssetDetailsReport : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/getAssetSummaryReport',
    			data : requestParams,
    			responseType: 'arraybuffer'
    		})
	        .then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	getAllIssuedAssetList : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + GET_ISSUEDASSETSLIST, //op0048.501
    			data : requestParams,
    			headers : {
					'Content-Type' : 'application/json'
				}
    		})
	        .then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	getAllReceivedAssetList : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + GET_RECEIVEDASSETLIST, ///op0048.503
    			data : requestParams,
    			headers : {
					'Content-Type' : 'application/json'
				}
    		})
	        .then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	getAllIssuedReceivedAssetList : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + GET_ISSUEDRECEIVEDASSETLIST, ///op0048.502
    			data : requestParams,
    			headers : {
					'Content-Type' : 'application/json'
				}
    		})
	        .then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	getMaintenanceHistoryReport : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP +'/getMaintenanceHistoryDetailsWithCheckList',
    			data : requestParams,
    			responseType: "arraybuffer"
    		})
	        .then(function onSuccess(response){
	        	successCallback(response);
	        },function onError(response){
	        	errorCallback(response);
	        })
    	},
    	
    	getMaintenanceHistoryForAllAssets : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP +'/getMaintenanceHistoryDetailsWithCheckListAll',
    			data : requestParams,
    			/*headers : {
					'Content-Type' : 'application/json'
				},*/
    			responseType: 'arraybuffer'
    		})
	        .then(function onSuccess(response){
	        	successCallback(response);
	        },function onError(response){
	        	errorCallback(response);
	        })
    	},
    	
    	
    }
  }]);
})();

(function(){
  'use strict';
  angular.module('rfidDemoApp.services')
  .service('StatusService', ['$http','HTTP', function($http, HTTP){
    return {
    	issueDetails : function(requestParams , successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0048.46', // get issue details
    			data : requestParams,
    			headers : {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	expiredAssetIssueDetails : function(requestParams , successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0048.462', // get expired details
    			data : requestParams,
    			headers : {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	receiveDetails : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0048.461',
    			data : requestParams,
    			headers : {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
    	
    	expiredAssetReceiveDetails : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0048.463',
    			data : requestParams,
    			headers : {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	}
    	
    }
  }])
  })(); 
(function(){
	'use strict';
	 angular.module('rfidDemoApp.services')
	  .factory('DashboardService', ['$http','HTTP','GET_CATEGORYWISE_ASSET','GET_TTLASSET_LICENSE','$localStorage','$cookies','$window',
	                                function($http,HTTP, GET_CATEGORYWISE_ASSET, GET_TTLASSET_LICENSE, $localStorage, $cookies, $window) {
		  var totalAssetPermission;
	      if($cookies.get('totalAssetPermission') && !angular.isUndefined($cookies.get('totalAssetPermission'))){
	    	  totalAssetPermission = JSON.parse($cookies.get('totalAssetPermission'));
//	    	  GlobalLabelObj = JSON.parse($cookies.get('GlobalLabelObj'));
	      }
	      
	      var uri='data:application/vnd.ms-excel;base64,',
          template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body>{table}</body></html>',
          base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
          format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
	      
		  return {
			  getCategorywiseAsset : function(requestParams, successCallback, errorCallback){
				  $http({
					  method : 'POST',
					  url : HTTP + GET_CATEGORYWISE_ASSET,
					  data : requestParams,
					  headers: {
						  'Content-Type' : 'application/json'
					  }
				  })
				  .then(function onSuccess(response) {
					  successCallback(response);
				  },
				  function onError(response) {
					  errorCallback(response);
				  });
			  },
			  
			  getTtlAssetsFrmLicense : function(requestParams, successCallback, errorCallback){
				  $http({
					  method : 'POST',
					  url : HTTP + GET_TTLASSET_LICENSE,
					  data : requestParams,
					  headers: {
						  'Content-Type' : 'application/json'
					  }
				  })
				  .then(function onSuccess(response) {
					  $localStorage.storeTtlAsset = response.data.resData;
					  $cookies.put('totalAssetPermission', JSON.stringify(response.data.resData));
					  successCallback(response);
				  },
				  function onError(response) {
					  errorCallback(response);
				  });
			  },
			  
			 /* factory('Excel',function($window){
			        var uri='data:application/vnd.ms-excel;base64,',
			            template='<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
			            base64=function(s){return $window.btoa(unescape(encodeURIComponent(s)));},
			            format=function(s,c){return s.replace(/{(\w+)}/g,function(m,p){return c[p];})};
			        return {
			            tableToExcel:function(tableId,worksheetName){
			                var table=$(tableId),
			                    ctx={worksheet:worksheetName,table:table.html()},
			                    href=uri+base64(format(template,ctx));
			                return href;
			            }
			        };
			    })*/
			  tableToExcel:function(tableId,worksheetName){
				  var table=$(tableId),
		  	 	ctx={worksheet:worksheetName,table:table.html()},
		  	 	href=uri+base64(format(template,ctx));
		    	return href;
		    }
			
		  }
	  }]);
})();
(function() {
	'use strict';
	angular.module('rfidDemoApp.services')
	.service('AssetService',['$http', 'HTTP','$cookies', function($http, HTTP, $cookies) {
		var assData;
		
		if ($cookies.get('assData') && !angular.isUndefined($cookies.get('assData'))) {
			assData = JSON.parse($cookies.get('assData'));
		}
		
		return {
			getSectDetailsByDiv : function(divisionID, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0004.31',
					data : divisionID,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
									
			getLocDetailsBySecID : function(sectorID, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0006.31',
					data : sectorID,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			getDepDetailsByLocID : function(locationID, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0005.31',
					data : locationID,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			assetAddDetails : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.2',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(
						function onSuccess(response) {
							successCallback(response);
						}, function onError(response) {
							errorCallback(response);
						});
			},
			
			/*assetPaginatDetails : function(pageNo,successCallback, errorCallback) {
										$http({
													method : 'GET',
													url : HTTP
															+ '/assetPagination/'
															+ pageNo,
													headers : {
														'Content-Type' : 'application/json'
													}
												}).then(
												function onSuccess(response) {
													successCallback(response);
												}, function onError(response) {
													errorCallback(response);
												})
									},*/
			
			AssetUpdateDetails : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.3',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},

			assetDeleteDetails : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.4',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
									
			getTotalAssets : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.41',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			assetGetByID : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.1',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			assetGetByTagId : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.11',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},

			searchAssetDetails : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.23',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			singleAsseDataStore : function(storeData) {
				assData = storeData;
				$cookies.put('assData', JSON.stringify(assData));
			},
			
			getAsset : function() {
				return assData;
			},

			uploadLogo : function(requestParams, file, successCallback, errorCallback) {
				var fd = new FormData();
				fd.append('companyId', requestParams.companyId);
				fd.append('uploadTo', requestParams.uploadTo);
				fd.append('imageId', requestParams.imageId);
				fd.append('file', file);
				
				$http.post(HTTP + IMAGE_UPLOAD, fd, {
					transformRequest : angular.identity,
					headers : {
						'Content-Type' : undefined
					}
				})
				.then(function onSuccess(response) {
					successCallback(response);
				},
				function onError(response) {
					errorCallback(response);
				})
			},
			
			downloadLogo : function(requestParams, successCallback1, errorCallback1) {
				$http({
					method : 'POST',
					url : HTTP + IMAGE_DOWNLOAD,
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback1(response);
				}, function onError(response) {
					errorCallback1(response);
				})
			},
			
			splitAddDetails : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.36',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			outOfstock : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.39',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				})
			},
			
			groupMasterDetails : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.42',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},

			getInOutDocRef : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0010.43',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			readTag : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.44',
					data : requestParams,
						headers : {
							'Content-Type' : 'application/json'
						}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},	
									
			releaseTag : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.28',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			uploadAttach : function(requestParams, successCallback, errorCallback) {
				var fd = new FormData();
				fd.append('companyId',requestParams.companyId);
				fd.append('uploadTo',requestParams.uploadTo);
				fd.append('id',requestParams.id);
				fd.append('fileName', requestParams.fileName);
				angular.forEach(requestParams.file, function(value){
					fd.append('file', value);
				})
				
				$http({
					method : 'POST',
					url : HTTP + '/uploadDocumentFile',
					data : fd,
					headers : { 'Content-Type': undefined}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			downloadAttachment : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/downloadDocumentFile',
					data : requestParams,
					responseType: "arraybuffer"
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			deleteAttachment : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/deleteAttachment',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			getAllUploadDetails : function(requestParams, successCallback, errorCallback) {
				$http({
					method : 'POST',
					url : HTTP + '/op0015.10',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			/*asset issue page asset search data*/
			assetSearchData : function(requestParams, successCallback, errorCallback){
				$http({
					method : 'POST',
					url : HTTP + '/op0048.23',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			getCategoryAssetDetails : function(requestParams, successCallback, errorCallback){
				$http({
					method : 'POST',
					url : HTTP + '/op0015.7',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
									
			getTotalIssuesAndReturn : function(successCallback, errorCallback){
				$http({
					method : 'POST',
					url : HTTP + '/op0048.12',
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
									
			getAssetIssueReceive : function(requestParams, successCallback, errorCallback){
				$http({
					method : 'POST',
					url : HTTP + '/op0048.13',
					data : requestParams,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
			
			getLocation : function(tagId, successCallback, errorCallback){
				$http({
					method: 'GET',
					url : HTTP + '/op0051.1/' + tagId,
					headers : {
						'Content-Type' : 'application/json'
					}
				}).then(function onSuccess(response) {
					successCallback(response);
				}, function onError(response) {
					errorCallback(response);
				});
			},
										
		};
	}]);
})();

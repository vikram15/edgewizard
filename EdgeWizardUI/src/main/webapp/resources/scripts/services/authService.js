(function(){
  'use strict';
  angular.module('rfidDemoApp.services')
    .factory('AuthService', ['$http','COMP_LIST','COMP_LIST_op0001','COMP_MODIFY','COMP_GET','DIV_ADD','$cookies','LICFILE_GET','HTTP','IMAGE_UPLOAD','IMAGE_DOWNLOAD','LABEL_CHANGED','$localStorage',
      function($http, COMP_LIST,COMP_LIST_op0001,COMP_MODIFY,COMP_GET, DIV_ADD, $cookies, LICFILE_GET, HTTP, IMAGE_UPLOAD,IMAGE_DOWNLOAD, LABEL_CHANGED, $localStorage) {
      var globals;
      var GlobalLabelObj;
      if($cookies.get('globals') && !angular.isUndefined($cookies.get('globals'))){
    	  globals = JSON.parse($cookies.get('globals'));
//    	  GlobalLabelObj = JSON.parse($cookies.get('GlobalLabelObj'));
      }

      return {
        loginService: function(requestParams, successCallback, errorCallback) {
          $http({
            method: 'POST',
            url: HTTP + COMP_LIST_op0001,
            data : requestParams,
            headers : {
            	  'Content-Type' : 'application/json'
            }
          })
          .then(function onSuccess(response) {
            successCallback(response);
          },
          function onError(response) {
            errorCallback(response);
          });
        },

        companyList : function(requestParams, successBlock, errorBlock){
        	$http({
	          method: 'POST',
	          url: HTTP + COMP_LIST,
	          data: requestParams,
	          headers: {
	            'Content-Type' : 'application/json'
	          }
	        })
	        .then(function onSuccess(response) {
	        	successBlock(response);
	        },
	        function onError(response) {
	        	errorBlock(response)
	        });
        },

        CompanyDetailService: function(requestParams, successDatal, errorData) {
            $http({
              method: 'POST',
              url: HTTP + COMP_GET,
              data : requestParams,
              headers: {
              	 'Content-Type' : 'application/json'
              }
            })
            .then(function onSuccess(response) {
            	successDatal(response);
            },
            function onError(response) {
            	errorData(response);
            });
         },

         CompanyModifyService: function(requestParams, successData, errorData) {
             $http({
               method: 'POST',
               url: HTTP + COMP_MODIFY,
               data : requestParams,
               headers: {
               	 'Content-Type' : 'application/json'
               }
             })
             .then(function onSuccess(response) {
             	successData(response);
             },
             function onError(response) {
             	errorData(response);
             });
           },

         DivisionAddService: function(requestParams, successData, errorData){
        	 $http({
                 method: 'POST',
                 url: HTTP + DIV_ADD,
                 data : requestParams,
                 headers: {
                 	 'Content-Type' : 'application/json'
                 }
             })
             .then(function onSuccess(response) {
               	successData(response);
             },
             function onError(response) {
               	errorData(response);
             });
         },

         licFileDetails : function(successCallback, errorCallback){
           $http({
             method : 'POST',
             url : HTTP + LICFILE_GET,
             headers : {
               'Content-Type' : 'application/json'
             }
           })
           .then(function onSuccess(response){
             successCallback(response);
           },function onError(response) {
             errorCallback(response);
           })
         },
         
         getLastLoginDetails : function(requestParams, successCallback, errorCallback){
        	 $http({
                 method : 'POST',
                 url : HTTP + '/op0002.47',
                 data : requestParams,
                 headers : {
                   'Content-Type' : 'application/json'
                 }
               })
               .then(function onSuccess(response){
                 successCallback(response);
               },function onError(response) {
                 errorCallback(response);
               }) 
         },
         
         uploadLogo : function(requestParams,file,successCallback, errorCallback){
        	 var fd = new FormData();
		        fd.append('companyId',requestParams.companyId);
		        fd.append('uploadTo',requestParams.uploadTo);
		        fd.append('imageId',requestParams.imageId);
		        fd.append('file', file);
		        
		        /*if(assetLogo_1){
		        	fd.append('file', file);
		        }
		        else{
		        	fd.append('file', file);
		        }*/
		        fd.append('imageData',"");
		       /* fd.append('imageData',requestParams.imageData)*/
		        
		        /*$http.post(HTTP + IMAGE_UPLOAD, fd, {
		            transformRequest: angular.identity,
		            headers: {'Content-Type':'multipart/form-data'}
		            headers: {'Content-Type': undefined}
		        })*/
		        $http({
		            url: HTTP + IMAGE_UPLOAD,
		            method: 'POST',
		            data: fd,
		            headers: { 'Content-Type': undefined}
		        })
	           .then(function onSuccess(response){
	             successCallback(response);
	           },function onError(response) {
	             errorCallback(response);
	           })
         },
         
         downloadLogo : function(requestParams,successCallback1, errorCallback1){
        	 $http({
                 method : 'POST',
                 url : HTTP + IMAGE_DOWNLOAD,
                 data:requestParams,
                 headers : {
                   'Content-Type' : 'application/json'
                 }
               })
	           .then(function onSuccess(response){
	             successCallback1(response);
	           },function onError(response) {
	             errorCallback1(response);
	           })
         },
         
         labelChange : function(requestParams,successCallback, errorCallback){
        	 $http({
                 method : 'POST',
                 url : HTTP + LABEL_CHANGED,
                 data:requestParams,
                 headers : {
                   'Content-Type' : 'application/json'
                 }
               })
	           .then(function onSuccess(response){
	        	 /*  console.log(response,"RESPONSE");*/
	             successCallback(response);
	           },function onError(response) {
	             errorCallback(response);
	           })
         },
         
        storeLabelInformation : function (labelInfo){
        	GlobalLabelObj = labelInfo;
        	$localStorage.GlobalLabelObj = GlobalLabelObj;
        },
         
        storeLoginInformation : function(userInfo, isReqFrom){
        	/*console.log(userInfo);*/
        	globals = {
        		'userId' : 	userInfo.UserId,
        		'userName' :userInfo.UserLoginName,
        		'companyId' : userInfo.CompanyId,
        		'userType' : userInfo.UserType,
        		'CompanyName' : userInfo.CompanyName,
        		'solution' : userInfo.solution,
        		'dockDoorId':userInfo.doockdoorId,
        		'dockDoorName': userInfo.dockdoorName
        	}
        	$cookies.put('globals', JSON.stringify(globals));
        	$cookies.put('isReqFrom', isReqFrom);
        },
        
        getLabelInfo : function(){
        	return $localStorage.GlobalLabelObj;
        },

        getUserId : function(){
        	return globals.userId;
        },
        
        getUserName : function(){
        	return globals.userName;
        },

        getCompanyId : function(){
        	return globals.companyId;
        },

        getUserType : function(){
        	return globals.userType;
        },

        getCompanyName : function(){
        	return globals.CompanyName;
        },
        
        getSystemName : function(){
        	return globals.solution;
        },
        getDockDoorName : function(){
        	return globals.dockDoorName;
        },
        getDockDoorId : function(){
        	return globals.dockDoorId;
        },
        
        checkTransactionPassword : function(requestParams, successCallback, errorCallback){
        	$http({
				method :'POST',
				url : HTTP + '/op1006.113',
				data : requestParams,
				headers : {
					'Content-Type' : 'application/json'
				}
			}).then(function onSuccess(response){
				successCallback(response);
			}, function onError(response) {
				errorCallback(response);
			})
        },

      };
    }])
})();



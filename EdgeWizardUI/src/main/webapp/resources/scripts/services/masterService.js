(function() {
    'use strict';
    angular
        .module('rfidDemoApp.services')
        .service(
            'MasterService', [
                '$http',
                'MANUFAC_GETALL',
                'MANUFAC_ADD',
                'MANUFAC_MODIFY',
                'MANUFAC_DELETE',
                'VENDOR_GETALL',
                'VENDOR_ADD',
                'VENDOR_DELETE',
                'VENDOR_UPDATE',
                'DIV_LIST',
                'DIV_MODIFY',
                'DIV_DELETE',
                'BATCHLOT_GETALL',
                'BATCHLOT_ADD',
                'BATCHLOT_UPDATE',
                'BATCHLOT_DELETE',
                'SEC_LIST',
                'SEC_ADD',
                'SEC_MODIFY',
                'SEC_DELETE',
                'LOC_LIST',
                'LOC_ADD',
                'LOC_MODIFY',
                'LOC_DELETE',
                'DEPT_LIST',
                'DEPT_ADD',
                'DEPT_MODIFY',
                'DEPT_DELETE',
                'HTTP',
                function($http, MANUFAC_GETALL, MANUFAC_ADD,
                    MANUFAC_MODIFY, MANUFAC_DELETE,
                    VENDOR_GETALL, VENDOR_ADD, VENDOR_DELETE,
                    VENDOR_UPDATE, DIV_LIST, DIV_MODIFY,
                    DIV_DELETE, BATCHLOT_GETALL, BATCHLOT_ADD,
                    BATCHLOT_UPDATE, BATCHLOT_DELETE, SEC_LIST,
                    SEC_ADD, SEC_MODIFY, SEC_DELETE, LOC_LIST,
                    LOC_ADD, LOC_MODIFY, LOC_DELETE, DEPT_LIST,
                    DEPT_ADD, DEPT_MODIFY, DEPT_DELETE, HTTP) {
                	
                    return {
                        manufactureAllDetails: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + MANUFAC_GETALL,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        manufactureAddDetails: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + MANUFAC_ADD,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response)
                                },
                                function onError(response) {
                                    errorCallback(response)
                                })
                        },

                        DivisionDetailService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + DIV_LIST,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        DivisionUpdateService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + DIV_MODIFY,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        manufactureUpdate: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + MANUFAC_MODIFY,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response)
                                },
                                function onError() {
                                    errorCallback(response);
                                })
                        },

                        manufactureDelete: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + MANUFAC_DELETE,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response)
                                },
                                function onError(response) {
                                    errorCallback(response)
                                })
                        },

                        getVendorData: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + VENDOR_GETALL,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response)
                                })
                        },

                        addVendorData: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + VENDOR_ADD,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        updateVendorDetails: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + VENDOR_UPDATE,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                })
                        },

                        deleteVendorDetails: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + VENDOR_DELETE,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function() {
                                    errorCallback(response);
                                })
                        },

                        DivisionDeleteService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + DIV_DELETE,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        SectorListService: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + SEC_LIST,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },
                        SectorAddService: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + SEC_ADD,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        SectorUpdateService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + SEC_MODIFY,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },
                        SectorDeleteService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + SEC_DELETE,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        LocationListService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + LOC_LIST,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },
                        LocationAddService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + LOC_ADD,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        LocationUpdateService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + LOC_MODIFY,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },
                        LocationDeleteService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + LOC_DELETE,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        getBatchLotData: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP +
                                    BATCHLOT_GETALL,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function() {
                                    errorCallback(response);
                                })
                        },

                        batchLostAddDetails: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + BATCHLOT_ADD,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        batchLotUpdateDetails: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP +
                                    BATCHLOT_UPDATE,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError() {
                                    errorCallback(response);
                                })
                        },

                        batchLotsDeleteDetails: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP +
                                    BATCHLOT_DELETE,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function(response) {
                                successCallback(response);
                            }, function(response) {
                                errorCallback(response);
                            })
                        },

                        categoryGetDetails: function(requestParams, successCallback, errorCallback) {
                            /* vm.dataLoaded = false; */
                            $http({
                                method: 'POST',
                                url: HTTP + '/op0007.1',
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function onSuccess(response) {
                            	successCallback(response);
                            },
                            function onError(response) {
                            	errorCallback(response);
                            })
                        },

                        categoryAddDetails: function(requestParams, successCallback, errorCallback) {
                        	$http({
                        		method: 'POST',
                        		url: HTTP + '/op0007.2',
                        		data: requestParams,
                                headers: {
                                	'Content-Type': 'application/json'
                                }
                        	}).then(function onSuccess(response) {
                        		successCallback(response);
                        	},function onError() {
                        		errorCallback(response);
                        	})
                        },

                        DepartmentListService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + DEPT_LIST,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },
                        DepartmentAddService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + DEPT_ADD,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        DepartmentUpdateService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + DEPT_MODIFY,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },
                        DepartmentDeleteService: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + DEPT_DELETE,
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        categoryUpdatedetails: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + '/op0007.3',
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        categoryGetINB: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + '/op0007.20',
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response)
                                },
                                function onError(response) {
                                    errorCallback(response);
                                })
                        },

                        getCategoryAll: function(requestParams,successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + '/op0007.10',
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(function onSuccess(response) {
                            	successCallback(response)
                            },
                            function onError(response) {
                            	errorCallback(response);
                            })
                        },

                        categoryDeleteDetails: function(
                            requestParams, successCallback,
                            errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + '/op0007.4',
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response)
                                },
                                function onError(response) {
                                    errorCallback(response);
                                })
                        },

                        RackListService: function(successCallback,  errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + '/op0017.37',
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        RackAddService: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + '/op0017.2',
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        RackUpdateService: function(requestParams,
                            successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + '/op0017.3',
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },
                        RackDeleteService: function(requestParams, successCallback, errorCallback) {
                            $http({
                                method: 'POST',
                                url: HTTP + '/op0017.4',
                                data: requestParams,
                                headers: {
                                    'Content-Type': 'application/json'
                                }
                            }).then(
                                function onSuccess(response) {
                                    successCallback(response);
                                },
                                function onError(response) {
                                    errorCallback(response);
                                });
                        },

                        taxMastersDetails: function(successCallback, errorCallback) {
                            $http({
                                    method: 'POST',
                                    url: HTTP + '/op1001.37',
                                    headers: {
                                        'Content-Type': 'application/json'
                                    }
                                })
                                .then(function onSuccess(response) {
                                    successCallback(response);
                                }, function onError(response) {
                                    errorCallback(response);
                                })
                        },
                        

                    };
                }
            ]);
})();

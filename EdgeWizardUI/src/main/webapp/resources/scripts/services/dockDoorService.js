
(function(){
	  'use strict';
	  angular.module('rfidDemoApp.services')
	    .factory('dockDoorService', ['$http','$cookies','HTTP','$localStorage',
	      function($http, $cookies, HTTP, $localStorage) {
	      var globals;
	      var GlobalLabelObj;
	      if($cookies.get('globals') && !angular.isUndefined($cookies.get('globals'))){
	    	  globals = JSON.parse($cookies.get('globals'));
//	    	  GlobalLabelObj = JSON.parse($cookies.get('GlobalLabelObj'));
	      }


	      return {
	        getAllDockDoor: function(requestParams, successCallback, errorCallback) {
	          $http({
	            method: 'POST',
	            url: HTTP + "/op0018.1",
	            data : requestParams,
	            headers: {
	            	 'Content-Type' : 'application/json'
	            }
	          })
	          .then(function onSuccess(response) {
	            successCallback(response);
	          },
	          function onError(response) {
	            errorCallback(response);
	          });
	        },
	        setDefaultDockDoor: function(requestParams, successCallback, errorCallback) {
		          $http({
		            method: 'POST',
		            url: HTTP + "/op1002.3",
		            data : requestParams,
		            headers: {
		            	 'Content-Type' : 'application/json'
		            }
		          })
		          .then(function onSuccess(response) {
		            successCallback(response);
		          },
		          function onError(response) {
		            errorCallback(response);
		          });
		     },
	        getDefaultDockDoor: function(requestParams, successCallback, errorCallback) {
		          $http({
		            method: 'POST',
		            url: HTTP + "/op1002.1",
		            data : requestParams,
		            headers: {
		            	 'Content-Type' : 'application/json'
		            }
		          })
		          .then(function onSuccess(response) {
		            successCallback(response);
		          },
		          function onError(response) {
		            errorCallback(response);
		          });
		      },
	      };
	    }])
	})();



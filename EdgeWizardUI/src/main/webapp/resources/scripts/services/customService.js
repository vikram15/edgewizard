  (function(){
  'use strict';
  angular.module('rfidDemoApp.services')
    .factory('CustomService', ['$http','$cookies','AuthService', function($http, $cookies, AuthService) {
    	var vm = this ;

        //print var
    	var assetNameList,serNo,temp = [];

		var golObj = $cookies.get("globals")
		if(golObj){
			vm.globalCompId = AuthService.getCompanyId();
			vm.globalCompName = AuthService.getCompanyName();
			vm.DefaultDockDoor = JSON.parse($cookies.get("globals"));
			vm.userID = AuthService.getUserId();
			vm.dockDoorName = AuthService.getDockDoorName();
			vm.userName = AuthService.getUserName();
		}

		return {
			//Receive Print service (USING small printer)
			receivePrintService: function(pData) {
				console.log("print service called");
				vm.printData = pData;

				//print code
				var n = 0;
				vm.serialList;
				for(var i=0 ; i < vm.printData.assetNameAndSerialNoList.length ; i++){
					assetNameList = vm.printData.assetNameAndSerialNoList[i].assetName;
					serNo = vm.printData.assetNameAndSerialNoList[i].serialNumber;
					n++;
					temp.push("<li>"+ n + ".&nbsp;"+ assetNameList + "&nbsp;&nbsp;&nbsp;"+ serNo+"</li>&nbsp;&nbsp;&nbsp;"+ vm.printData.returnDate + "<br>")
				};

				var popupWin = window.open('', '_blank', 'height=400,width=700');
				popupWin.document.open();
				popupWin.document.write('<html ng-app="printApp"> <head> <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script><link rel="stylesheet" type="text/css" href="style.css" />'+
					  '<style> body {width: 100%;height: 100%; margin: 0;  padding: 0; background-color: #FAFAFA; font: 12pt "Tahoma"; }'+
					      '* {'+
					         ' box-sizing: border-box;'+
					          '-moz-box-sizing: border-box;'+
					     '}'+
					      '.page {'+
					          /* width: 100mm; */
					         'min-height: auto;'+
					          /* padding-top: 1mm;
					          padding-bottom : 1mm; */
					          'margin-top: 0;'+
					          'border: 1px #D3D3D3 solid;'+
					          'border-radius: 5px;'+
					          'background: white;'+
					          'box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);'+
					      '}'+
					      '.subpage {padding: 0.5cm; width:110mm;	/* border: 3px red solid;  height: 50mm; */ outline: 2cm #FFEAEA solid; }'+
					      '.subpage p{font-size : 20px;line-height: 3px;}.subpage h3{font-size : 1em}.mylist li{list-style : none;font-size: 18px;}'+

					      '@page {  size: auto; margin: 0;  }'+
					      '@media print {  html, body { width: 210mm;  height: 1mm; }'+
					          '.page {'+
					              'margin: 0;'+
					              'border: initial;'+
					              'border-radius: initial;'+
					              'width: initial;'+
					              'min-height: initial;'+
					              'box-shadow: initial;'+
					              'background: initial;'+
					              'page-break-after: always;'+
					          '}'+
					      '}'+
					  '</style>'+
					  '<script>var app = angular.module("printApp",[]);app.controller("demoCtrl",function($scope){ $scope.name = "vikram"})</script>'+
					  '</head>'+
					  '<body onload="window.print()"  ng-controller="demoCtrl">'+
					  		'<div class="book">'+
					  			'<div class="page">'+
					  				'<div class="subpage">'+
					  					'<h3 style="text-align : center">'+ vm.globalCompName +'</h3>'+
					  					'<p>Date : '+ vm.printData.date +'</p>'+
					  					'<p>Received From :'+ vm.printData.receivedFrom+'</p>'+
					  					'<p>Received By :'+ vm.userName +'</p>'+
					  					'<p>DockDoor Name : '+ vm.dockDoorName+'</p>'+
					  					'<p>Transaction ID : '+ vm.printData.tranactionId +'</p>'+
					  					'---------------------------------------------------------'+
					  					'<div class="mylist">'+ temp.join('')  +'</div>'+
				  						'---------------------------------------------------------'+
				  						'<p style="margin-bottom: 8px; margin-top: 10px;">Total Received Books With User : '+ vm.printData.totalReceivedBooksFromUser+'</p>'+
				  						'---------------------------------------------------------'+
				  						'<p>Note : '+ vm.printData.receiptNote+'</p>'+
				  						'<p style="text-align: right;">By : Dolphin RFID Pvt. Ltd</p>'+
					  				'</div>'+
					  			'</div>'+
					  		'</div>'+
					  '</body>'+
					  '</html>');
			  popupWin.document.close();
			  assetNameList,serNo = ""
			  temp = [];
        },

        //Issue Print Service
        issuePrintService: function(pData) {
        	vm.printData = pData;

        	//print code
			var n = 0,
				assetName,
				tempRows = [];
				
			
			vm.serialList;
			for(var i=0 ; i < vm.printData.assetNameAndSerialNoList.length ; i++){
				assetName = vm.printData.assetNameAndSerialNoList[i].assetName;
				serNo = vm.printData.assetNameAndSerialNoList[i].serialNumber;
				n++;
				tempRows.push("<li>"+ n + ".&nbsp;"+ assetName + "&nbsp;&nbsp;&nbsp;"+ serNo+"</li>&nbsp;&nbsp;&nbsp;"+ vm.printData.returnDate + "<br>")
			};

			  var popupWin = window.open('', '_blank', 'height=400,width=700');
			  popupWin.document.open();
			  popupWin.document.write('<html ng-app="printApp"> <head> <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script><link rel="stylesheet" type="text/css" href="style.css" />'+
					  '<style> body {width: 100%;height: 100%; margin: 0;  padding: 0; background-color: #FAFAFA; font: 12pt "Tahoma"; }'+
					      '* {'+
					         ' box-sizing: border-box;'+
					          '-moz-box-sizing: border-box;'+
					     '}'+
					      '.page {'+
					          /* width: 100mm; */
					         'min-height: auto;'+
					          /* padding-top: 1mm;
					          padding-bottom : 1mm; */
					          'margin-top: 0;'+
					          'border: 1px #D3D3D3 solid;'+
					          'border-radius: 5px;'+
					          'background: white;'+
					          'box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);'+
					      '}'+
					      '.subpage {padding: 0.5cm; width:110mm;	/* border: 3px red solid;  height: 50mm; */ outline: 2cm #FFEAEA solid; }'+
					      '.subpage p{font-size : 20px;line-height: 3px;}.subpage h3{font-size : 1em}.mylist li{list-style : none;font-size: 18px;}'+

					      '@page {  size: auto; margin: 0;  }'+
					      '@media print {  html, body { width: 210mm;  height: 1mm; }'+
					          '.page {'+
					              'margin: 0;'+
					              'border: initial;'+
					              'border-radius: initial;'+
					              'width: initial;'+
					              'min-height: initial;'+
					              'box-shadow: initial;'+
					              'background: initial;'+
					              'page-break-after: always;'+
					          '}'+
					      '}'+
					  '</style>'+
					  '<script>var app = angular.module("printApp",[]);app.controller("demoCtrl",function($scope){ $scope.name = "vikram"})</script>'+
					  '</head>'+
					  '<body onload="window.print()"  ng-controller="demoCtrl">'+
					  		'<div class="book">'+
					  			'<div class="page">'+
					  				'<div class="subpage">'+
					  					'<h3 style="text-align : center">'+ vm.printData.companyName +'</h3>'+
					  					'<p>Date : '+ vm.printData.date +'</p>'+
					  					'<p>Issued To :'+ vm.printData.issuedTo+'</p>'+
					  					'<p>Issued By :'+ vm.printData.issuedBy+'</p>'+
					  					'<p>DockDoor Name : '+ vm.printData.dockDoorName+'</p>'+
					  					'<p>Transaction ID : '+ vm.printData.transactionId +'</p>'+
					  					'---------------------------------------------------------'+
					  					'<div class="mylist">'+ tempRows.join('')  +'</div>'+
				  						'---------------------------------------------------------'+
				  						'<p style="margin-bottom: 8px; margin-top: 10px;">Total Issued Books With User : '+ vm.printData.totalIssuedBookToUser+'</p>'+
				  						'---------------------------------------------------------'+
				  						'<p>Note : '+ vm.printData.receiptNote+'</p>'+
				  						'<p style="text-align: right;">By : Dolphin RFID Pvt. Ltd</p>'+
					  				'</div>'+
					  			'</div>'+
					  		'</div>'+
					  '</body>'+
					  '</html>');
			  popupWin.document.close();
			  
        },
        
        printCartOrders : function(orderData, otherData){
        	vm.printData = orderData;
        	vm.currDate = new Date();	
        	var currDate = moment(vm.currDate).format("DD-MMM-YYYY HH::mm:ss");
			var tempRows = [];
			
			for(var i=0 ; i < vm.printData.length ; i++){
				//assetName = vm.printData[i].assetName;
				//serNo = vm.printData.assetNameAndSerialNoList[i].serialNumber;
				var totalAmt = vm.printData[i].purchaseQuantity * vm.printData[i].cost;
				tempRows.push("<tr><td>"+ vm.printData[i].assetName + "</td><td>" + vm.printData[i].purchaseQuantity + "</td><td>" + vm.printData[i].cost + "</td><td>"+ totalAmt + "</td></tr>")
			};

			var popupWin = window.open('', '_blank', 'height=400,width=700');
			popupWin.document.open();
			popupWin.document.write('<html ng-app="printApp"> <head>  <link type="text/css" rel="stylesheet" href="resources/styles/materialCss/bootstrap.css"></link><script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.0/angular.min.js"></script><link rel="stylesheet" type="text/css" href="style.css" />'+
					'<style> body {width: 100%;height: 100%; margin: 0;  padding: 0; background-color: #FAFAFA; font: 12pt "Tahoma"; }'+
					'* {'+
					' box-sizing: border-box;'+
					'-moz-box-sizing: border-box;'+
					'}'+
					'.page {'+
					/* width: 100mm; */
					'min-height: auto;'+
					/* padding-top: 1mm;
					          padding-bottom : 1mm; */
					'margin-top: 0;'+
					'border: 1px #D3D3D3 solid;'+
					'border-radius: 5px;'+
					'background: white;'+
					'box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);'+
					'}'+
					'.subpage {padding: 0.2cm 8px; width:110mm;	/* border: 3px red solid;  height: 50mm; */ outline: 2cm #FFEAEA solid; }'+
					'.subpage p{font-size: 21px; line-height: 8px;}.subpage h3{font-size : 1em}.mylist li{list-style : none;font-size: 18px;}'+
					
					'@page {  size: auto; margin: 0;  }'+
					'@media print {  html, body { width: 210mm;  height: 1mm; }'+
					'.page {'+
					'margin: 0;'+
					'border: initial;'+
					'border-radius: initial;'+
					'width: initial;'+
					'min-height: initial;'+
					'box-shadow: initial;'+
					'background: initial;'+
					'page-break-after: always;'+
					'}'+
					'}'+
					'</style>'+
					'<script>var app = angular.module("printApp",[]);app.controller("demoCtrl",function($scope){ $scope.name = "vikram"})</script>'+
					'</head>'+
					'<body onload="window.print()"  ng-controller="demoCtrl">'+
					'<div class="book">'+
					'<div class="page">'+
					'<div class="subpage">'+
					'<h3 class="text-center"><strong>'+ vm.globalCompName +'</strong></h3>'+
					'<h3>Date : '+ currDate +'</h3>'+
					'<h3>Name : '+ otherData.userName +'</h3>'+
					'<h3>Order No : '+ otherData.orderNo +' </h3>'+
					'<h3>Counter No : 4 </h3>'+
					'<h3>Counter Name : S1</h3>'+
					'<div class="mylist" style="padding-top: 8px;padding-bottom: 9px; border-bottom: solid 2px; border-top: solid 2px;border-bottom-style: dotted;    border-top-style: dotted;">'+
					'<table class="table">'+
					'<thead><tr><th>Items </th><th>Qty </th><th>Price</th><th>Total</th></tr></thead>'+
					'<tbody>'+ tempRows +'</tbody>'+
					'</table>'+
					'</div>'+
					
					'<div style="margin-top : 10px; margin-bottom : 10px; font-size: 16px;">'+
					'<div class="col-md-12"><div class="col-xs-6">Total Amount</div><div class="col-md-6 text-right">'+ otherData.totalAmt +'</div></div>'+
					'<div class="col-md-12"><div class="col-xs-6">Tax('+ otherData.tax  +')</div><div class="col-md-6 text-right">'+ otherData.totalTax +'</div></div>'+
					'<div class="col-md-12"><div class="col-xs-6">Delivery Charges</div><div class="col-md-6 text-right">'+ otherData.deliveryCharges +'</div></div>'+
					'<div class="col-md-12"><div class="col-xs-6">Grand Total </div><div class="col-md-6 text-right">'+ otherData.grandTotal +'</div></div>'+
					'<div class="col-md-12"><div class="col-xs-6">Discount Total</div><div class="col-md-6 text-right">'+ otherData.discount +'</div></div>'+
					'<div class="col-md-12"><div class="col-xs-6">Yearly Discount</div><div class="col-md-6 text-right">0.00</div></div>'+
					'</div>'+
					
					'<div style="border-bottom: solid 2px;border-top: solid 2px;padding: 10px;border-bottom-style: dotted; border-top-style: dotted;" class="col-md-12"><div class="col-xs-6">Net Total</div><div class="col-md-6 text-right">'+ otherData.netTotal +'</div></div>'+
					'<div class="col-md-12"><div class="col-xs-6">Available Balance</div><div class="col-md-6 text-right">'+ otherData.availBalance +'</div></div>'+
					
					'<h3>Note : Thanks for Visiting.</h3>'+
					'<h3 class="text-right">By : '+ vm.globalCompName +'</h3>'+
					'</div>'+
					'</div>'+
					'</div>'+
					'</body>'+
			'</html>');
			
			popupWin.document.close();
        },

        validationService : function(data){
        	if(data === "null"){
        		return data = "";
        	}else{
        		return data;
        	}
        	if(data === "null"){
        		return data = "";
        	}else{
        		return data;
        	}
        },

        //USING PDFMAKER library
        openPdfService : function(value, column, tdWidth , otherDat){

        	function buildTableBody(data, columns) {
        		var body = [];

        		body.push(columns);

        		data.forEach(function(row) {
        			var dataRow = [row];
        			body.push(row);
        		});

        		return body;
        	}

              function table(data, columns) {
                  return {
                      table: {
                         /* headerRows: 1,*/
                          widths: tdWidth,
                          headerRows: 1,
                          fontSize : 10,
                          body: buildTableBody(data, columns)
                      }
                  };
              }

        	var docDefinition = {
        			pageSize: 'A3',

        			footer: {
        				table: {
        					widths: [ '*', '*', '*'],
        					body: [
        					       [
        					        {
        					        	border: [false, true, false, false],
        					        	//fillColor: '#eeeeee',
        					        	text: 'Date : ' + moment(vm.currentDate).format("DD-MMM-YYYY HH:mm:ss"),
        					        	style : 'tableRowFont', margin :[30, 0 , 0 , 0],
        					        },
        					        {
        					        	border: [false, true, false, false],
        					        	//fillColor: '#eeeeee',
        					        	text: '',
        					        	style : 'tableRowFont'
        					        },
        					        {
        					        	border: [false, true, false, false],
        					        	//fillColor: '#eeeeee',
        					        	text: 'By : Dolphin RFID Pvt.Ltd',
        					        	style : 'tableRowFont', margin :[0, 0 , 35 , 0],
        					        	alignment: 'right'
        					        }
        					        ]
        					       ]
        				}
        			},

        			content: [

        			          { text: otherDat.titleName , style: 'header',alignment: 'center' },
        			          { text: 'Total Records : '+ value.length, alignment: 'right' },
        			          /*{text: 'headerLineOnly:', fontSize: 14, bold: true, margin: [0, 20, 0, 8]},*/
        			          {
        			        	  style: 'tableExample',
        			        	  table: {
        			        		  widths: [250, 230,201,'*'],
        			        		  body: [
        			        		         [
        			        		          {
        			        		        	  border: [false, true, false, false],
        			        		        	  fillColor: '#eeeeee',
        			        		        	  text: 'Generated Day : ' + otherDat.currDay,
        			        		        	  style : 'myTableHeader'
        			        		          },
        			        		          {
        			        		        	  border: [false, true, false, false],
        			        		        	  fillColor: '#eeeeee',
        			        		        	  text: 'Generated Date :' + moment(otherDat.currentDate).format("DD-MMM-YYYY"),
        			        		        	  style : 'myTableHeader'
        			        		          },
        			        		          {
        			        		        	  border: [false, true, false, false],
        			        		        	  fillColor: '#eeeeee',
        			        		        	  text: 'Generated Time :' + moment(vm.currTime).format("HH:mm:ss"),
        			        		        	  style : 'myTableHeader'
        			        		          },
        			        		          {
        			        		        	  border: [false, true, false, true],
        			        		        	  fillColor: '#eeeeee',
        			        		        	  image: otherDat.dolpLogoBase64, width: 40,alignment: 'left',rowSpan: 2,
        			        		        	  style : 'myTableHeader'
        			        		          }
        			        		          ],
        			        		          [
        			        		           {
        			        		        	   border: [false, true, false, true],
        			        		        	   fillColor: '#eeeeee',
        			        		        	   text: 'Company Name : ' + otherDat.globalCompName,
        			        		        	   style : 'myTableHeader'
        			        		           },
        			        		           {
        			        		        	   border: [false, true, false, true],
        			        		        	   fillColor: '#eeeeee',
        			        		        	   text: 'Generated By : ' + otherDat.userName,
        			        		        	   style : 'myTableHeader'
        			        		           },
        			        		           {
        			        		        	   border: [false, true, false, true],
        			        		        	   fillColor: '#eeeeee',
        			        		        	   text: '',
        			        		        	   style : 'myTableHeader'
        			        		           },
        			        		           {
        			        		        	  border: [false, true, false, false],
        			        		        	  fillColor: '#eeeeee',
        			        		        	  text: 'Generated Time :' + moment(otherDat.currTime).format("HH:mm:ss"),
        			        		        	  style : 'myTableHeader'
        			        		          }
        			        		           ]
        			        		         ]
        			        	  },
        			        	  layout: {
        			        		  defaultBorder: false,
        			        	  }
        			          },
        					/*table(myData, [
				               {text : 'Sl.No',style: ['tableHeader','myTableHeader']},
				               {text : 'User Name',style: ['tableHeader','myTableHeader']}
				               ]),*/
        			          table(value, column)
        			          /*table(value, [{text:'Sl.No', style: ['tableHeader','myTableHeader']},
        					        								 {text: 'User Id', style: ['tableHeader','myTableHeader']},
        					        								 {text: 'User Name', style: ['tableHeader','myTableHeader']},
        					        								 {text: 'AssetId', style: ['tableHeader','myTableHeader']},
        					        								 {text: 'Asset Name', style: ['tableHeader','myTableHeader']},
        					        								 {text: 'Category', style: ['tableHeader','myTableHeader']},
        					        								 {text: 'Issue Date Time', style: ['tableHeader','myTableHeader']},
        					        								 {text: 'Receive Date', style: ['tableHeader','myTableHeader']},
        					        								 {text: 'Issued By', style: ['tableHeader','myTableHeader']}
        					        								 ])*/
        					/*table(externalDataRetrievedFromServer, ['slno', 'userId','userName','assetId','assetName','category','issueDatetime','receivedDateTime','issuedBy'])*/

        					/*['slno', 'userId','userName','assetId','assetName','category','issueDatetime','receivedDateTime','issuedBy']*/
        					/*{
        						style: 'tableExample',
        						table: {
        							widths: [ 30, '*', '*','*','*','*','*','*','*'],
        							headerRows: 1,
        							body: [
        								[
        								 {text:'Sl.No', style: ['tableHeader','myTableHeader']},
        								 {text: 'User Id', style: ['tableHeader','myTableHeader']},
        								 {text: 'User Name', style: ['tableHeader','myTableHeader']},
        								 {text: 'AssetId', style: ['tableHeader','myTableHeader']},
        								 {text: 'Asset Name', style: ['tableHeader','myTableHeader']},
        								 {text: 'Category', style: ['tableHeader','myTableHeader']},
        								 {text: 'Issue Date Time', style: ['tableHeader','myTableHeader']},
        								 {text: 'Receive Date', style: ['tableHeader','myTableHeader']},
        								 {text: 'Issued By', style: ['tableHeader','myTableHeader']}
        								 ],
        								 function1()
        								 $rootScope.value
        								 document.write($rootScope.value)
        								 [{text: '1' ,style : 'tableRowFont'}, {text: '456' ,style : 'tableRowFont'}, {text: 'Vikram' ,style : 'tableRowFont'}, {text: '6859' ,style : 'tableRowFont'}, {text: 'Computer' ,style : 'tableRowFont'}, {text: 'ASSET' ,style : 'tableRowFont'}, {text: '28-Feb-2017 14:27:24' ,style : 'tableRowFont'},{text: '03-Mar-2017 15:27:24' ,style : 'tableRowFont'}, {text: 'DEFAULT' ,style : 'tableRowFont'}],
        								 [{text: '1' ,style : 'tableRowFont'}, {text: '456' ,style : 'tableRowFont'}, {text: 'Vikram' ,style : 'tableRowFont'}, {text: '6859' ,style : 'tableRowFont'}, {text: 'Computer' ,style : 'tableRowFont'}, {text: 'ASSET' ,style : 'tableRowFont'}, {text: '28-Feb-2017 14:27:24' ,style : 'tableRowFont'},{text: '03-Mar-2017 15:27:24' ,style : 'tableRowFont'}, {text: 'DEFAULT' ,style : 'tableRowFont'}]
        							]
        						},
        						layout: 'headerLineOnly'
        					},*/
        			   ],

        			   styles: {
        			     header: {
        			       fontSize: 20,
        			       bold: true,
        			       fillColor: '#eeeeee',
        			     },
        			     myTableHeader : {
        			    	fontSize: 10,
        			    	fillColor: '#eeeeee',
        			    	margin: [0, 5, 0, 2],
							alignment: 'left',
        			     },
        			     tableRowFont : {
         			    	fontSize: 10,
         			     },
        			     tableHeader: {
        						bold: true,
        						fontSize: 13,
        						color: 'black',
        						border: [false, false, false, false]
        					}
        			   }
        			 };
        	/*pdfMake.createPdf(docDefinition).download('optionalName.pdf');*/

        	/*var externalDataRetrievedFromServer = [
        	                                       { name: 'Bartek', age: 34 },
        	                                       { name: 'John', age: 27 },
        	                                       { name: 'Elizabeth', age: 30 },
        	                                   ];*/

        	                                 /*  function buildTableBody(data, columns) {
        	                                       var body = [];

        	                                       body.push(columns);

        	                                       data.forEach(function(row) {
        	                                           var dataRow = [];

        	                                           columns.forEach(function(column) {
        	                                               dataRow.push(row[column].toString());
        	                                           })

        	                                           body.push(dataRow);
        	                                       });

        	                                       return body;
        	                                   }

        	                                   function table(data, columns) {
        	                                       return {
        	                                           table: {
        	                                               headerRows: 1,
        	                                               body: buildTableBody(data, columns)
        	                                           }
        	                                       };
        	                                   }

        	                                   var dd = {
        	                                       content: [
        	                                           { text: 'Dynamic parts', style: 'header' },
        	                                           table(externalDataRetrievedFromServer, ['name', 'age'])
        	                                       ]
        	                                   }*/

        	/*pdfMake.createPdf(docDefinition).download("report.pdf");*/
        	pdfMake.createPdf(docDefinition).open();

        },

        //USING PDFMAKER library
        pickupDropPdfService : function(column, value, tdWidth, otherDat,column1, value1){

        	function buildTableBody(data, columns) {
        		var body = [];

        		body.push(columns);

        		data.forEach(function(row) {
        			var dataRow = [row];
        			body.push(row);
        		});

        		return body;
        	};

        	function table(data, columns) {
        		return {
        			table: {
        				/* headerRows: 1,*/
        				widths: tdWidth,
        				headerRows: 1,
        				fontSize : 10,
        				body: buildTableBody(data, columns)
        			}
        		};
        	}

        	var docDefinition = {
        			pageSize: 'A3',
        			//pageOrientation: 'landscape',
        			titleName : "Vikram",
        			footer: {
        				table: {
        					widths: [ '*', '*', '*'],
        					body: [
        					       [
        					        {
        					        	border: [false, true, false, false],
        					        	//fillColor: '#eeeeee',
        					        	text: 'Date : ' + moment(vm.currentDate).format("DD-MMM-YYYY HH:mm:ss"),
        					        	style : 'tableRowFont', margin :[30, 0 , 0 , 0],
        					        },
        					        {
        					        	border: [false, true, false, false],
        					        	//fillColor: '#eeeeee',
        					        	text: '',
        					        	style : 'tableRowFont'
        					        },
        					        {
        					        	border: [false, true, false, false],
        					        	//fillColor: '#eeeeee',
        					        	text: 'By : Dolphin RFID Pvt.Ltd',
        					        	style : 'tableRowFont', margin :[0, 0 , 35 , 0],
        					        	alignment: 'right'
        					        }]]
        				}
        			},

        			content: [{ text: otherDat.titleName , style: 'header',alignment: 'center' },
        			          {
        				//style: 'tableExample',
        				table: {
        					widths: ['*','*','*'],
        					body: [
        					       [
        					        {
        					        	border: [false, true, false, false],
        					        	fillColor: '#eeeeee',
        			        		        	  text: 'Generated Day : ' + otherDat.currDay,
        			        		        	  style : 'myTableHeader'
        			        		          },
        			        		          {
        			        		        	  border: [false, true, false, false],
        			        		        	  fillColor: '#eeeeee',
        			        		        	  text: 'Generated Date :' + moment(otherDat.currentDate).format("DD-MMM-YYYY"),
        			        		        	  style : 'myTableHeader'
        			        		          },
        			        		          {
        			        		        	  border: [false, true, false, false],
        			        		        	  fillColor: '#eeeeee',
        			        		        	  text: 'Generated Time :' + moment(vm.currTime).format("HH:mm:ss"),
        			        		        	  style : 'myTableHeader'
        			        		          }
        			        		          ],
        			        		          [
        			        		           {
        			        		        	   border: [false, true, false, true],
        			        		        	   fillColor: '#eeeeee',
        			        		        	   text: 'JourneyId : ' + otherDat.journeyId,
        			        		        	   style : 'myTableHeader'
        			        		           },
        			        		           {
        			        		        	   border: [false, true, false, true],
        			        		        	   fillColor: '#eeeeee',
        			        		        	   text: 'Generated By : ' + otherDat.userName,
        			        		        	   style : 'myTableHeader'
        			        		           },
        			        		           {
        			        		        	   border: [false, true, false, true],
        			        		        	   fillColor: '#eeeeee',
        			        		        	   text: 'Operator :' + otherDat.operatorName,
        			        		        	   style : 'myTableHeader'
        			        		           }
        			        		           ]
        			        		         ]
        			        	  },
        			        	  layout: {
        			        		  defaultBorder: false,
        			        	  }
        			          },
        			          {
								text : 'Journey Report Screenshot', alignment: 'center',
								fontSize : 15,
								margin: [15, 0, 0, 0],
        			          },
        			          {
  			  					image: otherDat.base64img,
  			  					width:760,
  			  					height:650,
  			  					margin: [0, 10, 10, 10],
        			          },
        			          {
  								text : 'Journey Report', alignment: 'left',
  								fontSize : 15,
  								margin: [15, 0, 15, 10],
          			          },
          			          {
          			        	  alignment: 'justify',
          			        	  pageBreak: "after",
          			        	  ol: [
          			        	       'Journey Report Id : ' + otherDat.journeyId,
          			        	       'Route : '+ otherDat.route,
          			        	       'No of locations in the routes : ' + otherDat.noOfLocations,
          			        	       'Journey Start Date : ' + otherDat.jStartDate,
          			        	       'Journey End Date : '+otherDat.jEndDate,
          			        	       'Journey Operator : ' + otherDat.operatorName,
          			        	       'Users Picked Up in Journey : '+ otherDat.userPickupInJrny,
          			        	       'Users Dropped in Journey :' + otherDat.userDroppedInJrny
          			        	       ]
          			          }/*,
          			          {
        			  			alignment: 'justify',
        			  			pageBreak: "after",
        			  			columns: [
        			  				[
     								{
     									text : 'Journey Report Screenshot',
     									fontSize : 15,
     									margin: [15, 10, 10, 10],
     								},
     								{
     									columns: [
     										 {
        			  					image: otherDat.base64img,
        			  					width:300,
        			  					margin: [0, 10, 10, 10],
        			  				}]
     								}
     							],
        			  				[{
        			  					text : 'Journey Report',
        			  					fontSize : 15,
        			  					margin: [15, 10, 10, 10],
        			  				},
        			  				{
        			  					margin: [15, 10, 10, 10],
        			  					columns: [
        			  					          { text: 'Journey Report Id : ' + otherDat.journeyId +
        			  					        	  '\n\n Route : '+ otherDat.route +
        			  					        	  '\n\n No of locations in the routes : ' + otherDat.noOfLocations +
        			  					        	  '\n\n Journey Start Date : ' + otherDat.jStartDate +
        			  					        	  '\n\n Journey End Date : '+otherDat.jEndDate+
        			  					        	  '\n\n Journey Operator : ' + otherDat.operatorName +
        			  					        	  '\n\n Users Picked Up in Journey : '+ otherDat.userPickupInJrny +
        			  					        	  '\n\n Users Dropped in Journey :' + otherDat.userDroppedInJrny },
        			  					          ]
        			  				}]

        			  			]
        			          }*/,
        			          /*{ text: "Total Picked Up Users :" + 1 , style : 'tableHe', alignment: 'left' },
        			          table(value, column),*/
        			          { text: "Total Picked Up Users :" + value.length , style : 'tableHe', alignment: 'center' },
        			          {
           						style: 'tableExample',
           						table: {
           							widths: [ 30, '*', '*','*','*','*',35],
           							headerRows: 1,
           							body: buildTableBody(value, column)
           						},
           						//layout: 'headerLineOnly'
        			          },
        			          { text: "Total Dropped Users :" + value1.length , style : 'tableHe', alignment: 'center' },
        			          {
        			        	  style: 'tableExample',
        			        	  table: {
        			        		  widths: [ 30, '*', '*','*','*','*',35],
        			        		  headerRows: 1,
        			        		  body: buildTableBody(value1, column1)
        			        	  },
        			        	  //layout: 'headerLineOnly'
        			   	      }
        				       //  table(value1, column1)
        			          ],

        			          styles: {
        				   tableExample: {
        						margin: [30, 5, 30, 5],
        				   },
        				   header: {
        					   fontSize: 20,
        					   bold: true,
        					   fillColor: '#eeeeee',
        				   },
        				   myTableHeader : {
        					   fontSize: 10,
        					   fillColor: '#eeeeee',
        					   margin: [0, 5, 0, 2],
        					   alignment: 'left',
        				   },
        				   tableHe : {
        					   fontSize: 13,
        					   fillColor: '#eeeeee',
        					   margin: [0, 10, 0, 2],
        					   alignment: 'left',
        				   },
        				   tableRowFont : {
        					   fontSize: 10,
        				   },
        				   tableHeader: {
        					   bold: true,
        					   fontSize: 13,
        					   color: 'black',
        					   border: [false, false, false, false]
        				   },
        				   reportAlign : {
        					   margin: [0, 10, 0, 2],
        				   }
        			   }
        	};

        	/*pdfMake.createPdf(docDefinition).download("report.pdf");*/
        	pdfMake.createPdf(docDefinition).open();
        },
        
        generateReportPdf : function(crValue, crColumn,drValue, drColumn, tdWidth , otherDat, invoiceVal, invoiceColumn, invoiceTblwidth){
        	
        	function buildTableBody(data, columns) {
        		var body = [];			
        		body.push(columns);
        		data.forEach(function(row) {
        			var dataRow = [row];
        			body.push(row);
        		});
        		return body;
        	}

        	function table(data, columns) {
        		return {
        			table: {
        				/* headerRows: 1,*/
        				widths: tdWidth,
        				headerRows: 1,
        				fontSize : 10,
        				body: buildTableBody(data, columns)
        			}
        		};
        	}
              
        	function buildInvoiceTableBody(data, columns) {
        		var body = [];
          		body.push(columns);
          		data.forEach(function(row) {
          			var dataRow = [row];
          			body.push(row);
          		});
          		return body;
        	}

        	var docDefinition = {
        			pageSize: 'A3',
        			
        			footer: {
        				table: {
        					widths: [ '*', '*', '*'],
        					body: [
        					       [{
        					    	   border: [false, true, false, false],
        					    	   //fillColor: '#eeeeee',
        					    	   text: 'Date : ' + moment(otherDat.currentDate).format("DD-MM-YYYY") ,
        					    	   style : 'tableRowFont', margin :[30, 0 , 0 , 0],
        					       },{
        					    	   border: [false, true, false, false],
        					    	   //fillColor: '#eeeeee',
        					    	   text: '',
        					    	   style : 'tableRowFont'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   //fillColor: '#eeeeee',
        					    	   text: 'By : '  + otherDat.globalCompName ,
        					    	   style : 'tableRowFont', margin :[0, 0 , 35 , 0],
        					    	   alignment: 'right'
        					       }]
        					       ]
        				}
        			},

        			content: [{ text: otherDat.titleName , style: 'header',alignment: 'center' },
        			          //{ text: 'Total Records : 3333', alignment: 'right' },
        			          /*{text: 'headerLineOnly:', fontSize: 14, bold: true, margin: [0, 20, 0, 8]},*/
        			          {
        				style: 'tableExample',
        				table: {
        					widths: [250, 230,201,'*'],
        					body: [
        					       [{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Day : ' + otherDat.currDay,
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Date : '+  moment(otherDat.currentDate).format("DD-MM-YYYY"),
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Time : ' + moment(otherDat.currTime).format("HH:mm:ss"),
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   image: otherDat.dolpLogoBase64, width: 40,alignment: 'left',rowSpan: 2,
        					    	   style : 'myTableHeader'
        					       }],
        					       [{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Company Name : ' + otherDat.globalCompName,
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated By : ' + otherDat.userName,
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   text: '',
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Time :dddddddd',
        					    	   style : 'myTableHeader'
        					       }]
        					       ]
        				},
        				layout: {
        					defaultBorder: false,
        				}
        			          },{
        			        	  style: 'tableExample',
        			        	  margin:[0, 10, 0, 0],
        			        	  table: {
        			  				headerRows: 1,
        			  				widths: ['*','*','*'],
        			  				body: [
        			  				       [{text: 'User Id : ' + otherDat.userInfo.userId , style: 'tableHeader, myTableHeader'}, {text: 'User Name : ' +  otherDat.userInfo.userName , style: 'tableHeader, myTableHeader'}, {text: 'EPCID : ' +  otherDat.userInfo.tagId, style: 'tableHeader, myTableHeader'}],
        			  				       [{text: 'Current Balance : '+ "₹" + otherDat.currentBalance, style: 'tableHeader, myTableHeader'}, {text: 'Total CR : '+ "₹" + otherDat.crTotalValue, style: 'tableHeader, myTableHeader'}, {text: 'Total DR : '+ "₹" + otherDat.drTotalValue, style: 'tableHeader, myTableHeader'}]
        			  				       ]
        			        	  },
        			        	  //layout: 'noBorders'
        			          },
        			          { text: 'CR Details:', alignment: 'left', margin:[0, 10, 0 , 2] },
        			          //display CR Details
        			          table(crValue, crColumn),
        			          
        			          //display DR Details
        			          { text: 'DR Details:', alignment: 'left', margin:[0, 10, 0 , 2] },
        			          table(drValue, drColumn),
        			         
        			          //display invoice details
        			          { text: 'Invoice Details:', alignment: 'left', margin:[0, 10, 0 , 2] },
        			          {
        			        	  style: 'tableExample',
        			        	  table: {
        			        		  fontSize : 10,
        			        		  widths: invoiceTblwidth,
        			        		  headerRows: 1,
        			        		  body: buildInvoiceTableBody(invoiceVal, invoiceColumn)
        			        	  },
        			          }],

        			          styles: {
        			        	  header: {
        			        		  fontSize: 20,
        			        		  bold: true,
        			        		  fillColor: '#eeeeee',
        			        	  },
        			        	  myTableHeader : {
        			        		  fontSize: 10,
        			        		  fillColor: '#eeeeee',
        			        		  margin: [0, 5, 0, 2],
        			        		  alignment: 'left',
        			        	  },
        			        	  tableRowFont : {
        			        		  fontSize: 10,
        			        	  },
        			        	  tableHeader: {
        			        		  bold: true,
        			        		  fontSize: 13,
        			        		  color: 'black',
        			        		  border: [false, false, false, false]
        			        	  }
        			          }
        	};
        	
        	/*pdfMake.createPdf(docDefinition).download("report.pdf");*/
        	pdfMake.createPdf(docDefinition).open();

        }, 	
        
        posSettlementReport : function(userTableDetails, crDetails, drDetails, otherDat, mainUserDetails){
 			
        	var docDefinition = {
        			pageSize: 'A3',
        			footer: {
        				table: {
        					widths: [ '*', '*', '*'],
        					body: [
        					       [{
        					    	   border: [false, true, false, false],
        					    	   //fillColor: '#eeeeee',
        					    	   text: 'Date : ' + moment(otherDat.currentDate).format("DD-MM-YYYY") ,
        					    	   style : 'tableRowFont', margin :[30, 0 , 0 , 0],
        					       },{
        					    	   border: [false, true, false, false],
        					    	   //fillColor: '#eeeeee',
        					    	   text: '',
        					    	   style : 'tableRowFont'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   //fillColor: '#eeeeee',
        					    	   text: 'By : '  + otherDat.globalCompName ,
        					    	   style : 'tableRowFont', margin :[0, 0 , 35 , 0],
        					    	   alignment: 'right'
        					       }]
        					       ]
        				}
        			},

        			content: [{ text: otherDat.titleName , style: 'header',alignment: 'center' },
        			          {
        				style: 'tableExample',
        				table: {
        					widths: [250, 230,201,'*'],
        					body: [
        					       [{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Day : ' + otherDat.currDay,
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Date : '+  moment(otherDat.currentDate).format("DD-MM-YYYY"),
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Time : ' + moment(otherDat.currTime).format("HH:mm:ss"),
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   image: otherDat.dolpLogoBase64, width: 40,alignment: 'left',rowSpan: 2,
        					    	   style : 'myTableHeader'
        					       }],
        					       [{
        					    	   border: [false, true, false, true],
        					    	   margin : [0, 5, 0, 5],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Company Name : ' + otherDat.globalCompName,
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated By : ' + otherDat.userName,
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   text: '',
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Time :dddddddd',
        					    	   style : 'myTableHeader'
        					       }]
        					       ]
        				},
        				layout: {
        					defaultBorder: false,
        				}
        			    },{
        			    	style: 'tableExample',
            				table: {
            					widths: ['*','*',50,'*'],
            					body: [
            					       [{
            					    	   border: [false, true, false, false],
            					    	   fillColor: '#eeeeee',
            					    	   text: 'Booking Id :' + mainUserDetails.empId,
            					    	   style : 'myTableHeader'
            					       },{
            					    	   border: [false, true, false, false],
            					    	   fillColor: '#eeeeee',
            					    	   text: 'Created Date :' + mainUserDetails.createdDate,
            					    	   style : 'myTableHeader'
            					       },{
            					    	   border: [false, true, false, false],
            					    	   fillColor: '#eeeeee',
            					    	   text: 'for',
            					    	   style : 'myTableHeader'
            					       },{
            					    	   border: [false, true, false, false],
            					    	   fillColor: '#eeeeee',
            					    	   text:  mainUserDetails.activationDate + ' to ' + mainUserDetails.deactivationDate,
            					    	   style : 'myTableHeader'
            					       }]
            					       ]
            				},
            				layout: {
            					defaultBorder: false,
            				}
            			    },
            			    
            			    /*======$ display user summary $=======*/
            			    { text: 'User Details:', alignment: 'left', margin:[0, 10, 0 , 2] },
            			    {
            			    	style: 'tableExample',
            			    	table: {
            			    		fontSize : 10,
            			    		widths: userTableDetails.userTblDataWidth,
            			    		headerRows: 1,
            			    		body: buildUserDetailsBody(userTableDetails.userTableDataValue, userTableDetails.userTblDataClm)
            			    	},
            			    },
            			    {
            			    	style: 'tableExample',
            			    	table: {
            			    		fontSize : 10,
            			    		widths: userTableDetails.userTblDataWidth,
            			    		headerRows: 1,
            			    		body: [
            			    		       [{text: '', style: 'tableHeader, myTableHeader'},
            			    		        {text: '' , style: 'tableHeader, myTableHeader'},
        			    			        {text: '', style: 'tableHeader, myTableHeader'},
        			    			        {text: "₹" +  userTableDetails.currentBalTotal , style: 'tableHeader, myTableHeader'},
        			    			        {text: "₹" +  userTableDetails.totalCrAmount , style: 'tableHeader, myTableHeader'},
        			    			        {text: "₹" +  userTableDetails.totalDrAmount , style: 'tableHeader, myTableHeader'}]
            			    		       ]
            			    	},
            			    },
            			    
            			    /*======$ display cr details $=======*/
            			    { text: 'CR Details:', alignment: 'left', margin:[0, 10, 0 , 2] },
            			    {
            			    	style: 'tableExample',
            			    	table: {
            			    		fontSize : 10,
            			    		widths: crDetails.crTblWidth,
            			    		headerRows: 1,
            			    		body: buildCrTableBody(crDetails.crTblValue, crDetails.crTblclm)
            			    	},
            			    },
            			    {
            			    	style: 'tableExample',
            			    	table: {
            			    		fontSize : 10,
            			    		widths: crDetails.crTblWidth,
            			    		headerRows: 1,
            			    		body: [
            			    		       [{text: '', style: 'tableHeader, myTableHeader'},
        			    			        {text: '', style: 'tableHeader, myTableHeader'},
        			    			        {text: '', style: 'tableHeader, myTableHeader'},
        			    			        {text: "₹" + crDetails.crAmtTotal , style: 'tableHeader, myTableHeader'}]
            			    		       ]
            			    	},
            			    },
            			    
            			    /*======$ display dr details $=======*/
            			    { text: 'DR Details:', alignment: 'left', margin:[0, 10, 0 , 2] },
            			    {
            			    	style: 'tableExample',
            			    	table: {
            			    		fontSize : 10,
            			    		widths: drDetails.drTblWidth,
            			    		headerRows: 1,
            			    		body: buildDrTableBody(drDetails.drTblValue, drDetails.drTblclm)
            			    	},
            			    },
            			    {
            			    	style: 'tableExample',
            			    	table: {
            			    		fontSize : 10,
            			    		widths: drDetails.drTblWidth,
            			    		headerRows: 1,
            			    		body: [
            			    		       [{text: '', style: 'tableHeader, myTableHeader'},
            			    		        {text: '' , style: 'tableHeader, myTableHeader'},
        			    			        {text: '', style: 'tableHeader, myTableHeader'},
        			    			        {text: '', style: 'tableHeader, myTableHeader'},
        			    			        {text: '', style: 'tableHeader, myTableHeader'},
        			    			        {text: '', style: 'tableHeader, myTableHeader'},
        			    			        {text: "₹" + drDetails.drAmtTotal , style: 'tableHeader, myTableHeader'}]
            			    		       ]
            			    	},
            			    },
            			  
            			    ],
            			    /*End content block*/
            			    
            			    styles: {
            			    	header: {
            			    		fontSize: 20,
            			    		bold: true,
            			    		fillColor: '#eeeeee',
            			    	},
            			    	myTableHeader : {
            			    		fontSize: 10,
            			    		fillColor: '#eeeeee',
            			    		margin: [0, 5, 0, 2],
            			    		alignment: 'left',
            			    	},
            			    	tableRowFont : {
            			    		fontSize: 10,
            			    	},
        			        	  tableHeader: {
        			        		  bold: true,
        			        		  fontSize: 13,
        			        		  color: 'black',
        			        		  border: [false, false, false, false]
        			        	  }
        			          }
        	};

        	//Create user summary body
        	function buildUserDetailsBody(data, columns) {
        		var body = [];
          		body.push(columns);
          		data.forEach(function(row) {
          			var dataRow = [row];
          			body.push(row);
          		});
          		return body;
        	}
        	
        	function buildCrTableBody(data, columns){
        		var body = [];
          		body.push(columns);
          		data.forEach(function(row) {
          			var dataRow = [row];
          			body.push(row);
          		});
          		return body;
        	}
        	
        	function buildDrTableBody(data, columns){
        		var body = [];
          		body.push(columns);
          		data.forEach(function(row) {
          			var dataRow = [row];
          			body.push(row);
          		});
          		return body;
        	}
        	
        	pdfMake.createPdf(docDefinition).open();
        },
        
        posUserDetailsReport : function(userTblWidth, userTblVal, userTblClm, otherDat){
        	var docDefinition = {
        			pageSize: 'A3',
        			footer: {
        				table: {
        					widths: [ '*', '*', '*'],
        					body: [
        					       [{
        					    	   border: [false, true, false, false],
        					    	   //fillColor: '#eeeeee',
        					    	   text: 'Date : ' + moment(otherDat.currentDate).format("DD-MM-YYYY") ,
        					    	   style : 'tableRowFont', margin :[30, 0 , 0 , 0],
        					       },{
        					    	   border: [false, true, false, false],
        					    	   //fillColor: '#eeeeee',
        					    	   text: '',
        					    	   style : 'tableRowFont'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   //fillColor: '#eeeeee',
        					    	   text: 'By : '  + otherDat.globalCompName ,
        					    	   style : 'tableRowFont', margin :[0, 0 , 35 , 0],
        					    	   alignment: 'right'
        					       }]
        					       ]
        				}
        			},

        			content: [{ text: otherDat.titleName , style: 'header',alignment: 'center' },
        			          {
        				style: 'tableExample',
        				table: {
        					widths: [250, 230,201,'*'],
        					body: [
        					       [{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Day : ' + otherDat.currDay,
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Date : '+  moment(otherDat.currentDate).format("DD-MM-YYYY"),
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Time : ' + moment(otherDat.currTime).format("HH:mm:ss"),
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   image: otherDat.dolpLogoBase64, width: 40,alignment: 'left',rowSpan: 2,
        					    	   style : 'myTableHeader'
        					       }],
        					       [{
        					    	   border: [false, true, false, true],
        					    	   margin : [0, 5, 0, 5],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Company Name : ' + otherDat.globalCompName,
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated By : ' + otherDat.userName,
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, true],
        					    	   fillColor: '#eeeeee',
        					    	   text: '',
        					    	   style : 'myTableHeader'
        					       },{
        					    	   border: [false, true, false, false],
        					    	   fillColor: '#eeeeee',
        					    	   text: 'Generated Time :dddddddd',
        					    	   style : 'myTableHeader'
        					       }]
        					       ]
        				},
        				layout: {
        					defaultBorder: false,
        				}
        			    },
            			    
        			    /*======$ display user summary $=======*/
        			    { text: 'User Details:', alignment: 'left', margin:[0, 10, 0 , 2] },
        			    {
        			    	style: 'tableExample',
        			    	table: {
        			    		fontSize : 10,
        			    		widths: userTblWidth,
        			    		headerRows: 1,
        			    		body: buildUserDetailsBody(userTblVal, userTblClm)
        			    	},
        			    },
        			    {
        			    	style: 'tableExample',
        			    	table: {
        			    		fontSize : 10,
        			    		widths: userTblWidth,
        			    		headerRows: 1,
        			    		body: [
        			    		       [{text: '', style: 'tableHeader, myTableHeader'},
        			    		        {text: '', style: 'tableHeader, myTableHeader'},
        			    		        {text: '' , style: 'tableHeader, myTableHeader'},
    			    			        {text: '₹' + otherDat.totalCrCount, style: 'tableHeader, myTableHeader'},
    			    			        {text: '₹' + otherDat.totalParkCount, style: 'tableHeader, myTableHeader'},
    			    			        {text: '₹' + otherDat.totalPosCount, style: 'tableHeader, myTableHeader'},
    			    			        {text: '₹' + otherDat.totalCableCartCount, style: 'tableHeader, myTableHeader'},
    			    			        {text: '₹' + otherDat.totalCurreBalCount, style: 'tableHeader, myTableHeader'}]
        			    		       ]
        			    	},
        			    }],
        			    /*End content block*/
        			    
        			    styles: {
        			    	header: {
        			    		fontSize: 20,
        			    		bold: true,
        			    		fillColor: '#eeeeee',
        			    	},
        			    	myTableHeader : {
        			    		fontSize: 10,
        			    		fillColor: '#eeeeee',
        			    		margin: [0, 5, 0, 2],
        			    		alignment: 'left',
        			    	},
        			    	tableRowFont : {
        			    		fontSize: 10,
        			    	},
        			    	tableHeader: {
        			    		bold: true,
        			    		fontSize: 13,
        			    		color: 'black',
        			    		border: [false, false, false, false]
        			    	}
        			    }
        	};

        	//Create user summary body
        	function buildUserDetailsBody(data, columns) {
        		var body = [];
          		body.push(columns);
          		data.forEach(function(row) {
          			var dataRow = [row];
          			body.push(row);
          		});
          		return body;
        	}
        	
        	
        	pdfMake.createPdf(docDefinition).open();
        },
       
        
      };
    }])
})();

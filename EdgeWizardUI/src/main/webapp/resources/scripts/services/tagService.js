(function(){
  'use strict';
  angular.module('rfidDemoApp.services')
  .service('TagService',['HTTP','$http',function(HTTP, $http){
    return{
      tagSearchDetails : function(requestParams, successCallback, errorCallback){
          $http({
            method : 'POST',
            url : HTTP + '/op0014.23',
            data :requestParams,
            headers : {
              'Content-Type' : 'application/json'
            }
          })
          .then(function onSuccess(response){
            successCallback(response);
          },function onError(response){
            errorCallback(response);
          })
      }

    }
  }]);
})();

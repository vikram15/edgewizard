(function() {
    'use strict';
    angular.module('rfidDemoApp.services')
        .service('ReaderServices', ['$http', 'HTTP', function($http, HTTP) {
            return {
                getAllreadersList: function(requestParams, successCallback, errorCallback) {
                    $http({
                            method: 'POST',
                            url: HTTP + '/op1003.46',
                            data: requestParams,
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                        .then(function onSuccess(response) {
                                successCallback(response);
                            },
                            function onError(response) {
                                errorCallback(response);
                            });
                },

                getDockDoorReaders: function(requestParams, successCallback, errorCallback) {
                    $http({
                            method: 'POST',
                            url: HTTP + '/op0018.1',
                            data: requestParams,
                            headers: {
                                'Content-Type': 'application/json'
                            }
                        })
                        .then(function onSuccess(response) {
                                successCallback(response);
                            },
                            function onError(response) {
                                errorCallback(response);
                            });
                },

                getAllAreaList: function(requestParams, successCallback, errorCallback) {
                    $http({
                        'method': 'POST',
                        url: HTTP + '/op1007.13',
                        data: requestParams,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(function onSuccess(response) {
                        successCallback(response);
                    }, function onError(response) {
                        errorCallback(response);
                    });
                },

                addAreaDetails: function(requestParams, successCallback, errorCallback) {
                    $http({
                        'method': 'POST',
                        url: HTTP + '/op1007.2',
                        data: requestParams,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(function onSuccess(response) {
                        successCallback(response);
                    }, function onError(response) {
                        errorCallback(response);
                    });
                },

                updateAreaDetails: function(requestParams, successCallback, errorCallback) {
                    $http({
                        'method': 'POST',
                        url: HTTP + '/op1007.3',
                        data: requestParams,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(function onSuccess(response) {
                        successCallback(response);
                    }, function onError(response) {
                        errorCallback(response);
                    });
                },

                deleteAreaDetails: function(requestParams, successCallback, errorCallback) {
                    $http({
                        'method': 'POST',
                        url: HTTP + '/op1007.4',
                        data: requestParams,
                        headers: {
                            'Content-Type': 'application/json'
                        }
                    }).then(function onSuccess(response) {
                        successCallback(response);
                    }, function onError(response) {
                        errorCallback(response);
                    });
                },

                getAllZoneDetails: function(requestParams, successCallback, errorCallback) {
                    $http({
                    	method : 'POST',
                    	url : HTTP + '/op1008.13',
                    	data : requestParams,
                    	headers : {
                    		'Content-Type' : 'application/json'
                    	}
                    }).then(function onSuccess(response){
                    	successCallback(response);
                    }, function onError(response){
                    	errorCallback(response);
                    })
                },

				addZoneDetails : function(requestParams, successCallback, errorCallback){
					$http({
						method : 'POST',
						url : HTTP + '/op1008.2',
						data : requestParams,
						headers : {
							'Content-Type' : 'application/json'
						}
					}).then(function onSuccess(response) {
						successCallback(response);
					}, function onError(response) {
						errorCallback(response);
					})
				},
                
            }
        }]);
})();

(function(){
  'use strict';
  angular.module('rfidDemoApp.services')
  .service('PickUpDropService',['$http','HTTP', function($http, HTTP){
    return {
      //Get Device list
      getDeviceList : function(successCallBack, errorCallBack){
        $http({
          method : 'POST',
          url : HTTP + '/op1004.14',
          data : {},
          headers : {
              'Content-Type' : 'application/json'
          }
        }).then(function onSuccess(response){
          successCallBack(response);
        },function (response){
          errorCallBack(response);
        });
      },

      //Get Journey List
      getJourneyListByDeviceId : function(deviceId, successCallBack, errorCallBack){
        $http({
          method : 'POST',
          url : HTTP + '/op1004.1',
          data : deviceId,
          headers : {
		          'Content-Type' : 'application/json'
			    }
        }).then(function onSuccess(response){
          successCallBack(response);
        },function (response){
          errorCallBack(response);
        });
      },

      //Get Lat and Long of Ideal Route
      getRouteDetails : function(routeId, successCallBack, errorCallBack){
        $http({
          method : 'POST',
          url : HTTP + '/op1004.11',
          data : routeId,
          headers : {
		          'Content-Type' : 'application/json'
			    }
        }).then(function onSuccess(response){
          successCallBack(response);
        },function (response){
          errorCallBack(response);
        });
      },
      
    //Get Lat and Long of Actual Route
      getActualRouteDetails : function(journeyId, successCallBack, errorCallBack){
    	  $http({
    		  method : 'POST',
    		  url : HTTP + '/op1004.12',
    		  data : journeyId,
              headers : {
            	  'Content-Type' : 'application/json'
              }
    	  }).then(function onSuccess(response){
    		  successCallBack(response);
    	  },function (response){
    		  errorCallBack(response);
    	  });  
      },
      
      getPickupDropList : function(requestParams, successCallBack, errorCallBack){
    	$http({
    		method : 'POST',
    		url : HTTP + '/op1004.13',
    		data : requestParams,
    		headers : {
    			'Content-Type' : 'application/json'
    		}
    	}).then(function onSuccess(response){
    		successCallBack(response);
    	}, function onError(response){
    		errorCallBack(response);
    	}); 
      },
      
      getDeviceInfo : function(deviceId, successCallBack, errorCallBack){
    	$http({
    		method : 'POST',
    		url : HTTP + '/op1004.15',
    		data : deviceId,
    		headers : {
    			'Content-Type' : 'application/json'
    		}
    	}).then(function onSuccess(response){
    		successCallBack(response);
    	}, function onError(response){
    		errorCallBack(response);
    	});  
      },
      


    }
  }])
})();

(function(){
  'use strict';
  angular.module('rfidDemoApp.services')
  .factory('IssueReceiveService', ['$http','HTTP','ASSET_ISSUED_TO_USER','ASSET_RECEIVE_FROM_USER','ASSET_ISSUE_USER_READ_TAG','ASSET_ISSUE_ASSET_READ_TAG','GET_USER_DETAIL_FIELDS','GET_GRDETAILS','PRINT_OPTION',
      function($http,HTTP, ASSET_ISSUED_TO_USER, ASSET_RECEIVE_FROM_USER, ASSET_ISSUE_USER_READ_TAG, ASSET_ISSUE_ASSET_READ_TAG, GET_USER_DETAIL_FIELDS, GET_GRDETAILS, PRINT_OPTION ) {
	  return {
		  addAssetToUser: function(requestParams,successCallback, errorCallback) {
			  $http({
				  method: 'POST',
				  url: HTTP + ASSET_ISSUED_TO_USER,
				  data: requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
  	       })
  	       .then(function onSuccess(response) {
  	    	   successCallback(response);
  	       },
  	       function onError(response) {
  	    	   errorCallback(response);
  	       });
		  },
		  
		  receiveAssetFromUser: function(requestParams,successCallback, errorCallback) {
			  $http({
				  method: 'POST',
				  url: HTTP + ASSET_RECEIVE_FROM_USER,
				  data: requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
  	       })
  	       .then(function onSuccess(response) {
  	    	   successCallback(response);
  	       },
  	       function onError(response) {
  	    	   errorCallback(response);
  	       });
		  },
		  
		  issueAssetReadTag: function(requestParams,successCallback, errorCallback) {
			  $http({
				  method: 'POST',
				  url: HTTP + ASSET_ISSUE_ASSET_READ_TAG,
				  data: requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
  	       })
  	       .then(function onSuccess(response) {
  	    	   successCallback(response);
  	       },
  	       function onError(response) {
  	    	   errorCallback(response);
  	       });
		  },
		  
		  issueUserReadTag: function(requestParams,successCallback, errorCallback) {
			  $http({
				  method: 'POST',
				  url: HTTP + ASSET_ISSUE_USER_READ_TAG,
				  data: requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
			  })
			  .then(function onSuccess(response) {
				  successCallback(response);
			  },
			  function onError(response) {
				  errorCallback(response);
			  });
		  },
		  
		  getUserDetailFields: function(requestParams,successCallback, errorCallback){
			  $http({
				  method: 'POST',
				  url: HTTP + GET_USER_DETAIL_FIELDS,
				  data: requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
			  })
			  .then(function onSuccess(response) {
				  successCallback(response);
			  },
			  function onError(response) {
				  errorCallback(response);
			  });
		  },
		  
		  getGRSettingDetails : function(successCallback, errorCallback){
			  $http({
				  method: 'POST',
				  url: HTTP + GET_GRDETAILS,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
			  })
			  .then(function onSuccess(response) {
  	    	   successCallback(response);
			  },
			  function onError(response) {
				  errorCallback(response);
			  });
		  },
		  
		  updatePrintOption : function(requestParams, successCallback, errorCallback){
			  $http({
				  method: 'POST',
				  url: HTTP + PRINT_OPTION,
				  data : requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
			  })
			  .then(function onSuccess(response) {
  	    	   successCallback(response);
			  },
			  function onError(response) {
				  errorCallback(response);
			  });
		  }
		  
	  };
	 }]);
})();
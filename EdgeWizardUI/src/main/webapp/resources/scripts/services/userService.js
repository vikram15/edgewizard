(function(){
  'use strict';
  angular.module('rfidDemoApp.services')
  .service('UserService', ['$http','HTTP','GET_USER_ISSUED_ASSET', function($http, HTTP, GET_USER_ISSUED_ASSET){
    return {
    	/*Issue recieve user search*/
    	getUserSearchData : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0048.1',
    			data : requestParams,
    			headers : {
    				'Content-Type' : 'application/json'
    			}
    		})
	        .then(function onSuccess(response){
	          successCallback(response);
	        },function onError(response){
	          errorCallback(response);
	        })
    	},
	      
    	//Get user by userId
    	getUserDetails : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0013.1',
    			data : requestParams,
    			headers : {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response) {
    			errorCallback(response);
    		}) 
    	},
    	
    	getUserIssuedAsset: function(requestParams, successCallback, errorCallback){
			 $http({
				 method:'POST',
				 url: HTTP + GET_USER_ISSUED_ASSET,
				 data: requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
			 })
			 .then(function onSuccess(response) {
	  	    	   successCallback(response);
			 },
			 function onError(response) {
				 errorCallback(response);
			 });
    	},
		
    	//Add user
    	userAdd : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0013.2',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	//Get All users list with search options and pagination
    	getUserList : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0013.23',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	//Delete user
    	deleteUserList : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0013.4',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	userUpdate : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0013.3',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	getAllTimeZones : function(companyId, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0012.13',
    			data : companyId,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	getTimeZoneDetail : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0012.1',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	userReleaseTag : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0013.28',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	baggageAdd : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0013.21',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	//For Transaction Setting page
    	userTransactionSetting : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op1006.2',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	getTransactionSettingDetails : function(userId, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op1006.1',
    			data : userId,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	//End
    	
    	updateSettingForTransactionPassowrd : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op1006.3',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	modifyTransactionSettingPassword : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op1006.31',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	get10UsersList : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0013.11',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	},
    	
    	getTagsByReaders : function(requestParams, successCallback, errorCallback){
    		$http({
    			method : 'POST',
    			url : HTTP + '/op0013.12',
    			data : requestParams,
    			headers: {
    				'Content-Type' : 'application/json'
    			}
    		})
    		.then(function onSuccess(response){
    			successCallback(response);
    		},function onError(response){
    			errorCallback(response);
    		});
    	}
    	
    }
  }]);
})();

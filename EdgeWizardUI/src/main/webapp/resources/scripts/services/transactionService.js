(function(){
  'use strict';
  angular.module('rfidDemoApp.services')
  .service('TransactionService', ['$http','HTTP','GET_EVENTTRANS','GET_USERTRANS','GET_ASSETTRANS',
      function($http,HTTP, GET_EVENTTRANS, GET_USERTRANS, GET_ASSETTRANS) {
	  return {
		  getEventTransaction: function(requestParams, successCallback, errorCallback) {
			  $http({
				  method: 'POST',
				  url: HTTP + GET_EVENTTRANS,
				  data : requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
  	       })
  	       .then(function onSuccess(response) {
  	    	   successCallback(response);
  	       },
  	       function onError(response) {
  	    	   errorCallback(response);
  	       });
		  },

		  getUserTransaction: function(requestParams,successCallback, errorCallback) {
			  $http({
				  method: 'POST',
				  url: HTTP + GET_USERTRANS,
				  data: requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
				  
  	       })
  	       .then(function onSuccess(response) {
  	    	   successCallback(response);
  	       },
  	       function onError(response) {
  	    	   errorCallback(response);
  	       });
		  },

		  getAssetTransaction: function(requestParams,successCallback, errorCallback) {
			  $http({
				  method: 'POST',
				  url: HTTP + GET_ASSETTRANS,
				  data: requestParams,
				  headers: {
					  'Content-Type' : 'application/json'
				  }
  	       })
  	       .then(function onSuccess(response) {
  	    	   successCallback(response);
  	       },
  	       function onError(response) {
  	    	   errorCallback(response);
  	       });
		  },
	  };
	 }]);
})();
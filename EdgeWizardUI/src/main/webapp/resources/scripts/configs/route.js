(function(){
	
	angular.module('rfidDemoApp.config')
	.config(['$stateProvider','$urlRouterProvider','$httpProvider', function($stateProvider, $urlRouterProvider ,$httpProvider){
		
		$stateProvider
		.state('login', {
			url: '',
			templateUrl: 'resources/views/login.html',
			controller : 'LoginController',
			data: {
				displayName: 'Login',
			}
		})
	    
	    //for Android state
	    .state('euiAndroid',{
	    	url : '/euiAndroid/?un/:ps/:cmpId/:sysname/:activity/:epcId',
	    	//templateUrl : 'resources/views/home.html',
	    	//controller : 'LoginController',
	    	controller : 'AndroidCtrl',
	    	//controllerAs : 'euiAndroidCtrl',
		    data: {
		    	 displayName: 'eui',
		    }
	    })
	    
	    .state('homeDashboard',{
	    	url : '/home',
	    	templateUrl : 'resources/views/home.html',
	    	controller : 'HomeController',
	    	controllerAs : 'homeController',
		    data: {
		    	 displayName: 'Home Dashboard',
		    }
	    })
	    
	    .state('homeDashboard.masterMenu',{
	    	url : '/masterMenu',
	    	templateUrl : 'resources/views/masterMenus.html',
	    	controller : 'MasterMenuController',
	    	controllerAs : 'masterMenuController',
	    	data: {
	              displayName: 'Masters',
	          }
	    })
	    .state('homeDashboard.asset',{
	    	url : '/asset',
	    	templateUrl : 'resources/views/asset/asset.html',
	    	/*controller : 'AssetController',
	    	controllerAs : 'assetController',*/
	    	data: {
	              displayName: 'asset',
	          }
	    })
	    .state('homeDashboard.asset.assetAdd',{
	    	url : '/assetAdd/?assetID',
	    	templateUrl : 'resources/views/asset/assetAdd.html',
	    	controller : 'AssetAddController',
	    	controllerAs : 'assetAddController',
	    	data: {
	              displayName: 'asset registration',
	          }
	    })
	    .state('homeDashboard.asset.assetEdit',{
	    	url : '/assetEdit/?assetData',
	    	templateUrl : 'resources/views/asset/assetEdit.html',
	    	controller : 'AssetEditController',
	    	controllerAs : 'assetEditController',
	    	data: {
	              displayName: 'assetEdit',
	    	}
	    })
	    
	    .state('homeDashboard.asset.assetAddUpdate',{
	    	url : '/assetAddUpdate/:assetID?from',
	    	templateUrl : 'resources/views/asset/assetAddUpdate.html',
	    	controller : 'AssetAddUpdateCtrl',
	    	controllerAs : 'assetAddUpdateCtrl',
	    	data: {
	              displayName: 'asset registration',
	          }
	    })
	    
	    .state('homeDashboard.asset.assetAddUpdateByTagId',{
	    	url : '/assetAddUpdateByTagId/:tagId?activity',
	    	templateUrl : 'resources/views/asset/IclAssetAddUpdate.html',
	    	controller : 'IclAssetAddUpdateCtrl',
	    	controllerAs : 'iclAssetAddUpdateCtrl',
	    	data: {
	    		displayName: 'asset registration',
	    	}
	    })
	    .state('error',{
	    	url : '/error',
	    	templateUrl : 'resources/views/error.html',
	    	controller : 'ErrorCtrl',
	    	data: {
	    		displayName: 'Error | EdgeWizard'
	    	}
	    }) 
	    
	    .state('homeDashboard.asset.maintenanceEntry',{
	    	url : '/maintenaceEntry',
	    	templateUrl : 'resources/views/asset/maintenanceEntry.html',
	    	controller : 'MaintenanceEntryCtrl',
	    	controllerAs : 'maintenanceEntryCtrl',
	    	data: {
	              displayName: 'Maintenance Entry',
	    	}
	    })
	    .state('homeDashboard.asset.maintenanceEntry.maintenanceImage',{
	    	url : '/maintenanceImage',
	    	templateUrl : 'resources/views/asset/maintenanceImage.html',
	    	/*controller : 'AssetEditController',
	    	controllerAs : 'assetEditController',
	    	*/data: {
	              displayName: 'Maintenance Image',
	    	}
	    })
	    .state('homeDashboard.asset.maintenanceEntry.checkList',{
	    	url : '/checkList',
	    	templateUrl : 'resources/views/asset/maintenanceImage.html',
	    	/*controller : 'AssetEditController',
	    	controllerAs : 'assetEditController',
	    	*/data: {
	              displayName: 'Check List',
	    	}
	    })
	    .state('homeDashboard.asset.maintenanceEntry.mHistory',{
	    	url : '/mHistory',
	    	templateUrl : 'resources/views/asset/maintenanceHistory.html',
	    	controller : 'MaintenanceHistoryCtrl',
	    	controllerAs : 'maintenanceHistoryCtrl',
	    	data: {
	              displayName: 'History',
	    	}
	    })
	    
	    .state('homeDashboard.transaction',{
	    	url : '/transaction',
	    	templateUrl : 'resources/views/transaction/transaction.html',
	    	controller : 'TransactionController',
	    	controllerAs : 'transactionController',
	    	data: {
		             displayName: 'Transactions',
		      }
	    })
	    .state('homeDashboard.transaction.all',{
	    	url : '/alltransaction',
	    	templateUrl : 'resources/views/transaction/allTransaction.html',
	    	controller : 'AllTransactionController',
	    	controllerAs : 'alltransactionController',
    		data: {
	             displayName: 'All Transactions',
    		}
	    })
	    .state('homeDashboard.transaction.asset',{
	    	url : '/assettransaction',
	    	templateUrl : 'resources/views/transaction/assetTransaction.html',
	    	controller : 'AssetTransactionController',
	    	controllerAs : 'assettransactionController',
	    	data: {
	             displayName: 'Asset Transactions',
	    	}
	    })
	    .state('homeDashboard.transaction.user',{
	    	url : '/usertransaction',
	    	templateUrl : 'resources/views/transaction/userTransaction.html',
	    	controller : 'UserTransactionController',
	    	controllerAs : 'usertransactionController',
	    	data: {
	             displayName: 'User Transactions',
	    	}
	    })
	    
	    .state('homeDashboard.masterMenu.company',{
	    	url :'/company',
	    	templateUrl : 'resources/views/master/company.html',
	    	controller : 'CompanyController',
	    	controllerAs : 'companyController',
	    	data: {
	              displayName: 'company',
	          }
	    })
	    .state('homeDashboard.masterMenu.division',{
	    	url :'/division',
	    	/*reload :true,
	    	newtab : true,*/
	    	templateUrl : 'resources/views/master/division.html',
	    	controller : 'DivisionController',
	    	controllerAs : 'divisionController',
	    	data: {
	    		displayName: 'division',
	          }
	  	   
	    })
	    .state('homeDashboard.masterMenu.sector',{
	    	url : '/sector',
	    	templateUrl : 'resources/views/master/sector.html',
	    	controller : 'SectorController',
	    	controllerAs : 'sectorController',
	    	data: {
	              displayName: 'sector',
	          }
	  	    
	    })
	    .state('homeDashboard.masterMenu.manufacture',{
	    	url : '/manufacture',
	    	templateUrl : 'resources/views/master/manufacture.html',
	    	controller : 'ManufactureController',
	    	controllerAs : 'manufactureController',
	    	data: {
	              displayName: 'manufacturer',
	          }
	  	    
	    })
	    .state('homeDashboard.masterMenu.vendor',{
	    	url : '/vendor',
	    	templateUrl : 'resources/views/master/vendor.html',
	    	controller : 'VendorController',
	    	controllerAs : 'vendorController',
	    	data: {
	              displayName: 'vendor',
	          }
	    })
	    .state('homeDashboard.masterMenu.location',{
	    	url : '/location',
	    	templateUrl : 'resources/views/master/location.html',
	    	controller : 'LocationController',
	    	controllerAs : 'locationController',
	    	data: {
	              displayName: 'location',
	          }
	    })
	    .state('homeDashboard.masterMenu.department',{
	    	url : '/department',
	    	templateUrl : 'resources/views/master/department.html',
	    	controller : 'DepartmentController',
	    	controllerAs : 'departmentController',
	    	data: {
	              displayName: 'department',
	          }
	    })
		.state('homeDashboard.masterMenu.batchLot',{
			url : '/batchLot',
			templateUrl : 'resources/views/master/batchLot.html',
			controller : 'BatchLotController',
			controllerAs : 'batchLotController',
			data: {
	              displayName: 'batchlot',
	          }
		})
		.state('homeDashboard.masterMenu.category',{
			url : '/category',
			templateUrl : 'resources/views/master/category.html',
			controller : 'CategoryController',
			controllerAs : 'categoryController',
			data: {
	              displayName: 'category',
			}
		})
		.state('homeDashboard.masterMenu.rack_registration',{
			url : '/rack_registration',
			templateUrl : 'resources/views/master/rack_registration.html',
			controller : 'RackController',
			controllerAs : 'rackController',
			data: {
	              displayName: 'rackmaster',
			}
		})
		.state('homeDashboard.masterMenu.tax',{
			url : '/tax',
			templateUrl : 'resources/views/master/tax.html',
			controller : 'TaxController',
			controllerAs : 'taxController',
			data: {
	              displayName: 'tax',
	          }
		})
		
		//POS USER
		.state('homeDashboard.posUser',{
			url : '/posUser',
			templateUrl : 'resources/views/user/user.html',
			/*controller : 'UserCtrl',
			controllerAs : 'userCtrl',*/
			data: {
				displayName: 'user',
			}
		})
		.state('homeDashboard.posUser.userAdd',{
			url : '/userAdd',
			templateUrl : 'resources/views/user/posUserAddUpdate.html', // userAddUpdate.html
			controller : 'UserAddUpdateCtrl',
			controllerAs : 'userAddUpdateCtrl',
			data: {
				displayName: 'user registration',
			}	
		})
		.state('homeDashboard.posUser.userUpdate',{
			url : '/userUpdate/:userId/:type',
			templateUrl : 'resources/views/user/posUserAddUpdate.html',
			controller : 'UserAddUpdateCtrl',
			controllerAs : 'userAddUpdateCtrl',
			data: {
				displayName: 'user update',
			}	
		})
		.state('homeDashboard.posUser.userView',{
			url : '/userView/:userId/:type',
			templateUrl : 'resources/views/user/userView.html',
			controller : 'UserViewCtrl',
			controllerAs : 'userViewCtrl',
			data: {
				displayName: 'user view',
			}	
		})
		//End users
		
		.state('homeDashboard.user',{
			url : '/user',
			templateUrl : 'resources/views/user/user.html',
			/*controller : 'UserCtrl',
			controllerAs : 'userCtrl',*/
			data: {
				displayName: 'user',
			}
		})
		.state('homeDashboard.userPhoto',{
			url : '/userPhoto',
			templateUrl : 'resources/views/user/userPhoto.html',
			controller : 'UserPhotoCtrl',
			controllerAs : 'userPhotoCtrl',
			data: {
				displayName: 'user photo',
			}
		})
		
		.state('homeDashboard.user.userAdd',{
			url : '/userAdd',
			templateUrl : 'resources/views/user/userAddUpdate.html',
			controller : 'UserAddUpdateCtrl',
			controllerAs : 'userAddUpdateCtrl',
			data: {
				displayName: 'user registration',
			}	
		})
		.state('homeDashboard.user.userUpdate',{
			url : '/userUpdate/:userId/:type',
			templateUrl : 'resources/views/user/userAddUpdate.html',
			controller : 'UserAddUpdateCtrl',
			controllerAs : 'userAddUpdateCtrl',
			data: {
				displayName: 'user update',
			}	
		})
		.state('homeDashboard.user.userView',{
			url : '/userView/:userId/:type',
			templateUrl : 'resources/views/user/userView.html',
			controller : 'UserViewCtrl',
			controllerAs : 'userViewCtrl',
			data: {
				displayName: 'user view',
			}	
		})
		.state('homeDashboard.reports',{
			url : '/reports',
			templateUrl : 'resources/views/reports/reportMenus.html',
			/*controller : 'DashoardReportsController',
			controllerAs : 'dashoardReportsController',*/
			data: {
	              displayName: 'All Reports',
			}
		})
		.state('homeDashboard.reports.maintenanceReports',{
			url : '/maintenanceReports',
			templateUrl : 'resources/views/reports/maintenanceReport.html',
			controller : 'MaintenanceReportCtrl',
			controllerAs : 'maintenanceReportCtrl',
			data: {
	              displayName: 'Maintenance Reports',
			}
		})
		.state('homeDashboard.reports.posReports',{
			url : '/posReports',
			templateUrl : 'resources/views/reports/posReports.html',
			controller : 'PosReportCtrl',
			controllerAs : 'posReportCtrl',
			data: {
	              displayName: 'POS Reports',
			}
		})
		.state('homeDashboard.assetIssueRecieve',{
			url : '/assetIssueRecieve',
			templateUrl : 'resources/views/assetIR/assetIRMenus.html',
			data: {
	              displayName: 'Asset Menus',
	          }
		})
		.state('homeDashboard.assetIssueRecieve.assetIssue',{
			url : '/assetIssue',
			templateUrl : 'resources/views/assetIR/assetIssue.html',
			controller : 'IssueController',
			controllerAs : 'issueController',
			data: {
				displayName: 'Asset Issue',
			}
		})
		.state('homeDashboard.assetIssueRecieve.assetReceive',{
			url : '/assetReceive',
			templateUrl : 'resources/views/assetIR/assetReceive.html',
			controller : 'ReceiveController',
			controllerAs : 'receiveController',
			data: {
				displayName: 'Asset Receive',
			}
		})
		.state('homeDashboard.assetIssueRecieve.reports',{
			url : '/reports',
			templateUrl : 'resources/views/master/reports.html',
			controller : 'ReportsController',
			controllerAs : 'reportsController',
			data: {
	              displayName: 'Reports',
	          }
		})
		.state('homeDashboard.assetIssueRecieve.reports.issuedAssetRpt',{
			url : '/issuedAssetRpt/?myParam',
			templateUrl : 'resources/views/reports/issuedAssetReport.html',
			controller : 'IssuedAssetReportCtrl',
			controllerAs : 'issuedAssetReportCtrl',
			data: {
	              displayName: 'All Issued Assets Reports',
	          }
		})
		.state('homeDashboard.assetIssueRecieve.assetIRstatus',{
			url : '/status',
			templateUrl : 'resources/views/assetIR/assetIRstatus.html',
			controller : 'AssetIRstatusCtrl',
			controllerAs : 'assetIRstatusCtrl',
			data: {
	              displayName: 'Status',
	          }
		})
		.state('homeDashboard.readers',{
	    	url : '/readers',
	    	templateUrl : 'resources/views/readers/readersMenus.html',
	    	//controller : 'HomeController',
	    	//controllerAs : 'homeController',
		    data: {
		    	 displayName: 'Readers',
		    }
	    })
	    .state('homeDashboard.readers.area',{
	    	url : '/area',
	    	templateUrl : 'resources/views/readers/area.html',
	    	controller : 'AreaCtrl',
	    	controllerAs : 'areaCtrl',
	    	data : {
	    		displayName : 'Area'
	    	}
	    })
	    .state('homeDashboard.readers.zone',{
	    	url : '/zone',
	    	templateUrl : 'resources/views/readers/zone.html',
	    	controller : 'ZoneCtrl',
	    	controllerAs : 'zoneCtrl',
	    	data : {
	    		displayName : 'Zone'
	    	}
	    })
	    .state('homeDashboard.dockdoor',{
	    	url : '/dockdoor',
	    	templateUrl : 'resources/views/masterData/dockdoor.html',
	    	controller : 'DockDoorCtrl',
	    	controllerAs : 'dockDoorCtrl',
	    	data : {
	    		displayName : 'Dockdoor'
	    	}
	    })	    
		
		//pickUpDrop ui $states
		.state('pickUpDropDashBoard',{
			url : '/pdDashboard',
			templateUrl : 'resources/views/PD/pdDashboard.html',
			controller : 'PdDashboardCtrl',
			controllerAs : 'pdDashboardCtrl',
			data: {
	              displayName: 'PickUp Drop Dashboard',
			}
		})
		.state('pickUpDropDashBoard.deviceLocation',{
			url : '/deviceLocation/:deviceId/:deviceName',
			templateUrl : 'resources/views/PD/deviceLocation.html',
			controller : 'PdDeviceLocationCtrl',
			controllerAs : 'pdDeviceLocationCtrl',
			data: {
	              displayName: 'Device Location',
			}
		})
		.state('pickUpDropDashBoard.fullMap',{
			url : '/fullMap/:journeyID/:routeID',
			templateUrl : 'resources/views/PD/fullMap.html',
			controller : 'PdFullMapCtrl',
			controllerAs : 'pdFullMapCtrl',
			data: {
	              displayName: 'full map',
			}
		})
		
		.state('pickUpDropDashBoard.user',{
			url : '/user',
			templateUrl : 'resources/views/user/user.html',
			controller : 'UserCtrl',
			controllerAs : 'userCtrl',
			data: {
				displayName: 'user',
			}
		})
		
	    .state('pickUpDropDashBoard.division',{
	    	url :'/division',
	    	templateUrl : 'resources/views/master/division.html',
	    	controller : 'DivisionController',
	    	controllerAs : 'divisionController',
	    	data: {
	    		displayName: 'division',
	    	}
	    })
	     .state('pickUpDropDashBoard.sector',{
	    	url : '/sector',
	    	templateUrl : 'resources/views/master/sector.html',
	    	controller : 'SectorController',
	    	controllerAs : 'sectorController',
	    	data: {
	              displayName: 'sector',
	    	}
	    })
	    .state('pickUpDropDashBoard.location',{
	    	url : '/location',
	    	templateUrl : 'resources/views/master/location.html',
	    	controller : 'LocationController',
	    	controllerAs : 'locationController',
	    	data: {
	              displayName: 'location',
	          }
	    })
	    .state('pickUpDropDashBoard.department',{
	    	url : '/department',
	    	templateUrl : 'resources/views/master/department.html',
	    	controller : 'DepartmentController',
	    	controllerAs : 'departmentController',
	    	data: {
	              displayName: 'department',
	          }
	    })
		.state('pickUpDropDashBoard.user.userAdd',{
			url : '/userAdd',
			templateUrl : 'resources/views/user/userAddUpdate.html',
			controller : 'UserAddUpdateCtrl',
			controllerAs : 'userAddUpdateCtrl',
			data: {
				displayName: 'user registration',
			}	
		})
		.state('pickUpDropDashBoard.user.userUpdate',{
			url : '/userUpdate/:userId',
			templateUrl : 'resources/views/user/userAddUpdate.html',
			controller : 'UserAddUpdateCtrl',
			controllerAs : 'userAddUpdateCtrl',
			data: {
				displayName: 'user update',
			}	
		})
		.state('pickUpDropDashBoard.deviceRegistration',{
			url : '/deviceRegistration',
			templateUrl : 'resources/views/PD/deviceRegistration.html',
			controller : 'DeviceRegistrationCtrl',
			controllerAs : 'deviceRegistrationCtrl',
			data: {
				displayName: 'Device Registration',
			}
		})
		.state('pickUpDropDashBoard.area',{
			url : '/area',
			templateUrl : 'resources/views/readers/area.html',
			controller : 'AreaCtrl',
			controllerAs : 'areaCtrl',
			data: {
				displayName: 'Area',
			}
		})
		.state('pickUpDropDashBoard.zone',{
			url : '/zone',
			templateUrl : 'resources/views/readers/zone.html',
			controller : 'ZoneCtrl',
			controllerAs : 'zoneCtrl',
			data: {
				displayName: 'Zone',
			}
		})
		
		/*POS $states*/
		.state('homeDashboard.posMenus',{
			url : '/posMenus',
			templateUrl : 'resources/views/POS/posMenus.html',
			/*controller : 'ZoneCtrl',
			controllerAs : 'zoneCtrl',*/
			data: {
				displayName: 'POS Menus',
			}
		})
		.state('homeDashboard.posMenus.posOrder',{
			url : '/posOrder',
			templateUrl : 'resources/views/POS/posOrderDetails.html',
			controller : 'PosOrderDetailsCtrl',
			controllerAs : 'posOrderDetailsCtrl',
			data: {
				displayName: 'POS Order Details',
			}
		})
		.state('homeDashboard.posMenus.posSetting',{
			url : '/posSetting',
			templateUrl : 'resources/views/POS/posSetting.html',
			controller : 'PosSettingCtrl',
			controllerAs : 'posSettingCtrl',
			data: {
				displayName: 'POS Setting',
			}
		})
		/*.state('homeDashboard.posMenus.posSetting',{
			url : '/posSetting',
			templateUrl : 'resources/views/POS/posSetting.html',
			controller : 'PosSettingCtrl',
			controllerAs : 'posSettingCtrl',
			data: {
				displayName: 'POS Setting',
			}
		})*/
		.state('homeDashboard.posMenus.posCategorySetting',{
			url : '/posCategorySetting',
			templateUrl : 'resources/views/POS/posCategorySetting.html',
			controller : 'PosCategorySettingCtrl',
			controllerAs : 'posCategorySettingCtrl',
			data: {
				displayName: 'POS Category Setting',
			}
		})
		.state('homeDashboard.posMenus.posAdminMode',{
			url : '/posAdminDetails',
			templateUrl : 'resources/views/POS/posAdminDetails.html',
			controller : 'PosAdminDetailsCtrl',
			controllerAs : 'posAdminDetailsCtrl',
			data: {
				displayName: 'POS Admin	Details',
			}
		})
		.state('homeDashboard.posMenus.posKitchenMode',{
			url : '/posKitchenMode',
			templateUrl : 'resources/views/POS/posKitchenDetails.html',
			controller : 'PosKitchenDetailsCtrl',
			controllerAs : 'posKitchenDetailsCtrl',
			data: {
				displayName: 'POS Kitchen Details',
			}
		})

		$urlRouterProvider.otherwise('');
	}])
	.factory('myInterceptor', ['$q','$injector', function($q, $injector) {  
		var requestInterceptor = {
		        request: function(config) {
		        	 return config || $q.when(config);
		        },
		        
		        response: function(response) {
		        	return response || $q.when(response);
		        },
		        
		        requestError: function(rejectReason) {
		           return $q.reject(rejectReason);
		        },
		        
		        responseError: function(response) {
		        	if (response.status == 401 || response.status == -1 ){
		        		alert('Something went wrong. Please login again.');
		        		/*$injector.get('$state').transitionTo('login');*/
		                var stateService = $injector.get('$state');
		                stateService.go('login');
		        	}
		            return $q.reject(response);
		        }
		    };

		return requestInterceptor;
	}])
	.config(['$httpProvider', function($httpProvider) {  
		$httpProvider.interceptors.push('myInterceptor');
	}])
	.run(function($filter, $location, $state, $stateParams,  $injector, $cookieStore,$rootScope, $localStorage, $cookies,serverTimeService){
		/*$location.path("/login");*/
		$rootScope.$on('$stateChangeStart', function() {
	        $rootScope.stateLoading = true;
		})
	      
		$rootScope.$on('$stateChangeSuccess', function(currentRoute, previousRoute) {
			/*var globals = $cookies.get('globals');
			if(globals === undefined){
				$state.go("login");
			};*/
			
			//check if request come from android or web
			if(previousRoute.name === 'euiAndroid'){
				$cookies.put('isReqFrom', 'AND');
			}else{
				var globals = $cookies.get('globals');
				if(globals === undefined){
					$state.go("login");
				};
			};
			
			if(previousRoute.name === "homeDashboard"){
				var cookieStoreTtlAssets = JSON.parse($cookies.get("totalAssetPermission"));
				if(cookieStoreTtlAssets){
					$rootScope.totalAssetFrmLicense = cookieStoreTtlAssets.assetPermissionCount;
					$rootScope.totalRegisteredAssets =[];
					$rootScope.totalRegisterAssets = cookieStoreTtlAssets.totalRegisteredAssetCount;
					$rootScope.totalRegisteredAssets.push($rootScope.totalRegisterAssets);
				}
			}
			//console.log("stateChangeSuccess" + currentRoute , previousRoute);
			$rootScope.stateLoading = false;
			$rootScope.pageTitle = $state.current.data.displayName;
		})
		
		/*var currentTime = new Date();*/
		/*var currentOffset = currentTime.getTimezoneOffset();
		var ISTOffset = 0;
		var ISTTime = new Date(currentTime.getTime() + (ISTOffset + currentOffset)*60000);
		var offset = TimeZoneInfo.Local.GetUtcOffset(DateTime.UtcNow); */
		// create Date object for current location
		var d = new Date();
	 
	    // convert to msec
	    // add local time zone offset 
	    // get UTC time in msec
		var utc = d.getTime() + (d.getTimezoneOffset() * 60000);
	   
		// create new Date object for different city
	    // using supplied offset
		var nd = new Date(utc + (3600000* + 5.50));
		
		/*var server_time = new Date().getTime() + (60*60*2*1000);*/ //Simulate different server time
		//var server_time = new Date().getTimezoneOffset() * 60 * 1000 ; //We assume that server clock sets to UTC (if no, you can multiple this with server timezone offset)
		$rootScope.serverClock = serverTimeService.init(nd);
		
		$rootScope.logout = function () {
            $cookieStore.remove('globals');
            $cookieStore.remove('totalAssetPermission');
            $cookieStore.remove('isReqFrom');
            
			$localStorage.$reset();
            $state.go("login");
        };
	});
})();

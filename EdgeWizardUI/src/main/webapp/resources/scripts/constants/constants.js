(function(){
	'use strict';

	angular.module('rfidDemoApp.constants')
	//http://192.168.0.222:8888/op0001.38

	.constant('HTTP','http://192.168.0.59:8989/EdgeWizard_pos')
	//.constant('HTTP','http://192.168.0.20:8888/EdgeWizard')
	//.constant('HTTP','http://192.168.0.132:8080/EdgeWizard')
	.constant('_', window._)
	.constant('LICFILE_GET','/op0001.38') /*Get LIC file data */

	.constant('COMP_LIST','/op0001.30')  /*All company load*/
	.constant('COMP_LIST_op0001','/op0001')
	.constant('COMP_GET','/op0002.1')
	.constant('COMP_MODIFY','/op0002.3')

	.constant('MANUFAC_GETALL','/op0011.13')/* Get All Manufacture details */
	.constant('MANUFAC_ADD','/op0011.2')/* Add Manufacture details */
	.constant('MANUFAC_MODIFY','/op0011.3')/* Update Manufacture details*/
	.constant('MANUFAC_DELETE','/op0011.4') /* Delete Manufacture details*/

	.constant('VENDOR_GETALL','/op0009.13') /* Get All vendor details*/
	.constant('VENDOR_ADD','/op0009.2')/* Add Vendor details*/
	.constant('VENDOR_UPDATE','/op0009.3') /* Update vendor details*/
	.constant('VENDOR_DELETE','/op0009.4') /* Delete vendor details*/

	.constant('DIV_ADD','/op0003.2')
	.constant('DIV_LIST','/op0003.13') /*Get All divisions*/
	.constant('DIV_MODIFY','/op0003.3') /*update division*/
	.constant('DIV_DELETE','/op0003.4') /*delete division*/

	.constant('SEC_LIST','/op0004.13') /*Get All Sector*/
	.constant('SEC_ADD','/op0004.2') /* Add sector*/
	.constant('SEC_MODIFY','/op0004.3') /* update sector*/
	.constant('SEC_DELETE','/op0004.4') /* delete sector*/

	.constant('BATCHLOT_GETALL','/op0008.13') /* Get all batch details */
	.constant('BATCHLOT_ADD','/op0008.2') /*Add batch lot details */
	.constant('BATCHLOT_UPDATE','/op0008.3') /* Update batch lot details */
	.constant('BATCHLOT_DELETE','/op0008.4') /* Delete batch details*/

	.constant('LOC_LIST','/op0006.13') /*get all locations*/
	.constant('LOC_ADD','/op0006.2') /* Add location*/
	.constant('LOC_MODIFY','/op0006.3') /* update location*/
	.constant('LOC_DELETE','/op0006.4') /* delete Location*/

	.constant('DEPT_LIST','/op0005.13') /*get all locations*/
	.constant('DEPT_ADD','/op0005.2') /* Add location*/
	.constant('DEPT_MODIFY','/op0005.3') /* update location*/
	.constant('DEPT_DELETE','/op0005.4') /* delete Location*/

	/*.constant('ASSET_DELETE','/op0015.4')
	.constant('TOTAL_ASSET','/op0015.41')
	.constant('ASSET_GETBYID','/op0015.1')*/

	.constant('GET_EVENTTRANS','/op0020.1') /*Get all event transaction*/
	.constant('GET_USERTRANS','/op0022.1') /*Get all event transaction*/
	.constant('GET_ASSETTRANS','/op0021.1') /*Get all event transaction*/

	.constant('IMAGE_UPLOAD','/uploadFileORI') /*Image upload*/
	.constant('IMAGE_DOWNLOAD','/downloadFile') /*Image download*/

	.constant('LABEL_CHANGED','/op0001.29') /*Label changed*/
	
	/*Image values constant */
	.constant('USERSMIMG','HHUSER') /* get user small image */
	.constant('USERBGIMG','USER') /* get user big image */
	
	.constant('GET_CATEGORYWISE_ASSET','/op0015.7') /* get category wise asset */
	.constant('GET_USER_ISSUED_ASSET','/op0048.11') /* get category wise asset */
	
	.constant('ASSET_ISSUED_TO_USER','/op0048.2') /* issue save to related user */
	.constant('ASSET_RECEIVE_FROM_USER','/op0048.21') /* issue receive from related user */
	.constant('ASSET_ISSUE_USER_READ_TAG','/op0048.44') /* Asset user read tag */
	.constant('ASSET_ISSUE_ASSET_READ_TAG','/op0048.48') /* Asset issue read tag */
	
	.constant('GET_USER_DETAIL_FIELDS','/op0050.1') /* get user detail fields */
	.constant('GET_GRDETAILS','/op0048.14')
	.constant('GET_TTLASSET_LICENSE','/op0015.46')
	.constant('PRINT_OPTION','/op0048.8')
	
	//reports
	.constant('GET_ISSUEDASSETSLIST','/op0048.501')
	.constant('GET_RECEIVEDASSETLIST','/op0048.503')
	.constant('GET_ISSUEDRECEIVEDASSETLIST','/op0048.502')
	
	//date format
	.constant('DTFORMAT','dd-MMM-yyyy')
	
	.constant('_', window._ );
})();

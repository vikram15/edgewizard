(function(){
	'use strict';

	var app = angular.module('rfidDemoApp',['ui.bootstrap',
	                              'ui.router',
	                              'ngCookies',
	                              'ngFileUpload',
	                              'rfidDemoApp.constants',
                                  'rfidDemoApp.config',
                                  'rfidDemoApp.controller',
                                  'rfidDemoApp.services',
                                  'rfidDemoApp.directives',
                                  'rfidDemoApp.filters',
//                                  'ncy-angular-breadcrumb',
                                  'ngStorage',
                                  'ngAnimate', 
                                  'ngSanitize', 
                                  'nvd3'
                                  ]);
	angular.module('rfidDemoApp.constants',[]);
	angular.module('rfidDemoApp.config',[]);
	angular.module('rfidDemoApp.controller',[]);
	angular.module('rfidDemoApp.services',[]);
	angular.module('rfidDemoApp.directives',[]);
	angular.module('rfidDemoApp.filters',[]);
})();
(function(){
	'use strict';
	angular.module('rfidDemoApp.directives')
	.directive('ngRightClick', function($parse) {
	    return function(scope, element, attrs) {
	    	restrict : 'A';
	        var fn = $parse(attrs.ngRightClick);
	        element.bind('contextmenu', function(event) {
	        	 return false;
	        });
	    };
	})
	.directive('homeHeader', function(){
		return {
			restrict : 'E',
			templateUrl : 'resources/views/homeHeader.html'
		}
	})
	.directive('homeSideMenus', function(){
		return{
			restrict : 'E',
			templateUrl : 'resources/views/homeSideMenuBar.html'
		}
	})
	.directive('widgets', function(){
		return{
			restrict : 'E',
			templateUrl : 'resources/views/widgets.html'
		}
	})
	.directive('homeBodyContent', function(){
		return{
			restrict : 'E',
			templateUrl : 'resources/views/homeBodyContent.html' 
		}
	})
	.directive('numbersOnly', function() {
		return {
			require: 'ngModel',
			link: function(scope, element, attr, ngModelCtrl) {
				function fromUser(text) {
					if (text) {
						var transformedInput = text.replace(/[^0-9]/g, '');
						if (transformedInput !== text) {
							ngModelCtrl.$setViewValue(transformedInput);
							ngModelCtrl.$render();
						}
						return transformedInput;
					}
					return undefined;
				}
				ngModelCtrl.$parsers.push(fromUser);
			}
		};
	})
	.directive('validSubmit', function ($parse) {
		/*var ValidSubmit = ['$parse', function ($parse) {*/
	    return {
	        compile: function compile(tElement, tAttrs, transclude) {
	            return {
	                post: function postLink(scope, element, iAttrs, controller) {
	                    var form = element.controller('form');
	                    form.$submitted = false;
	                    var fn = $parse(iAttrs.validSubmit);
	                    element.on('submit', function(event) {
	                        scope.$apply(function() {
	                            element.addClass('ng-submitted');
	                            form.$submitted = true;
	                            if(form.$valid) {
	                                fn(scope, {$event:event});
	                            }
	                        });
	                    });
	                    scope.$watch(function() { return form.$valid}, function(isValid) {
	                        if(form.$submitted == false) return;
	                        if(isValid) {
	                            element.removeClass('has-error').addClass('has-success');
	                        } else {
	                            element.removeClass('has-success');
	                            element.addClass('has-error');
	                        }
	                    });
	                }
	            };
	        }
	    };
	/*}];*/
	})
	
	.directive('fileModel', ['$parse', function ($parse) {
	    return {
	        restrict: 'A',
	        link: function(scope, element, attrs) {
	        	//console.log("file model called");
	            var model = $parse(attrs.fileModel);
	            var modelSetter = model.assign;
	            
	            element.bind('change', function(){
	                scope.$apply(function(){
	                    modelSetter(scope, element[0].files[0]);
	                });
	            });
	        }
	    };
	}])
	.directive('highChartGauge', function ($timeout, $rootScope) {
		     return {
		       restrict: 'A',
		       scope: {
		         gaugeOptions: '='
		       },
		       template: '<div></div>',
		       link: function(scope, elem, attr) {
		    	   scope.gaugeOptions.chart.renderTo = elem[0];
		    	   
		    	   var chart = new Highcharts.Chart(scope.gaugeOptions);
		    	   chart.redraw();
		    	   
		    	   /*scope.$watchCollection('gaugeOptions.series', 
		    			   function (valores, antigosValores) {
		    		   console.log('FROM DIRECTIVE 1', valores[0].data[0]);
		    		   var point = chart.series[0].data[0];
						             point.update(valores[0].data[0]);
		    	   });*/
		    	   
		    	   
		    	  /* setInterval(function () {
		    		   chart.series[0].data = $rootScope.totalRegisteredAssets;
		    		   chart.yAxis.max = $rootScope.totalAssetFrmLicense;
		    		   var ch = chart.series[0].data[0];
		    		   chart.update($rootScope.totalRegisteredAssets);
		    		   chart.redraw();
		  			 console.log('FROM CONTROLLER', chart.series[0].data, chart.yAxis.max); 
		  		 }, 1000);*/
			          
		    	   /*Update chart data series ===  means used assets*/ 
		    	   chart.series[0].update({
		    		    //pointStart: newSeries[0].pointStart,
		    		    data: $rootScope.totalRegisteredAssets
		    		}, true); 
		    	   
		    	   /*update yAxis max count === means total assets*/ 
		    	   chart.yAxis[0].update({
		    		   max: $rootScope.totalAssetFrmLicense
		           });
		    	   
		         /*  setInterval(function () {
		            console.log('FROM DIRECTIVE 2', scope.gaugeOptions.series[0].data);
		          }, 10000);*/
		          
		          /*scope.$watchCollection('gaugeOptions.series', 
		            function (valores, antigosValores) {
		              console.log('FROM DIRECTIVE 1', valores[0].data[0]);
		              
		          	  var point = chart.series[0].data[0];
		             point.update(valores[0].data[0]);
		             
		          });*/
//		          elem.highcharts(
//		            scope.gaugeOptions,
//		            function (chart) {
//		              if (!chart.renderer.forExport) {
//		                setInterval(function () {
//		                  var point = chart.series[0].points[0];
//		                  point.update(scope.values);
//		                }, 3000);
//		              }
//		            });
		       }
		     };
		  })
		  .directive('focusMe', function($timeout, $parse) {
			  return {
				  link: function(scope, element, attrs) {
					  var model = $parse(attrs.focusMe);
					  scope.$watch(model, function(value) {
						  /*console.log('value=',value);*/
						  if(value === true) { 
							  $timeout(function() {
								  element[0].focus(); 
							  });
						  }
					  });
				      /*element.bind('blur', function() {
				        console.log('blur')
				        scope.$apply(model.assign(scope, false));
				      })*/
				  }
			  };
		  })
		
		//Password check
		.directive('pwCheck', function () {
			return {
				require: 'ngModel',
				link: function (scope, elem, attrs, ctrl) {
					var firstPassword = '#' + attrs.pwCheck;
					elem.add(firstPassword).on('keyup', function () {
						scope.$apply(function () {
							 /*console.info(elem.val());
							 console.info($(firstPassword).val());*/
							 
							ctrl.$setValidity('pwmatch', elem.val() === $(firstPassword).val());
						});
					});
				}
			}
		})
		.directive('pdSideMenubar', function(){
			return {
				restrict : 'E',
				templateUrl : 'resources/views/PD/pdSideMenuBar.html'
			}
		})
		.directive('ddDraggable', function(){
			return {
		        restrict: "A",
		        link: function(scope, element, attributes, ctlr) {
		            element.attr("draggable", true);

		            element.bind("dragstart", function(eventObject) {
		                eventObject.originalEvent.dataTransfer.setData("text", attributes.itemid);
		            });
		        }
		    };
		})
		.directive('rrDraggable', function(){
			return {
		        restrict: "A",
		        link: function(scope, element, attributes, ctlr) {
		            element.attr("draggable", true);

		            element.bind("dragstart", function(eventObject) {
		                eventObject.originalEvent.dataTransfer.setData("text1", attributes.itemid);
		            });
		        }
		    };
		})
		.directive('ddDropTarget', function(){
			return {
		        restrict: "A",
		        link: function (scope, element, attributes, ctlr) {

		            element.bind("dragover", function(eventObject){
		                eventObject.preventDefault();
		            });

		            element.bind("drop", function(eventObject) {
		                
		                // invoke controller/scope move method
		                scope.moveToBox(parseInt(eventObject.originalEvent.dataTransfer.getData("text")));

		                // cancel actual UI element from dropping, since the angular will recreate a the UI element
		                eventObject.preventDefault();
		            });
		        }
		    };
		})
		.directive('rrDropTarget', function(){
			return {
		        restrict: "A",
		        link: function (scope, element, attributes, ctlr) {

		            element.bind("dragover", function(eventObject){
		                eventObject.preventDefault();
		            });

		            element.bind("drop", function(eventObject) {
		                
		                // invoke controller/scope move method
		                scope.moveTofirstDiv(parseInt(eventObject.originalEvent.dataTransfer.getData("text1")));

		                // cancel actual UI element from dropping, since the angular will recreate a the UI element
		                eventObject.preventDefault();
		            });
		        }
		    };
		})
		.directive("contenteditable", function() {
			return {
				require: "ngModel",
				link: function(scope, element, attrs, ngModel) {

					function read() {
						ngModel.$setViewValue(element.html());
					}

					ngModel.$render = function() {
						element.html(ngModel.$viewValue || "");
					};

					element.bind("blur keyup change", function() {
						scope.$apply(read);
					});
				}
			};
		}).directive('numberCheck', function() {
			return {
				require: 'ngModel',
				link: function (scope, elem, attrs, ctrl) {
					elem.on('keyup', function () {
						scope.$apply(function () {
							console.log(elem.val());
							if(elem.val() !== '/^[0-9]*$/'){
								ctrl.$setValidity('zero',true);
							}
						});
					});
				}
			};
		})
		
})();
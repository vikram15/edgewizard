<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html>

<head>
<meta charset="utf-8">
<title>Demo Login</title>
<meta name="description" content="">
<meta name="viewport" content="width=device-width">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link href="<c:url value="resources/styles/style.css"/>"
	rel="stylesheet">

<!-- slider scripts -->
	<!-- Include Hammer (optional) -->
	<script src="<c:url value="resources/scripts/hammer-v2.0.3.js"/>"></script>

	<!-- Include Flickerplate -->
	<script src="<c:url value="resources/scripts/flickerplate.js"/>"></script>
	<link href="<c:url value="resources/styles/flickerplate.css"/>"  type="text/css" rel="stylesheet">
	
	 <script src="<c:url value="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.8/angular.min.js"/>"></script>
     <script src="<c:url value="resources/scripts/angular-ui-router.min.js"/>"></script>
	
<%-- 	<script src="<c:url value="resources/scripts/scripts.79afa33b.js"/>"></script> --%>
	<script src="<c:url value="resources/scripts/app.js"/>"></script>
	<script src="<c:url value="resources/scripts/configs/configs.js"/>"></script>


</head>
<body ng-app="rfidDemoApp">
<div class="container">
	<div class="row">
		<div ui-view></div>
	</div>
</div>

{{ 2 + 2}}

	<script>
	new flickerplate('.flicker-example');
	</script>
	

</body>

</html>
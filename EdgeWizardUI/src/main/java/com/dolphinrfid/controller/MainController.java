package com.dolphinrfid.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class MainController {

	@RequestMapping(value="/loginform")
	public ModelAndView loginForm(){ 
		System.out.println("login form");
		return new ModelAndView("index");
	}
	
	@RequestMapping(value="/homePage")
	public ModelAndView homePage(){ 
		return new ModelAndView("home");
	}
	
}

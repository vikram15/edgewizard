package com.dolphinrfid.util;
 

public class AjaxResponseBody {
	
	private String resMsg;	
	private String resCode; 
	private String opcode;	 
//	private List<?> resData;
	private Object resData;

	public String getResMsg() {
		return resMsg;
	}

	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}

	public String getOpcode() {
		return opcode;
	}

	public void setOpcode(String opcode) {
		this.opcode = opcode;
	}

	public Object getResData() {
		return resData;
	}

	public void setResData(Object resData) {
		this.resData = resData;
	}

//	public List<?> getResData() {
//		return resData;
//	}
//
//	public void setResData(List<?> resData) {
//		this.resData = resData;
//	}

	
}

package com.dolphinrfid.util;

 

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.Policy.Parameters;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import javax.print.DocPrintJob;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.print.attribute.PrintServiceAttribute;
import javax.print.attribute.standard.PrinterName;
import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

import org.apache.log4j.spi.ErrorHandler;
 
public class UtilityMethods {

	private boolean debugMode = false;
	private String fullPath;
	private char extensionSeparator;

	String[] Monthval = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
	Dimension dim = null;

	public String Convert_Seconds(long time) {

		/*
		 * int milliseconds = (int)(time % 1000); int seconds =
		 * (int)((time/1000) % 60); int minutes = (int)((time/60000) % 60); int
		 * hours = (int)((time/3600000) % 24); String millisecondsStr =
		 * (milliseconds<10 ? "00" : (milliseconds<100 ? "0" : ""))+
		 * milliseconds; String secondsStr = (seconds<10 ? "0" : "")+ seconds;
		 * String minutesStr = (minutes<10 ? "0" : "")+ minutes; String hoursStr
		 * = (hours<10 ? "0" : "")+ hours; System.out.println( hoursStr + ":" +
		 * minutesStr + ":" + secondsStr + "." + millisecondsStr );
		 */

		long days = 0, hours = 0, minutes = 0, seconds = 0;
		long Ttime;
		String DTField = "";
		try {
			Ttime = time * 60;
			days = Ttime / 86400;
			hours = (Ttime / 3600) - (days * 24);
			minutes = (Ttime / 60) - (days * 1440) - (hours * 60);
			seconds = time % 60;

			if (days != 0) {
				DTField = days + " Day ";
			}
			if (hours != 0) {
				DTField = DTField + hours + "hh";
			}
			if (minutes != 0) {
				DTField = DTField + minutes + "mm";
			} else {
				DTField = DTField + seconds + "SS";
			}
			//
			return DTField;
			// return days + "DD " + hours +"HH " + minutes + "MM";

		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return "" + time;
	}

	public CharSequence removeDuplicateWhitespace(CharSequence inputStr) {
		String patternStr = "\\s+";
		String replaceStr = " ";
		java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(patternStr);
		java.util.regex.Matcher matcher = pattern.matcher(inputStr);
		return matcher.replaceAll(replaceStr);
	}

	public String convertToAge(long seconds) {
		String DTField = "";
		try {
			int day = (int) TimeUnit.SECONDS.toDays(seconds);
			long hours = TimeUnit.SECONDS.toHours(seconds) - TimeUnit.DAYS.toHours(day);
			long minute = TimeUnit.SECONDS.toMinutes(seconds) - TimeUnit.HOURS.toMinutes(TimeUnit.SECONDS.toHours(seconds));
			long second = TimeUnit.SECONDS.toSeconds(seconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.SECONDS.toMinutes(seconds));
			// System.out.println("Day " + day + " Hour " + hours + " Minute " +
			// minute + " Seconds " + second);

			if (day > 0) {
				DTField = day + " Days ";
			}
			if (hours > 0) {
				DTField = DTField + hours + " hr ";
			}
			if (minute > 0) {
				DTField = DTField + minute + " min ";
			}
			if (second >= 0) {
				DTField = DTField + second + " sec";
			}

			return DTField;
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return "" + seconds;
	}

	public String convertToSeconds(long time) {
		long days = 0, hours = 0, minutes = 0, seconds = 0;
		long Ttime;
		String DTField = "";
		try {
			Ttime = time * 60;
			days = Ttime / 86400;
			hours = (Ttime / 3600) - (days * 24);
			minutes = (Ttime / 60) - (days * 1440) - (hours * 60);
			seconds = time % 60;

			if (days != 0) {
				DTField = days + " Days ";
			}
			if (hours != 0) {
				DTField = DTField + hours + " hr";
			}
			if (minutes != 0) {
				DTField = DTField + minutes + " min";
			} else {
				DTField = DTField + seconds + " sec";
			}

			return DTField;
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return "" + time;
	}

	//
	public boolean mobileNumberValidation(String strNumber) {
		if (NumberValidation(strNumber)) {
			int length = strNumber.length();
			if (length == 10) {
				return true;
			}
		}
		return false;
	}

	// by vitthal on 20130417--start
	public String getTotalTime(long time) {
		long days = 0, hours = 0, minutes = 0, seconds = 0;
		String field = "";
		try {
			days = (time * 1000) / (24 * 60 * 60 * 1000);
			hours = (time * 1000) / (60 * 60 * 1000) % 24;
			minutes = (time * 1000) / (60 * 1000) % 60;
			seconds = ((time * 1000) / 1000) % 60;

			if (days != 0) {
				field = days + "Day ";
			}
			if (hours != 0) {
				field = field + hours + "hh";
			}
			if (minutes != 0) {
				field = field + minutes + "mm";
			}
			if (seconds != 0) {
				field = field + seconds + "ss";
			}
			return field;

		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return null;
	}

	// by vitthal on 20130417--start

	public String GetApplicationPath() {
		try {
			java.io.File currentDir = new java.io.File("");
			return currentDir.getAbsolutePath();
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return null;
	}

	public String getServerPath() {
//		try {
//			if (Parameters.ISCLIENT) {
//				String Query1 = "", SERVERIP = null;
//				Query1 = "SELECT SERVERIP FROM SERVERIP";
//				Statement stmt = DBConnection.getInstance().getDBConnection().createStatement();
//				ResultSet rs = stmt.executeQuery(Query1);
//				if (rs.next()) {
//					SERVERIP = "\\\\" + rs.getString("SERVERIP");
//					SERVERIP = SERVERIP.trim();
//				}
//				return SERVERIP;
//			} else {
//				java.io.File currentDir = new java.io.File("");
//				return currentDir.getAbsolutePath();
//			}
//		} catch (Exception E) {
//			E.printStackTrace();
//			
//		}
		return null;
	}

	public synchronized String getServerIpAddress() {
//		try {
//			if (Parameters.ISCLIENT) {
//				String Query1 = "", SERVERIP = null;
//				Query1 = "SELECT SERVERIP FROM SERVERIP";
//				Statement stmt = DBConnection.getInstance().getDBConnection().createStatement();
//				ResultSet rs = stmt.executeQuery(Query1);
//				if (rs.next()) {
//					SERVERIP = rs.getString("SERVERIP");
//					SERVERIP = SERVERIP.trim();
//				}
//				rs.close();
//				stmt.close();
//				return SERVERIP;
//			}
//		} catch (Exception E) {
//			E.printStackTrace();
//			
//		}
		return null;
	}

	public double GetScreenHeight() {
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		return dim.getHeight();
	}

	public double GetScreenWidth() {
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		return dim.getWidth();
	}

	public boolean SpecialCharValidation(KeyEvent e) {
		try {
			if (e.getKeyCode() == KeyEvent.VK_F1)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F2)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F3)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F4)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F5)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F6)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F7)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F8)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F9)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F10)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_F11)
				return true;

			if (e.getKeyCode() == KeyEvent.VK_ALT)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_CONTROL)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_CANCEL)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_CAPS_LOCK)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_TAB)
				return true;

			if (e.getKeyCode() == KeyEvent.VK_BACK_SPACE)
				return true;

			if (e.getKeyCode() == KeyEvent.VK_INSERT)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_DELETE)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_HOME)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_END)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_PAGE_UP)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_PAGE_DOWN)
				return true;

			if (e.getKeyCode() == KeyEvent.VK_DOWN)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_UP)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_RIGHT)
				return true;
			if (e.getKeyCode() == KeyEvent.VK_LEFT)
				return true;

			return false;
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return false;
	}

	public boolean isNumeric(String str) {

		boolean flag = false;

		for (int i = 0; i < str.length(); i++) {
			if (Character.isDigit(str.charAt(i))) {
				flag = true;
			} else {
				return false;
			}
		}
		return flag;
	}

	public boolean isDouble(String str) {

		boolean flag = false;

		for (int i = 0; i < str.length(); i++) {
			if (Character.isDigit(str.charAt(i))) {
				flag = true;
			} else {
				return false;
			}
		}
		return flag;
	}

	public boolean AlphaNumericValidation(String str) {
		// String allow = "0123456789abcdefABCDEF";
		String allow = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMONPQRSTUVWXY";
		boolean bol_Char = false;
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			bol_Char = false;
			for (int j = 0; j < allow.length(); j++) {
				if (ch == allow.charAt(j)) {
					bol_Char = true;
					break;
				}
			}
			if (bol_Char == false)
				return false;
		}
		return true;
	}

	public boolean NumberValidation(String str) {
		String Allow = "0123456789";
		boolean bol_Char = false;
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			bol_Char = false;
			for (int j = 0; j < Allow.length(); j++) {
				if (ch == Allow.charAt(j)) {
					bol_Char = true; // System.out.print("\t bol_Char = true	");
					break;
				}
			}
			if (bol_Char == false)
				return false; // Invalide character found Except Number Values
			// only
		}
		return true;
	}

	public boolean AssetNameValidation(String str) {
		String notAllow = "!`?@&#$,'[]~`%^()+=|\\/\";:><'*{}";

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			for (int j = 0; j < notAllow.length(); j++) {
				if (ch == notAllow.charAt(j)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean StringValidation(String str) {
		// String notAllow=".!`?@&#$,'[]~`%^()+=|\\/\";:><'*{}";
		String notAllow = "!`?@&#$,'[]~`%^()+=|\\/\";:><'*{}"; // '.' is removed
		// from notallow
		// list.

		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			for (int j = 0; j < notAllow.length(); j++) {
				if (ch == notAllow.charAt(j)) {
					return false;
				}
			}
		}
		return true;
	}

	public boolean alphaNumericStringValidation(String str) {
		String allowCharacters = "_ 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (allowCharacters.indexOf(ch) < 0) {
				return false;
			}
		}
		return true;
	}

	public boolean Ipvalidation(String Ipaddress) {
		String[] temp = Ipaddress.split("\\."); // System.out.println( Ipaddress
		// + "\t" + temp.length );
		if (temp.length == 4) {
			for (int i = 0; i <= temp.length - 1; i++) {
				int val = Integer.parseInt(temp[i]); // System.out.println( val
				// );
				if (val > 255) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public boolean MACvalidation(String MACaddress) {
		String[] temp = MACaddress.split("\\-");
		System.out.println(MACaddress + "\t" + temp.length);
		if (temp.length == 6) {
			for (int i = 0; i <= temp.length - 1; i++) {
				if (AlphaNumericValidation(temp[i]) == false) {
					return false;
				}
			}
			return true;
		}
		return false;
	}

	public boolean IMEIvalidation(String IMEINumber) {
		if (IMEINumber.length() == 15) {
			return true;
		}
		return false;
	}

	public boolean EmailValidation(String compString) {
		/*
		 * try{
		 * 
		 * Pattern p = Pattern.compile(".+@.+\\.[a-z]+"); // Set the email
		 * pattern string
		 * 
		 * Matcher m = p.matcher( compString ); // Match the given string with
		 * the pattern
		 * 
		 * boolean matchFound = m.matches(); // check whether match is found
		 * 
		 * if (matchFound) return true; // //
		 * System.out.println("\n\nValid Email Id.\n\n"); else return false; //
		 * // System.out.println("\n\nInvalid Email Id.\n\n");
		 * 
		 * }catch( Exception E ){ E.printStackTrace(); } return false;
		 */
		try {
			int findATRATEChar = compString.indexOf("@");
			int findDOTChar = compString.indexOf(".", findATRATEChar);
			if ((findATRATEChar == -1) || (findDOTChar == -1)) {
				return false;
			}
			if ((findATRATEChar > 0) && (findDOTChar > 0)) {
				if (findATRATEChar > findDOTChar) {
					return false;
				}
				String[] temp = null;
				temp = compString.split("@");
				if (temp.length != 2) {
					return false;
				}
				String[] temp1 = null;
				temp1 = compString.split("\\.");
				if (temp1.length <= 1) {
					return false;
				}
				if (compString.charAt(compString.length() - 1) == '@') {
					return false;
				}
				if (compString.charAt(compString.length() - 1) == '.') {
					return false;
				}
			}
			return true;

		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return false;

	}

	public boolean FileCopy(File sourceFile, File targetFile) {
		try {
			if (targetFile.exists() == true) {
				targetFile.delete();
			}
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(sourceFile), 4096);
			BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(targetFile), 4096);
			int theChar;
			while ((theChar = bis.read()) != -1) {
				bos.write(theChar);
			}
			bos.close();
			bis.close();
			return true; // // System.out.println ("copy done!");
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return false;
	}

	public boolean HHFileCopy(File sourceFile, File targetFile) {
		try {

			if (targetFile.exists() == true) {
				targetFile.delete();
			}

			ImageCompress ImgComp = new ImageCompress();
			ImgComp.resizeImage(sourceFile.getAbsolutePath(), targetFile.getAbsolutePath());

			return true; // // System.out.println ("copy done!");
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return false;
	}

	public boolean FileMove(String sourceFile, String DestinationFolder) {
		try {

			// System.out.println("OK"); // File (or directory) to be moved
			File file = new File(sourceFile); // Destination directory
			File dir = new File(DestinationFolder); // Move file to new
			// directory
			// System.out.println("File "+file.getName());
			boolean success = file.renameTo(new File(dir, file.getName()));
			if (!success) {
				// System.out.println("File was not successfully moved");
				// File was not successfully moved
				return false;
			}
			return true;
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return false;
	}

	public boolean DateTimeValidation(String StTime, String EdTime) {
		try {
			java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("dd-MMM-yyyy hh:mm:ss");
			java.util.Date FromDate = dateFormat.parse(StTime);

			// System.out.println( " FromDate : " + FromDate );
			java.util.Date ToDate = dateFormat.parse(EdTime);
			// System.out.println( " ToDate : " + ToDate );
			if (ToDate != null) {
				if (ToDate.before(FromDate)) {
					return false; // fales
				}
				return true; // Date validated
			}
			return false;
		} catch (ParseException E) {
			// System.out.println("ERROR: could not parse date in string ");
			
		}
		return false;
	}

	public boolean DateValidation(String StTime, String EdTime) {
		try {
			java.text.SimpleDateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MMM-dd hh:mm:ss");
			java.util.Date FromDate = dateFormat.parse(StTime);
			// System.out.println( " FromDate : " + FromDate );
			java.util.Date ToDate = dateFormat.parse(EdTime);
			// System.out.println( " ToDate : " + ToDate );
			if (ToDate != null) {
				if (ToDate.before(FromDate)) {
					return false; // fales
				}
				return true; // Date validated
			}
			return false;
		} catch (ParseException E) {
			// System.out.println("ERROR: could not parse date in string ");
			
		}
		return false;
	}

	public String getCurrentDateTime() {
		java.util.Date curDate = new java.util.Date();
		String returnDateTime = "";
		returnDateTime += curDate.getDate() + "_";
		returnDateTime += curDate.getMonth() + 1 + "_";
		returnDateTime += curDate.getYear() + "_";
		returnDateTime += curDate.getHours() + "_";
		returnDateTime += curDate.getMinutes() + "_";
		returnDateTime += curDate.getSeconds();
		return returnDateTime;
	}

	public String getCurrentTime() {
		// by vitthal on 17-10-2012
		Calendar cal = new GregorianCalendar();
		java.text.DateFormat df = new java.text.SimpleDateFormat("HH:mm:ss");
		return df.format(cal.getTime());
	}

	public String getCurrentDate() {
		java.util.Date curDate = new java.util.Date();
		String returnDate = "";
		returnDate += (curDate.getYear() + 1900) + "-";
		int month = (curDate.getMonth() + 1);
		if (month < 10) {
			returnDate += "0" + month + "-";
		} else {
			returnDate += month + "-";
		}
		int dt = curDate.getDate();
		if (dt < 10) {
			returnDate += "0" + dt;
		} else {
			returnDate += dt;
		}
		return returnDate;
	}

	public int getDay(int year, int month, int date) {
		try {
			int day = 0;
			java.util.Calendar dayCal = java.util.Calendar.getInstance();
			dayCal.set(year, month, date);
			day = dayCal.get(java.util.Calendar.DAY_OF_WEEK);
			// System.out.println("Return day "+day+" for "+year+"-"+(month+1)+"-"+date);
			return day;
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return 0;
	}

	public boolean DtimeValidation(String dtval) {
		try {

			String[] dtArry = dtval.split("-");

			// String month =
			// "JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC,Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec"
			// ;
			String[] monthData = { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };

			if (Integer.parseInt(dtArry[0]) >= 3015) {
				return false;
			} else if (Integer.parseInt(dtArry[0]) < 1900) {
				return false;
			}

			int i = 0;
			for (i = 0; i <= 11; i++) {
				String month = monthData[i];
				if (month.equalsIgnoreCase(dtArry[1])) {
					break;
				}
			}
			if (i == 12) {
				return false;
			}
			// if( month.indexOf( dtArry[1] )==-1 ){ return false; }

			if (Integer.parseInt(dtArry[2]) >= 32) {
				return false;
			} else if (Integer.parseInt(dtArry[2]) <= 0) {
				return false;
			}
			return true;
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		return false;
	}

	public boolean MoneyValidation(String str) {
		String Allow = "0123456789,.";
		boolean bol_Char = false;
		;
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			bol_Char = false;
			for (int j = 0; j < Allow.length(); j++) {
				if (ch == Allow.charAt(j)) {
					bol_Char = true;
					printToConsole("\t bol_Char = true	");
					break;
				}
			}
			if (bol_Char == false)
				return false; // Invalide character found Except Number Values
			// only
		}

		return true;
	}

	public boolean QuantityValidation(String str) {
		String Allow = "0123456789";
		boolean bol_Char = false;
		;
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			bol_Char = false;
			for (int j = 0; j < Allow.length(); j++) {
				if (ch == Allow.charAt(j)) {
					bol_Char = true;
					printToConsole("\t bol_Char = true	");
					break;
				}
			}
			if (bol_Char == false)
				return false; // Invalide character found Except Number Values
			// only
		}

		return true;
	}

	public boolean isValideExtenstion(String ext) {
		try {
			if (ext.equalsIgnoreCase("gif")) {
				return true;
			}
			if (ext.equalsIgnoreCase("jpge")) {
				return true;
			}
			if (ext.equalsIgnoreCase("jpg")) {
				return true;
			}
			if (ext.equalsIgnoreCase("png")) {
				return true;
			}
			return false;
		} catch (Exception E) {
			E.printStackTrace();
		}
		return false;
	}

	public void CloseAllResources() {/*
		
		 * try{ for( int i=0; i<=9; i++ ){ if( GRdrVar.obj_Intelleflex[i] !=
		 * null ) GRdrVar.obj_List[i].isExit = true; } }catch( Exception E ){
		 * E.printStackTrace(); Parameters.obj_Err.stackTrace( E ); }
		 
		try {
			Readers.intelleflex.ReaderSettingsDB obj_RdrSett = new Readers.intelleflex.ReaderSettingsDB();
			obj_RdrSett.CloseAllOpenChannels();
		} catch (Exception E) {
			E.printStackTrace();
			
		}
		try {
			Readers.xtenna.ReaderSettingsDB obj_RdrSett = new Readers.xtenna.ReaderSettingsDB();
			obj_RdrSett.CloseAllOpenChannels();
		} catch (Exception E) {
			E.printStackTrace();
			
		}

		try {
			Statement stmt = null;

			DBConnect.setConnection();
			stmt = DBConnect.getStmt();

			String Update = "";

			Update = "UPDATE READER_STATUS SET STATUS=0 WHERE READERID=" + Parameters.DeskReaderId;
			System.out.println("@@@@@@@@@@--------Utility Method Update Status---------@@@@@@@@@@@@@@@");
			stmt.executeUpdate(Update);

			Update = "UPDATE READER_CLIENT SET STATUS=0 ";
			stmt.executeUpdate(Update);

			DBConnect.conn.close();

			java.io.File currentDir = new java.io.File("");
			String errorLogPath = currentDir.getAbsolutePath() + "\\ErrorLog" + "";
			currentDir = null;
			final long aDayMilSecs = 24 * (60 * 60 * 1000);
			java.io.File errorLogFolder = new java.io.File(errorLogPath);
			if (errorLogFolder.exists() && errorLogFolder.isDirectory()) {
				File[] fileListArr = errorLogFolder.listFiles();
				long fileListSize = fileListArr.length;
				File errorFile = null;
				for (int index = 0; index < fileListSize; index++) {
					errorFile = fileListArr[index];
					Long modifiedTime = errorFile.lastModified();
					if ((System.currentTimeMillis() - modifiedTime) >= (2 * aDayMilSecs)) {
						String fileName = errorFile.getName();
						boolean isDelete = errorFile.delete();
						if (!isDelete) {
							System.out.println("Could not delete file::" + fileName);
						}
					}
				}
			}

		} catch (Exception E) {
			E.printStackTrace();
			
		}

		// try{ Thread.sleep( 2000 ); }catch( Exception E ){
		// E.printStackTrace(); }
	*/}

	public UtilityMethods() {

	}

	public String extension() {
		int dot = fullPath.lastIndexOf(extensionSeparator);
		return fullPath.substring(dot + 1);
	}

	public void Filename(String str, char sep, char ext) {
		fullPath = str;
		extensionSeparator = ext;
	}

	public boolean ConfirmModify() {
		Object[] options = { "NO", "YES", };
		int item_selected = JOptionPane.showOptionDialog(null, "  You want to Modify ", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
				null, options, options[0]);
		if (item_selected == 1) {
			return true;
		} else {
			// System.out.println("---INTEGER VALUE means No---->>"+item_selected);
			return false;
		}
	}

	// by vitthal on 20130409--start

	public boolean confirmModifyForDialog(JDialog dialog) {
		Object[] options = { "NO", "YES", };
		int item_selected = JOptionPane.showOptionDialog(dialog, "You want to Modify", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
				null, options, options[0]);
		if (item_selected == 1) {
			return true;
		} else {
			// System.out.println("---INTEGER VALUE means No---->>"+item_selected);
			return false;
		}
	}

	public boolean ConfirmDelete() {
		Object[] options = { "NO", "YES", };
		int item_selected = JOptionPane.showOptionDialog(null, "  Are you sure want to Delete?", "Warning", JOptionPane.DEFAULT_OPTION,
				JOptionPane.WARNING_MESSAGE, null, options, options[0]);
		if (item_selected == 1) {
			return true;
		} else {
			// System.out.println("---INTEGER VALUE means No---->>"+item_selected);
			return false;
		}
	}

	public boolean confirmSave() {
		Object[] options = { "NO", "YES", };
		int item_selected = JOptionPane.showOptionDialog(null, "Do you want To Save?", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
				null, options, options[0]);
		if (item_selected == 1) {
			return true;
		} else {
			// System.out.println("---INTEGER VALUE means No---->>"+item_selected);
			return false;
		}
	}

	public boolean ConfirmClear() {
		Object[] options = { "NO", "YES", };
		int item_selected = JOptionPane.showOptionDialog(null, "You want To Clear", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
				options, options[0]);
		if (item_selected == 1) {
			return true;
		} else {
			// System.out.println("---INTEGER VALUE means No---->>"+item_selected);
			return false;
		}
	}

	public boolean ConfirmRemove() {
		Object[] options = { "NO", "YES", };
		int item_selected = JOptionPane.showOptionDialog(null, "Do you want To Remove", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE,
				null, options, options[0]);
		if (item_selected == 1) {
			return true;
		} else {
			// System.out.println("---INTEGER VALUE means No---->>"+item_selected);
			return false;
		}
	}

	public boolean ConfirmAdd() {
		Object[] options = { "NO", "YES", };
		int item_selected = JOptionPane.showOptionDialog(null, "Do you want To Add", "Warning", JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null,
				options, options[0]);
		if (item_selected == 1) {
			return true;
		} else {
			// System.out.println("---INTEGER VALUE means No---->>"+item_selected);
			return false;
		}
	}

	public boolean ConfirmOut() {
		Object[] options = { "NO", "YES", };
		int item_selected = JOptionPane.showOptionDialog(null, "Do you want To Out Visitor", "Warning", JOptionPane.DEFAULT_OPTION,
				JOptionPane.WARNING_MESSAGE, null, options, options[0]);
		if (item_selected == 1) {
			return true;
		} else {
			// System.out.println("---INTEGER VALUE means No---->>"+item_selected);
			return false;
		}
	}

	public boolean ConfirmVerification() {
		Object[] options = { "NO", "YES", };
		int item_selected = JOptionPane.showOptionDialog(null, "Other Verification Start with this doocDoorNo it will Stop", "Warning",
				JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[0]);
		if (item_selected == 1) {
			return true;
		} else {
			// System.out.println("---INTEGER VALUE means No---->>"+item_selected);
			return false;
		}
	}

	public String GetReportDate() {
		String currentDateTime1 = getCurrentDate();// + " " +
		// objUtil.getCurrentTime()
		// ;
		java.util.Date CurrDate1 = null;
		java.text.SimpleDateFormat OutdateFormat = null;
		java.text.SimpleDateFormat IndateFormat = null;
		try {
			IndateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd");
			OutdateFormat = new java.text.SimpleDateFormat("dd-MMM-yyyy");
			CurrDate1 = IndateFormat.parse(currentDateTime1);
			// return (OutdateFormat.format ( CurrDate1 ) ) ;
		} catch (ParseException pe) {
			System.out.println("ERROR: could not parse date in string \"" + currentDateTime1 + "\"");
		}
		return (OutdateFormat.format(CurrDate1));
	}

	private void printToConsole(String msg) {
		if (debugMode) {
			System.out.println("UtilityMethods :\t\t" + msg);
		}
	}

	// BY VITTHAL ON 15-10-2012

	public static Image getLogoImage() {
//		// System.out.println("COMPANY NAME "+ Parameters.CompanyNameForLogo);
//		String CompLogo = null;
//		if (Parameters.loginComLogo.containsKey(Parameters.CompanyNameForLogo) == true) {
//			CompLogo = (String) Parameters.loginComLogo.get(Parameters.CompanyNameForLogo);
//		}
//		int CompID1 = 1;
//		if (Parameters.loginHT.containsKey(Parameters.CompanyNameForLogo) == true) {
//			CompID1 = (Integer) Parameters.loginHT.get(Parameters.CompanyNameForLogo);
//		}
//		ImageIcon imageIcon = null;
//		Image image = null;
//		String imagePath = null;
//		// System.out.println("company image logo value:: "+CompLogo);
//		if (CompLogo.length() > 0) {
//			imagePath = Parameters.AppPath + "\\images\\CompanyId_" + CompID1 + "\\CompanyLogo";
//			imagePath = imagePath + "\\" + CompLogo;
//			imageIcon = new ImageIcon(imagePath);
//			image = imageIcon.getImage();
//		} else {
//			imagePath = null;
//			image = null;
//		}
//		// System.out.println("===Photo_Path=========of the company====>>"+imagePath);
//		return image;
		return null;
	}

	// by vitthal 2012-12-10-- start....
	public void closeDialogAfterPressEsc(final JDialog dialog) {
		dialog.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Cancel");
		dialog.getRootPane().getActionMap().put("Cancel", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				dialog.setVisible(false);
			}
		});
	}

	public void closeFrameAfterPressEsc(final JFrame frame) {
		frame.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "Cancel");
		frame.getRootPane().getActionMap().put("Cancel", new AbstractAction() {

			private static final long serialVersionUID = 1L;

			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}
		});
	}

	// by vitthal 2012-12-10-- end ....

	// by vitthal 2013-06-13--start---
/*
	public synchronized JSONObject parseEncryptedFile(String fileName) {/*
		System.out.println("fileName" + fileName);

		JSONObject jsonObject = null;
		Cryptography encrypter = Cryptography.getInstance();

		FileInputStream fileInputAfterEncry = null;
		FileOutputStream fileOutputAfterDecry = null;
		String tempFileName = fileName + "_Temp";
		JSONParser parser = new JSONParser();
		try {
			fileInputAfterEncry = new FileInputStream(fileName);
			fileOutputAfterDecry = new FileOutputStream(tempFileName);
			encrypter.decryptFile(fileInputAfterEncry, fileOutputAfterDecry);
			FileReader fileReader = new FileReader(tempFileName);
			jsonObject = (JSONObject) parser.parse(fileReader);
			fileReader.close();
			File fileOne = new File(tempFileName);
			if (fileOne.exists()) {
				boolean success = fileOne.delete();
				if (!success) {
					System.out.println(tempFileName + "file  Deletion failed.");
				} else {
					System.out.println(tempFileName + " File successfully deleted.");
				}
			}

			return jsonObject;
		} catch (FileNotFoundException e) {
			JOptionPane.showMessageDialog(Parameters.frm_Main, " No  Software Licence key Found ", "Asset Tracking System", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (IOException e) {
			JOptionPane.showMessageDialog(Parameters.frm_Main, " No  Software Licence key Found ", "Asset Tracking System", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(Parameters.frm_Main, " No  Software Licence key Found ", "Asset Tracking System", JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
		return jsonObject;
	}
/*/
	// by vitthal 2013-06-13--end---

	// by vitthal 2013-08-19--start---

	/*@SuppressWarnings("unchecked")
	public JSONArray getTableJsonArrayObject(String query, String[] columnName) {
		Statement stmt;
		try {
			stmt = DBConnection.getInstance().buildDBConnection().createStatement();
			ResultSet rs;
			JSONArray jsonArrayObj = new JSONArray();
			rs = stmt.executeQuery(query);
			int size = columnName.length;
			while (rs.next()) {
				JSONObject jsonObj = new JSONObject();
				for (int index = 1; index <= size; index++) {
					String temp = rs.getString(index);
					temp = temp != null && temp.trim().length() > 0 ? temp.trim() : "";
					jsonObj.put(columnName[index - 1], temp);
				}
				jsonArrayObj.add(jsonObj);
			}
			return jsonArrayObj;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public JSONObject getTableJsonObject(String query, String[] columnName) {
		Statement stmt;
		try {
			stmt = DBConnection.getInstance().buildDBConnection().createStatement();
			ResultSet rs;
			JSONObject jsonObj = new JSONObject();
			rs = stmt.executeQuery(query);
			int size = columnName.length;
			while (rs.next()) {
				for (int index = 1; index <= size; index++) {
					String temp = rs.getString(index);
					temp = temp != null && temp.trim().length() > 0 ? temp.trim() : "";
					jsonObj.put(columnName[index - 1], temp);
				}
			}
			return jsonObj;
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

*/	// by vitthal 2013-08-19--end---
	// by vitthal 2013-11-25--start---
	public long getNextTableIDCount(String tableName) {
//		Statement stmt = null;
//		ResultSet rs = null;
//
//		try {
//			stmt = RFIDDBConnection.getInstance().buildRFIDConnection().createStatement();
//			String query = "";
//			query = "SELECT COUNT =" + " CASE WHEN ( (SELECT  COUNT(*) FROM " + tableName + ") = 0) THEN 1  ELSE" + "  IDENT_CURRENT('" + tableName
//					+ "') + IDENT_INCR('" + tableName + "')  END" + " FROM " + tableName + " ";
//			System.out.println(" Count:" + query);
//			rs = stmt.executeQuery(query);
//
//			long MaxId = 1;
//			if (rs.next()) {
//				MaxId = rs.getInt(1);
//			}
//			rs.close();
//			stmt.close();
//			RFIDDBConnection.getInstance().buildRFIDConnection().close();
//			// System.out.println("User Group   Id Count : " + MaxId);
//			return MaxId;
//		} catch (Exception E) {
//			E.printStackTrace();
//		}
		return 0;
	}

	// by vitthal 2013-11-25--end---

	public void getReportPageHeader(final HashMap<String, Object> parameter) {
//		parameter.put("GENERATEDBY", Parameters.DBUsrName);
//		parameter.put("COMPANYNM", Parameters.CompanyName);
//		String path = null;
//		path = Parameters.AppPath + "\\images\\CompanyId_" + Parameters.sessionid + "\\CompanyLogo\\" + getCompanyLogo() + "";
//		parameter.put("LOGO", path);
//		parameter.put("COMPANYID", Parameters.sessionid);
	}

	private String getCompanyLogo() {
		String companyLogo = "";
//		Statement stmt = null;
//		ResultSet rs = null;
	
//		try {
//			stmt = RFIDDBConnection.getInstance().buildRFIDConnection().createStatement();
//			String query = "SELECT LOGO FROM COMPANY WHERE COMPANYID=" + Parameters.sessionid + "";
//			rs = stmt.executeQuery(query);
//			if (rs.next()) {
//				companyLogo = rs.getString(1);
//			}
//			rs.close();
//			stmt.close();
//			return companyLogo;
//		} catch (Exception E) {
//			E.printStackTrace();
//		}
		return companyLogo;
	}

	// by vitthal 2014-02-17 start........

	// by vitthal 2014-02-17 end ........

	public String getDay() {
		String returnDay = null;
		Calendar cal = new GregorianCalendar();
		int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
		switch (dayOfWeek) {
		case 1:
			returnDay = "Sunday";
			break;
		case 2:
			returnDay = "Monday";
			break;
		case 3:
			returnDay = "Tuesday";
			break;

		case 4:
			returnDay = "Wednesday";
			break;
		case 5:
			returnDay = "Thursday";
			break;
		case 6:
			returnDay = "Friday";
			break;
		case 7:
			returnDay = "Saturday";
			break;

		default:
			break;
		}

		return returnDay;
	}

//	public void setReportOnDialog(ReportViewDialog dialog, JapserViewPanel jasperPanel) {
//		dialog.jDialog.getContentPane().add(jasperPanel, BorderLayout.CENTER);
//		dialog.jDialog.setVisible(true);
//	}

	public synchronized Boolean isIpAddressReachable(String ipAddress) {
		InetAddress inet;
		try {
			inet = InetAddress.getByName(ipAddress.trim());
			// System.out.println("Sending Ping Request to " + ipAddress);
			return inet.isReachable(5000) ? true : false;
		} catch (UnknownHostException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	public String getLangaugeType() {
		String langaue = "English";
//		Statement stmt = null;
//		ResultSet rs = null;
//		
//		try {
//			stmt = RFIDDBConnection.getInstance().buildRFIDConnection().createStatement();
//			String query = "";
//			query = "Select Langauge_Option from APPLICATION_TABLE ";
//			System.out.println(" Count:" + query);
//			rs = stmt.executeQuery(query);
//			int MaxId = 1;
//			if (rs.next()) {
//				MaxId = rs.getInt(1);
//			}
//			rs.close();
//			stmt.close();
//			switch (MaxId) {
//			case 0:
//				langaue = "English";
//				break;
//			case 1:
//				langaue = "Arabic";
//				break;
//			default:
//				langaue = "English";
//				break;
//			}
//			return langaue;
//		} catch (Exception E) {
//			E.printStackTrace();
//		}
		return langaue;
	}

	public DocPrintJob getPrinter(String printServiceName) {
		try {

			PrintService psZebra = null;
			String sPrinterName = null;
			PrintService[] services = PrintServiceLookup.lookupPrintServices(null, null);
			for (int i = 0; i < services.length; i++) {
				PrintServiceAttribute attr = services[i].getAttribute(PrinterName.class);
				sPrinterName = ((PrinterName) attr).getValue();
				System.out.println("-------------------------- \n\n sPrinterName : " + sPrinterName + "\n------\n");
				if (sPrinterName.equalsIgnoreCase(printServiceName)) {
					psZebra = services[i];
					System.out.println("PRinter Found " + sPrinterName);
					break;
				}
				// break; //commented due to complete hole for loop...
			}
			if (psZebra == null) {
				System.out.println(" printer is not found.");
				return null;
			}
			DocPrintJob job = psZebra.createPrintJob();
			return job;
		} catch (Exception E) {
			E.printStackTrace();
		}
		return null;
	}

	// added by vitthal on 20150812

	public synchronized void closeAllConnection(Statement statement,
			ResultSet resultSet) {
		try {
			if (resultSet != null) {
				resultSet.close();
			}
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public synchronized String getAllReaderFileter(String loginUserID) {
//		Statement stmt = null;
//		ResultSet rs = null;
//		StringBuffer readerChannelID = new StringBuffer();
//		try {
//			Connection connection = RFIDDBConnection.getInstance().buildRFIDConnection();
//			stmt = connection.createStatement();
//			String query = "SELECT READER_ID FROM DESKTOP_FILTER WHERE ADMIN_ID=" + loginUserID + " AND COMPANY_ID=" + Parameters.sessionid + "";
//			rs = stmt.executeQuery(query);
//			while (rs.next()) {
//				readerChannelID.append(rs.getString(1) + ",");
//			}
//			readerChannelID.append("-1");
//			rs.close();
//			stmt.close();
//			connection.close();
//			System.gc();
//			return readerChannelID.toString();
//		} catch (Exception E) {
//			E.printStackTrace();
//		}
		return null;
	}

	public synchronized String getAllChannelFileter(String loginUserID) {
//		Statement stmt = null;
//		ResultSet rs = null;
//		StringBuffer readerChannelID = new StringBuffer();
//		try {
//			Connection connection = RFIDDBConnection.getInstance().buildRFIDConnection();
//			stmt = connection.createStatement();
//			String query = "SELECT CHANNEL_ID FROM DESKTOP_FILTER WHERE ADMIN_ID=" + loginUserID + " AND COMPANY_ID=" + Parameters.sessionid + "";
//			rs = stmt.executeQuery(query);
//			while (rs.next()) {
//				readerChannelID.append(rs.getString(1) + ",");
//			}
//			readerChannelID.append("-1");
//			rs.close();
//			stmt.close();
//			connection.close();
//			System.gc();
//			return readerChannelID.toString();
//		} catch (Exception E) {
//			E.printStackTrace();
//		}
		return null;
	}
	public String getDate(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");  
	    String strDate = formatter.format(date);  
	    return strDate;
	}
	public String getDateWithoutTime(Date date){
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy");  
	    String strDate = formatter.format(date);  
	    return strDate;
	}
}
